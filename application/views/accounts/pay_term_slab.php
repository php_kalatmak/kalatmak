
<script type="text/javascript">
	
	$(document).ready(function() {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-accounts').addClass('active');
	$('#menu-accounts > a').attr('aria-expanded', 'true');
	$('#menu-accounts > ul').addClass('in');

	$('#menu-payTermSlab').addClass('active');
	
	$('#PaytermSlabadd').hide();
	$('#paytermSlabEdit').hide();

	$('#PaytermSlab').click(function() {
	$('#PaytermSlabadd').show();
	});
	
	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#paySlab_Reset').click(function(){
	$('#payTermCode').val('');
	$('#slabSeqId').val('');
	$('#creditDays').val('');
	$('#percentage').val('');	
	});

});

</script>
<section class="content">
		<div class="page-heading">
			<h1>Masters</h1>
			<ol class="breadcrumb">
				<li><a href="../../erp/home">Master</a></li>
				<li><a href="javascript:void(0);">General</a></li>
				<li class="active">PaymentTerm Slab</li>
			</ol>
		</div>
		<?php $pertermslab = $this->db->query("select * from erpgpts order by id DESC")->result(); ?>
		<div class="page-body">
			<div class="row">
				<div class="col-lg-12">

					<div class="page-body" id="dtab">
						<div class="row clearfix margin_full">
							<div class="col-lg-8">
								<div class="panel panel-default">
									<div class="panel-heading">PaymentTerm Slab</div>
									<div class="panel-body table-responsive">
										<button class="btn btn-sm btn-success" id="PaytermSlab">New</button>
										<br clear="left">
										<table
											class="table table-striped table-hover js-exportable dataTable"
											data-search=true>

											<thead>
												<tr>
													<th data-field="payTermCodeTbl">PaymentTerm Code</th>
													<th data-field="SlabSeqIdTbl">Slab Sequence Id</th>
													<th data-field="creditDaysTbl">Credit Days</th>
													<th data-field="percentageTbl">Percentage</th>
													<th data-field="action" data-formatter="actionFormatter"
														data-events="actionEvents">Edit</th>
												</tr>
											</thead>
											<tbody>
													<tr>
														<td>PT21</td>
														<td>SEQID001</td>
														<td>14</td>
														<td>5%</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>PT45</td>
														<td>SEQID002</td>
														<td>30</td>
														<td>3%</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="paytermSlab_info modal " id="StateEdit" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 25%">
							<?php echo form_open(base_url('general/add_paytermslab'),array('id'=>'paytermslab','name'=>'paytermslab','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Payterm Slab</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Payterm
											Code</label> <input type="text" class="form-control" id="payTermCode" name="payterm_code">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Slab Seq Id
											</label> <input type="text" class="form-control" id="slabSeqId" name="slab_seqid"
											maxlength="30">
									</div>
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Credit
											Days</label> <input type="text" class="form-control" id="creditDays" name="credit_days">
									</div>
									
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Percentage
											</label> <input type="text" class="form-control" id="percentage" name="percentage">
									</div>
									
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="paySlab_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

									</div>
								</div>
								</form>
							</div>
						</div>
					</div>
											
											<div class="paytermSlab_info modal" id="paytermSlabEdit<?php echo $i; ?>" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">

							<div class="modal-content" style="margin-top: 25%">
							<?php echo form_open(base_url('general/update_paytermslab')); ?>
							<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Payterm Slab</h4>
								</div>
								<div class="modal-body" style="height: 150px">
								
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Payterm
											Code</label> <input type="text" class="form-control" name="paytermcode" id="paymentTermCodeEdit"  value="<?php echo $row->payterm_code; ?>">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Slab Seq Id
											</label> <input type="text" class="form-control" name="slabseqid" id="slabSeqIdEdit" value="<?php echo $row->slab_seqid; ?>"
											maxlength="30">
									</div>
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Credit
											Days</label> <input type="text" class="form-control" name="creditdays" id="creditDaysEdit" value="<?php echo $row->credit_days; ?>">
									</div>
									
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Percentage
											</label> <input type="text" class="form-control" name="percentage" id="percentageEdit" value="<?php echo $row->percentage; ?>">
									</div>

								</div>
							</div>
							<div class="modal-footer clearfix">

								<div class="form-group" style="">
									<button class="btn btn-danger pull-right btn-sm RbtnMargin" id="uom_Reset" type="button">Reset</button>
									<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
								</div>

							</div>
							</form>
						</div>
					</div>
											</tbody>
											
												

										</table>
										
									</div>
								</div>
							</div>
						</div>
					</div>






					<div class="paytermSlab_info modal " id="PaytermSlabadd" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 25%">
							<?php echo form_open(base_url('general/add_paytermslab'),array('id'=>'paytermslab','name'=>'paytermslab','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Payterm Slab</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Payterm
											Code</label> <input type="text" class="form-control" id="payTermCode" name="payterm_code">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Slab Seq Id
											</label> <input type="text" class="form-control" id="slabSeqId" name="slab_seqid"
											maxlength="30">
									</div>
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Credit
											Days</label> <input type="text" class="form-control" id="creditDays" name="credit_days">
									</div>
									
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Percentage
											</label> <input type="text" class="form-control" id="percentage" name="percentage">
									</div>
									
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="paySlab_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

									</div>
								</div>
								</form>
							</div>
						</div>
					</div>


					

				</div>
			</div>
		</div>
		</section>

	