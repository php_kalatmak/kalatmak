<script type="text/javascript">
	$(document).ready(function() {
	
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-accounts').addClass('active');
	$('#menu-accounts > a').attr('aria-expanded', 'true');
	$('#menu-accounts > ul').addClass('in');

	$('#menu-paymentTerm').addClass('active');
	
	$('#Paytermadd').hide();
	$('#paytermEdit').hide();

	$('#payTermAdd').click(function() {
	$('#Paytermadd').show();
	});
	
	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#payterm_Reset').click(function(){
	$('#paymentTermCode').val('');
	$('#paymentTermDesc').val('');
	$('#creditDays').val('');
	$('#toleranceDays').val('');
	})
	
	var checkboxSlab =$("#isSlab");
	var slabLevel = $("#slabLevel").val();
	
	//$("#isSlab").hide();
	$("#slabLevel").hide();
	
	checkboxSlab.change(function(){
		if(checkboxSlab.is(':checked')){
			$("#slabLevel").show();
		}else{
			$("#slabLevel").hide();
		}
	});
	
	var checkboxslabedit = $("#isSlabEdit");
	var slableveledit = $("#slabLevelEdit").val();
	
	$("#slabLevelEdit").hide();
	
	checkboxslabedit.change(function (){
		if(checkboxslabedit.is(':checked')){
			$("#slabLevelEdit").show();
		}else {
			$("#slabLevelEdit").hide();
		}
		
	});
	
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Payment Term</li>
				</ol>
			</div>
			<?php $payterm = $this->db->query("select * from erpgpt order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">

						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Payment Term</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="payTermAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th>PaymentTerm Code</th>
														<th>PaymentTerm Name</th>
														<th>Credit Days</th>
														<th>Is slab</th>
														<th>Tolerance Days</th>
														<th>Slab Level</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Code001</td>
														<td>PT21</td>
														<td>21</td>
														<td>True</td>
														<td>2</td>
														<td>2</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Code002</td>
														<td>PT45</td>
														<td>45</td>
														<td>True</td>
														<td>2</td>
														<td>2</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Code003</td>
														<td>PT90</td>
														<td>90</td>
														<td>True</td>
														<td>3</td>
														<td>3</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="Payterm_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/add_payterm'),array('id'=>'payterm','name'=>'payterm','autocomplete'=>'off')); ?>
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Payterm</h4>
									</div>
									<div class="modal-body" style="height: 150px">
										<div class="row">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Payterm
												Code</label> <input type="text" class="form-control" name="payterm_code"
												id="paymentTermCode">
										</div>

										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Payterm
												Name</label> <input type="text" class="form-control" name="payterm_name"
												id="paymentTermDesc" maxlength="30">
										</div>
										</div>
										<div class="row">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Relate to Customer </label> <input type="checkbox" name="load" id="isSlab">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Related to Vendor </label> <input type="checkbox" name="load" id="isSlab">
										</div>
										</div>
										<div class="row">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Credit
												Days</label> <input type="text" class="form-control" name="credit_days" id="creditDays">
										</div>
										
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Is
												Slabs </label> <input type="checkbox" name="load" id="isSlab1">
											<select class="form-control select2" id="slabLevel1" name="slab_level">
											<option value="">Please Select</option>
										    <option value="1">1</option>
										    <option value="2">2</option>
											<option value="3">3</option>
											</select>
										</div>
										</div>
										<div class="row">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Tolerance
												Days </label> <input type="text" class="form-control"
												id="toleranceDays" name="tolerance_days">
										</div>
																	
									</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="payterm_Reset" type="button">Reset</button>
											<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
																		

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="Payterm_info modal " id="Paytermadd" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Payterm</h4>
									</div>
									<div class="modal-body">
							        <form>
							        	<div class="row">
									<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Payterm
												Code</label> <input type="text" class="form-control" name="payterm_code"
												id="paymentTermCode">
										</div>

										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Payterm
												Name</label> <input type="text" class="form-control" name="payterm_name"
												id="paymentTermDesc" maxlength="30">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Relate to Customer </label> <input type="checkbox" name="load" id="isSlab">
											</div>
											<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Related to Vendor </label> <input type="checkbox" name="load" id="isSlab">
											</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Credit
												Days</label> <input type="text" class="form-control" name="credit_days" id="creditDays">
										</div>
										
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Is Slabs </label> <input type="checkbox" name="load" id="slab_check">
											<div id="slabLevel_123">
											<select class="form-control select2" id="order_type" name="slab_level">
											<option value="">Please Select</option>
										    <option value="1">PaymentTerm Slap Select</option>
											</select>

											</div>
										</div>
										<div class="row" id="slabLevel99">
											<div class="col-md-12 table-responsive">
												<table class="table table-bordered">
													<a href="#" class="control-label btn btn-sm btn-primary" onclick="addmore()">Add More</a>
													<thead>
														<tr>
															<th>PayTerm Code</th>
															<th>Slap Seq</th>
															<th>Credit Days</th>
															<th>Percentage</th>
															<th>Delete</th>
														</tr>
													</thead>
													<tbody id="service_tab">
														<tr>
															<td>
																<label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="paytermcode" id="paymentTermCodeEdit"  value="<?php echo $row->payterm_code; ?>">
															</td>
															<td>
																<label for="countryname" class="control-label"></label> <input type="text" class="form-control" name="slabseqid" id="slabSeqIdEdit" value="<?php echo $row->slab_seqid; ?>"maxlength="30">
															</td>
															<td>
																<label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="creditdays" id="creditDaysEdit" value="<?php echo $row->credit_days; ?>">
															</td>
															<td>
																<label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="percentage" id="percentageEdit" value="<?php echo $row->percentage; ?>">
															</td>
															<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										</div>
										<div class="row">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Tolerance
												Days </label> <input type="text" class="form-control"
												id="toleranceDays" name="tolerance_days">
										</div>
										</div>
										</form>
										<div class="clearfix"></div>							
									</div>
									
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="payterm_Reset" type="button">Reset</button>
											<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

										</div>
									</div>
									
								</div>
							</div>
						</div>
		</section>

		<script>
	$("#slabLevel99").hide();
	$("#order_type").on("change",function(){
		var type = $("#order_type").val();
		if(type == '1'){
			$("#slabLevel99").show();
		} else if(type == '2'){
			// $("#slabLevel99").show();
			
		}
	});

	$("#slabLevel_123").hide();
	$("#slab_check").on("click",function(){
		if(this.checked){
			$("#slabLevel_123").show();
		} else{
			$("#slabLevel_123").hide();
		}
	});

	function addmore(){
var txt = '<tr><td><label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="paytermcode" id="paymentTermCodeEdit"  value="<?php echo $row->payterm_code; ?>"></td><td><label for="countryname" class="control-label"></label> <input type="text" class="form-control" name="slabseqid" id="slabSeqIdEdit" value="<?php echo $row->slab_seqid; ?>"maxlength="30"></td><td><label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="creditdays" id="creditDaysEdit" value="<?php echo $row->credit_days; ?>"></td><td><label for="countrycode" class="control-label"></label> <input type="text" class="form-control" name="percentage" id="percentageEdit" value="<?php echo $row->percentage; ?>"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}
function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}
	</script>