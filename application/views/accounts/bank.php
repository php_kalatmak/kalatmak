
<script type="text/javascript">
	$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-accounts').addClass('active');
					$('#menu-accounts > a').attr('aria-expanded', 'true');
					$('#menu-accounts > ul').addClass('in');

					$('#menu-bank').addClass('active');

					$('#Bankadd').hide();
					$('#bankEdit').hide();

					$('#bankAdd').click(function() {
					$('#Bankadd').show();
					});

					$(".close").click(function() {
					$(".modal").hide();
					});

					$('#bank_Reset').click(function() {
					$('#bankCode').val('');
					$('#bankName').val('');
					});
				});	
</script>
<section class="content">
		<div class="page-heading">
			<h1>Masters</h1>
			<ol class="breadcrumb">
				<li><a href="../../erp/home">Master</a></li>
				<li><a href="javascript:void(0);">General</a></li>
				<li class="active">Bank</li>
			</ol>
		</div>
		<?php $bank = $this->db->query("select * from erpgbk order by id DESC")->result(); ?>
		<div class="page-body">
			<div class="row">
				<div class="col-lg-12">

					<div class="page-body" id="dtab">
						<div class="row clearfix margin_full">
							<div class="col-lg-8">
								<div class="panel panel-default">
									<div class="panel-heading">Bank Master</div>
									<div class="panel-body">
										<button class="btn btn-sm btn-success" id="bankAdd">New</button>
										<br clear="left">
										<table
											class="table table-striped table-hover js-exportable dataTable"
											data-search=true>

											<thead>
												<tr>
													<th>Bank Code</th>
													<th >Bank Name</th>
													<th data-field="action" data-formatter="actionFormatter"
														data-events="actionEvents">Edit</th>
												</tr>
											</thead>
											<tbody>
											<?php $i=0; foreach($bank as $row) { ?>
											<tr>
											<td><?php echo $row->bank_code; ?></td>
											<td><?php echo $row->bank_name; ?></td>
											<td><a class="edit ml10" title="" data-toggle="modal" data-target="#bankEdit<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-pencil"></i></a></td>
											</tr>
											
						<div class="bank_info modal" id="bankEdit<?php echo $i; ?>" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">

							<div class="modal-content" style="margin-top: 10%">
							 <?php echo form_open(base_url('general/update_bank')); ?>
							 <input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Bank</h4>
								</div>
								<div class="modal-body" style="height: 150px">
								
									<div class="form-group col-md-6">
										<label>Bank Code</label> 
										<!-- <select id="uomCodeEdit" class="form-control"></select> -->
										 <input type="text" class="form-control" readonly name="bankcode" value="<?php echo $row->bank_code; ?>"
											id="bankCodeEdit">
									</div>

									<div class="form-group col-md-6">
										<label for="description" class="control-label">Bank
											Name</label> <input type="text" class="form-control" name="bankname" value="<?php echo $row->bank_name; ?>"
											id="bankDescNewEdit">
									</div>								
								</div>
							</div>
							<div class="modal-footer clearfix">

								<div class="form-group" style="">
									<button class="btn btn-danger pull-right btn-sm RbtnMargin" id="bank_Reset" type="button">Reset</button>
									<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
								</div>

							</div>
							
							</form>
						</div>
					</div>
											</tbody>
											 <?php $i++; } ?>	

										</table>
									</div>
								</div>
							</div>
						</div>
					</div>






					<div class="bank_info modal " id="Bankadd" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 10%">							
							<?php echo form_open(base_url('general/add_bank'),array('id'=>'bank','name'=>'bank','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Bank</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Bank
											Code</label> <input type="text" class="form-control" name="bank_code" id="bankCode">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Bank
											Name</label> <input type="text" class="form-control" name="bank_name" id="bankName"
											maxlength="30"
											onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
									</div>
									
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="bank_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
									</div>
								</div>
								</form>
							</div>
						</div>
					</div>


					

				</div>
			</div>
		</div>
		</section>