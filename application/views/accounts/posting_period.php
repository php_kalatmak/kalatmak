<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-accounts').addClass('active');
	$('#menu-accounts > a').attr('aria-expanded', 'true');
	$('#menu-accounts > ul').addClass('in');

	$('#menu-postingPeriod').addClass('active');
	$('#PostingPeriod').hide();

	$('#postingPeriodAdd').click(function() {
	$('#PostingPeriod').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function(){
		
		$('#periodCode').val('');
		$('#period').val('');
		$('#fiscalStart').val('');
	
	})
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Posting Period</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-12 ">
									<div class="panel panel-default">
										<div class="panel-heading">POSTING PERIOD</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="postingPeriodAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="company">Company</th>
														<th data-field="period">Period</th>
														<!-- <th data-field="subPeriod">Sub Period</th> -->
														<th data-field="fiscal">Fiscal Year</th>
														<th data-field="fiscalStart">Fiscal Year Start Date</th>
														<th data-field="periodStart">Period Start Date</th>
														<th data-field="periodEnd">Period End Date</th>
														<th data-field="status">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>FY1819-1</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/04/2018</td>
														<td>30/04/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													
													<tr>
														<td>Quadraerp</td>
														<td>FY1819-2</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/05/2018</td>
														<td>31/05/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-3</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/06/2018</td>
														<td>30/06/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-4</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/07/2018</td>
														<td>31/07/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-5</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/08/2018</td>
														<td>31/08/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-6</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/09/2018</td>
														<td>30/09/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-7</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/10/2018</td>
														<td>31/10/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-8</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/11/2018</td>
														<td>30/11/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-9</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/12/2018</td>
														<td>31/12/2018</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-10</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/01/2019</td>
														<td>31/01/2019</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-11</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/02/2019</td>
														<td>28/02/2019</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>

													<tr>
														<td>Quadraerp</td>
														<td>FY1819-12</td>
														<td>FY1819</td>
														<td>01/04/2018</td>
														<td>01/03/2019</td>
														<td>31/03/2019</td>
														<td>UnLocked</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">POSTING
											PERIOD</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row ">
												<div class="form-group col-md-6">
													<label class="control-label">Company </label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company </option>
													<option>Quadraerp</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Period Code</label> <input type="text" maxlength="7"
														id="periodCode" class="form-control">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Period Name</label> <input type="text" id="period"
														maxlength="12" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Sub Period</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Sub Period</option>
													<option>Year</option>
													<option>Months</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label>No. of Periods</label> <input type="text"
														id="noofperiod" class="form-control" readonly>
												</div>
												<div class="form-group col-md-6">
													<label>Fiscal Year Start</label> <input type="date"
														id="fiscalStart" class="form-control">
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="periodSave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="PostingPeriod" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">POSTING
											PERIOD</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row ">
												<div class="form-group col-md-6">
													<label class="control-label">Company </label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company </option>
													<option>Quadraerp</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Period Code</label> <input type="text" maxlength="7"
														id="periodCode" class="form-control">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Period Name</label> <input type="text" id="period"
														maxlength="12" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Sub Period</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Sub Period</option>
													<option>Year</option>
													<option>Months</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label>No. of Periods</label> <input type="text"
														id="noofperiod" class="form-control" readonly>
												</div>
												<div class="form-group col-md-6">
													<label>Fiscal Year Start</label> <input type="date"
														id="fiscalStart" class="form-control">
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="periodSave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="PostingPeriodEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">POSTING
											PERIOD</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-4">
													<label>Company</label> <select id="compEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Period Code</label> <input type="text"
														readonly="readonly" id="periodCodeEdit"
														class="form-control">
												</div>
												<div class="form-group col-md-4">
													<label>Period Name</label> <input type="text"
														maxlength="12" id="periodEdit" readonly
														class="form-control">
												</div>
												<div class="form-group col-md-4">
													<label>Sub Period</label> <select id="subPeriodEdit"
														class="form-control">
													</select>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>No. of Periods</label> <input type="text"
														id="noofperiodEdit" class="form-control"
														readonly="readonly">
												</div> -->
												<div class="form-group col-md-4">
													<label>Fiscal Year</label> <input type="text"
														readonly="readonly" id="fiscalEdit" class="form-control">
												</div>
												<div class="form-group col-md-4">
													<label>Fiscal Year Start</label> <input type="text"
														readonly="readonly" id="fiscalStartEdit"
														class="form-control">
												</div>
												<div class="form-group col-md-4">
													<label>Period Start Date</label> <input type="text"
														id="periodStartEdit" class="form-control"
														readonly="readonly">
												</div>
												<div class="form-group col-md-4">
													<label>Period Start Date</label> <input type="text"
														id="periodEndEdit" class="form-control"
														readonly="readonly">
												</div>
												<div class="form-group col-md-4">
													<label>Period Status</label> <select id="periodStatusEdit"
														class="form-control">
													</select>
												</div>

											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="periodUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>