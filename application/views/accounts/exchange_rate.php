
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-accounts').addClass('active');
	$('#menu-accounts > a').attr('aria-expanded', 'true');
	$('#menu-accounts > ul').addClass('in');

	$('#menu-exchangeRate').addClass('active');
	$('#ExchangeRate').hide();

	$('#exchangerateAdd').click(function() {
	$('#ExchangeRate').show();
	});

	$(".close").click(function() {
	$(".modal").hide();	
	});

	$("#country_Reset").click(function(){
		$('#amt').val('');

	})

});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Exchange Rate</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">EXCHANGE RATE</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="exchangerateAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="company">Company</th>
														<th data-field="date">Date</th>
														<th data-field="curr">Exchange Currency</th>
														<th data-field="amt">Exchange Amount</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>19/02/2019</td>
														<td>USD</td>
														<td>70/-</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>18/02/2019</td>
														<td>USD</td>
														<td>71/-</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>17/02/2019</td>
														<td>USD</td>
														<td>73/-</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>16/02/2019</td>
														<td>USD</td>
														<td>71/-</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>15/02/2019</td>
														<td>USD</td>
														<td>70/-</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">EXCHANGE
											RATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label"  >Company</label> 
														
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Company</option>
													<option>Quadraerp</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Date</label> <input type="date" id="date" readonly id="date"
														class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Currency </label>
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Currency </option>
													<option>INR</option>
													<option>USD</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount</label> <input type="text" id="amt"
														maxlength="9"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')"
														class="form-control">
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="exchSave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>

												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ExchangeRate" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">EXCHANGE RATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Company</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company</option>
													<option>Quadraerp</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Date</label> <input type="date" id="date"
														class="form-control">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Currency </label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency </option>
													<option>INR</option>
													<option>USD</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount</label> <input type="text" id="amt"
														maxlength="9"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')"
														class="form-control">
												</div>
											</div>
											</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="exchSave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ExchangeRateEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">EXCHANGE
											RATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Company</label> <select id="compEdit"
														disabled="disabled" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Date</label> <input type="text" id="dateEdit"
														readonly="readonly" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Currency</label> <select id="currEdit"
														disabled="disabled" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount</label> <input type="text" id="amtEdit"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')"
														maxlength="9" class="form-control">
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="exchUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>