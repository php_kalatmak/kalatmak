

<script>
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-user').addClass('active');
	$('#menu-user > a').attr('aria-expanded', 'true');
	$('#menu-user > ul').addClass('in');

	$('#menu-User').addClass('active');
	$('#Branch').hide();
	$('input:checkbox').removeAttr('checked');


	$('#UserAdd').click(function() {
		$('#User').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

});

</script>



<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Company</a></li>
					<li class="active">User</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">User</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="UserAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="companyDesc">Company</th>
														<th data-field="usercode">User Code</th>
														<th data-field="userdesc">User Name</th>
														<th data-field="emailId">Email Id</th>
														<th data-field="mobileno">Mobile No</th>
														<th data-field="weblogic">Web Logic</th>
														<th data-field=mobilelogic>Mobile Logic</th>
														<th data-field=pwdneverexpired>Password Never Expired</th>
														<th data-field=locked>Locked</th>
														<th data-field="statusdesc">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>COM01</td>
														<td>admin</td>
														<td>admin@quadraerp.com</td>
														<td>9630258741</td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>COM02</td>
														<td>Sales</td>
														<td>sales@quadraerp.com</td>
														<td>8965741230</td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td><input type="checkbox" value="1" checked="checked"></td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">USER</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general">General</a></li>
											<li><a data-toggle="tab" href="#branch">Branch
													Mapping</a></li>
											<li><a data-toggle="tab" href="#role">Role Mapping</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general" class="tab-pane fade in active">
											<div class="modal-body" style="height: 300px">

												<div class="form-group col-md-4">
													<label class="control-label">Company Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>

												</div>
												<div class="form-group col-md-4">
													<label for="usercode" class="control-label">User
														Code </label> <input type="text" class="form-control"
														id="usercode" maxlength="10">
												</div>
												<div class="form-group col-md-4">
													<label for="UserName" class="control-label">User
														Name </label> <input type="text" class="form-control"
														id="UserName" maxlength="50"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="PassWord" class="control-label">PassWord
													</label> <input type="password" class="form-control" id="PassWord"
														maxlength="200">
												</div>
												<div class="form-group col-md-4">
													<label for="emailId" class="control-label">Email Id
													</label> <input type="email" class="form-control" id="emailId"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label for="mobile" class="control-label">Mobile No
													</label> <input type="text" class="form-control" id="Mobile"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
												</div>


												<!-- 	
												<div class="form-group col-md-6">
														<label class="col-md-12">Role Id</label>
														<div class="col-md-12">
															<select id="roleid_map" multiple="multiple" style=""
																class="multiselect-ui form-control">
															</select>
														</div>
													</div> -->


												<!-- 
													<div class="form-group col-md-6">
														<label class="col-md-12">Branch</label>
														<div class="col-md-12">
															<select id="branch_map" multiple="multiple" style=""
																class="multiselect-ui form-control">
															</select>
														</div>
											</div>
									 -->
												<div class="form-group col-md-4">
													<input type="checkbox" name="weblogic"><b> Is
														Web Logic </b><br> <input type="checkbox"
														name="mobilelogic"><b> Is Mobile Logic</b> <br>
													<input type="checkbox" name="passwordcheckbox"> <b>
														Is PassWord Never Expire</b> <br> <input type="checkbox"
														name="lock"><b> Is Locked</b><br>
												</div>


												<div class="form-group col-md-4">
													<label>Password Expire Period</label> <input type="text"
														name="probation" class="form-control" maxlength="2"
														id="passwordexpper"
														onkeypress="return isNumberKey(event);" tabindex="16">
												</div>

												<div class="form-group col-md-4">
													<label>Last PassWord Expire Date</label> <input type="text"
														id="passexpiredt" class="form-control" tabindex="17">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">User Image </label> <input type="file" class="form-control"
														id="compGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>


											</div>
										</div>

										<div id="branch" class="tab-pane fade">
											<div class="modal-body" style="height: 300px">
												<!-- <div class="form-group col-md-4">
													<label for="branch" class="control-label" onchange="">Branch Name
													</label> <select class="form-control" id="branchId"></select>														
												</div> -->

												<div class="form-group col-md-4">
													<label class="col-md-12">Branch Name</label>
													<div class="col-md-12">
													<select id="Comp1" multiple="multiple" style=""
														class="multiselect-ui form-control select2">
													<option>General</option>
													</select>
													</div>
												</div>
											</div>
										</div>

										<div id="role" class="tab-pane fade">
											<div class="modal-body" style="height: 300px">
												<!--  <div class="form-group col-md-4">
													<label for="role" class="control-label" onchange="">Role Name
													</label> <select class="form-control" id="roleId"></select>														
												</div>	 -->

												<div class="form-group col-md-4">
													<label class="col-md-12">Role Name</label>
													<div class="col-md-12">
													<select id="Comp_Map1" multiple="multiple" style=""
														class="multiselect-ui form-control select2">
													<option>Marketing</option>
													</select>
													</div>
												</div>
											</div>
										</div>

									</div>


									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="User_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="UserSave" type="button" style="margin-right: 5px">Save</button>

										</div>
									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="User" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">USER</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general2">General</a></li>
											<li><a data-toggle="tab" href="#branch2">Branch
													Mapping</a></li>
											<li><a data-toggle="tab" href="#role2">Role Mapping</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general2" class="tab-pane fade in active">
											<div class="modal-body" style="height: 300px">

												<div class="form-group col-md-4">
													<label class="control-label">Company Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>

												</div>
												<div class="form-group col-md-4">
													<label for="usercode" class="control-label">User
														Code </label> <input type="text" class="form-control"
														id="usercode" maxlength="10">
												</div>
												<div class="form-group col-md-4">
													<label for="UserName" class="control-label">User
														Name </label> <input type="text" class="form-control"
														id="UserName" maxlength="50"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="PassWord" class="control-label">PassWord
													</label> <input type="password" class="form-control" id="PassWord"
														maxlength="200">
												</div>
												<div class="form-group col-md-4">
													<label for="emailId" class="control-label">Email Id
													</label> <input type="email" class="form-control" id="emailId"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label for="mobile" class="control-label">Mobile No
													</label> <input type="text" class="form-control" id="Mobile"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
												</div>


												<!-- 	
												<div class="form-group col-md-6">
														<label class="col-md-12">Role Id</label>
														<div class="col-md-12">
															<select id="roleid_map" multiple="multiple" style=""
																class="multiselect-ui form-control">
															</select>
														</div>
													</div> -->


												<!-- 
													<div class="form-group col-md-6">
														<label class="col-md-12">Branch</label>
														<div class="col-md-12">
															<select id="branch_map" multiple="multiple" style=""
																class="multiselect-ui form-control">
															</select>
														</div>
											</div>
									 -->
												<div class="form-group col-md-4">
													<input type="checkbox" name="weblogic"><b> Is
														Web Logic </b><br> <input type="checkbox"
														name="mobilelogic"><b> Is Mobile Logic</b> <br>
													<input type="checkbox" name="passwordcheckbox"> <b>
														Is PassWord Never Expire</b> <br> <input type="checkbox"
														name="lock"><b> Is Locked</b><br>
												</div>


												<div class="form-group col-md-4">
													<label>Password Expire Period</label> <input type="text"
														name="probation" class="form-control" maxlength="2"
														id="passwordexpper"
														onkeypress="return isNumberKey(event);" tabindex="16">
												</div>

												<div class="form-group col-md-4">
													<label>Last PassWord Expire Date</label> <input type="text"
														id="passexpiredt" class="form-control" tabindex="17">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">User Image </label> <input type="file" class="form-control"
														id="compGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>


											</div>
										</div>

										<div id="branch2" class="tab-pane fade">
											<div class="modal-body" style="height: 300px">
												<!-- <div class="form-group col-md-4">
													<label for="branch" class="control-label" onchange="">Branch Name
													</label> <select class="form-control" id="branchId"></select>														
												</div> -->

												<div class="form-group col-md-4">
													<label class="col-md-12">Branch Name</label>
													<div class="col-md-12">
													<select id="Comp" multiple="multiple" style=""
														class="multiselect-ui form-control select2">
													<option>General</option>
													</select>
													</div>
												</div>
											</div>
										</div>

										<div id="role2" class="tab-pane fade">
											<div class="modal-body" style="height: 300px">
												<!--  <div class="form-group col-md-4">
													<label for="role" class="control-label" onchange="">Role Name
													</label> <select class="form-control" id="roleId"></select>														
												</div>	 -->

												<div class="form-group col-md-4">
													<label class="col-md-12">Role Name</label>
													<div class="col-md-12">
													<select id="Comp_Map" multiple="multiple" style=""
														class="multiselect-ui form-control select2">
													<option>Marketing</option>
													</select>
													</div>
												</div>
											</div>
										</div>

									</div>


									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="User_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="UserSave" type="button" style="margin-right: 5px">Save</button>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- <div class="storage_info modal " id="userEdit" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close " data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">User</h4>
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab"
											href="#generalEdit">General</a></li>
										<li><a data-toggle="tab" href="#branchEdit">Branch
												Mapping</a></li>
										<li><a data-toggle="tab" href="#roleEdit">Role
												Mapping</a></li>
									</ul>
								</div>
								<div class="tab-content">
									<div id="generalEdit" class="tab-pane fade in active">
										<div class="modal-body" style="height: 300px">
											<div class="form-group col-md-4">
												<label>Company Name</label> <select id="companyEdit"
													class="form-control"></select>
											</div>
											<div class="form-group col-md-4">
												<label for="storagenames" class="control-label">User
													Code</label> <input type="text" class="form-control"
													id="usercodeEdit" maxlength="10" readonly
													onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
											</div>

											<div class="form-group col-md-4">
												<label for="Bincode" class="control-label">User Name</label>
												<input type="text" class="form-control" id="usernameEdit"
													maxlength="50"
													onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
											</div>

											<div class="form-group col-md-4">
												<label for="BinName" class="control-label">PassWord
												</label> <input type="text" class="form-control" id="passwordEdit"
													maxlength="200"
													onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')">
											</div>

											<div class="form-group col-md-4">
												<label for="capacity" class="control-label">Email Id
												</label> <input type="text" class="form-control" id="EmailIdEdit"
													maxlength="100"
													onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')">
											</div>
											<div class="form-group col-md-4">
												<label for="capacity" class="control-label">Mobile
													Number </label> <input type="text" class="form-control"
													id="mobilenoEdit" maxlength="15"
													onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
											</div>
											<div class="form-group col-md-4">
												<input type="checkbox" name="weblogicEdit" id="wlogicEdit"><b>
													Is Web Logic </b><br> <input type="checkbox"
													name="mobilelogicEdit" id="mobileEdit"><b> Is
													Mobile Logic</b> <br> <input type="checkbox"
													name="passwordcheckbEdit" id="passwordcheckboxEdit"><b>
													Is PassWord Never Expire</b> <br> <input type="checkbox"
													name="LockEdit" id="lockEdit"><b> Is Locked</b><br>
											</div>

											<div class="form-group col-md-4">
												<label>Password Expire Period</label> <input type="text"
													name="passwordexpperEdit" class="form-control"
													maxlength="2" id="passwordexpperEdit" hidden="hidden"
													onkeypress="return isNumberKey(event);" tabindex="16">
											</div>

											<div class="form-group col-md-4">
												<label>Last PassWord Expire Date</label> <input type="text"
													id="passexpiredtEdit" class="form-control" tabindex="17"
													hidden="hidden">
											</div>
											<div class="form-group col-md-4">
												<label>Status</label> <select id="statususerEdit"
													class="form-control">

												</select>
											</div>
											<div class="form-group col-md-4">
												<label for="countryname" class="control-label"> </label> <input
													type="text" hidden="true" id="getcompId" maxlength="30">
											</div>
										</div>
									</div>
									<div id="branchEdit" class="tab-pane fade">
										<div class="modal-body" style="height: 300px">
											<div class="form-group col-md-4">
												<label class="col-md-12">Branch Name</label>
												<div class="col-md-12">
													<select id="branch_map_Edit" multiple="multiple" style=""
														class="multiselect-ui form-control">
													</select>
												</div>
											</div>
										</div>
									</div>

									<div id="roleEdit" class="tab-pane fade">
										<div class="modal-body" style="height: 300px">
											<div class="form-group col-md-4">
												<label class="col-md-12">Role Name</label>
												<div class="col-md-12">
													<select id="roleid_map_Edit" multiple="multiple" style=""
														class="multiselect-ui form-control">
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-success pull-right btn-sm"
											id="userUpdate" type="button" style="margin-right: 5px">Update</button>
									</div>
								</div>
							</div>
						</div>
					</div> -->

				</div>
			</div>
		</section>