
<script type="text/javascript">
	$(document).ready(function () {

$('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-user').addClass('active');
    $('#menu-user > a').attr('aria-expanded', 'true');
    $('#menu-user > ul').addClass('in');

    $('#menu-formRightsCreation').addClass('active');
    $("#FormRightsAdd").hide();

  $('#formRightsAdd').click(function () {
        $('#FormRightsAdd').show();
    });

    $('.close').click(function () {
        $('#FormRightsAdd').hide();
    });

     $('#formRightsReset').click(function () {
        $('#rightsName').val('');
        $('#moduleRightsId').val('-1');
        $('#rightsTypeId').val('-1');
    });
 });
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Rights</a></li>
					<li class="active">Module</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Form Rights</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="formRightsAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr><th data-field="rightsId">Rights ID</th>
														<th data-field="rightsName">Rights Name</th>
														<th data-field="moduleName">Module Name</th>
														<th data-field="rightsTypeName">Rights Type</th>
														<th data-field="statusName">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>R0001</td>
														<td>Purchase Order</td>
														<td>Purchase</td>
														<td>Web</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>R0002</td>
														<td>Sale Order</td>
														<td>Sales</td>
														<td>Mobile</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="Module_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">Forms Rights</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
										<div class="form-group col-md-6">
											<label for="rightsId" class="control-label">Rights ID
												</label> <input type="text" class="form-control" readonly
												id="rightsId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="rightsName" class="control-label">Rights Name
												</label> <input type="text" class="form-control"
												id="rightsName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
													<label class="control-label">Module Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Module Name</option>
													<option>Production</option>
													<option>Purchase</option>
													<option>Sales</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Rights Type</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Rights Type</option>
													<option>Mobile</option>
													<option>Web</option>
													</select>
												</div>										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="formRightsReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="formRightsSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="Module_info modal " id="FormRightsAdd" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">Forms Rights</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
										<div class="form-group col-md-6">
											<label for="rightsId" class="control-label">Rights ID
												</label> <input type="text" class="form-control" readonly
												id="rightsId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="rightsName" class="control-label">Rights Name
												</label> <input type="text" class="form-control"
												id="rightsName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
													<label class="control-label">Module Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Module Name</option>
													<option>Production</option>
													<option>Purchase</option>
													<option>Sales</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Rights Type</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Rights Type</option>
													<option>Mobile</option>
													<option>Web</option>
													</select>
												</div>										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="formRightsReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="formRightsSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
						 <div class="moduleCreationEdit modal " id="FormRightsCreationEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Form Rights Edit</h4>
									</div>
									<div class="modal-body" style="height: 250px">
									
										<div class="form-group col-md-6">
											<label for="rightsId" class="control-label">Rights ID
												</label> <input type="text" class="form-control" readonly
												id="rightsEditId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="rightsName" class="control-label">Rights Name
												</label> <input type="text" class="form-control"
												id="rightsEditName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
													<label>Module Name</label> <select id="moduleRightsEditId"
														class="form-control" >
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label>RightsType</label> <select id="rightsTypeEditId"
														class="form-control" >
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Status</label> <select id="statusEditId"
														class="form-control" >
													</select>
												</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="formRightsEditReset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="formRightsEditUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</section>