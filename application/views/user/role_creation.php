<script type="text/javascript">
	$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-user').addClass('active');
    $('#menu-user > a').attr('aria-expanded', 'true');
    $('#menu-user > ul').addClass('in');

    $('#menu-rolecreation').addClass('active');
    

    var compId = $("#comp_id_hidden").val();
    $("#RoleAdd").hide();

    $('#roleAdd').click(function() {
		$('#RoleAdd').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	 $('#role_Reset').click(function() {
		$("#companyId").val('-1');
		$("#roleId").val('');
		$("#rolename").val('');
	});   


	 $('#roleEdit_Reset').click(function() {	
		$("#rolenameEdit").val('');
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Rights</a></li>
					<li class="active">Role Creation</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Role Creation</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="roleAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr><th data-field="companyName">Company Name</th>
														<th data-field="roleId">Role ID</th>
														<th data-field="roleName">Role Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>Quadraerp</td>
														<td>RO0001</td>
														<td>developer</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>Quadraerp</td>
														<td>RO0002</td>
														<td>Designer</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>Quadraerp</td>
														<td>RO0003</td>
														<td>Marketing</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>Quadraerp</td>
														<td>RO0004</td>
														<td>Sales</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>Quadraerp</td>
														<td>RO0005</td>
														<td>Manager</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Role Creation</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
									<div class="form-group col-md-6">
													<label class="control-label">Company Name</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>
												</div>

										<div class="form-group col-md-6">
											<label for="roleId" class="control-label">Role ID
												</label> <input type="text" class="form-control" readonly
												id="roleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="rolename" class="control-label">Role
												Name</label> <input type="text" class="form-control"
												id="rolename" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="role_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="roleSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="RoleAdd" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Role Creation</h4>
									</div>
									<div class="modal-body" style="height: 150px">
										<div class="row">
									<div class="form-group col-md-6">
													<label class="control-label">Company Name</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>
									</div>

										<div class="form-group col-md-6">
											<label for="roleId" class="control-label">Role ID
												</label> <input type="text" class="form-control" readonly
												id="roleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										</div>
										<div class="row">
										<div class="form-group col-md-6">
											<label for="rolename" class="control-label">Role
												Name</label> <input type="text" class="form-control"
												id="rolename" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="role_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="roleSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
						 <div class="country_info modal " id="roleCreationEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10% !important">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Role Creation Edit</h4>
									</div>
									<div class="modal-body" style="height: 150px">
												<div class="form-group col-md-6">
											<label for="companyIdEdit" class="control-label">Company Name
												</label> <input type="text" class="form-control"
												id="companyIdEdit" readonly >
										</div>
											<input type="hidden" 
												id="companyEdit" >	
										<div class="form-group col-md-6">
											<label for="roleId" class="control-label">Role ID
												</label> <input type="text" class="form-control"
												id="roleIdEdit" readonly >
										</div>
										<div class="form-group col-md-6">
											<label for="rolenameEdit" class="control-label">Role
												Name</label> <input type="text" class="form-control"
												id="rolenameEdit" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="roleEdit_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="roleEditUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</section>