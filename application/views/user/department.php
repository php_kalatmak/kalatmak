

<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-user').addClass('active');
	$('#menu-user > a').attr('aria-expanded', 'true');
	$('#menu-user > ul').addClass('in');

	$('#menu-department').addClass('active');
	$('#Department').hide();

	$('#cityAdd').click(function() {
		$('#City').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Department</li>
				</ol>
			</div>
			
			<?php $state = $this->db->query("SELECT b.id,b.country_id,a.country_name,b.state_code,b.state_name,b.gst_code,b.union_territory,b.status from erpgcy a ,erpgste b WHERE a.id = b.country_id and b.status = '1' order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Department</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="cityAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="country">Company Name</th>
														<th data-field="stateId">Department Code</th>
														<th data-field="stateDesc">Department Name</th>
														<th data-field="gstCode">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>SL</td>
														<td>Sales</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>MAK</td>
														<td>Marketing</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>MED</td>
														<td>medical</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>PRO</td>
														<td>Production</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Quadraerp</td>
														<td>ACC</td>
														<td>Accounts</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Department</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label>Company Name</label>
													
													<select class="form-control select2" name="countryid"  >
													<option value="select country">Select Company Name</option>
													<option value="select country">Quadraerp</option>
													
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Department Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" readonly="readonly" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Department Name</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3"  value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Status</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select class="form-control select2" name="stateid"  >
													<option value="select country">Select Status</option>
													    <option value ="Active">Active</option>
														<option value ="InActive">InActive</option>
													
													</select>						
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												<!-- <?php $i=0; foreach($state as $row) { ?>
												<tr>
												<td><?php echo $row->country_name ?></td>
												<td><?php echo $row->state_code?></td>
												<td><?php echo $row->state_name?></td>
												<td><?php echo $row->gst_code?></td>
												<td><a class="edit ml10" title="" data-toggle="modal" data-target="#CityEdit<?php echo $i; ?>">
												                <i class="glyphicon glyphicon-pencil"></i></a></td>
												
						    <div class="country_info modal " id="CityEdit<?php echo $i; ?>" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Department</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label>Company Code</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select class="form-control select2" name="countryid"  >
													<option value="select country">Select Company Code</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->country_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Department Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" readonly="readonly" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Department Name</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3"  value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Status</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select class="form-control select2" name="stateid"  >
													<option value="select country">Select Status</option>
													    <option value ="Active">Active</option>
														<option value ="InActive">InActive</option>
													
													</select>						
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												
												
												</tr>
												<?php $i++; } ?> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="country_info modal " id="City" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Department</h4>
									</div>
									<div class="modal-body" style="height: 250px">
											<div class="row">
												<div class="form-group col-md-6">
													<label>Company Name</label>
													<select class="form-control select2" name="countryid"  >
													<option value="select country">Select Company Name</option>
													<option value="select country">Quadraerp</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Department Code</label> <input
														type="text" class="form-control" readonly="readonly" id="statename"
														maxlength="30" name="statename"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Department Name</label> <input
														type="text" class="form-control"  id="statename"
														maxlength="30" name="statename"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Status</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select id="stateedit" class="form-control select2" name="stateid"  >
													<option value="select country">Select Status</option>
													    <option value ="Active">Active</option>
														<option value ="InActive">InActive</option>
													
													</select>
												</div>
											</div>
											</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
										</div>
									</div>
							</div>
						</div>
					</div>


		</section>