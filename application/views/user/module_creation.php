<script type="text/javascript">
	$(document).ready(function () {

    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-user').addClass('active');
    $('#menu-user > a').attr('aria-expanded', 'true');
    $('#menu-user > ul').addClass('in');

    $('#menu-moduleCreation').addClass('active');
    $("#ModuleAdd").hide();

    $('#moduleAdd').click(function () {
        $('#ModuleAdd').show();
    });

    $('.close').click(function () {
        $('#ModuleAdd').hide();
    });

    $('#moduleReset').click(function () {
        $('#moduleName').val('');
        $('#parentModuleId').val('');
    });

     $('#moduleEditReset').click(function () {
        $('#modulenameEdit').val('');
        $('#parentModuleIdEdit').val('-1');
    });
 });
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Rights</a></li>
					<li class="active">Module</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Module Creation</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="moduleAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr><th data-field="moduleId">Module ID</th>
														<th data-field="moduleName">Module Name</th>
														<th data-field="parentModuleName">Parent Module ID</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>MOD001</td>
														<td>Production</td>
														<td>1000001</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>MOD002</td>
														<td>Purchase</td>
														<td>1000002</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>MOD003</td>
														<td>Sales</td>
														<td>1000003</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<tr>
														<td>MOD004</td>
														<td>Accounts</td>
														<td>1000004</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="Module_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">Module Creation</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
										<div class="form-group col-md-6">
											<label for="moduleId" class="control-label">Module ID
												</label> <input type="text" class="form-control" readonly
												id="moduleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="moduleName" class="control-label">Module Name
												</label> <input type="text" class="form-control"
												id="moduleName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
													<label class="control-label">Parent Module ID</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Parent Module ID</option>
													<option>10001</option>
													<option>10002</option>
													<option>10004</option>
													<option>10005</option>
													</select>
												</div>
										<!-- <div class="form-group col-md-6">
											<label for="parentModuleId" class="control-label">Parent Module ID
												</label> <input type="text" class="form-control" 
												id="parentModuleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div> -->
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="moduleReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="moduleSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="Module_info modal " id="ModuleAdd" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">Module Creation</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
										<div class="form-group col-md-6">
											<label for="moduleId" class="control-label">Module ID
												</label> <input type="text" class="form-control" readonly
												id="moduleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="moduleName" class="control-label">Module Name
												</label> <input type="text" class="form-control"
												id="moduleName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
													<label class="control-label">Parent Module ID</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Parent Module ID</option>
													<option>10001</option>
													<option>10002</option>
													<option>10004</option>
													<option>10005</option>
													</select>
												</div>
										<!-- <div class="form-group col-md-6">
											<label for="parentModuleId" class="control-label">Parent Module ID
												</label> <input type="text" class="form-control" 
												id="parentModuleId" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div> -->
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="moduleReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="moduleSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
						 <div class="moduleCreationEdit modal " id="moduleCreationEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Module Creation Edit</h4>
									</div>
									<div class="modal-body" style="height: 150px">
									
										<div class="form-group col-md-6">
											<label for="moduleIdEdit" class="control-label">Module ID
												</label> <input type="text" class="form-control"
												id="moduleIdEdit" readonly >
										</div>
										<div class="form-group col-md-6">
											<label for="mopdulenameEdit" class="control-label">Module Name
												</label> <input type="text" class="form-control"
												id="modulenameEdit" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
													<label>Parent Module ID</label> <select id="parentModuleIdEdit"
														class="form-control" >
													</select>
												</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="moduleEditReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="moduleEditUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</section>