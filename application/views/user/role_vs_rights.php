<script type="text/javascript">
	$(document).ready(function () {

    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-user').addClass('active');
    $('#menu-user > a').attr('aria-expanded', 'true');
    $('#menu-user > ul').addClass('in');

    $('#menu-roleVsRights').addClass('active');
    $("#RolesVsRightsAdd").hide();

    $('#rolesVsRightsAdd').click(function () {
        $('#RolesVsRightsAdd').show();
    });

    $('.close').click(function () {
        $('#RolesVsRightsAdd').hide();
    });
     $('input:checkbox').removeAttr('checked');

    $('#roleVsRightsReset').click(function(){
        $('#companyId').val('-1');
        $('#roleId').val('-1');
        $('#rightsId').val('-1');
         $("#moduleID").val('-1');
        $('input:checkbox').removeAttr('checked');
    });
  });  
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Rights</a></li>
					<li class="active">Module</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">RolesVsRights</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="rolesVsRightsAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr><th data-field="companyName">Company Name</th>
													    <th data-field="roleName">Role Name</th>
														<th data-field="rightsName">Rights Name</th>														
														<th data-field="create">Create</th>
														<th data-field="edit">Edit</th>
														<th data-field="view">View</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>Manager</td>
														<td>Purchase Order</td>
														<td>Yes</td>
														<td>No</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												    </tr>
												    <tr>
														<td>Quadraerp</td>
														<td>Designer</td>
														<td>Sale Order</td>
														<td>Yes</td>
														<td>No</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												    </tr>
												    <div class="Module_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">RoleVsRights</h4>
									</div>
									<div class="modal-body" style="height: 200px">
									
									         <div class="form-group col-md-6">
													<label class="control-label">Company Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Role Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Role Name</option>
													<option>Marketing</option>
													<option>Designer</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Module Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Module Name</option>
													<option>Production</option>
													<option>Purchase</option>
													<option>Sale</option>
													<option>Accounts</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Rights Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Rights Name</option>
													<option>Purchase Order</option>
													<option>Sales Order</option>
													</select>
												</div>
									<div class="form-group col-md-4">
									<input type="checkbox" name="createlogic" id="isCreate"><b> Is Create</b><br></div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="editlogic" Id="isEdit"><b> Is Edit</b><br>	</div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="viewlogic" Id="isView"><b> Is View</b><br>	
									</div>										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="roleVsRightsReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="roleVsRightsSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="Module_info modal " id="RolesVsRightsAdd" tabindex="-1"
							role="dialog" aria-labelledby="ModuleTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="ModuleTypeLabel">RoleVsRights</h4>
									</div>
									<div class="modal-body" style="height: 200px">
									
									         <div class="form-group col-md-6">
													<label class="control-label">Company Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company Name</option>
													<option>Quadraerp</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Role Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Role Name</option>
													<option>Marketing</option>
													<option>Designer</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Module Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Module Name</option>
													<option>Production</option>
													<option>Purchase</option>
													<option>Sale</option>
													<option>Accounts</option>
													</select>
												</div>
												
												<div class="form-group col-md-6">
													<label class="control-label">Rights Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Rights Name</option>
													<option>Purchase Order</option>
													<option>Sales Order</option>
													</select>
												</div>
									<div class="form-group col-md-4">
									<input type="checkbox" name="createlogic" id="isCreate"><b> Is Create</b><br></div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="editlogic" Id="isEdit"><b> Is Edit</b><br>	</div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="viewlogic" Id="isView"><b> Is View</b><br>	
									</div>										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="roleVsRightsReset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="roleVsRightsSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
						 <div class="moduleCreationEdit modal " id="RoleVsRightsCreationEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">RoleVsRights Edit</h4>
									</div>
									<div class="modal-body" style="height: 250px">
									
										 <div class="form-group col-md-6">
											<label for="rightsId" class="control-label">Company Name
												</label> <input type="text" class="form-control" readonly
												id="companyEditName" maxlength="2" onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="roleName" class="control-label">Role Name
												</label> <input type="text" class="form-control" readonly
												id="roleEditName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="rightsName" class="control-label">Rights Name
												</label> <input type="text" class="form-control" readonly
												id="rightsEditName" maxlength="50" onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div> 
										
										<input type="hidden" id="companyEditId">
										<input type="hidden" id="roleEditId">
										<input type="hidden" id="rightsEditId">
										
										
										<!--  <div class="form-group col-md-6">
													<label>Company Name</label> <select id="companyEditId"
														class="form-control" readonly="true">
													</select>
												</div>
												 <div class="form-group col-md-6">
													<label>Role Name</label> <select id="roleEditName"
														class="form-control" readonly="true">
													</select>
												</div>
												 <div class="form-group col-md-6">
													<label>Rights Name</label> <select id="rightsEditName"
														class="form-control" readonly="true">
													</select>
												</div> -->
										<div class="form-group col-md-4">
									<input type="checkbox" name="createEditlogic" id="isEditCreate"><b> Is Create</b><br></div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="editEditlogic" Id="iseditEdit"><b> Is Edit</b><br>	</div>	
									<div class="form-group col-md-4">
									<input type="checkbox" name="viewEditlogic" Id="isEditView"><b> Is View</b><br>	
									</div>
										
										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="formRightsEditReset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="roleVsRightsEditUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</section>