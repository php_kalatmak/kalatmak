<script type="text/javascript">
	$(document).ready(function () {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-businesspartner').addClass('active');
	$('#menu-businesspartner > a').attr('aria-expanded', 'true');
	$('#menu-businesspartner > ul').addClass('in');
	

	

	$('#menu-salesgroup').addClass('active');
	$('#salesgroup').hide();
	$('#salesgroupEdit').hide();

	$('#salesgroupAdd').click(function () {
		$('#salesgroup').show();
	});

	$('#sales_Reset').click(function () {
		$('#salesgroupid').val('');
		$('#salesgroupname').val('');
	});
  
	$(".close").click(function () {
		$(".modal").hide();
	});
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Business Partner</a></li>
					<li class="active">Sales Group</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Sales Group</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="salesgroupAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="salesgroupId">Sales Group Id</th>
														<th data-field="salesDesc">Sales Group Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>GroupID001</td>
													<td>Group Name 1</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>GroupID002</td>
													<td>Group Name 2</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Sales Group</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="salesgroupid" class="control-label">Sales Group Id
												</label> <input type="text" class="form-control" readonly name="Sales Group Id"
												id="salesgroupid"  maxlength="2"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="salesgroupname" class="control-label">Sales group 
												Name</label> <input type="text" class="form-control"
												id="salesgroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="sales_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="salesgroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="salesgroup" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Sales Group</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="salesgroupid" class="control-label">Sales Group Id
												</label> <input type="text" class="form-control"
												id="salesgroupid"  maxlength="2"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="salesgroupname" class="control-label">Sales group 
												Name</label> <input type="text" class="form-control"
												id="salesgroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="sales_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="salesgroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="salesgroupEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Sales Group</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										
									<!-- 	<div class="form-group col-md-6">
											<label for="salesgroupEdit" class="control-label">Sales Group 
												Id</label> <select class="form-control"
												id="salesgroupidEdit">
												
												</select>
										</div> -->
										<div class="form-group col-md-6">
											<label for="salesgroupEdit" class="control-label">Sales Group
												</label> <input type="text" class="form-control"
												id="salesgroupidEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="storagename" class="control-label">Sales Group
												Name</label> <input type="text" class="form-control"
												id="salesnameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="salesgroupUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>