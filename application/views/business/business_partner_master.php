
<script type="text/javascript">
	$(document).ready(function () {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-businesspartner').addClass('active');
	$('#menu-businesspartner > a').attr('aria-expanded', 'true');
	$('#menu-businesspartner > ul').addClass('in');

	$('#menu-businessPartnerMaster').addClass('active');
	$('#businessmastersave').hide();
	$('#businessgroupEdit').hide();



	  $("#chooseFile").hide();
	  $("#fileInput").hide();
	   $("#chooseFile1").hide();
	  $("#fileInput1").hide();
	   $("#chooseFile2").hide();
	  $("#fileInput2").hide();
	   $("#chooseFile3").hide();
	  $("#fileInput3").hide();
	   $("#chooseFile4").hide();
	  $("#fileInput4").hide();
	   $("#chooseFile5").hide();
	  $("#fileInput5").hide();
	   $("#chooseFile6").hide();
	  $("#fileInput6").hide();
	   $("#chooseFile7").hide();
	  $("#fileInput7").hide();
	   $("#chooseFile8").hide();
	  $("#fileInput8").hide();
	   $("#chooseFile9").hide();
	  $("#fileInput9").hide();
	    $("#chooseFile10").hide();
	  $("#fileInput10").hide();

	var counter = 1;

	$("#addDiv").click(function () {



		if (counter == 1) {
			$("#fileInput").show();
			$("#chooseFile").show();
             alert(1)
	}

		if (counter == 2) {
			$("#fileInput1").show();
			$("#chooseFile1").show();
		
               alert(2)

		}

		if (counter == 3) {
		$("#fileInput2").show();
			$("#chooseFile2").show();
		alert(3)

		}
		if (counter == 4) {
		$("#fileInput3").show();
			$("#chooseFile3").show();
		
            alert(4)
		}
		if (counter == 5) {
		$("#fileInput4").show();
			$("#chooseFile4").show();
		alert(5)

		}
		if (counter == 6) {
		$("#fileInput5").show();
			$("#chooseFile5").show();
		alert(6)

		}
		if (counter == 7) {
		$("#fileInput6").show();
			$("#chooseFile6").show();
		alert(7)

		}
		if (counter == 8) {
		$("#fileInput7").show();
			$("#chooseFile7").show();
		alert(8)

		}
		if (counter == 9) {
		$("#fileInput8").show();
			$("#chooseFile8").show();
		alert(9)

		}
		if (counter == 10) {
		$("#fileInput9").show();
			$("#chooseFile9").show();
		alert(10)

		}
		if (counter == 11) {
		$("#fileInput10").show();
			$("#chooseFile10").show();
		alert(11)

		}
		

		/*if(counter>4){
		   return false;
		}*/

		counter++;
	});
	$('#businessmasterAdd').click(function () {
		$('#businessmastersave').show();
	});

	$('#businesspartnergroup_Reset').click(function () {
		$('#businesspartnergroupId').val('');
		$('#businesspartnergroupname').val('');
		$('#businesspartnercode').val('');
		$('#businesspartnername').val('');
	});

	$(".close").click(function () {
		$(".modal").hide();
	});
$('#chooseFile').click(function(event) {
    event.preventDefault();
    // prevents our button from submitting or doing anything else
    $('#fileInput').trigger('click');
    // passes on the click event to our hidden choose file input (or rather,
    // it triggers a new click event, whatevs)
});

$("#fileInput").on("change", function() {
    var file = this.files[0],
        fileName = file.name,
        fileSize = file.size;
    // determining the filename of the newly selected file
    $('#fileName').text("Uploading: " + fileName + " (" + fileSize + " bytes)").show();
    // setting the text of the fileName element, and showing it
});

});

</script>
	
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Business Partner Master</a></li>
					<li class="active">BusinessPartnerMaster</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Business Partner Master</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="businessmasterAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="bpcode">Business Partner Code</th>
														<th data-field="bpname">Business Partner Name</th>
														<th data-field="bpcode">Business Partner Type</th>
														<th data-field="bpname">Business Partner Group</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>PartID001</td>
													<td>Asian Paints</td>
													<td>Vendor</td>
													<td>External</td>
													<td><a class="edit ml10" title="">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartID002</td>
													<td>Fevicol</td>
													<td>Vendor</td>
													<td>Local</td>
													<td><a class="edit ml10" title="">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="storage_info modal " id="StateEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog ">
								<div class="modal-content" style="height: auto; margin-bottom: 20px">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business
											Partner Master</h4>

									</div>
									<div class="modal-body" style="height: 250px">
										<div class="row">
										<div class="form-group col-md-4">
											<label for="businesspartnercode" class="control-label">Business
												Partner Code </label> <input type="text" class="form-control"
												id="businesspartnercode" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnername" class="control-label">Business
												Partner Name</label> <input type="text" class="form-control"
												id="businesspartnername" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Type</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Type</option>
											<option>Lead</option>
											<option>Customer</option>
											<option>Vendor</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Group </label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Group </option>
											<option>Local</option>
											<option>External</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Currency</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Currency </option>
											<option>INR</option>
											<option>USD</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label for="countrycode" class="control-label">Business
												Partner Image </label> <input type="file" class="form-control"
												id="businesspartnerimage" onchange="readURL1(this);" />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Status</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Status</option>
											<option>Active</option>
											<option>In-Active</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<img id="blah1" src="../../../quadraerp/assets/images/avatars/def.png"
												alt="..." style="height: 72px;" />
										</div>
										
									</div>
									</div>
									<div class="inner-tabs">
										<ul class="nav nav-tabs">
											<li><a data-toggle="tab" href="#general1">General</a></li>
											<li><a data-toggle="tab" href="#sales1">Sales</a></li>
											<li><a data-toggle="tab" href="#account1">Account</a></li>
											<li><a data-toggle="tab" href="#tax1">Tax</a></li>
											<li><a data-toggle="tab" href="#paymentterm1">PayMentTerm</a></li>
											<li><a data-toggle="tab" href="#remarks1">Remarks</a></li>
											<li><a data-toggle="tab" href="#attachment1">Attachment</a></li>
										</ul>

										<div class="tab-content">
											<div id="general1" class="tab-pane fade" style="height: 300px">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#billtoparty1">Bill
															To Party</a></li>
													<li><a data-toggle="tab" href="#shiptoparty1">Shipped
															to party</a></li>
												</ul>
												<div class="tab-content">

													<div id="billtoparty1" class="tab-pane">
														<div class="scroll-modal" id="billparty">
															<div class="form-group col-md-4" hidden="true">
																<label class="control-label">Business Partner
																	Code</label> <input type="text" class="form-control"
																	readonly="readonly" id="businesspartnercodebill"
																	maxlength="8">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">House No</label> <input
																	type="text" class="form-control" id="houseNo"
																	maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street</label> <input
																	type="text" class="form-control" id="street"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 1</label> <input
																	type="text" class="form-control" id="street1"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 2</label> <input
																	type="text" class="form-control" id="street2"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Landmark</label> <input
																	type="text" class="form-control" id="landmark"
																	maxlength="50">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">City</label> <input
																	type="text" class="form-control" id="city"
																	maxlength="50"
																	onkeyup="this.value = this.value.replace(/[^A-Za-z ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Country</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">State</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>India</option>
																<option>America</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Pin Code</label> <input
																	type="text" class="form-control" id="pincode"
																	maxlength="6"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Mobile Number</label> <input
																	type="text" class="form-control" id="mobile"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Number</label> <input
																	type="text" class="form-control" id="telephone"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Extension</label>
																<input type="text" class="form-control" id="teleExten"
																	maxlength="4"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Email</label> <input
																	type="text" class="form-control" id="email"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Web Site</label> <input
																	type="text" class="form-control" id="website"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Mobile</label> <input
																	type="text" class="form-control" id="altMob"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Telephone</label>
																<input type="text" class="form-control" id="altTel"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Contact Person</label>
																<input type="text" class="form-control" id="altTel"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-12">
																<input type="checkbox" name="billtopartysame"
																	id="billsame"><b> Same As Bill To Party </b><br>
															</div>
														</div>
													</div>


													<div id="shiptoparty1" class="tab-pane">
														<div class="scroll-modal" id="shipparty">
															<div class="form-group col-md-4" hidden="true">
																<label class="control-label">Business Partner
																	Code</label> <input type="text" class="form-control"
																	readonly="readonly" id="businesspartnercodeShip"
																	maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">House No</label> <input
																	type="text" class="form-control" id="houseNoShip"
																	maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street</label> <input
																	type="text" class="form-control" id="streetShip"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 1</label> <input
																	type="text" class="form-control" id="street1Ship"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 2</label> <input
																	type="text" class="form-control" id="street2Ship"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Landmark</label> <input
																	type="text" class="form-control" id="landmarkShip"
																	maxlength="50">
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Pin Code</label> <input
																	type="text" class="form-control" id="pincodeShip"
																	maxlength="6"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Mobile Number</label> <input
																	type="text" class="form-control" id="mobileShip"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Number</label> <input
																	type="text" class="form-control" id="telephoneShip"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Extension</label>
																<input type="text" class="form-control"
																	id="teleExtenShip" maxlength="4"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Email</label> <input
																	type="text" class="form-control" id="emailShip"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Web Site</label> <input
																	type="text" class="form-control" id="websiteShip"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Mobile</label> <input
																	type="text" class="form-control" id="altMobShip"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Telephone</label>
																<input type="text" class="form-control" id="altTelShip"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Contact Person</label>
																<input type="text" class="form-control" id="altTel"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>

														</div>
													</div>

												</div>
											</div>
											<div id="sales1" class="tab-pane fade" style="height: 55px">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#generals1">General</a></li>
													<li><a data-toggle="tab" href="#branch1">Branch</a></li>
												</ul>
												<div class="tab-content">
													<div id="generals1" class="tab-pane fade"
														style="height: 172px">
														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="businesspartnercodegeneral"
																maxlength="8">
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Sales group Id  </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Sales group Id  </option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Grade Id  </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Grade Id </option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label for="field1" class="control-label">Field 1
															</label> <input type="text" class="form-control" id="field1"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-4">
															<label for="field2" class="control-label">Field 2
															</label> <input type="text" class="form-control" id="field2"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-4">
															<label for="field3" class="control-label">Field 3
															</label> <input type="text" class="form-control" id="field3"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>

													</div>

													<div id="branch1" class="tab-pane fade"
														style="height: 160px">

														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="businesspartnercodebranch"
																maxlength="10">
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Branch Code   </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code  </option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Employee Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Employee Code </option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status</option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Division Code  </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division Code</option>
																<option>India</option>
																<option>America</option>
																</select>
														</div>

													</div>
												</div>
											</div>


											<div id="account1" class="tab-pane fade">
												<div class="scroll-modal">

													<div class="form-group col-md-4" hidden="true">
														<label class="control-label">Business Partner Code</label>
														<input type="text" class="form-control"
															readonly="readonly" id="businesspartnercodeaccount"
															maxlength="8">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Payment Term Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Payment Term Code</option>
																<option>India</option>
																<option>America</option>
																</select>
													</div>

													<div class="form-group col-md-4">
														<label class="control-label">Interest On Arrears</label> <input
															type="text" class="form-control" id="intereston"
															maxlength="3"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Dunning Term code</label> <input
															type="text" class="form-control" id="dunningterm"
															maxlength="4"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Revenu Account</label> <input
															type="text" class="form-control" id="revenuaccount"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Down Payment</label> <input
															type="text" class="form-control" id="downpayment"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Interim Account</label> <input
															type="text" class="form-control" id="interimaccount"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Payment Method Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Payment Method Code</option>
																<option>India</option>
																<option>America</option>
																</select>
													</div>
													<div class="form-group col-md-12">
														<input type="checkbox" name="ispaymentblock"><b>
															Is Payment Block </b><br>
													</div>

													<div class="form-group col-md-4">
														<label for="field1" class="control-label">Field 1
														</label> <input type="text" class="form-control" id="fields1"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label for="fields2" class="control-label">Field 2
														</label> <input type="text" class="form-control" id="fields2"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label for="fields3" class="control-label">Field 3
														</label> <input type="text" class="form-control" id="fields3"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
												</div>
											</div>
											<div id="tax1" class="tab-pane fade">
												<div class="scroll-modal">
													<div class="form-group col-md-4" hidden="true">
														<label class="control-label"></label> <input type="text"
															class="form-control" readonly id="addressId"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Pan Number</label> <input
															type="text" class="form-control" id="pannumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tin Number</label> <input
															type="text" class="form-control" id="tinnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Service Tax Number</label> <input
															type="text" class="form-control" id="servicetaxnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tan Number</label> <input
															type="text" class="form-control" id="tannumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Cst Number</label> <input
															type="text" class="form-control" id="cstnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Ist Number</label> <input
															type="text" class="form-control" id="istnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Gst Number</label> <input
															type="text" class="form-control" id="gstnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>

													<div class="form-group col-md-4">
														<label class="control-label">Gst Type code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Gst Type code</option>
																<option>India</option>
																<option>America</option>
																</select>
													</div>

												</div>
											</div>
											<div id="paymentterm1" class="tab-pane fade"
												style="height: 200px">

												<div class="form-group col-md-4" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control"
														id="businesspayment" maxlength="8" readonly
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Country code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country code</option>
																<option>Code1</option>
																<option>code2</option>
																</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Bank code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bank code</option>
																<option>India</option>
																<option>America</option>
																</select>
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Ifsc Code</label> <input
														type="text" class="form-control" id="ifsccode"
														maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Bank Branch Name</label> <input
														type="text" class="form-control" id="bankbranchname"
														maxlength="35"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Bank Holder Name</label> <input
														type="text" class="form-control" id="bankholdername"
														maxlength="50"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Bank Account Number</label> <input
														type="text" class="form-control" id="bankaccountno"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>

											</div>

											<div id="remarks1" class="tab-pane fade "
												style="height: 200px">

												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control" readonly
														id="businessremark" maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												<div class="form-group col-md-12">
													<label for="remarks" class="control-label">Remarks</label>
													<textarea rows="2" cols="" maxlength="200" id="remarks"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')"></textarea>

												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark1" class="control-label">Field
														1 </label> <input type="text" class="form-control"
														id="fieldremark1" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark2" class="control-label">Field
														2 </label> <input type="text" class="form-control"
														id="fieldremark2" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark3" class="control-label">Field
														3 </label> <input type="text" class="form-control"
														id="fieldremark3" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
											</div>
											<div id="attachment1" class="tab-pane fade"
												style="height: 150px">

												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control" id="businessatch"
														maxlength="8" readonly
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
											<div class="form-group col-md-4">
											<button type="button" class="btn btn-primary" id="addDiv">+ Add Attachment</button>
											</div>
											
											 <div class="form-group col-md-4">
											 <input
													type="file" id="fileInput">
												<button id="chooseFile" class="btn">
													<i class="icon-file"></i> Choose File
												</button>										
												
											</div>
										</div>

									</div>

									<div class="modal-footer clearfix">

										<div class="form-group">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartnergroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnerMasterSave" type="button"
												style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
					</div>

											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="businessmastersave"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog ">
								<div class="modal-content" style="height: auto; margin-bottom: 20px">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business
											Partner Master</h4>

									</div>
									<div class="modal-body" style="height: 550px !important; padding: 25px;">
										<div class="row">
										<div class="form-group col-md-4">
											<label for="businesspartnercode" class="control-label">Business
												Partner Code </label> <input type="text" class="form-control"
												id="businesspartnercode" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnername" class="control-label">Business
												Partner Name</label> <input type="text" class="form-control"
												id="businesspartnername" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Type</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Type</option>
											<option>Lead</option>
											<option>Customer</option>
											<option>Vendor</option>
											</select>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Group </label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Group </option>
											<option>Local</option>
											<option>External</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Currency</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Currency </option>
											<option>INR</option>
											<option>USD</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label for="countrycode" class="control-label">Business
												Partner Image </label> <input type="file" class="form-control"
												id="businesspartnerimage" onchange="readURL1(this);" />
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">
											<label class="control-label">Business Partner Status</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Status</option>
											<option>Active</option>
											<option>In-Active</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<img id="blah1" src="../../../quadraerp/assets/images/avatars/def.png"
												alt="..." style="height: 72px;" />
										</div>
										
									</div>
									<div class="inner-tabs">
										<ul class="nav nav-tabs">
											<li><a data-toggle="tab" href="#general">General</a></li>
											<li><a data-toggle="tab" href="#sales">Sales</a></li>
											<li><a data-toggle="tab" href="#account">Account</a></li>
											<li><a data-toggle="tab" href="#tax">Tax</a></li>
											<li><a data-toggle="tab" href="#paymentterm">PayMentTerm</a></li>
											<li><a data-toggle="tab" href="#remarks2">Remarks</a></li>
											<li><a data-toggle="tab" href="#attachment">Attachment</a></li>
										</ul>
										<div class="tab-content" style="height:300px; margin-bottom:30px; overflow-y:scroll">
											<div id="general" class="tab-pane fade">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#billtoparty">Bill
															To Party</a></li>
													<li><a data-toggle="tab" href="#shiptoparty">Shipped
															to party</a></li>
												</ul>
												<div class="tab-content">
													<div id="billtoparty" class="tab-pane">
														<div class="" id="billparty">
															<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<div class="col-md-4">
																</div>
																<input type="checkbox" name="billtopartysame"
																	id="billsame"><b> Same As Bill To Party </b><br>
															</div>
														</form>
														</div>
														</div>
                                                        <div class="clearfix"></div>
													</div>
                                                        <div id="shiptoparty" class="tab-pane">
                                                        	<div class="row form-group">
                                                        	<div class="col-md-2 ">
                                                        	<div class="col-md-12">
                                                        		<button type="button" class="btn btn-success">Address1</button>
                                                        	</div>
                                                        	<div style="padding: 20px"></div>
                                                        	<div class="col-md-12">
                                                        		<button type="button" class="btn btn-success">Address2</button>
                                                        	</div>
                                                        	<div style="padding: 20px"></div>
                                                        	<div class="col-md-12">
                                                        		<button type="button" class="btn btn-success">Address3</button>
                                                        	</div>
                                                        	<div style="padding: 20px"></div>
                                                        	<div class="col-md-12">
                                                        		<button type="button" class="btn btn-success">Address4</button>
                                                        	</div>
                                                       		 </div>
                                                        	<div class="col-md-10">
                                                            <div class="scroll-modal" id="shipparty">
                                                                <a class="btn btn-sm btn-primary" onClick="add_ship()">Add More</a>
                                                                <a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
                                                                <div id="append_ship">
                                                                	<h4>Shipping Address</h4>
                                                                    <div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<div class="col-md-5">
																</div>
																<button type="button" class="btn btn-success">Save</button>
															</div>
														</form>
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        
													</div>
                                                    <div class="clearfix"></div>
												</div>
											
											<div id="sales" class="tab-pane fade" style="height: auto; margin-bottom: 20px">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#generals">General</a></li>
													<li><a data-toggle="tab" href="#branch">Branch</a></li>
												</ul>
												<div class="tab-content">
													<div id="generals" class="tab-pane fade" style="height: auto; margin-bottom: 20px">
													<div class="col-md-4">
														<div class="form-group col-md-12">
															<label class="control-label">Sales group </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Sales group</option>
																<option>Group 1</option>
																<option>Group 2</option>
																</select>
														</div>
														<div class="form-group col-md-12">
															<label class="control-label">Grade </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Grade  </option>
																<option>A</option>
																<option>B</option>
																<option>C</option>
																</select>
														</div>
													</div>
													<div class="col-md-2">
													</div>
													<div class="col-md-4">
														<div class="form-group col-md-12">
															<label for="field1" class="control-label">Field 1
															</label> <input type="text" class="form-control" id="field1"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-12">
															<label for="field2" class="control-label">Field 2
															</label> <input type="text" class="form-control" id="field2"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-12">
															<label for="field3" class="control-label">Field 3
															</label> <input type="text" class="form-control" id="field3"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
													</div>
														<div class="clearfix"></div>
													</div>

													<div id="branch" class="tab-pane fade"
														style="height: auto; margin-bottom: 20px">

														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="businesspartnercodebranch"
																maxlength="10">
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Branch Code   </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code  </option>
																<option>1000</option>
																<option>1001</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Employee Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Employee Code </option>
																<option>EMP00001</option>
																<option>EMP00002</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status</option>
																<option>Active</option>
																<option>In-Active</option>
																</select>
														</div>
														<div class="form-group col-md-4">
															<label class="control-label">Division  </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division </option>
																<option>Carpentry</option>
																<option>Metal</option>
																</select>
														</div>
														<div class="clearfix"></div>
													</div>
												</div>
                                                <div class="clearfix"></div>
											</div>
											<div id="account" class="tab-pane fade">
												<div class="row">
													<div class="col-md-4">
													<div class="form-group col-md-12">
														<label class="control-label">Payment Term</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Payment Term Code</option>
																<option>PS21</option>
																<option>PS45</option>
																</select>
													</div>
													<div class="form-group col-md-12">
														<label class="control-label">Payment Method </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Payment Method </option>
																<option>Cash</option>
																<option>Cheque</option>
																<option>NEFT</option>
																</select>
													</div>
													<div class="form-group col-md-12">
														<label class="control-label">Interest On Arrears</label> <input
															type="text" class="form-control" id="intereston"
															maxlength="3"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-12">
														<label class="control-label">Dunning Term code</label> <input
															type="text" class="form-control" id="dunningterm"
															maxlength="4"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group col-md-12">
														<label class="control-label">Revenu Account</label> <input
															type="text" class="form-control" id="revenuaccount"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-12">
														<label class="control-label">Down Payment</label> <input
															type="text" class="form-control" id="downpayment"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-12">
														<label class="control-label">Interim Account</label> <input
															type="text" class="form-control" id="interimaccount"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
													</div>
													<div class="form-group col-md-12">
														<input type="checkbox" name="ispaymentblock"><b>
															Is Payment Block </b><br>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group col-md-12">
														<label for="field1" class="control-label">Field 1
														</label> <input type="text" class="form-control" id="fields1"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-12">
														<label for="fields2" class="control-label">Field 2
														</label> <input type="text" class="form-control" id="fields2"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-12">
														<label for="fields3" class="control-label">Field 3
														</label> <input type="text" class="form-control" id="fields3"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
												</div>
												</div>
                                                <div class="clearfix"></div>
											</div>
											<div id="tax" class="tab-pane fade">
												<div class="">
													<div class="form-group col-md-4" hidden="true">
														<label class="control-label"></label> <input type="text"
															class="form-control" readonly id="addressId"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Pan Number</label> <input
															type="text" class="form-control" id="pannumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tin Number</label> <input
															type="text" class="form-control" id="tinnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Service Tax Number</label> <input
															type="text" class="form-control" id="servicetaxnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tan Number</label> <input
															type="text" class="form-control" id="tannumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Cst Number</label> <input
															type="text" class="form-control" id="cstnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Ist Number</label> <input
															type="text" class="form-control" id="istnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Gst Number</label> <input
															type="text" class="form-control" id="gstnumber"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>

													<div class="form-group col-md-4">
														<label class="control-label">Gst Type</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Gst Type</option>
																<option>Regular</option>
																</select>
													</div>

												</div>
                                                <div class="clearfix"></div>
											</div>
											<div id="paymentterm" class="tab-pane fade">
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Country Name</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country Name</option>
																<option>India</option>
																<option>America</option>
																</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Bank Name</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bank Name</option>
																<option>Axis</option>
																<option>HDFC</option>
																</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Ifsc Code</label> <input
														type="text" class="form-control" id="ifsccode"
														maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Bank Branch Name</label> <input
														type="text" class="form-control" id="bankbranchname"
														maxlength="35"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Bank Holder Name</label> <input
														type="text" class="form-control" id="bankholdername"
														maxlength="50"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Bank Account Number</label> <input
														type="text" class="form-control" id="bankaccountno"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
											</div>
												<div class="clearfix"></div>
											</div>
											<div id="remarks2" class="tab-pane fade">
												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control" readonly
														id="businessremark" maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												<div class="form-group col-md-12">
													<label for="remarks" class="control-label">Remarks</label>
													<div class="" style="padding: 3px"></div>
													<textarea rows="2" cols="" maxlength="200" id="remarks"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')"></textarea>

												</div>
												<div class="form-group col-md-4">
													<label for="fieldremark1" class="control-label">Field
														1 </label> <input type="text" class="form-control"
														id="fieldremark1" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="fieldremark2" class="control-label">Field
														2 </label> <input type="text" class="form-control"
														id="fieldremark2" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="fieldremark3" class="control-label">Field 3 </label> <input type="text" class="form-control"
														id="fieldremark3" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
                                                <div class="clearfix"></div>
											</div>
											<div id="attachment" class="tab-pane fade">
												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control" id="businessatch" maxlength="8" readonly onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
                                                <div class="form-group col-md-4">
                                                    <input type="file" id="fileInput" multiple>
                                                    <!--<button id="chooseFile" class="btn"><i class="icon-file"></i> Choose File</button>-->	
                                                </div>
                                                <div class="clearfix"></div>
											</div>
									</div>
								</div>
							</div>

									<div class="modal-footer clearfix">

										<div class="form-group">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartnergroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnerMasterSave" type="button"
												style="margin-right: auto;">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>


						<div class="country_info modal " id="businessgroupEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business
											Partner Group</h4>
									</div>
									<div class="modal-body" style="height: 250px">

										<div class="form-group col-md-4">
											<label for="businesspartnercode" class="control-label">Business
												Partner Code </label> <input type="text" class="form-control"
												id="businesspartnercodeEdit" maxlength="8"
												readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnername" class="control-label">Business
												Partner Name</label> <input type="text" class="form-control"
												id="businesspartnernameEdit" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnertypeId" class="control-label">Business
												Partner Type Id</label> <select class="form-control"
												id="businesspartnertypeIdEdit">
												<option>Vendor</option>
													<option>Customer</option>
													<option>Lead</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnergroupId" class="control-label">Business
												Partner Group </label> <select class="form-control"
												id="businesspartnergroupIdEdit">
													<option>Local</option>
													<option>External</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label for="businesspartnercurrencyCode"
												class="control-label">Business Partner Currency 
											</label> <select class="form-control"
												id="businesspartnercurrencyCodeEdit">
												<option>INR</option>
													<option>USD</option>
											</select>
										</div>
										<div class="form-group col-md-4">
											<label for="countrycode" class="control-label">Business
												Partner Image </label> <input type="file" class="form-control"
												id="businesspartnerimageEdit" onchange="readURL3(this);" />
										</div>
										<div class="form-group col-md-4">
											<img id="blah1Edit" src="../../../quadraerp/assets/images/avatars/def.png"
												alt="..." style="height: 72px;" />
										</div>




									</div>


									<div class="inner-tabs">
										<ul class="nav nav-tabs">
											<li><a data-toggle="tab" href="#generalEdit">General</a></li>
											<li><a data-toggle="tab" href="#salesEdit">Sales</a></li>
											<li><a data-toggle="tab" href="#accountEdit">Account</a></li>
											<li><a data-toggle="tab" href="#taxEdit">Tax</a></li>
											<li><a data-toggle="tab" href="#paymenttermEdit">PayMentTerm</a></li>
											<li><a data-toggle="tab" href="#remarksEdit">Remarks</a></li>
											<li><a data-toggle="tab" href="#attachmentEdit">Attachment</a></li>
										</ul>

										<div class="tab-content">
											<div id="generalEdit" class="tab-pane fade"
												style="height: auto; margin-bottom: 20px;">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#billtopartyEdit">Bill
															To Party</a></li>
													<li><a data-toggle="tab" href="#shiptopartyEdit">Shipped
															to party</a></li>
												</ul>
												<div class="tab-content">

													<div id="billtopartyEdit" class="tab-pane">
														<div class="scroll-modal" id="billparty">
															<div class="form-group col-md-4" hidden="true">
																<label class="control-label">Business Partner
																	Code</label> <input type="text" class="form-control"
																	id="addressIdBilltoparty" maxlength="8">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">House No</label> <input
																	type="text" class="form-control" id="houseNoEdit"
																	maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street</label> <input
																	type="text" class="form-control" id="streetEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 1</label> <input
																	type="text" class="form-control" id="street1Edit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 2</label> <input
																	type="text" class="form-control" id="street2Edit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Landmark</label> <input
																	type="text" class="form-control" id="landmarkEdit"
																	maxlength="50">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">City</label> <input
																	type="text" class="form-control" id="cityEdit"
																	maxlength="50">
															</div>
															<div class="form-group col-md-4">
																<label>Country</label> <select id="countryEdit"
																	class="form-control">
																	<option>India</option>
																	<option>Aus</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label>State</label> <select id="stateEdit"
																	class="form-control">
																	<option>Tamil Nadu</option>
																	<option>Karnataka</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Pin Code</label> <input
																	type="text" class="form-control" id="pincodeEdit"
																	maxlength="6"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Mobile Number</label> <input
																	type="text" class="form-control" id="mobileEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Number</label> <input
																	type="text" class="form-control" id="telephoneEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Extension</label>
																<input type="text" class="form-control"
																	id="teleExtenEdit" maxlength="4"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Email</label> <input
																	type="text" class="form-control" id="emailEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Web Site</label> <input
																	type="text" class="form-control" id="websiteEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Mobile</label> <input
																	type="text" class="form-control" id="altMobEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Telephone</label>
																<input type="text" class="form-control" id="altTelEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-12">
																<input type="checkbox" name="billtopartysameEdit"
																	id="billsameEdit"><b> Same As Bill To Party
																</b><br>
															</div>
														</div>
													</div>


													<div id="shiptopartyEdit" class="tab-pane">
														<div class="scroll-modal" id="shipparty">
															<div class="form-group col-md-4" hidden="true">
																<label class="control-label">Business Partner
																	Code</label> <input type="text" class="form-control"
																	readonly="readonly" id="addressShipEdit" maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">House No</label> <input
																	type="text" class="form-control" id="houseNoShipEdit"
																	maxlength="10">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street</label> <input
																	type="text" class="form-control" id="streetShipEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 1</label> <input
																	type="text" class="form-control" id="street1ShipEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Street 2</label> <input
																	type="text" class="form-control" id="street2ShipEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Landmark</label> <input
																	type="text" class="form-control" id="landmarkShipEdit"
																	maxlength="50">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">City</label> <input
																	type="text" class="form-control" id="cityShipEdit"
																	maxlength="50">
															</div>
															<div class="form-group col-md-4">
																<label>Country</label> <select id="countryShipEdit"
																	class="form-control">
																		<option>India</option>
																		<option>USA</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label>State</label> <select id="stateShipEdit"
																	class="form-control">
																		<option>Tamil Nadu</option>
																		<option>Karnataka</option>
																</select>
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Pin Code</label> <input
																	type="text" class="form-control" id="pincodeShipEdit"
																	maxlength="6"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Mobile Number</label> <input
																	type="text" class="form-control" id="mobileShipEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Number</label> <input
																	type="text" class="form-control" id="telephoneShipEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Telephone Extension</label>
																<input type="text" class="form-control"
																	id="teleExtenShipEdit" maxlength="4"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Email</label> <input
																	type="text" class="form-control" id="emailShipEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Web Site</label> <input
																	type="text" class="form-control" id="websiteShipEdit"
																	maxlength="100">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Mobile</label> <input
																	type="text" class="form-control" id="altMobShipEdit"
																	maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>
															<div class="form-group col-md-4">
																<label class="control-label">Alternate Telephone</label>
																<input type="text" class="form-control"
																	id="altTelShipEdit" maxlength="15"
																	onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
															</div>

														</div>
													</div>

												</div>
											</div>
											<div id="salesEdit" class="tab-pane fade"
												style="height: 55px">
												<ul class="nav nav-tabs">
													<li><a data-toggle="tab" href="#generalsEdit">General</a></li>
													<li><a data-toggle="tab" href="#branchEdit">Branch</a></li>
												</ul>
												<div class="tab-content">
													<div id="generalsEdit" class="tab-pane fade"
														style="height: 172px">
														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="branchId" maxlength="8">
														</div>
														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="EmployeeId" maxlength="8">
														</div>

														<div class="form-group col-md-4">
															<label for="salesgroupId" class="control-label">Sales
																group  </label> <select class="form-control"
																id="salesgroupIdEdit">
																	<option>Group 1</option>
																	<option>Group 2</option>
															</select>
														</div>
														<div class="form-group col-md-4">
															<label for="gradeId" class="control-label">Grade
																 </label> <select class="form-control" id="gradeIdEdit">
																 		<option>A</option>
																 		<option>B</option>
																 		<option>C</option>
															</select>
														</div>
														<div class="form-group col-md-4">
															<label for="field1" class="control-label">Field 1
															</label> <input type="text" class="form-control" id="field1Edit"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-4">
															<label for="field2" class="control-label">Field 2
															</label> <input type="text" class="form-control" id="field2Edit"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>
														<div class="form-group col-md-4">
															<label for="field3" class="control-label">Field 3
															</label> <input type="text" class="form-control" id="field3Edit"
																maxlength="20"
																onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
														</div>

													</div>

													<div id="branchEdit" class="tab-pane fade"
														style="height: 160px">

														<div class="form-group col-md-4" hidden="true">
															<label class="control-label">Business Partner
																Code</label> <input type="text" class="form-control"
																readonly="readonly" id="businesspartnercodebranchEdit"
																maxlength="10">
														</div>
														<!-- <div class="form-group col-md-4">
															<label for="branchCode" class="control-label">Branch
																Code </label> <select class="form-control" id="branchCodeEdit">

															</select>
														</div> -->

														<div class="form-group col-md-4">
															<label class="control-label">Branch Name Code</label> <input
																type="text" class="form-control" readonly
																id="branchCodeEdit">
														</div>
														<!-- <div class="form-group col-md-4">
															<label for="employeeCode" class="control-label">Employee
																Code </label> <select class="form-control" id="employeeCodeEdit">

															</select>
														</div> -->
														<div class="form-group col-md-4">
															<label class="control-label">Employee Name Code</label> <input
																type="text" class="form-control" readonly
																id="employeeCodeEdit">
														</div>
														<div class="form-group col-md-4">
															<label for="statusbranch" class="control-label">Status
															</label> <select class="form-control" id="statusbranchEdit">
																<option>Active</option>
																<option>In-Active</option>
															</select>
														</div>
														<div class="form-group col-md-4">
															<label for="divisioncode" class="control-label">Division
																Code </label> <select class="form-control" id="divisioncodeEdit">
																	<option>Carpentry</option>
																	<option>Metal</option>
															</select>
														</div>

													</div>
												</div>
											</div>


											<div id="accountEdit" class="tab-pane fade">
												<div class="scroll-modal">

													<div class="form-group col-md-4" hidden="true">
														<label class="control-label">Business Partner Code</label>
														<input type="text" class="form-control"
															readonly="readonly" id="businesspartnercodeaccountEdit"
															maxlength="8">
													</div>
													<div class="form-group col-md-4">
														<label for="paymentermcode" class="control-label">Payment
															Term </label> <select class="form-control"
															id="paymentermcodeEdit">
																<option>PS21</option>
																<option>PS45</option>
														</select>
													</div>

													<div class="form-group col-md-4">
														<label class="control-label">Interest On Arrears</label> <input
															type="text" class="form-control" id="interestonEdit"
															maxlength="6"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Dunning Term code</label> <input
															type="text" class="form-control" id="dunningtermEdit"
															maxlength="4"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Revenu Account</label> <input
															type="text" class="form-control" id="revenuaccountEdit"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Down Payment</label> <input
															type="text" class="form-control" id="downpaymentEdit"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Interim Account</label> <input
															type="text" class="form-control" id="interimaccountEdit"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label for="paymentmethodcode" class="control-label">Payment
															Method </label> <select class="form-control"
															id="paymentmethodcodeEdit">
															<option>Cash</option>
															<option>Cheque</option>
															<option>NEFT</option>
														</select>
													</div>
													<div class="form-group col-md-12">
														<input type="checkbox" name="ispaymentblockEdit"><b>
															Is Payment Block </b><br>
													</div>

													<div class="form-group col-md-4">
														<label for="field1" class="control-label">Field 1
														</label> <input type="text" class="form-control" id="fields1Edit"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label for="fields2" class="control-label">Field 2
														</label> <input type="text" class="form-control" id="fields2Edit"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label for="fields3" class="control-label">Field 3
														</label> <input type="text" class="form-control" id="fields3Edit"
															maxlength="20"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
												</div>
											</div>
											<div id="taxEdit" class="tab-pane fade">
												<div class="scroll-modal">
													<div class="form-group col-md-4" hidden="true">
														<label class="control-label">Business Partner Code</label>
														<input type="text" class="form-control"
															readonly="readonly" id="businesspartnercodetaxEdit"
															maxlength="8"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Pan Number</label> <input
															type="text" class="form-control" id="pannumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tin Number</label> <input
															type="text" class="form-control" id="tinnumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Service Tax Number</label> <input
															type="text" class="form-control"
															id="servicetaxnumberEdit" maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Tan Number</label> <input
															type="text" class="form-control" id="tannumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Cst Number</label> <input
															type="text" class="form-control" id="cstnumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Ist Number</label> <input
															type="text" class="form-control" id="istnumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>
													<div class="form-group col-md-4">
														<label class="control-label">Gst Number</label> <input
															type="text" class="form-control" id="gstnumberEdit"
															maxlength="15"
															onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
													</div>

													<div class="form-group col-md-4">
														<label for="gsttypecode" class="control-label">Gst
															Type  </label> <select class="form-control"
															id="gsttypecodeEdit">
																<option>Regular</option>
														</select>
													</div>

												</div>
											</div>
											<div id="paymenttermEdit" class="tab-pane fade"
												style="height: 200px">

												<div class="form-group col-md-4" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control"
														id="businesspaymentEdit" maxlength="8" readonly
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label for="countrycodepayment" class="control-label">Country
														 </label> <select class="form-control"
														id="countrycodepaymentEdit">
														<option>India</option>
														<option>USA</option>
													</select>
												</div>
												<!-- <div class="form-group col-md-4">
													<label for="bankcodepayment" class="control-label">Bank
														Code </label> <select class="form-control"
														id="bankcodepaymentEdit">

													</select>
												</div> -->
												<div class="form-group col-md-4">
													<label class="control-label">Bank Name</label> <input
														type="text" class="form-control" id="bankcodepaymentEdit"
														readonly="readonly">
												</div>
												<div class="form-group col-md-4" hidden="true">
													<label class="control-label">Bank Name</label> <input
														type="text" class="form-control" id="bankIdpaymentEdit"
														readonly="readonly">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Ifsc Code</label> <input
														type="text" class="form-control" id="ifsccodeEdit"
														maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Bank Branch Name</label> <input
														type="text" class="form-control" id="bankbranchnameEdit"
														maxlength="35"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label class="control-label">Bank Holder Name</label> <input
														type="text" class="form-control" id="bankholdernameEdit"
														maxlength="50"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>

												<!-- <div class="form-group col-md-4">
													<label class="control-label">Bank Account Number</label> <input
														type="text" class="form-control" id="bankaccountnoEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div> -->
												<div class="form-group col-md-4">
													<label class="control-label">Bank Account Number</label> <input
														type="text" class="form-control" id="bankaccountnoEdit"
														readonly="readonly">
												</div>

											</div>

											<div id="remarksEdit" class="tab-pane fade "
												style="height: 200px">

												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control" readonly
														id="businessremarkEdit" maxlength="8"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												<div class="form-group col-md-12">
													<label for="remarks" class="control-label">Remarks</label>
													<textarea rows="2" cols="" maxlength="200" id="remarksEdit"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')"></textarea>

												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark1" class="control-label">Field
														1 </label> <input type="text" class="form-control"
														id="fieldremark1Edit" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark2" class="control-label">Field
														2 </label> <input type="text" class="form-control"
														id="fieldremark2Edit" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>

												<div class="form-group col-md-4">
													<label for="fieldremark3" class="control-label">Field
														3 </label> <input type="text" class="form-control"
														id="fieldremark3Edit" maxlength="20"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
											</div>
											<div id="attachmentEdit" class="tab-pane fade"
												style="height: 150px">

												<div class="form-group col-md-6" hidden="true">
													<label class="control-label">Business Partner Code</label>
													<input type="text" class="form-control"
														id="businessatchEdit" maxlength="8" readonly
														onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
												</div>
												  <div class="form-group col-md-6">
												<label>Upload Resume(.doc,.docx,.pdf)</label> <input
													type="file" id="fileInput">
												<button id="chooseFile" class="btn">
													<i class="icon-file"></i> Choose File
												</button>

												
												
											</div>
										</div>

									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="BusinesspartneMasterUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<style>
			.modal-dialog{
				width: 75% !important;
			}
		</style>
        <script>
function add_ship()
{
	var txt =  '<div id="append_ship"><h4>Shipping Address</h4><div class="col-md-6 "><form class="form-horizontal"><div class="form-group col-md-12"><label class="control-label col-md-3">House No</label><div class="col-md-9"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Street1</label><div class="col-md-9"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Street2</label><div class="col-md-9"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Landmark</label><div class="col-md-9"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Country</label><div class="col-md-9"><select class=" form-control select2" name="cityid"><option value="select country">Select Country</option><option>India</option><option>America</option></select></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">State</label><div class="col-md-9"><select class=" form-control select2" name="cityid"><option value="select country">Select State</option><option>Tamil Nadu</option><option>Karnataka</option></select></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">City</label><div class="col-md-9"><select class=" form-control select2" name="cityid"><option value="select country">Select City</option><option>Chennai</option><option>Madurai</option></select></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Area</label><div class="col-md-9"><select class=" form-control select2" name="cityid"><option value="select country">Select Area</option><option>Guindy</option><option>Adayar</option></select></div></div><div class="form-group col-md-12"><label class="control-label col-md-3">Pin Code</label><div class="col-md-9"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div></form></div><div class="col-md-6"><form class="form-horizontal"><div class="form-group col-md-12"><label class="control-label col-md-4">Mobile Number</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Telephone Number</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Telephone Extension</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Email</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Web Site</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Alternate Mobile</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Alternate Telephone</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><label class="control-label col-md-4">Contact Person</label><div class="col-md-8"><input type="text" class="form-control" id="houseNo" maxlength="10"></div></div><div class="form-group col-md-12"><div class="col-md-5"></div><button type="button" class="btn btn-success">Save</button></div></form></div></div>';

	$("#append_ship").html(txt);
	
}
function delete_contact(row, id,)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}
		</script>