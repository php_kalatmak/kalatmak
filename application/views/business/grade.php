
<script type="text/javascript">
	$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-grade').addClass('active');


    $('#grade').hide();
    $('#gradeEdit').hide();

    $('#gradeAdd').click(function () {
        $('#grade').show();
    });

    $('#grade_Reset').click(function () {
        $('#gradeid').val('');
        $('#gradename').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });
});
</script>

<section class="content">


			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Business Partner</a></li>
					<li class="active">Grade</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Grade</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="gradeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="gradeId">Grade Id</th>
														<th data-field="gradedesc">Grade Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>GradeID001</td>
													<td>A</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>GradeID002</td>
													<td>B</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>GradeID003</td>
													<td>C</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Grade</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="gradeid" class="control-label">Grade Id
												</label> <input type="text" class="form-control" readonly name="Grade Id"
												id="gradeid"  maxlength="2"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="gradename" class="control-label">Grade
												Name</label> <input type="text" class="form-control"
												id="gradename" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="grade_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="gradeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="grade" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Grade</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="gradeid" class="control-label">Grade Id
												</label> <input type="text" class="form-control"
												id="gradeid"  maxlength="2"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="gradename" class="control-label">Grade
												Name</label> <input type="text" class="form-control"
												id="gradename" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="grade_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="gradeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="gradeEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Grade</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<!-- 
										<div class="form-group col-md-6">
											<label for="gradeIdEdit" class="control-label">Grade Id
												Id</label> <select class="form-control"
												id="gradeIdEdit">
												
												</select>
										</div> -->
										<div class="form-group col-md-6">
											<label for="gradeIdEdit" class="control-label">Grade Id
												</label> <input type="text" class="form-control"
												id="gradeIdEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="gradenameEdit" class="control-label">Grade Name
												Name</label> <input type="text" class="form-control"
												id="gradenameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="gradeUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>