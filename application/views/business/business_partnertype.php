
<script type="text/javascript">
	$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-bnusinessPartnerType').addClass('active');
    $('#businesssave').hide();
    $('#businessEdit').hide();

    $('#businessAdd').click(function () {
        $('#businesssave').show();
    });

    $('#businesspartner_Reset').click(function () {
        $('#businesspartnerId').val('');
        $('#businesspartnername').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Business Partner</a></li>
					<li class="active">BusinessPartnerType</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Business Partner Type</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="businessAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="bptypeId">Business Partner Id</th>
														<th data-field="bptypeDesc">Business Partner Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>PartID001</td>
													<td>Vendor</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartID002</td>
													<td>Customer</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartID003</td>
													<td>Lead</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Type</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="businesspartnerId" class="control-label">Business Partner Id
												</label> <input type="text" class="form-control" readonly name="Business Partner Id"
												id="businesspartnerId"  maxlength="1"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner
												Name</label> <input type="text" class="form-control"
												id="businesspartnername" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartner_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnerSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="businesssave" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Type</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label for="businesspartnerId" class="control-label">Business Partner Id
												</label> <input type="text" class="form-control"
												id="businesspartnerId"  maxlength="1"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner
												Name</label> <input type="text" class="form-control"
												id="businesspartnername" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartner_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnerSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="businessEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

									<!-- 	
										<div class="form-group col-md-6">
											<label for="businesspartnerId" class="control-label">Business Partner
												Id</label> <select class="form-control"
												id="businesspartnerIdEdit">
												
												</select>
										</div>
										 -->
										 <div class="form-group col-md-6">
											<label for="businesspartnerId" class="control-label">Business Partner Id
												</label> <input type="text" class="form-control"
												id="businesspartnerIdEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="gradenameEdit" class="control-label">Business Partner
												Name</label> <input type="text" class="form-control"
												id="businesspartnernameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="BusinesspartnerUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>