
<script type="text/javascript">

	$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-businessPartnergroup').addClass('active');
    $('#businessgroupsave').hide();
    $('#businessgroupEdit').hide();

    $('#businessgroupAdd').click(function () {
        $('#businessgroupsave').show();
    });

    $('#businesspartnergroup_Reset').click(function () {
        $('#businesspartnergroupId').val('');
        $('#businesspartnergroupname').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });

});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Business Partner</a></li>
					<li class="active">BusinessPartnerGroup</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Business Partner Group</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="businessgroupAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="businessgroupId">Business Partner Type</th>
														<th data-field="businessgroupId">Business Partner Type</th>
														<th data-field="businessgroupId">Business Partner Group Id</th>
														<th data-field="businessgroupDesc">Business Partner Group Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>PartTypeID001</td>
													<td>Vendor</td>
													<td>PartGroupID001</td>
													<td>Local</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartTypeID002</td>
													<td>Vendor</td>
													<td>PartGroupID002</td>
													<td>External</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartTypeID003</td>
													<td>Customer</td>
													<td>PartGroupID003</td>
													<td>Export</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>PartTypeID004</td>
													<td>Customer</td>
													<td>PartGroupID004</td>
													<td>Dealer</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Group</h4>
									</div>
									<div class="modal-body" style="height: 50px">

										<div class="form-group col-md-6">
											<label class="control-label">Business Partner Type</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Type</option>
											<option>Vendor</option>
											<option>Customer</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner Group
												ID</label> <input type="text" class="form-control"
												id="businesspartnergroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner Group
												Name</label> <input type="text" class="form-control"
												id="businesspartnergroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartnergroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnergroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="businessgroupsave" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Group</h4>
									</div>
									<div class="modal-body" style="height: 50px">
										<div class="row">
										<div class="form-group col-md-6">
											<label class="control-label">Business Partner Type</label> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Business Partner Type</option>
											<option>Vendor</option>
											<option>Customer</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner Group
												ID</label> <input type="text" class="form-control"
												id="businesspartnergroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-6">
											<label for="businesspartnername" class="control-label">Business Partner Group
												Name</label> <input type="text" class="form-control"
												id="businesspartnergroupname" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="businesspartnergroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="businesspartnergroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="businessgroupEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Business Partner Group</h4>
									</div>
									<div class="modal-body" style="height: 150px">

									<!-- 	
										<div class="form-group col-md-6">
											<label for="businesspartnergroupId" class="control-label">Business Partner group
												Id</label> <select class="form-control"
												id="businesspartnergroupIdEdit">
												
												</select>
										</div> -->
										<div class="form-group col-md-6">
											<label for="businesspartnergroupId" class="control-label">Business Partner group
												</label> <input type="text" class="form-control"
												id="businesspartnergroupIdEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="businesspartnergroupname" class="control-label">Business Partner Group
												Name</label> <input type="text" class="form-control"
												id="businesspartnergroupnameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="BusinesspartnerGroupUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>