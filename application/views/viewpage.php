<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title><?=(@$meta_title)?></title>
       
        <?=$header; ?>
    </head>
    <body>
        <!--Top Navigation-->
    
        <!--Sidebar-->
        <?=(isset($sidebar)) ? $sidebar : ''; ?>

        <!--Content-->
     
        <?=(isset($content)) ? $content : ''; ?>

        <!--Footer-->
        <?=$footer; ?>
        <!--Page Specific Footer-->
        <?php if (isset($extra_footer)) echo $extra_footer; ?>
    </body>
</html>