<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">AP DownPayment</h4>
</div>
<div class="modal-body">
	<div class="panel-body row">
									<div class="form-group" id="order_type">
									<div class="col-md-2 ">
										<button class="form-control btn btn-lg btn-success" onclick="myFunction()">Batch</button>
									</div>
									<div class="col-md-2 ">
										<button class="form-control btn btn-lg btn-success" onclick="myFunction2()">Serial</button>
									</div>
									</div>
									<div style="padding: 20px"></div>
									<div id="batch">
										<div class="tab-pane active" id="1">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<h3>Batch</h3>
														<h5>Row From Document</h5>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Doc.No</th>
																	<th>Item Code</th>
																	<th>Description</th>
																	<th>Branch Code</th>
																	<th>Storage Location</th>
																	<th>Bin Location</th>
																	<th>Total Need</th>
																	<th>Total Created</th>
																</tr>
															</thead>
															<tbody id="Batch">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Doc.No" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Item Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Description" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Branch Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Storage Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Need" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Created" readonly="">
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-10 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Batch</th>
																	<th>Qty</th>
																	<th>Mfr Date</th>
																	<th>Expiry Date</th>
																	<th>Unit Cost</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="cost_tab">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Batch">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Qty">
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Mfr Date" >
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Expiry Date">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-2 ">
														<a href="#" class="form-control btn btn-sm btn-primary" onclick="addmore()">Add More</a> 
													</div>
												</div>
											</div>
										</div>
									<div id="serial">
										<div class="tab-pane active" id="1">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<h3>Serial</h3>
														<h5>Row From Document</h5>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Doc.No</th>
																	<th>Item Code</th>
																	<th>Description</th>
																	<th>Branch Code</th>
																	<th>Storage Location</th>
																	<th>Bin Location</th>
																	<th>Total Need</th>
																	<th>Total Created</th>
																</tr>
															</thead>
															<tbody id="Serial">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Doc.No" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Item Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Description" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Branch Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Storage Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Need" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Created" readonly="">
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-10 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Mfr.Serial</th>
																	<th>Serial Number</th>
																	<th>Lot Number</th>
																	<th>Qty</th>
																	<th>Mfr Date</th>
																	<th>Expiry Date</th>
																	<th>Unit Cost</th>
																	<th>Warranty Start</th>
																	<th>Warranty End</th>
																	<th>delete</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Mfr.Serial">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Serial Number">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Lot Number">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Oty">
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Mfr Date" >
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Expiry Date">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Warranty Start" >
																</td>
																<td>
																	<input type="Date" name="" class="form-control  col-sm-1" placeholder="Warranty End">
																</td>
																<td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-2 ">
														<a href="#" class="form-control btn btn-sm btn-primary" onclick="addrow()">Add More</a> 
													</div>
												</div>
											</div>
										</div>
								</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>


<script>


function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Batch"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Qty"></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Mfr Date"></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Expiry Date"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly=""></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#cost_tab").append(txt);

}

function addrow(){
var txt = '<tr><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Mfr.Serial"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Serial Number"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Lot Number"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Oty"></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Mfr Date"></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Expiry Date"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly=""></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Warranty Start"></td><td><input type="Date" name="" class="form-control  col-sm-1" placeholder="Warranty End"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);
}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}

$("#batch").hide();
$("#serial").hide();

function myFunction() {
  var x = document.getElementById("batch");
  if (x.style.display === "none") {
    x.style.display = "block";
    $("#serial").hide();
  } else {
    x.style.display = "";
    $("#serial").hide();
  }
}


function myFunction2() {
  var x = document.getElementById("serial");
  if (x.style.display === "none") {
    x.style.display = "block";
      $("#batch").hide();
  } else {
    x.style.display = "";
    $("#batch").hide();
  }
}

</script>