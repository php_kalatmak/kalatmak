<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-apdownpayment').addClass('active');
	$('#menu-apdownpayment > a').attr('aria-expanded', 'true');
	$('#menu-apdownpayment > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">AP Downpayment</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">AP Downpayment</div>
								<div class="panel-body">
									<div class="form-group">
										<form class="form-vertical">
											<div class="row">
											    <div class="col-md-3">
											    	<label class="control-label">Company</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Order Type</option>
															<option>Conpany1</option>
															<option>Company2</option>
															</select>
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label">Branch</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Branch</option>
															<option>Branch1</option>
															<option>Branch2</option>
															</select>
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label">Transaction Type</label> 
															<select class=" form-control select2" name="cityid" id="order_type">
															<option value="select country">Select Transaction Type</option>
															<option value="check">Check</option>
															<option value="bank">Bank Transfer</option>
															<option value="cash">Cash</option>
															</select>
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Posting Date</label>
											    	<input type="date" class="form-control" id="">
											    </div> 
										</div>
										
											<div class="form-group col-lg-8">											    
											</div>										
										</form>
									</div>
									<br clear="left">
									<div style="padding: 20px"></div>
									<div class="tab-content">
									<ul class="nav nav-tabs" id="tabs">
										<li><a data-toggle="tab" href="#check">Check</a></li>
										<li><a data-toggle="tab" href="#bank">Bank Transfer</a></li>
										<li><a data-toggle="tab" href="#cash">Cash</a></li>
									</ul>
									
										   <div class="tab-pane active" id="check">
										<div class="form-group" >
											<h3>Check</h3>
										<div class="row">
										<div class="col-md-3"> 
												    	<label class="control-label" for="fdate">Check Date</label>
												    	<input type="date" class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label for="comment">Amount</label>
	      									<input type="text"class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label class="control-label">Country</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																<option>China</option>
																</select>
										</div>
										<div class="col-md-3">
											<label class="control-label">House Bank Name</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bank Name</option>
																<option>Name1</option>
																<option>Name2</option>
																<option>Name3</option>
																</select>
										</div>
									    </div>
									    <div class="row">
										<div class="col-md-3">
											<label for="comment">Branch</label>
	      									<input type="text"class="form-control" id="" readonly="">
										</div>
										<div class="col-md-3">
											<label for="comment">Account No.</label>
	      									<input type="text"class="form-control" id="" readonly="">
										</div>
										<div class="col-md-3 checkbox">
										  <label><input type="checkbox" value="">Manual</label>
										</div>
										<div class="col-md-3">
											<label for="comment">Check No.</label>
	      									<input type="text"class="form-control" id="">
										</div>
										</div>
										<div class="row">
										<div class="col-md-3">
											<label for="comment">GL Account No.</label>
	      									<input type="text"class="form-control" id="" readonly="">
										</div>
										<div class="col-md-3">
											<label for="comment">Remarks</label>
	      									<input type="text"class="form-control" id="" readonly="">
										</div>
										<div class="col-md-3">
											<label for="comment">Total</label>
	      									<input type="text"class="form-control" id="" readonly="">
										</div>
										</div>
									    </div>
									</div>
									     <div class="tab-pane " id="bank">
										<div class="form-group">
											<h3>Bank Transfer</h3>
										<div class="row">
										<div class="col-md-3">
											<label class="control-label">GL Account</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																<option>China</option>
																</select>
										</div>
										<div class="col-md-3"> 
												    	<label class="control-label" for="fdate">Transfer Amount</label>
												    	<input type="text" class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label for="comment">Transfer Ref No.</label>
	      									<input type="text"class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label for="comment">Remarks</label>
	      									<input type="text"class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label for="comment">Total</label>
	      									<input type="text"class="form-control" id="">
										</div>
									    </div>
										</div>
									</div>
										<div class="tab-pane " id="cash">
										<div class="form-group" >
											<h3>Cash</h3>
										<div class="row">
										<div class="col-md-3">
											<label class="control-label">GL Account</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																<option>China</option>
																</select>
										</div>
										<div class="col-md-3"> 
												    	<label class="control-label" for="fdate">Amount</label>
												    	<input type="text" class="form-control" id="">
										</div>
										<div class="col-md-3">
											<label for="comment">Total</label>
	      									<input type="text"class="form-control" id="">
										</div>
									    </div>
										</div>
									</div>
								</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>

<!-- <script>
	$("#check").show();
	$("#bank").show();
	$("#cash").show();
	$("#order_type").on("change",function(){
		var type = $("#order_type").val();
		if(type == 'check'){
			$("#check").show();
			$("#bank").hide();
			$("#cash").hide();
		} else if(type == 'bank'){
			$("#check").hide();
			$("#bank").show();
			$("#cash").hide();
		}else if(type == 'cash'){
			$("#check").hide();
			$("#bank").hide();
			$("#cash").show();
		}
	});
	</script>
 -->