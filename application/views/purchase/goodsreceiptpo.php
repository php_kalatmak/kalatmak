<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-goodsreceiptpo').addClass('active');
	$('#menu-goodsreceiptpo > a').attr('aria-expanded', 'true');
	$('#menu-goodsreceiptpo > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">Goods Receipt PO</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Goods Receipt PO</div>
									<div class="panel-body">



									<div class="form-group">
										<form class="form-vetical">
											<div class="row">
										   		 <div class="col-md-3">
										    		<label class="control-label">Order Type</label> 
														<select class=" form-control select2" name="cityid" id="order_type">
														<option value="material">Item</option>
														<option value="sale">Service</option>
														</select>
									    		</div>	
									    		<div class="col-md-3"> 
										    		<label class="control-label">Company</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Company</option>
														<option>Quadraerp</option>
														</select>
										   		 </div>						    
											    <div class="col-md-2"> 
											    	<label class="control-label" for="todate">Document Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>									    
											    <div class="col-md-2"> 
										    		<label class="control-label">Status</label> 
														<select class=" form-control select2" name="cityid">
															<option value="select country">Select Status</option>
															<option>Active</option>
															<option>Inactive</option>
														</select>
											    </div>
											    <div class="col-md-2">
													<button type="button" class="form-control btn btn-lg btn-success" data-toggle="modal" data-target="#myModal">AP_ DownPayment</button>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3"> 
											    	<label class="control-label" for="itmqty">Document Number</label>
													<input type="text" name="" class="form-control" placeholder="Document Number">
											    </div>	
											    <div class="col-md-3"> 
										    		<label class="control-label">Factory/Branch</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Factory/Branch</option>
														<option>Factory</option>
														<option>Branch</option>
														</select>
										   	 	</div>	
										   	 	<div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Delivery Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>	
											    <div class="col-md-2"> 
										    		<label class="control-label">Ship To</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Ship To</option>
														<option>Type1</option>
														<option>Type2</option>
														</select>
											    </div>	
											    <div class="col-md-2">
													<button type="button" class="form-control btn btn-lg btn-success" data-toggle="modal" data-target="#myModal1">GRPO batch & Serial</button>
												</div>	
										    </div>
										    <div class="row">										    
											    <div class="col-md-3">
										    	      <label class="control-label">Ref Type</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Ref Type</option>
														<option>Puchase Order</option>
														</select>
								          		</div>
								          		<div class="col-md-3"> 
										    		<label class="control-label">Vendor</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Vendor</option>
														<option>Panit</option>
														<option>Fevicol</option>
														</select>
										    	</div>
										    	<div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Post Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											    <div class="col-md-2">
											    </div>		
											</div>
											<div class="row">
												<div class=" col-md-3 ">
											    	<label class="control-label" for="itmqty">Ref Number</label>
													<input type="text" name="" class="form-control" placeholder="Ref Number">
											    </div>								    
										    	<div class="col-md-3"> 
										    		<label class="control-label">Currency</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Currency</option>
														<option>INR</option>
														<option>USD</option>
														</select>
										    	</div>
											</div>
										</form>
										</div>
										<!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
											<div class="modal-dialog">

											<!-- Modal content-->
												<div class="modal-content">
													<?php
														include 'ap_downpayment.php';
													?>
													</div>

												</div>
										</div>
										<div class="form-group col-md-2">
											<div id="myModal1" class="modal fade" role="dialog">
												<div class="modal-dialog">

												<!-- Modal content-->
													<div class="modal-content">
														<?php
															include 'grpo.php';
														?>
													</div>

												</div>
											</div>
										</div>
										
									


									<div style="padding: 5px"></div>
											<div id="Material">
														<div class="row">
															<div class="col-md-1 ">
																	<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore10()">Add</button> 
															<div style="padding: 5px"></div>
														</div>
													</div>
												<div class="row" style="margin: 5px">
													<div class="table-responsive">
														<table class="table table-bordered" >
															<thead>
																<tr>
																	<th>Bar_Code</th>
																	<th>Item_Code</th>
																	<th>Description</th>
																	<th>Oty</th>
																	<th>UOM</th>
																	<th>Price</th>
																	<th>Discount</th>
																	<th>Frieght</th>
																	<th>Tax</th>
																	<th>Withholding_Tax</th>
																	<th>Before_Tax</th>
																	<th>Gross_Total</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Referance_Type</th>
																	<th>Ref.No</th>
																	<th>Cost_Center</th>
																	<th>Storage_Location</th>
																	<th>Pin_Location</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab10">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="Bar Code" style="width: 100px;">
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Qty" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Price" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Frieght" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Tax" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Before Tax" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gross Total" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref No" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost Centre" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Storage Location" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>

											<div id="Service">
														<div class="row">
															<div class="col-md-1 ">
																	<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore1()">Add</button> 
															<div style="padding: 5px"></div>
															</div>
														</div>
												<div class="row" style="margin: 5px">
													<div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>SAC</th>
																	<th>Gl_Account</th>
																	<th>Gl_Name</th>
																	<th>Description</th>
																	<th>Amount</th>
																	<th>Discount</th>
																	<th>TDS</th>
																	<th>Frieght</th>
																	<th>Tax</th>
																	<th>Withholding_Tax</th>
																	<th>Before_Tax</th>
																	<th>Gross_Total</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Reference_Type</th>
																	<th>Ref.No</th>
																	<th>Cost_Center</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab1">
																<tr>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gl Name" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Amount" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="TDS" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Frieght" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Tax" readonly="" >
																	</td>
																	<td>
																		<select class="form-control ">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Before Tax" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gross Total" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " disabled="" >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref No" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost Center" readonly="" >
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>	


									<div style="padding: 5px"></div>
									<div class="form-group">
									<div class="col-md-4">
										<label for="comment">Remarks</label>
										<div style="padding: 3px"></div>
      									<textarea class="textarea form-control" style="height: 120px!important"  id="comment"></textarea>
									</div>
									<div class="col-md-2">
									</div>
									<div class="col-md-6">
										<form class="form-horizontal" action="">
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Total Before Discount:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Discount:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Frieght:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Withholding Tax:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Tax:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Total Payment Due:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <br>
										  <div class="form-group">
										  <div class="row">
										  	<div class=" col-sm-12" style="text-align: right; padding-right: 30px;">
										    	<button type="submit" class="btn btn-default">Copy From</button>
										      <button type="submit" class="btn btn-default">Copy To</button>
										    </div>
										    </div> 
										  </div>
										</form>
										</div>
										<div class="form-group">
											<div class="row col-md-4">
											</div>
										  	<div class="row col-md-1">
										  		<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px">Save</button>
										  	</div>
										</div>
									</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>
	$("#Material").show();
	$("#Service").hide();
	$("#order_type").on("change",function(){
		var type = $("#order_type").val();
		if(type == 'material'){
			$("#Material").show();
			$("#Service").hide();
		} else if(type == 'sale'){
			$("#Material").hide();
			$("#Service").show();
		}
	});


	function addmore10(){
var txt = '<tr><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Bar Code" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Description" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Qty" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Price" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Discount" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Frieght" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Tax" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Before Tax" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Gross Total" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Total" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Ref No" ></td><td><input type="text" name="" class="form-control" placeholder="Cost Centre" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Storage Location" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab10").append(txt);

}

function addmore1(){
var txt = '<tr><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Gl Name" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Description" ></td><td><input type="text" name="" class="form-control" placeholder="Amount" ></td><td><input type="text" name="" class="form-control" placeholder="Discount" ></td><td><input type="text" name="" class="form-control" placeholder="TDS" ></td><td><input type="text" name="" class="form-control" placeholder="Frieght" ></td><td><input type="text" name="" class="form-control" placeholder="Tax" readonly="" ></td><td><select class="form-control "><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Before Tax" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Gross Total" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Total" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " disabled="" ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Ref No" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Cost Center" readonly="" ></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab1").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}


	</script>