<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">AP DownPayment</h4>
</div>
<div class="modal-body">
	<div class="panel-body row">
									<div class="form-group" id="order_type">
									<div class="col-md-2 ">
										<button class="form-control btn btn-lg btn-success" onclick="myFunction1()">Serial Number</button>
									</div>
									<div class="col-md-2 ">
										<button class="form-control btn btn-lg btn-success" onclick="myFunction2()">Batch</button>
									</div>
									</div>
									<div style="padding: 20px"></div>
									<div id="batch">
										<div class="tab-pane active" id="1">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<h3>Serial Number</h3>
														<h5>Row From Document</h5>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Doc.No</th>
																	<th>Item Code</th>
																	<th>Description</th>
																	<th>Branch Code</th>
																	<th>Storage Location</th>
																	<th>Bin Location</th>
																	<th>Total Need</th>
																	<th>Out</th>
																</tr>
															</thead>
															<tbody id="Batch">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Doc.No" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Item Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Description" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Branch Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Storage Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Need" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Out" readonly="">
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-6 table-responsive">
														<h3>Available Serial Number</h3>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Bin Location</th>
																	<th>Mfg.Serial</th>
																	<th>Mfr Date</th>
																	<th>Serial Number</th>
																	<th>Unit Cost</th>
																	<!-- <th>Delete</th> -->
																</tr>
															</thead>
															<tbody id="cost_tab">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Mfg.Serial" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Mfr Date" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Serial Number" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<!-- <td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td> -->
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-6 table-responsive">
														<h3>Selected Serial Number</h3>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Bin Location</th>
																	<th>Mfg.Serial</th>
																	<th>Mfr Date</th>
																	<th>Serial Number</th>
																	<th>Unit Cost</th>
																	<!-- <th>Delete</th> -->
																</tr>
															</thead>
															<tbody id="cost_tab">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Mfg.Serial" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Mfr Date" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Serial Number" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<!-- <td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td> -->
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									<div id="serial">
										<div class="tab-pane active" id="1">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<h3>Batch</h3>
														<h5>Row From Document</h5>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Doc.No</th>
																	<th>Item Code</th>
																	<th>Description</th>
																	<th>Branch Code</th>
																	<th>Storage Location</th>
																	<th>Bin Location</th>
																	<th>Total Need</th>
																	<th>Out</th>
																</tr>
															</thead>
															<tbody id="Serial">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Doc.No" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Item Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Description" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Branch Code" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Storage Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Bin Location" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Total Need" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Out" readonly="">
																</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-6 table-responsive">
														<h3>Available Batch</h3>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Batch</th>
																	<th>Available Qty</th>
																	<th>Selected Oty</th>
																	<th>Unit Cost</th>
																	<!-- <th>delete</th> -->
																</tr>
															</thead>
															<tbody id="service_tab1">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Batch" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Available Qty" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Selected Oty">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<!-- <td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td> -->
																</tr>
															</tbody>
														</table>
													</div>
													<div class="col-md-6 table-responsive">
														<h3>Selected Batch</h3>
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Batch</th>
																	<th>Available Qty</th>
																	<th>Selected Oty</th>
																	<th>Unit Cost</th>
																	<!-- <th>delete</th> -->
																</tr>
															</thead>
															<tbody id="service_tab333">
																<tr>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Batch" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Available Qty" readonly="">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Selected Oty">
																</td>
																<td>
																	<input type="text" name="" class="form-control  col-sm-1" placeholder="Unit Cost" readonly="">
																</td>
																<!-- <td>
																	<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																</td> -->
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
								</div>
</div>

<script>

$("#batch").hide();
$("#serial").hide();

function myFunction1() {
  var x = document.getElementById("batch");
  if (x.style.display === "none") {
    x.style.display = "block";
    $("#serial").hide();
  } else {
    x.style.display = "";
    $("#serial").hide();
  }
}


function myFunction2() {
  var x = document.getElementById("serial");
  if (x.style.display === "none") {
    x.style.display = "block";
      $("#batch").hide();
  } else {
    x.style.display = "";
    $("#batch").hide();
  }
}

</script>
	