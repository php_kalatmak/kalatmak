<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-purchaserequest').addClass('active');
	$('#menu-purchaserequest > a').attr('aria-expanded', 'true');
	$('#menu-purchaserequest > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">Purchase Request</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Purchase Request</div>
								<div class="panel-body">


								<form data-parsley-validate="">
									<div class="form-group">
										<form class="form-vertical" data-parsley-validate="">
											<div class="row">
											    <div class="col-md-3">
											    	<label class="control-label">Order Type</label> 
															<select class=" form-control select2" name="cityid" required="" id="order_type">
															<option value="material">Item</option>
															<option value="sale">Service</option>
															</select>
											    </div>
											    <div class="col-md-3"> 
									    		<label class="control-label">Company Code</label> 
													<select class=" form-control select2" required="" name="cityid">
													<option value="">Select Company Code</option>
													<option>Code1</option>
													<option>Code2</option>
													</select>
										    	</div>	
										    	<div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Doc Date:</label>
											    	<input type="date" class="form-control" id="" required="" data-parsley-palindrome="" >
											    </div>
											    <div class="col-md-2"> 
											    	<label class="control-label" for="todate">Delivery Date:</label>
											    	<input type="date" class="form-control" id="">
											    </div>	
											    <div class="col-md-2"> 
											    	<label class="control-label">Ship To</label> 
															<select class=" form-control select2" name="cityid" required="">
															<option value="select country">Select Ship To</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
											    </div>
											</div>
										<div class="row">
												<div class="col-md-3"> 
											    	<label class="control-label" for="itmqty">Document Number</label>
													<input type="text" name="" class="form-control" placeholder="Document Number" readonly="" >
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label">Factory/Branch</label> 
															<select class=" form-control select2" name="cityid" required="">
															<option value="select country">Select Request Type</option>
															<option>Factory</option>
															<option>Branch</option>
															</select>
										    	</div>
										    	<div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Posting Date</label>
											    	<input type="date" class="form-control" id="" required="" data-parsley-palindrome="">
										    	</div>
										    	<div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Valid Date</label>
											    	<input type="date" class="form-control" id="" required="" data-parsley-palindrome="">
											    </div>	
											    
											    <div class="col-md-2"> 
											    	<label class="control-label">Status</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Status</option>
															<option>Draft</option>
															<option>Open</option>
															<option>Close</option>
															</select>
											    </div>
											</div>									
										</form>
									</div>
									<div style="padding: 5px"></div>
									<div id="tab">
										<div class="tab-content">
											<div class="" id="Material">
												<div class="row">
													<div class="col-md-1 ">
														<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore()">Add</button> 
													<div style="padding: 5px"></div>
													</div>
												</div>
												<div class="row" style="margin: 5px">
													<div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Bar_Code</th>
																	<th>Item_Code</th>
																	<th>Description</th>
																	<th>Delivery Date</th>
																	<th>Vendor</th>
																	<th>Qty</th>
																	<th>UOM</th>
																	<th>Price</th>
																	<th>Discount</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Ref. Type</th>
																	<th>Ref.No</th>
																	<th>Cost_Center</th>
																	<th>Storage_Location</th>
																	<th>Pin_Location</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control " placeholder="Bar Code" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Item Code" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description"  readonly="">
																	</td>
																	<td>
																		<input type="date" class="form-control" name="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control"  placeholder="Qty" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="UOM" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Price" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref.No" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost Center" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Storage Location" readonly="" 	>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="" id="Service">
												<div class="row">
												<div class="col-md-1 ">
													<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore1()">Add</button> 
												</div>
												</div>
												<div style="padding: 5px"></div>
												<div class="row" style="margin: 5px">
													<div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>SAC</th>
																	<th>Gl Account</th>
																	<th>GL Name</th>
																	<th>Description</th>
																	<th>Delivery Date</th>
																	<th>Vendor</th>
																	<th>Amount</th>
																	<th>Discount</th>
																	<th>TDS</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Ref. Type</th>
																	<th>Ref. No</th>
																	<th>Cost Center</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab1">
																<tr>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="GL Name" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description" >
																	</td>
																	<td>
																		<input type="date" class="form-control" name="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Amount" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="TDS" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="" >
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref" readonly="" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost" readonly="" >
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>		
										</div>
									</div>
									<div style="padding: 5px"></div>
									<div class="form-group">
									<div class="col-md-4 ">
										<label for="comment">Remarks</label>
										<div style="padding: 3px"></div>
      									<textarea class="textarea form-control" style="height: 120px!important"  id="comment"></textarea>
									</div>
									<div class="col-md-2">
									</div>
									<div class="col-md-6">
										<form class="form-horizontal" action="">
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Total Before Discount:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Discount:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Frieght:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Tax:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="email">Total Payment Due:</label>
										    <div class="col-sm-8">
										      <input type="email" class="form-control" id="email" placeholder="" readonly="">
										    </div>
										  </div>
										  <br/>
										  <div class="form-group">
										  <div class="row">
										  	<div class=" col-sm-12" style="text-align: right; padding-right: 30px;">
										    	<button type="submit" class="btn btn-default">Copy From</button>
										      <button type="submit" class="btn btn-default">Copy To</button>
										    </div>
										    </div> 
										  </div>
										</form>
									</div>
									</div>
									<div class="form-group">
										<div class="col-md-4">
										</div>
									  	<div class="col-md-1">
									  		<input type="submit" class="form-control btn btn-lg btn-success" style="line-height: 0.333px" value="Save">
									  	</div>
									  	<div class="col-md-7">
										</div>
									</div>
								</form>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>

<script>
	$("#Material").show();
	$("#Service").hide();
	$("#order_type").on("change",function(){
		var type = $("#order_type").val();
		if(type == 'material'){
			$("#Material").show();
			$("#Service").hide();
		} else if(type == 'sale'){
			$("#Material").hide();
			$("#Service").show();
		}
	});


	function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Bar Code" ></td><td><input type="text" name="" class="form-control" placeholder="Item Code" ></td><td><input type="text" name="" class="form-control" placeholder="Description"  readonly=""></td><td><input type="date" class="form-control" name=""></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control"  placeholder="Qty"></td><td><input type="text" name="" class="form-control" placeholder="UOM" ></td><td><input type="text" name="" class="form-control" placeholder="Price" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Discount" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Total" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Ref.No" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Cost Center" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Storage Location" readonly="" ></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}

function addmore1(){
var txt = '<tr><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " ><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="GL Name" readonly="" ></td><td><input type="text" name="" class="form-control" placeholder="Description" ></td><td><input type="date" class="form-control" name="" ></td><td><select class="form-control " style="width: 100px;"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Amount" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Discount" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="TDS" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Total" readonly="" style="width: 100px;"></td><td><select class="form-control " style="width: 100px;"><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control " style="width: 100px;"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Ref" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Cost" readonly="" style="width: 100px;"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab1").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}


	</script>
