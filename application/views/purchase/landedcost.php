<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-landedcost').addClass('active');
	$('#menu-landedcost > a').attr('aria-expanded', 'true');
	$('#menu-landedcost > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">Landed Cost</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Landed Cost</div>
								<div class="panel-body">


									<div class="form-group">
										<form class="form-vertical">
											<div class="row">
											    <div class="col-md-3">
										    		<label class="control-label">Company</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Company</option>
														<option>Quadraerp</option>
														<option>Company2</option>
														</select>
											    </div>
											     <div class="col-md-3">
										    		<label class="control-label">Ref_Type</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Ref_Type</option>
														<option>Type1</option>
														<option>Type2</option>
														</select>
											    </div>
											    <div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Document Number</label>
											    	<input type="text" class="form-control" id="">
											    </div>
											    <div class="col-md-2">
											    	<label class="control-label">Currency</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Order Type</option>
															<option>Conpany1</option>
															<option>Company2</option>
															</select>
											    </div>
											    <div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Posting Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											</div>
											<div class="row">
												<div class="col-md-3">
										    		<label class="control-label">Branch</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Order Type</option>
														<option>General</option>
														</select>
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Ref_Number</label>
											    	<input type="text" class="form-control" id="">
											    </div>
											    <div class="col-md-2"> 
											    </div>
											    <div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Amount</label>
											    	<input type="text" class="form-control" id="">
											    </div> 
											    <div class="col-md-2"> 
											    	<label class="control-label" for="fdate">Document Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											</div>										
										</form>
									</div>

									<br clear="left">
									<div class="tab-content">
									<ul class="nav nav-tabs" id="tabs">
										<li><a data-toggle="tab" href="#check">Item</a></li>
										<li><a data-toggle="tab" href="#bank">Cost</a></li>
										<li><a data-toggle="tab" href="#cash">Attachment</a></li>
									</ul>
									
								<div class="tab-pane active" id="check">
									<div id="tab">
										<div class="tab-pane active" id="1">
											<div class="col-md-2 ">
												<button class="form-control btn btn-lg btn-success" onclick="addmore()">Add</button> 
												<div style="padding: 10px"></div>
											</div>
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Item_Code</th>
																	<th>Item_Desc</th>
																	<th>Oty</th>
																	<th>Base_Price</th>
																	<th>Base_Value</th>
																	<th>Base_Value(FC)</th>
																	<th>Custom_Duty</th>
																	<th>Custom_Duty(FC)</th>
																	<th>Expentiure</th>
																	<th>Expentiure(FC)</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Item Code" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Item Desc" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Oty" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Base Price" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Base Value" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Base Value(FC)" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Custom Duty" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Custom Duty(FC)" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Expentiure" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Expentiure(FC)" >
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												
											</div>
										</div>
									</div>
										</div>
									     <div class="tab-pane " id="bank">
										<div id="tab">
										<div class="tab-pane active" id="2">
											<div class="col-md-2 ">
												<a href="#" class="form-control btn btn-sm btn-primary" onclick="addrow()">Add</a> 
												<div style="padding: 10px"></div>
											</div>
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Landed Cost</th>
																	<th>Amount</th>
																	<th>Amount(FC)</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="cost_tab">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Item Code" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Item Desc" >
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Oty" >
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
													<div class="form-group col-md-4">
												  <label for="comment">Remarks</label>
										<div style="padding: 3px"></div>
      									<textarea class="textarea form-control" style="height: 120px!important"  id="comment"></textarea>
												</div>
											    </div>
										       </div>
											</div>
											</div>
										<div class="tab-pane " id="cash">
											<div class="form-group" >
												<h5>Attachment</h5>
												<div class="row">
													<div class="col-md-3">
														 <form action="/action_page.php">
														  Select File: <input type="file" name="img" multiple>
														  <div style="padding: 5px"></div>
														  <input type="submit">
														</form>									
												    </div>
												</div>
											<div style="padding: 10px"></div>
												<div class="row">
													<div class=" col-sm-6">
												    		<button type="submit" class="btn btn-default">Add</button>
													      <button type="submit" class="btn btn-default">Cancel</button>
													 </div>
													 <div class=" col-sm-6">
													    	<button type="submit" class="btn btn-default">Copy From</button>
													      <button type="submit" class="btn btn-default">Copy To</button>
													 </div>
												</div>
											</div>
										</div>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>

<script>
function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control" placeholder="Item Code" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Item Desc" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Oty" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Base Price" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Base Value" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Base Value(FC)" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Custom Duty"></td><td><input type="text" name="" class="form-control" placeholder="Custom Duty(FC)"></td><td><input type="text" name="" class="form-control" placeholder="Expentiure"></td><td><input type="text" name="" class="form-control" placeholder="Expentiure(FC)"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}
function addrow(){
var txt = '<tr><td><input type="text" name="" class="form-control" placeholder="Item Code" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Item Desc"></td><td><input type="text" name="" class="form-control" placeholder="Oty"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#cost_tab").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}
</script>