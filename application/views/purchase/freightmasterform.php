<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-freightmasterform').addClass('active');
	$('#menu-freightmasterform > a').attr('aria-expanded', 'true');
	$('#menu-freightmasterform > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">Freight Setup</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Freight Setup</div>
								<div class="panel-body">
									

									<div class="row">
										<div class="col-md-1 ">
											<button class="form-control btn btn-lg btn-success" onclick="addmore()">Add</button> 
										</div>
									</div>
									<br>
												<div class="row" style="margin: 5px;">
													<div class="table-responsive form-group">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Serial No</th>
																	<th>Name</th>
																	<th>Revenue Account</th>
																	<th>Expense Account</th>
																	<th>Fixed Revenue Amount</th>
																	<th>Fixed Expense Amount</th>
																	<th>WithHolding Tab Liable</th>
																	<th>SAC Code</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="Serial No">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="Name">
																	</td>
																	<td>
																		<select class="form-control ">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control ">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Fixed Revenue Amount" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Fixed Expense Amount" readonly="">
																	</td>
																	<td>
																		<select class="form-control " >
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="unit Price" readonly="">
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
													</div>

													<div class="form-group">
														<div class="col-md-5">
														</div>
													  	<div class="col-md-1">
													  		<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px">Save</button>
													  	</div>
													</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
<script>
function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Serial No"></td><td><input type="text" name="" class="form-control  col-sm-1" placeholder="Name"></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Fixed Revenue Amount" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Fixed Expense Amount" readonly=""></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="unit Price" readonly=""></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}
</script>
