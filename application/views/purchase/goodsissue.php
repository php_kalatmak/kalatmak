<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-goodsissue').addClass('active');
	$('#menu-goodsissue > a').attr('aria-expanded', 'true');
	$('#menu-goodsissue > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PURCHASE</h1>
		<ol class="breadcrumb">
			<li><a href="">Purchase</a></li>
			<li class="active">Goods Issue</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Goods Issue</div>
								<div class="panel-body">


									<div class="form-group">
										<form class="form-vertical">
											<div class="row">
											    <div class="col-md-3">
										    		<label class="control-label">Reference Type</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Order Type</option>
														<option>Goods Receipt PO</option>
														<option>AP Invoice</option>
														</select>
											    </div>
											    <div class="col-md-3"> 
											    	<label class="control-label" for="todate">Reference Number</label>
											    	<input type="text" class="form-control" id="">
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Posting Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											    <div class="form-group col-md-2">
													<button type="button" class="form-control btn btn-lg btn-success" data-toggle="modal" data-target="#myModal">Transfer GR Batch & Serial</button>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3"> 
										    		<label class="control-label">Transaction Type</label> 
														<select class=" form-control select2" name="cityid">
														<option value="select country">Select Transaction Type</option>
														<option>Bill Of Supply</option>
														<option>GST Invoice</option>
														<option>Credit Memo</option>
														</select>
											    </div>
												<div class="col-md-3">
													<label class="control-label" for="todate">Document Number</label>
											    	<input type="text" class="form-control" id="">
												</div>						    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Doc.Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											</div>									
										</form>
									</div>
										<!-- Modal -->
											<div id="myModal" class="modal fade" role="dialog">
												<div class="modal-dialog">

												<!-- Modal content-->
													<div class="modal-content">
														<?php
															include 'transbatchserial.php';
														?>
													</div>

												</div>
											</div>
											
											<div style="padding: 5px"></div>
											<div class="row">
											<div class="col-md-1 ">
												<button class="form-control btn btn-lg btn-success" onclick="addmore()">Add</button> 
												<div style="padding: 5px"></div>
											</div>
											</div>
												<div class="row" style="margin: 5px">
													<div class="table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Bar_Code</th>
																	<th>Item_No.</th>
																	<th>Item_Description</th>
																	<th>Oty</th>
																	<th>UOM</th>
																	<th>Unit_Price</th>
																	<th>Frieght</th>
																	<th>Storage_Location</th>
																	<th>Pin_Location</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>GL_Account</th>
																	<th>Delete</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Bar Code">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Qty">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="unit Price" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Frieght" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>


									<div class="form-group">
										<div class="col-md-4">
											<label for="comment">Remarks</label>
											<div style="padding: 3px"></div>
	      									<textarea class="textarea form-control" style="height: 120px!important"  id="comment"></textarea>
										</div>
										<div class="col-md-2">
										</div>
										<div class="col-md-6">
											<form class="form-horizontal" action="">
												<br>
											 	 <div class="row">
											 	 	<div class=" col-sm-12" style="text-align: right; padding-right: 30px;">
												    	<button type="submit" class="btn btn-default">Copy From</button>
												      	<button type="submit" class="btn btn-default">Copy To</button>
												    </div>
											   </div> 
											</form>
										</div>
									</div>
									<br>
									<div class="form-group">
									  	<div class="col-md-1">
									  		<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px">Save</button>
									  	</div>
									</div>
								
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</section>
<script>
function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control" placeholder="Bar Code"></td><td><select class="form-control"><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Description" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Qty"></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="unit Price" readonly=""></td><td><input type="text" name="" class="form-control" placeholder="Frieght" readonly=""></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Total" readonly=""></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><select class="form-control"><option>select</option><option>1002E</option><option>201T</option></select></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}
</script>
