<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Add JE</h4>
</div>
<div class="modal-body" style="height: 600px !important">
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Journal Entry</div>
								<div class="panel-body">
									<div class="row">
									<div class="col-md-2 form-group">
										 <div class="col-md-12">
									    	<label class="control-label">Company Code</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Company Code</option>
											<option value="material">CO01</option>
											<option value="sale">CO01</option>
											</select>
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label">Branch Code</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Branch Code</option>
											<option value="material">BO01</option>
											<option value="sale">BO01</option>
											</select>
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label">Project Code</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Project Code</option>
											<option value="material">PO2154</option>
											<option value="sale">PO5645</option>
											</select>
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label">Currency</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Currency</option>
											<option value="material">INR</option>
											<option value="sale">USD</option>
											</select>
									    </div>
									</div>
									<div class="col-md-2 form-group">
										 <div class="col-md-12">
									    	<label class="control-label">Transaction Type</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Transaction Type</option>
											<option value="material">Bank</option>
											<option value="sale">Cash</option>
											</select>
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label">Reference Type</label> 
											<select class=" form-control select2" name="cityid" id="order_type">
											<option value="select country">Select Reference Type</option>
											<option value="material">Type1</option>
											<option value="sale">Type2</option>
											</select>
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label" for="itmqty">Reference Number</label>
											<input type="text" name="" class="form-control" placeholder="Reference Number">
									    </div>
									</div>
									<div class="col-md-2 form-group">
										 <div class="col-md-12">
									    	<label class="control-label" for="todate">Document Date</label>
											<input type="date" class="form-control" id="">
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label" for="todate">Posting Date</label>
											<input type="date" class="form-control" id="">
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label" for="todate">Due Date</label>
											<input type="date" class="form-control" id="">
									    </div>
									    <div class="col-md-12">
									    	<label class="control-label" for="todate">Tax Date</label>
											<input type="date" class="form-control" id="">
									    </div>
									</div>
									<div class="col-md-6 form-group">
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="checkbox">
												  <label class="control-label"><input type="checkbox" id="checkbox1" onclick="myFunction()">Fixed Exchange Rate</label>
												</div>
											</div>
											<div id="Currency1">
												<div class="col-md-4">
													<label class="control-label">Currency</label> 
													<select class=" form-control select2" name="cityid" id="order_type">
													<option value="select country">Select Currency</option>
													<option value="material">INR</option>
													<option value="sale">USD</option>
													</select>
												</div>
												<div class="col-md-4 form-group">
													<div class="col-md-11">
												    	<label class="control-label" for="itmqty">Amount</label>
														<input type="text" name="" class="form-control" placeholder="Amount">
												    </div>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-4">
												<div class="checkbox">
												  <label class="control-label"><input type="checkbox" id="checkbox2" onclick="myFunction1()">Reverse</label>
												</div>
											</div>
											<div class="col-md-4" id="date1">
												<label class="control-label" for="itmqty"></label>
												<input type="date" class="form-control" id="">
											</div>
										</div>
										<div class="col-md-12">
											<div class="col-md-4">
										    	<label class="control-label">Status</label> 
												<select class=" form-control select2" name="cityid" id="order_type">
													<option value="select country">Posted</option>
													<option value="material">Canceled</option>
													<option value="sale">Reversed</option>
												</select>
										    </div>
										</div>
									</div>
								</div>
								<br/>
								<div class="row">
									<div class="col-md-12 table-responsive">
										<div class="col-md-1 ">
												<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore15()">Add</button> 
										<div style="padding: 5px"></div>
										</div>
										<table class="table table-bordered" >
											<thead>
												<tr>
													<th>BP/GL_Code</th>
													<th>BP/GL_Name</th>
													<th>Control_Account</th>
													<th>Debit</th>
													<th>Credit</th>
													<th>Debit(SC)</th>
													<th>Credit(SC)</th>
													<th>Debit(FC)</th>
													<th>Credit(FC)</th>
													<th>Remarks</th>
													<th>Ref1</th>
													<th>Due_Date</th>
													<th>Posting_Date</th>
													<th>Doc_Date</th>
													<th>Project</th>
													<th>Tax_Amount</th>
													<th>Tax_Amount(SC)</th>
													<th>Gross_Value</th>
													<th>Gross_Value(FC)</th>
													<th>Offset_Account</th>
													<th>Payment_Reference_No.</th>
													<th>Payment_Block</th>
													<th>Block_Reason</th>
													<th>Payment_Order_Run</th>
													<th>Branch</th>
													<th>Cost_Center</th>
													<th>Delete</th>
												</tr>
											</thead>
											<tbody id="service_tab15">
												<tr>
													<td>
														<select class="form-control select2" style="width: 100px;">
															<option>select</option>
															<option>1002E</option>
															<option>201T</option>
														</select>
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="BP/GL_Name" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Control Account" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Debit" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Credit" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Debit(sc)" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Credit(sc)" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Debit(fc)" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Credit(fc)" style="width: 100px;">
													</td>
													<td>
														<textarea class="textarea" style="width: 100px; height: 25px !important;" rows="5" id="comment"></textarea>
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Ref1" style="width: 100px;">
													</td>
													<td>
														<input type="date" name="" class="form-control" placeholder="due">
													</td>
													<td>
														<input type="date" name="" class="form-control" placeholder="posting">
													</td>
													<td>
														<input type="date" name="" class="form-control" placeholder="doc">
													</td>
													<td>
														<select class="form-control select2" style="width: 100px;">
															<option>select</option>
															<option>1002E</option>
															<option>201T</option>
														</select>
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Tax_Amount" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Tax_Amount(sc)" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Gross Value" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Gross Value(FC)" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Offset Account" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Payment Reference No." readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Payment Block"  style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Block Reason" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Payment Order Run" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Branch" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Costcenter"  style="width: 100px;">
													</td>
													<td>
														<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>									
								</div>
								<div style="padding: 10px"></div>
								<div class="row">
										<div class="col-md-2">
											<div class="col-md-12">
											  <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Add</button>
											</div>
										</div>
										<div class="col-md-2">
											<div class="col-md-12">
											  <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Cancel</button>
											</div>
										</div>
										<div class=" col-md-4">
										</div>
										<div class="form-group">
											<div class="checkbox col-md-2">
												  <label class="control-label"><input type="checkbox" value="">Display (FC)</label>
											</div>
										</div>
										<div class="form-group">
											<div class="checkbox col-md-2">
												  <label class="control-label"><input type="checkbox" value="">Display(SC)</label>
											</div>
										</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>

<script>

function addmore15(){
var txt = '<tr><td><select class="form-control select2" style="width: 100px;"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="BP/GL_Name" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Control Account" style="width: 100px;" ></td><td><input type="text" name="" class="form-control" placeholder="Debit" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Credit" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Debit(sc)" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Credit(sc)" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Debit(fc)" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Credit(fc)" style="width: 100px;"></td><td><textarea class="textarea" style="width: 100px; height: 25px !important;" rows="5" id="comment"></textarea></td><td><input type="text" name="" class="form-control" placeholder="Ref1" style="width: 100px;"></td><td><input type="date" name="" class="form-control" placeholder="due"></td><td><input type="date" name="" class="form-control" placeholder="posting"></td><td><input type="date" name="" class="form-control" placeholder="doc"></td><td><select class="form-control select2" style="width: 100px;"><option>select</option><option>1002E</option><option>201T</option></select></td><td><input type="text" name="" class="form-control" placeholder="Tax_Amount" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Tax_Amount(sc)" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Gross Value" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Gross Value(FC)" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Offset Account" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Payment Reference No." readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Payment Block" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Block Reason" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Payment Order Run" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Branch" readonly="" style="width: 100px;"></td><td><input type="text" name="" class="form-control" placeholder="Costcenter" style="width: 100px;"></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab15").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}

$("#Currency1").hide();
$("#date1").hide();

function myFunction() {
  var checkBox = document.getElementById("checkbox1");
  var text = document.getElementById("Currency1");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
     text.style.display = "none";
  }
}

function myFunction1() {
  var checkBox = document.getElementById("checkbox2");
  var text = document.getElementById("date1");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
     text.style.display = "none";
  }
}
	</script>