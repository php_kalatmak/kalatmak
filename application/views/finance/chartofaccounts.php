<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-finance').addClass('active');
	$('#menu-finance > a').attr('aria-expanded', 'true');
	$('#menu-finance> ul').addClass('in');

	$('#menu-chartofaccounts').addClass('active');
	$('#menu-chartofaccounts > a').attr('aria-expanded', 'true');
	$('#menu-chartofaccounts > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>Finance</h1>
		<ol class="breadcrumb">
			<li><a href="">Finance</a></li>
			<li class="active">Chart Of Accounts</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab" >
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Chart Of Accounts</div>
								<div class="panel-body" style="height: 625px">



									<div class="col-lg-4 ">
										<div class="row" id="">
											<div class="col-sm-6">
												<label class="radio-inline"><input type="radio" value="click1" id="radiobtn" name="optradio" checked>Title</label>
											</div>
											<div class="col-sm-6">
												<label class="radio-inline"><input type="radio" value="click2" id="radiobtn1" name="optradio">Account</label>
											</div>
										</div>
										<br/>
										<form class="form-horizontal" action="" id="Title" data-parsley-validate="">
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">GL Code</label>
										    <div class="col-sm-8">
										       <input type="text" class="form-control" name="" required="" data-parsley-palindrome="" placeholder="GL Code">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">GL Name</label>
										    <div class="col-sm-8"> 
										      <input type="text" class="form-control" name="" required=""  data-parsley-palindrome="" placeholder="GL Name">
										    </div>
										  </div>
										  <div class="form-group">
									    	<label class="control-label col-sm-4" for="">Level</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<br/>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Drawer</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2"  required="" name="cityid">
												<option value="">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4"  for="">Parent Code</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">After Drawer</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2"  required="" name="cityid">
												<option value="">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
										  <div class="form-group"> 
										    <div class="col-sm-offset-9 col-sm-3">
										    	<input type="submit" class="form-control btn btn-lg btn-success" style="line-height: 0.333px" value="Add">
										       
										    </div>
										  </div>	
										</form>
										<form class="form-horizontal" action="" id="Account" data-parsley-validate="">
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">GL Code</label>
										    <div class="col-sm-8">
										      <input type="text" class="form-control" required=""  data-parsley-palindrome="" id="" placeholder="GL Code">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">GL Name</label>
										    <div class="col-sm-8"> 
										      <input type="text" class="form-control" required=""  data-parsley-palindrome="" id="" placeholder="GL Name">
										    </div>
										  </div>
										  <div class="form-group">
									    	<label class="control-label col-sm-4" for="">Drawer</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="select country">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Parent Code</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="select country">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">After Drawer</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="select country">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">External Code</label>
										    <div class="col-sm-8"> 
										      <input type="text" class="form-control" id="" required=""  data-parsley-palindrome="" placeholder="GL Name">
										    </div>
										  </div>
										  <div class="form-group">
									    	<label class="control-label col-sm-4" for="">Currency</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Currency</option>
												<option>INR</option>
												</select>
											 </div>
											</div>
										  <div class="form-group">
									    	<label class="control-label col-sm-4" for="">Level</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Level</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Balance</label> 
									    	 <div class="col-sm-8">
											 </div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Balance_sheet/Profit & Loss</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Balance sheet /Profit & Loss</option>
												<option>Level1</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
												<div class="row">
												 <div class="checkbox col-sm-6">
												    <label><input type="checkbox"> Control Account</label>
												  </div>
												  <div class="checkbox col-sm-6">
												    <label><input type="checkbox"> Cash Account</label>
												  </div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
												 <div class="checkbox col-sm-6">
												    <label><input type="checkbox"> Block Manual Posting</label>
												  </div>
												  <div class="checkbox col-sm-6">
												    <label><input type="checkbox">Budget</label>
												  </div>
												</div>
											</div>
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Status</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" required="" name="cityid">
												<option value="">Select Status</option>
												<option>Active</option>
												<option>InActive</option>
												</select>
											 </div>
											</div>
											<div class="form-group">
										    <label class="control-label col-sm-4" for="">From Date</label>
										    <div class="col-sm-8"> 
										      <input type="date" class="form-control" id="" required=""  data-parsley-palindrome="">
										    </div>
										  </div>
										  <div class="form-group">
										    <label class="control-label col-sm-4" for="">To Date</label>
										    <div class="col-sm-8"> 
										      <input type="date" class="form-control" id="" required=""  data-parsley-palindrome="" >
										    </div>
										  </div>

										  <div class="form-group row"> 
										    <div class="col-sm-offset-9 col-sm-3">
										      <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Add</button> 
										    </div>
										  </div>	
										</form>
									</div>
									<div class="col-lg-7">
										<div class="row">
											<div class="col-md-12 box" >
											  <div class="panel-group" id="accordion">
											  	<h4> List Of Company Code</h4>
											    <div class="panel panel-default">
											      <div class="panel-heading">
											        <h4 class="panel-title">
											          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Collapsible Group 1</a>
											        </h4>
											      </div>
											      <div id="collapse1" class="panel-collapse collapse in">
											        <div class="panel-body">123545874968</div>
											      </div>
											    </div>
											    <div class="panel panel-default">
											      <div class="panel-heading">
											        <h4 class="panel-title">
											          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Collapsible Group 2</a>
											        </h4>
											      </div>
											      <div id="collapse2" class="panel-collapse collapse">
											        <div class="panel-body">6548946515156</div>
											      </div>
											    </div>
											    <div class="panel panel-default">
											      <div class="panel-heading">
											        <h4 class="panel-title">
											          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Collapsible Group 3</a>
											        </h4>
											      </div>
											      <div id="collapse3" class="panel-collapse collapse">
											        <div class="panel-body">56498146546514</div>
											      </div>
											    </div>
											  </div> 
											</div>
										</div>
									</div>
									<div class="col-lg-1">
										<form class="form-horizontal" action="">
										<div class="form-group">
										    <div class="col-sm-12"> 
										     	<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Add</button>
										    </div>
										  </div>
										  <div class="form-group">
										    <div class="col-sm-12"> 
										     <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Add</button>
										    </div>
										  </div>
										  <div class="form-group">
										    <div class="col-sm-12"> 
										     <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Add</button>
										    </div>
										  </div>
										</form>
									</div>



								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script>

	$("#Title").show();
	$("#Account").hide();
	$("#radiobtn").on("change",function(){
		var type = $("#radiobtn").val();
		if(type == 'click1'){
			$("#Title").show();
			$("#Account").hide();
		} else if(type == 'click2'){
			$("#Title").hide();
			$("#Account").show();
			}
	});
	$("#radiobtn1").on("change",function(){
		var type = $("#radiobtn1").val();
		if(type == 'click1'){
			$("#Title").show();
			$("#Account").hide();
		} else if(type == 'click2'){
			$("#Title").hide();
			$("#Account").show();
			}
	});


</script>
