<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-finance').addClass('active');
	$('#menu-finance > a').attr('aria-expanded', 'true');
	$('#menu-finance> ul').addClass('in');

	$('#menu-journalvoucher').addClass('active');
	$('#menu-journalvoucher > a').attr('aria-expanded', 'true');
	$('#menu-journalvoucher > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>Finance</h1>
		<ol class="breadcrumb">
			<li><a href="">Finance</a></li>
			<li class="active">Journal Voucher</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Journal Voucher</div>
								<div class="panel-body">
									
									<div class="row">
									<div class="col-md-12 table-responsive">
										<table class="table table-bordered" >
											<thead>
												<tr>
													<th>Select</th>
													<th>Journal_No</th>
													<th>Status</th>
													<th>Posting_Date</th>
													<th>Amount</th>
													<th>Remarks</th>
													<th>Ref1</th>
													<th>Branch_Code</th>
												</tr>
											</thead>
											<tbody id="service_tab">
												<tr>
													<td>
														<input type="checkbox">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Journal_No" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Status" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Posting_Date" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Amount" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Remarks" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Ref1" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Branch_Code" readonly="" style="width: 100px;">
													</td>
												</tr>
											</tbody>
										</table>
									</div>									
								</div>
								<div class="row">
										<div class="form-group"> 
									    <div class="col-sm-1">
									      <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Cancel</button> 
									    </div>
									     <div class="col-sm-8">
									     </div>
									    <div class="col-sm-1">
									      <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" data-toggle="modal" data-target="#myModal">Add JE</button> 
									      <!-- Modal -->
											<div id="myModal" class="modal fade" role="dialog">
											<div class="modal-dialog">

											<!-- Modal content-->
												<div class="modal-content">
													<?php
														include 'je.php';
													?>
												</div>

											</div>
											</div>
									    </div>
									    <div class="col-sm-1">
									      <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Edit JE</button> 
									    </div>
									    <div class="col-sm-1">
									      <button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" >Post JE</button> 
									    </div>
									  </div>
								</div>

								


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


