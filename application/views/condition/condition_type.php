
<script type="text/javascript">
	
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-condition').addClass('active');
	$('#menu-condition > a').attr('aria-expanded', 'true');
	$('#menu-condition > ul').addClass('in');

	$('#menu-conditionType').addClass('active');


	$('#conditionTypeAdd').click(function() {
		$('#ConditionType').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	$('#country_Reset').click(function(){
		$('#condType').val('');
		$('#condName').val('');
	})
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Condition Type</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CONDITION TYPE</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="conditionTypeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="condType">Condition Type</th>
														<th data-field="condName">Condition Name</th>
														<th data-field="condSign">Condition Sign</th>
														<th data-field="scaleBasic">Scale Basic</th>
														<th data-field="headerItem">Header Item</th>
														<th data-field="strtSlab">Straight Slab</th>
														<th data-field="amtper">Amount / Percentage</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>CType1</td>
														<td>CName1</td>
														<td>Minus/plus</td>
														<td>Quantity</td>
														<td>HI1</td>
														<td>SL1</td>
														<td>Amount</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType2</td>
														<td>CName2</td>
														<td>Minus/plus</td>
														<td>Quantity</td>
														<td>HI2</td>
														<td>SL2</td>
														<td>Percentage</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType3</td>
														<td>CName3</td>
														<td>Minus/plus</td>
														<td>Quantity</td>
														<td>HI3</td>
														<td>SL3</td>
														<td>Amount</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType4</td>
														<td>CName4</td>
														<td>Minus/plus</td>
														<td>Quantity</td>
														<td>HI4</td>
														<td>SL4</td>
														<td>Percentage</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType5</td>
														<td>CName5</td>
														<td>Minus/plus</td>
														<td>Quantity</td>
														<td>HI5</td>
														<td>SL5</td>
														<td>Amount</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Condition Type</label> <input type="text" maxlength="4" readonly="" 
														id="condType" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Name</label> <input type="text" maxlength="20"
														id="condName" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Condition Sign </label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Condition Sign </option>
													<option>Minus</option>
													<option>Plus</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Scale Basic</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Scale Basic</option>
													<option>CM</option>
													<option>M</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Header Item</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Header Item</option>
													<option>Header</option>
													<option>Item</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Straight Slab</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Straight Slab</option>
													<option>Straight</option>
													<option>Slab</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Amount / Percentage </label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Amount / Percentage </option>
													<option>Amount</option>
													<option>Percentage</option>
													</select>
												</div>
												<div class="form-group col-md-6">
												<label class="control-label">Status</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>Active</option>
													<option>Inactive</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTypeSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionType" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Condition Type</label> <input type="text" maxlength="4"
														id="condType" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Name</label> <input type="text" maxlength="20"
														id="condName" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Condition Sign </label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Condition Sign </option>
													<option>Minus</option>
													<option>Plus</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Scale Basic</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Scale Basic</option>
													<option>CM</option>
													<option>M</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Header Item</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Header Item</option>
													<option>Header</option>
													<option>Item</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Straight Slab</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Straight Slab</option>
													<option>Straight</option>
													<option>Slab</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Amount / Percentage </label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Amount / Percentage </option>
													<option>Amount</option>
													<option>Percentage</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTypeSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionTypeEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Condition Type</label> <input type="text" maxlength="4"
														readonly="readonly" id="condTypeEdit" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Name</label> <input type="text" maxlength="20"
														id="condNameEdit" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Sign</label> <select id="condSignEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Scale Basic</label> <select id="scaleBasicEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Header Item</label> <select id="headerItemEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Straight Slab</label> <select id="strtSlabEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount / Percentage</label> <select id="amtperEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Status</label> <select id="statusEdit"
														class="form-control">
														<option value="-1">Select Status</option>
														<option value="L">Active</option>
														<option value="X">In-Active</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTypeUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>