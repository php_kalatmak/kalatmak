
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-condition').addClass('active');
	$('#menu-condition > a').attr('aria-expanded', 'true');
	$('#menu-condition > ul').addClass('in');

	$('#menu-sequenceMapping').addClass('active');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Sequence Mapping</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">SEQUENCE MAPPING</div>
										<div class="panel-body">
											<div class="form-group col-md-6">
												<label class="control-label">Condition </label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Condition </option>
													<option>type1</option>
													<option>type2</option>
													</select>
											</div>
											<div class="form-group col-md-6">
												<label class="col-md-12">Tap</label>
												<div class="col-md-12">
													<select id="Comp_Map" multiple="multiple" style=""
														class="multiselect-ui form-control select2">
													<option>Tap1</option>
													<option>Tap2</option>
													<option>Tap3</option>
													<option>Tap4</option>
													</select>
												</div>
											</div>
										</div>
										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="saveAssgn" type="button" style="margin-right: 5px">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>