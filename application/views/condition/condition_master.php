<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-condition').addClass('active');
	$('#menu-condition > a').attr('aria-expanded', 'true');
	$('#menu-condition > ul').addClass('in');

	$('#menu-conditionMaster').addClass('active');


	$('#conditionMasterAdd').click(function() {
		$('#ConditionMaster').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	$('#country_Reset').click(function(){
		$('#hsn').val('');
		$('#value').val('');
	})
});
</script>	


<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Condition Master</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CONDITION MASTER</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success"
												id="conditionMasterAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="condType">Condition Type</th>
														<th data-field="tab">Tab</th>
														<th data-field="country">Country</th>
														<th data-field="state">State</th>
														<th data-field="business">Business Partner</th>
														<th data-field="company">Company</th>
														<th data-field="hsn">HSN / SAC</th>
														<th data-field="item">Item</th>
														<th data-field="itemGrp">Item Group</th>
														<th data-field="uom">UOM</th>
														<th data-field="amtper">Amount / Percentage</th>
														<th data-field="fromDate">From Date</th>
														<th data-field="toDate">To Date</th>
														<th data-field="status">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>CType1</td>
														<td>Tap1</td>
														<td>India</td>
														<td>Tamilnadu</td>
														<td>BP1</td>
														<td>Company1</td>
														<td>HSN/SAC</td>
														<td>Item1</td>
														<td>IG1</td>
														<td>UOM1</td>
														<td>Amount</td>
														<td>1/1/2000</td>
														<td>12/12/2012</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType2</td>
														<td>Tap2</td>
														<td>India</td>
														<td>Tamilnadu</td>
														<td>BP2</td>
														<td>Company2</td>
														<td>HSN/SAC</td>
														<td>Item2</td>
														<td>IG2</td>
														<td>UOM2</td>
														<td>Amount</td>
														<td>1/2/2000</td>
														<td>12/2/2012</td>
														<td>InActive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType3</td>
														<td>Tap3</td>
														<td>India</td>
														<td>Kerala</td>
														<td>BP3</td>
														<td>Company3</td>
														<td>HSN/SAC</td>
														<td>Item1</td>
														<td>IG3</td>
														<td>UOM3</td>
														<td>Amount</td>
														<td>1/1/2000</td>
														<td>12/12/2014</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType4</td>
														<td>Tap1</td>
														<td>India</td>
														<td>Tamilnadu</td>
														<td>BP4</td>
														<td>Company4</td>
														<td>HSN/SAC</td>
														<td>Item4</td>
														<td>IG4</td>
														<td>UOM4</td>
														<td>Amount</td>
														<td>12/1/2012</td>
														<td>12/12/2012</td>
														<td>Active</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>CType5</td>
														<td>Tap5</td>
														<td>India</td>
														<td>Tamilnadu</td>
														<td>BP5</td>
														<td>Company1</td>
														<td>HSN/SAC</td>
														<td>Item5</td>
														<td>IG5</td>
														<td>UOM1</td>
														<td>Amount</td>
														<td>1/5/2005</td>
														<td>12/12/2012</td>
														<td>InActive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionMaster"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION MASTER</h4>
									</div>
									<div class="modal-body" style="height: 400px">
										<form>
											<div class="row">
												<form>
												<div class="" id="Material">
												<div class="form-group col-md-4" >
													<label class="control-label">Condition Type</label> 
													<select class=" form-control select2" name="conditione" id="condition_type">
													<option value="select country">Select Condition Type</option>
													<option value="ct_discount">Discount</option>
													<option value="ct_price">Price</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Tab </label> 
													<select class=" form-control select2" name="order_type" id="order_type">
													</select>
												</div>
											</div>
											<div class="" id="Service">
												<div class="form-group col-md-4" id="countryDiv">
													<label class="control-label">Country</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Country</option>
													<option>India</option>
													<option>America</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="stateDiv">
													<label class="control-label">State</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select State</option>
													<option>Tamil Nadu</option>
													<option>Kerala</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="businessDiv">
													<label class="control-label">Business Partner</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Business Partner</option>
													<option>BP1</option>
													<option>BP2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="compDiv">
													<label class="control-label">Company</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company</option>
													<option>Company1</option>
													<option>Company2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="hsnDiv">
													<label>HSN /SAC</label> <input type="text" id="hsn"
														maxlength="19" class="form-control">
												</div>
												<div class="form-group col-md-4" id="itemDiv">
													<label class="control-label">Item</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item</option>
													<option>Item1</option>
													<option>Item2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="itemGrpDiv">
													<label class="control-label">Item Group</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item Group</option>
													<option>Group1</option>
													<option>Group2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="uomDiv">
													<label class="control-label">Scale UOM</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select UOM</option>
													<option>01</option>
													<option>02</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="valueDiv">
													<label>Amount / Percentage</label> <input type="text"
														id="value" maxlength="9" class="form-control"
														onkeyup="this.value = this.value.replace(/[^0-9 . ]/, '')">
												</div>
												<div class="form-group col-md-4" id="fromDateDiv">
													<label>From Date</label> <input type="text" id="fromDate"
														class="form-control">
												</div>
												<div class="form-group col-md-4" id="toDateDiv">
													<label>To Date</label> <input type="text" id="toDate"
														class="form-control">
												</div>
											</div>
											<div class="" id="Itemgroup">
												<div class="form-group col-md-4" id="countryDiv">
													<label class="control-label">Country</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Country</option>
													<option>India</option>
													<option>America</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="compDiv">
													<label class="control-label">Company</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company</option>
													<option>Company1</option>
													<option>Company2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="itemDiv">
													<label class="control-label">Item</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item</option>
													<option>Item1</option>
													<option>Item2</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="valueDiv">
													<label>Amount / Percentage</label> <input type="text"
														id="value" maxlength="9" class="form-control"
														onkeyup="this.value = this.value.replace(/[^0-9 . ]/, '')">
												</div>
												<div class="form-group col-md-4" id="fromDateDiv">
													<label>From Date</label> <input type="text" id="fromDate"
														class="form-control">
												</div>
												<div class="form-group col-md-4" id="toDateDiv">
													<label>To Date</label> <input type="text" id="toDate"
														class="form-control">
												</div>
											</div>
											<div class="" id="Item">
												<div class="form-group col-md-4" id="countryDiv">
													<label class="control-label">Country</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Country</option>
													<option>India</option>
													<option>America</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="stateDiv">
													<label class="control-label">State</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select State</option>
													<option>Tamil Nadu</option>
													<option>Kerala</option>
													</select>
												</div>
											</div>
											</form>
										</div>
										
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionMasterSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>


<script>
	$("#Material").show();
	$("#Service").hide();
	$("#Itemgroup").hide();
	$("#Item").hide();
	$("#order_type").on("change",function(){
		var type = $("#order_type").val();
		if(type == 'item_group'){
			$("#Service").show();
			$("#Itemgroup").hide();
			$("#Item").hide();
		} else if(type == 'item'){
			$("#Service").hide();
			$("#Itemgroup").show();
			$("#Item").hide();
		} else {
			$("#Service").hide();
			$("#Itemgroup").hide();
			$("#Item").show();
		}
	});

	
	$("#condition_type").on("change",function(){
		var type = $("#condition_type").val();
		if(type == 'ct_discount'){
			var txt = '<option value="select country">Select Tab </option><option value="item_group">Item Group</option>';
			$("#order_type").html(txt);
		} else if(type == 'ct_price'){
			var txt = '<option value="select country">Select Tab </option><option value="item">Material</option><option value="item_group">Item Group</option><option value="material">Item</option> ';
			$("#order_type").html(txt);
		}
	});
// 	order_type
// <option value="select country">Select Tab </option>
// 													<option value="item">Material</option>
// 													<option value="item_group">Item Group</option>
// 													<option value="material">Item</option>
	</script>