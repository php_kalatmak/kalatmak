
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-country').addClass('active');
	$('#Country').hide();
	$('#countryEdit').hide();

	$('#countryAdd').click(function() {
		$('#Country').show();
	});

	$('#country_Reset').click(function() {
		$('#countrycode').val('');
		$('#countryname').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
});
	
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Country</li>
				</ol>
			</div>
			<?php $country = $this->db->query("select * from erpgcy order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">COUNTRY</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="countryAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>
													
												<thead>
													<tr>
														<th>Country Code</th>
														<th>Country Name</th>
														<th>Edit</th>
													</tr>
												</thead>
												<tbody>
												<?php $i=0; foreach($country as $row) { ?>
												<tr>
												<td><?php echo $row->country_code; ?></td>
												<td><?php echo $row->country_name; ?></td>
												<td><a class="edit ml10" title="" data-toggle="modal" data-target="#countryEdit<?php echo $i; ?>">
												<i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												
												
				<div class="country_info modal " id="countryEdit<?php echo $i; ?>" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/update_country')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COUNTRY</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Country
												Code</label> <input type="text" class="form-control" name = "countrycode"
												id="countrycodeEdit" maxlength="3" readonly="readonly" value="<?php echo $row->country_code; ?>"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Country
												Name</label> <input type="text" class="form-control" name = "countryname"
												id="countrynameEdit" maxlength="30" value="<?php echo $row->country_name; ?>"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
										
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>									
																			
												</tbody>
												<?php $i++; } ?>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="country_info modal " id="Country" tabindex="-1"	role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/add_country'), array('id'=>'country','name'=>'country','autocomplete'=>'off')); ?>
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COUNTRY</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Country
												Code</label> <input type="text" class="form-control"
												id="countrycode" maxlength="3" name="country_code"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')" required>
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Country
												Name</label> <input type="text" class="form-control" name="country_name"
												id="countryname" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')" required>
										</div>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>											
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>