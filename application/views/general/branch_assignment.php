
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-branchAssignment').addClass('active');
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Branch Assignment</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">BRANCH ASSIGNMENT</div>
										<div class="panel-body">
											<div class="form-group col-md-6">
												<label class="control-label">Company</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Company</option>
													<option>Quadraerp</option>
													</select>
											</div>
											<div class="form-group col-md-6">
												<label class="col-md-12">Branch</label>
												<div class="col-md-12">
													<select id="Comp_Map" multiple="multiple" style="" class="form-control multiselect select2">
													<option>General</option>
													</select>
												</div>
											</div>
										</div>
										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="saveAssgn" type="button" style="margin-right: 5px">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>