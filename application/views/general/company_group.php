

<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-companyGroup').addClass('active');
	$('#CompanyGroup').hide();

	$('#CompanyGroupAdd').click(function() {
		$('#CompanyGroup').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

});
	
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Company</a></li>
					<li class="active">Company Group</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">COMPANY GROUP</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="CompanyGroupAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="compGrpCode">Company Group Code</th>
														<th data-field="compGrpName">Company Group Name</th>
														<th data-field="compGrpCurr">Currency</th>
														<th data-field="compGrpLang">Language</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>1000</td>
														<td>Quadraerp</td>
														<td>INR</td>
														<td>English</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													
													<div class="country_info modal " id="StateEdit" tabindex="-1"
														role="dialog" aria-labelledby="EmployementTypeLabel">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close " data-dismiss="modal">
																		<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																	</button>
																	<h4 class="modal-title" id="EmployementTypeLabel">COMPANY
																		GROUP</h4>
																	<ul class="nav nav-tabs">
																		<li class="active"><a data-toggle="tab" href="#general">General</a></li>
																		<li><a data-toggle="tab" href="#address">Address</a></li>
																	</ul>
																</div>
																<div class="tab-content">
																	<div id="general" class="tab-pane fade in active">
																		<div class="modal-body" style="height: 350px">
																			<div class="row">
																			<div class="form-group col-md-4">
																				<label for="countrycode" class="control-label">Company
																					Group Code </label> <input type="text" class="form-control"
																					id="compGrpCode" maxlength="4"
																					onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
																			</div>
																			<div class="form-group col-md-4">
																				<label for="countrycode" class="control-label">Company
																					Group Name </label> <input type="text" class="form-control"
																					id="compGrpName" maxlength="50">
																			</div>
																			<div class="form-group col-md-4">
																				<label class="control-label">Currency</label> 
																					
																				<select class=" form-control select2" name="cityid">
																				<option value="select country">Select Currency</option>
																				<option>INR</option>
																				<option>EUR</option>
																				</select>
																			</div>
																		</div>
																			<div class="row">
																			<div class="form-group col-md-4">
																				<label class="control-label">Language</label> 
																					
																				<select class=" form-control select2" name="cityid">
																				<option value="select country">Select Language</option>
																				<option>English</option>
																				<option>Tamil</option>
																				</select>
																			</div>
																			<div class="form-group col-md-4">
																				<label for="countrycode" class="control-label">Company
																					Group Image </label> <input type="file" class="form-control"
																					id="compGrpImage" onchange="readURL(this);" />
																			</div>
																			<div class="form-group col-md-4">
																				<img id="blah" src="resources/images/avatars/def.png"
																					alt="..." />
																			</div>
																		</div>
																			
																		</div>
																	</div>
																	<div id="address" class="tab-pane fade">
																		<div class="modal-body" style="height: 430px">
																			<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
																		</div>
																	</div>

																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="compGrpSave" type="button" style="margin-right: 5px">Save</button>

																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="CompanyGroup" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COMPANY
											GROUP</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general1">General</a></li>
											<li><a data-toggle="tab" href="#address1">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general1" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="row">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Code </label> <input type="text" class="form-control"
														id="compGrpCode" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Name </label> <input type="text" class="form-control"
														id="compGrpName" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Currency</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
												</div>
											</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Language</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Language</option>
													<option>English</option>
													<option>Tamil</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Image </label> <input type="file" class="form-control"
														id="compGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
											</div>
												
											</div>
										</div>
										<div id="address1" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="compGrpSave" type="button" style="margin-right: 5px">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		</section>