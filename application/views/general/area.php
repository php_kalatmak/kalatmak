

<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-area').addClass('active');
	$('#State').hide();

	$('#stateAdd').click(function() {
		$('#State').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Area</li>
				</ol>
			</div>
			
			<?php $state = $this->db->query("SELECT b.id,b.country_id,a.country_name,b.state_code,b.state_name,b.gst_code,b.union_territory,b.status from erpgcy a ,erpgste b WHERE a.id = b.country_id and b.status = '1' order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Area</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="stateAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="country">Country Name</th>
														<th data-field="stateId">State Name</th>
														<th data-field="stateDesc">City Name</th>
														<th data-field="gstCode">Area Code</th>
														<th data-field="union">Area Name</th>														
														<th data-field="action" data-formatter="actionFormatter"	data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>India</td>
														<td>Tamil Nadu</td>
														<td>Chennai</td>
														<td>CHT</td>
														<td>Chetpet</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>India</td>
														<td>Tamil Nadu</td>
														<td>Chennai</td>
														<td>CHT</td>
														<td>Chetpet</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>India</td>
														<td>Tamil Nadu</td>
														<td>Chennai</td>
														<td>CHT</td>
														<td>Chetpet</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>India</td>
														<td>Tamil Nadu</td>
														<td>Chennai</td>
														<td>CHT</td>
														<td>Chetpet</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>India</td>
														<td>Tamil Nadu</td>
														<td>Chennai</td>
														<td>CHT</td>
														<td>Chetpet</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">City</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country Name</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select  class=" form-control select2" name="countryid"  >
													<option value="select country">Select Country Name</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->country_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Name</label>
															
													<select class=" form-control select2" name="stateid"  >
													<option value="select country">Select State Name</option>
													<option>Tamil Nadu</option>
													<option>Andhra Pradesh</option>
													<option>Karnataka</option>
													<option>Kerala</option>
													<option>Telegana</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">City Name</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select City Name</option>
													<option>Chennai</option>
													<option>Madurai</option>
													<option>Trichy</option>
													<option>Thanjavur</option>
													<option>Thiruvallur</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Code</label> <input 
														type="text" class="form-control" id="statenameEdit" readonly="readonly" name="statename" value="<?php echo $row->state_name?>"
														maxlength="30"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Name</label> <input
														type="text" class="form-control" id="gstCodeEdit" name="gstcode" value="<?php echo $row->gst_code; ?>"
														maxlength="3">
												</div>									
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												<!-- <?php $i=0; foreach($state as $row) { ?>
												<tr>
												<td><?php echo $row->country_name ?></td>
												<td><?php echo $row->state_code?></td>
												<td><?php echo $row->state_name?></td>
												<td><?php echo $row->gst_code?></td>
												<td><?php echo $row->union_territory?></td>
												<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit<?php echo $i; ?>">
												                <i class="glyphicon glyphicon-pencil"></i></a></td>
												
						    <div class="country_info modal " id="StateEdit<?php echo $i; ?>" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">City</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country Code</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select  class=" form-control select2" name="countryid"  >
													<option value="select country">Select Country Code</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->country_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Code</label>
														<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select class=" form-control select2" name="stateid"  >
													<option value="select country">Select State Code</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->stste_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">City Code</label> 
														<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select City Code</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->stste_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Code</label> <input 
														type="text" class="form-control" id="statenameEdit" readonly="readonly" name="statename" value="<?php echo $row->state_name?>"
														maxlength="30"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Name</label> <input
														type="text" class="form-control" id="gstCodeEdit" name="gstcode" value="<?php echo $row->gst_code; ?>"
														maxlength="3">
												</div>									
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												
												
												</tr>
												<?php $i++; } ?> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="country_info modal " id="State" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
							
							<?php echo form_open(base_url('general/add_state'), array('id'=>'state','name'=>'state','autocomplete'=>'off')); ?>
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Area</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country Name</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>	
													<select id="country" class=" form-control select2" name="countryid"  >
													<option value="select country">Select Country Name</option>
													<?php foreach($cont as $row) { ?>
														<option value ="<?php echo $row->id; ?>"><?php echo $row->country_name; ?></option>
													<?php } ?>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Name</label>
															
													<select id="state" class=" form-control select2" name="stateid"  >
													<option value="select country">Select State Name</option>
													<option>Tamil Nadu</option>
													<option>Andhra Pradesh</option>
													<option>Karnataka</option>
													<option>Kerala</option>
													<option>Telegana</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">City Name</label> 
														
													<select id="city" class=" form-control select2" name="cityid">
													<option value="select country">Select City Name</option>
													<option>Chennai</option>
													<option>Madurai</option>
													<option>Trichy</option>
													<option>Thanjavur</option>
													<option>Thiruvallur</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Code</label> <input
														type="text" class="form-control" readonly="readonly"id="areaCode" name="gstcode"
														maxlength="3">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Area Name</label> <input
														type="text" class="form-control" id="statename"
														maxlength="30" name="statename"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												</div>
											</div>
										</form>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>

												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</section>