
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-Company').addClass('active');
	$('#Company').hide();

	$('#CompanyAdd').click(function() {
	$('#Company').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function(){
	$('#compCode').val('');
	$('#compName').val('');	

	});

});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Company</a></li>
					<li class="active">Company </li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">COMPANY </div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="CompanyAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="compGrpCode">Company Code</th>
														<th data-field="compGrpName">Company Name</th>
														<th data-field="compGrpCurr">Currency</th>
														<th data-field="compGrpLang">Language</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>1000</td>
														<td>Quadraerp</td>
														<td>INR</td>
														<td>English</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COMPANY
											</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general2">General</a></li>
											<li><a data-toggle="tab" href="#address2">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general2" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="row">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														 Code </label> <input type="text" class="form-control"
														id="compGrpCode" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														 Name </label> <input type="text" class="form-control"
														id="compGrpName" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Currency</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
												</div>
											</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Language</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Language</option>
													<option>English</option>
													<option>Tamil</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														 Image </label> <input type="file" class="form-control"
														id="compGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
											</div>
												
											</div>
										</div>
										<div id="address2" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="compGrpSave" type="button" style="margin-right: 5px">Save</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="Company" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COMPANY
											</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general3">General</a></li>
											<li><a data-toggle="tab" href="#address3">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general3" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="row">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Code </label> <input type="text" class="form-control"
														id="compGrpCode" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Name </label> <input type="text" class="form-control"
														id="compGrpName" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Currency</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
												</div>
											</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Language</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Language</option>
													<option>English</option>
													<option>Tamil</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														 Image </label> <input type="file" class="form-control"
														id="compGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
											</div>
												
											</div>
										</div>
										<div id="address3" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select City</option>
																<option>Chennai</option>
																<option>Madurai</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Area</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Area</option>
																<option>Guindy</option>
																<option>Adayar</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="compGrpSave" type="button" style="margin-right: 5px">Save</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="CompanyEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">COMPANY
											GROUP</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab"
												href="#generalEdit">General</a></li>
											<li><a data-toggle="tab" href="#addressEdit">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="generalEdit" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Code </label> <input type="text" class="form-control"
														id="compGrpCodeEdit" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Name </label> <input type="text" class="form-control"
														id="compGrpNameEdit" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label>Currency</label> <select id="compGrpCurrEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Language</label> <select id="compGrpLangEdit"
														class="form-control">
														<option value="-1">Select Language</option>
														<option value="EN">English</option>
														<option value="AR">Arab</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Company
														Group Image </label> <input type="file" class="form-control"
														id="compGrpImageEdit" onchange="readURL1(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah1" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
											</div>
										</div>
										<div id="addressEdit" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="form-group col-md-4">
													<label class="control-label">House No</label> <input
														type="text" class="form-control" id="houseNoEdit"
														maxlength="10">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street</label> <input
														type="text" class="form-control" id="streetEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street 1</label> <input
														type="text" class="form-control" id="street1Edit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street 2</label> <input
														type="text" class="form-control" id="street2Edit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Landmark</label> <input
														type="text" class="form-control" id="landmarkEdit"
														maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">City</label> <input
														type="text" class="form-control" id="cityEdit"
														maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label>Country</label> <select id="countryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>State</label> <select id="stateEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Pin Code</label> <input
														type="text" class="form-control" id="pincodeEdit"
														maxlength="6"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Mobile Number</label> <input
														type="text" class="form-control" id="mobileEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Telephone Number</label> <input
														type="text" class="form-control" id="telephoneEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Telephone Extension</label> <input
														type="text" class="form-control" id="teleExtenEdit"
														maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Email</label> <input
														type="text" class="form-control" id="emailEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Web Site</label> <input
														type="text" class="form-control" id="websiteEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Alternate Mobile</label> <input
														type="text" class="form-control" id="altMobEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Alternate Telephone</label> <input
														type="text" class="form-control" id="altTelEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="compGrpUpdate" type="button" style="margin-right: 5px">Update</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>