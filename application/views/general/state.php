

<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-state').addClass('active');
	$('#State').hide();

	$('#stateAdd').click(function() {
		$('#State').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">State</li>
				</ol>
			</div>
			
			<?php $state = $this->db->query("SELECT b.id,b.country_id,a.country_name,b.state_code,b.state_name,b.gst_code,b.union_territory,b.status from erpgcy a ,erpgste b WHERE a.id = b.country_id and b.status = '1' order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">STATE</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="stateAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="country">Country</th>
														<th data-field="stateId">State Code</th>
														<th data-field="stateDesc">State Name</th>
														<th data-field="gstCode">GST Code</th>
														<th data-field="union">Is Union Territory</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>INDIA</td>
														<td>TN</td>
														<td>Tamil nadu</td>
														<td>IGST</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>INDIA</td>
														<td>ADR</td>
														<td>Andrapradesh</td>
														<td>IGST</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>INDIA</td>
														<td>KL</td>
														<td>Kerala</td>
														<td>IGST</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>INDIA</td>
														<td>KA</td>
														<td>Karnataka</td>
														<td>IGST</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>INDIA</td>
														<td>BGN</td>
														<td>Bangalur</td>
														<td>IGST</td>
														<td>Yes</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
							
							<?php echo form_open(base_url('general/add_state'), array('id'=>'state','name'=>'state','autocomplete'=>'off')); ?>
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">STATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country</label>
													
													<select  class="form-control select2" name="countryid"  >
													<option value="select country">Select Country</option>
													
														<option value ="">India</option>
														<option value ="">America</option>
														<option value ="">Jappan</option>
														<option value ="">china</option>
													
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Code</label> <input
														type="text" class="form-control" id="statecode"
														maxlength="3" name="statecode"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Name</label> <input
														type="text" class="form-control" id="statename"
														maxlength="30" name="statename"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">GST Code </label> <input
														type="text" class="form-control" id="gstCode" name="gstcode"
														maxlength="3">
												</div>
												<div class="form-group col-md-6">
													<label>Union Territory </label>
													<div class="radio">
														<label> <input type="radio" name="load"
															id="empGen" value="T">YES
														</label> <label> <input type="radio" name="load"
															id="empGen" value="F">NO
														</label>
													</div>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>

												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
												<!-- <?php $i=0; foreach($state as $row) { ?>
												<tr>
												<td><?php echo $row->country_name ?></td>
												<td><?php echo $row->state_code?></td>
												<td><?php echo $row->state_name?></td>
												<td><?php echo $row->gst_code?></td>
												<td><?php echo $row->union_territory?></td>
												<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit<?php echo $i; ?>">
												                <i class="glyphicon glyphicon-pencil"></i></a></td>
												
						    <div class="country_info modal " id="StateEdit<?php echo $i; ?>" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">STATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country</label>
													<?php $cont = $this->db->query("select * from erpgcy")->result(); ?>
													<select  class="form-control select2" name="country_id">
													<?php foreach($cont as $rows) { ?>
														<option value ="<?php echo $rows->id; ?>" <?php if($rows->id == $row->country_id){ echo "selected"; } ?>><?php echo $rows->country_name; ?></option>
													<?php } ?>
														
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" readonly="readonly" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Name</label> <input 
														type="text" class="form-control" id="statenameEdit" name="statename" value="<?php echo $row->state_name?>"
														maxlength="30"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">GST Code </label> <input
														type="text" class="form-control" id="gstCodeEdit" name="gstcode" value="<?php echo $row->gst_code; ?>"
														maxlength="3">
												</div>
												<div class="form-group col-md-6">
													<label>Union Territory </label>
													<div class="radio">
														<label> <input type="radio" name="load"
															id="empGen" value="T" <?php if($row->union_territory == 'T'){ echo"checked"; } ?>>YES
														</label> <label> <input type="radio" name="load"
															id="empGen" value="F" <?php if($row->union_territory == 'F'){ echo"checked"; } ?>>NO
														</label>
													</div>
												</div>
												
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												
												
												</tr>
												<?php $i++; } ?> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="country_info modal " id="State" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">STATE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Country</label>
													<select id="country" class="form-control select2" name="countryid"  >
													<option value="select country">Select Country</option>
														<option value ="">India</option>
														<option value ="">America</option>
													</select>
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">State Code</label> <input
														type="text" class="form-control" id="statecode"
														maxlength="3" name="statecode"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">State Name</label> <input
														type="text" class="form-control" id="statename"
														maxlength="30" name="statename"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">GST Code </label> <input
														type="text" class="form-control" id="gstCode" name="gstcode"
														maxlength="3">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Union Territory </label>
													<div class="radio">
														<label> <input type="radio" name="load"
															id="empGen" value="T">YES
														</label> <label> <input type="radio" name="load"
															id="empGen" value="F">NO
														</label>
													</div>
												</div>
											</div>
											</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</section>