
<script>
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-branch').addClass('active');
	$('#Branch').hide();

	$('#BranchAdd').click(function() {
	$('#Branch').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function() {
	$('#branchCode').val('');
	$('#branchName').val('');
	$('#gst').val('');	
	});

	$('#country_Reset1').click(function() {
	$('#houseNo').val('');
	$('#street').val('');
	$('#street1').val('');
	$('#street2').val('');
	$('#landmark').val('');
	$('#city').val('');
	$('#pincode').val('');
	$('#mobile').val('');
	$('#telephone').val('');
	$('#teleExten').val('');
	$('#email').val('');
	$('#website').val('');
	$('#altMob').val('');
	$('#altTel').val('');
	

	});

	});

</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Company</a></li>
					<li class="active">Branch</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">BRANCH</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="BranchAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="branchCode">Branch Code</th>
														<th data-field="branchName">Branch Name</th>
														<th data-field="branchCurr">Currency</th>
														<th data-field="branchLang">Language</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tr>
														<tr>
														<td>1000</td>
														<td>General</td>
														<td>INR</td>
														<td>English</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BRANCH
										</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general">General</a></li>
											<li><a data-toggle="tab" href="#address">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="row">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Code </label> <input type="text" class="form-control"
														id="branchCode" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Name </label> <input type="text" class="form-control"
														id="branchName" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Currency</label>
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
												</div>
												</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Language</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Language</option>
													<option>English</option>
													<option>Tamil</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">GST
														Number </label> <input type="text" class="form-control" id="gst"
														maxlength="15">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">GST Type</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Gst Type</option>
													<option>Regular</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Status</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>Active</option>
													<option>InActive</option>
													</select>
												</div>
											</div>
												<div class="form-group col-md-4">
													<label>IS Plant</label>
													<div class="radio">
														<label> <input type="radio" name="load" id="plant"
															value="T">YES
														</label> <label> <input type="radio" name="load"
															id="plant" value="F">NO
														</label>
													</div>
												</div>
												<div class="form-group col-md-4">
													<label>IS Depot</label>
													<div class="radio">
														<label> <input type="radio" name="load1"
															id="depot" value="T">YES
														</label> <label> <input type="radio" name="load1"
															id="depot" value="F">NO
														</label>
													</div>
												</div>
												<div class="form-group col-md-4">
													<label>IS Warehouse</label>
													<div class="radio">
														<label> <input type="radio" name="load2" id="ware"
															value="T">YES
														</label> <label> <input type="radio" name="load2"
															id="ware" value="F">NO
														</label>
													</div>
												</div>
											</div>
										</div>
										<div id="address" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset1" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="branchSave" type="button" style="margin-right: 5px">Save</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="Branch" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BRANCH
										</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#general5">General</a></li>
											<li><a data-toggle="tab" href="#address5">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="general5" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="row">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Code </label> <input type="text" class="form-control"
														id="branchCode" maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Name </label> <input type="text" class="form-control"
														id="branchName" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Currency</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Currency</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
												</div>
												</div>
												<div class="row">
												<div class="form-group col-md-4">
													<label class="control-label">Language</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Language</option>
													<option>English</option>
													<option>Tamil</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">GST
														Number </label> <input type="text" class="form-control" id="gst"
														maxlength="15">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">GST Type</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Gst Type</option>
													<option>Regular</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Status</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>Active</option>
													<option>InActive</option>
													</select>
												</div>
											</div>
												<div class="form-group col-md-4">
													<label>IS Plant</label>
													<div class="radio">
														<label> <input type="radio" name="load" id="plant"
															value="T">YES
														</label> <label> <input type="radio" name="load"
															id="plant" value="F">NO
														</label>
													</div>
												</div>
												<div class="form-group col-md-4">
													<label>IS Depot</label>
													<div class="radio">
														<label> <input type="radio" name="load1"
															id="depot" value="T">YES
														</label> <label> <input type="radio" name="load1"
															id="depot" value="F">NO
														</label>
													</div>
												</div>
												<div class="form-group col-md-4">
													<label>IS Warehouse</label>
													<div class="radio">
														<label> <input type="radio" name="load2" id="ware"
															value="T">YES
														</label> <label> <input type="radio" name="load2"
															id="ware" value="F">NO
														</label>
													</div>
												</div>
											</div>
										</div>
										<div id="address5" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="col-md-6 ">
															<form class="form-horizontal">
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">House No</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street1</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Street2</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Landmark</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">City</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Country</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Country</option>
																<option>India</option>
																<option>America</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">State</label> 
																<div class="col-md-9">
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select State</option>
																<option>Tamil Nadu</option>
																<option>Karnataka</option>
																</select>
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-3">Pin Code</label>
																<div class="col-md-9"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
														<div class="col-md-6">
															<form class="form-horizontal">
																<div class="form-group col-md-12">
																<label class="control-label col-md-4">Mobile Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Number</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Telephone Extension</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Email</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Web Site</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Mobile</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Alternate Telephone</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
															<div class="form-group col-md-12">
																<label class="control-label col-md-4">Contact Person</label>
																<div class="col-md-8"> 
																	<input type="text" class="form-control" id="houseNo" maxlength="10">
																</div>
															</div>
														</form>
														</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset1" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="branchSave" type="button" style="margin-right: 5px">Save</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div> 

						<!-- <div class="country_info modal " id="BranchEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BRANCH
										</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab"
												href="#generalEdit">General</a></li>
											<li><a data-toggle="tab" href="#addressEdit">Address</a></li>
										</ul>
									</div>
									<div class="tab-content">
										<div id="generalEdit" class="tab-pane fade in active">
											<div class="modal-body" style="height: 350px">
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Code </label> <input type="text" class="form-control"
														id="branchCodeEdit" maxlength="4" readonly
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Branch
														Name </label> <input type="text" class="form-control"
														id="branchNameEdit" maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label>Currency</label> <select id="branchCurrEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Language</label> <select id="branchLangEdit"
														class="form-control">
														<option value="-1">Select Language</option>
														<option value="EN">English</option>
														<option value="AR">Arab</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">GST
														Number </label> <input type="text" class="form-control"
														id="gstEdit" maxlength="15">
												</div>
												<div class="form-group col-md-4">
													<label>GST Type</label> <select id="gstTypeEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>IS Plant</label> <select id="plantEdit"
														class="form-control">
														<option value="0">True</option>
														<option value="1">False</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>IS Depot</label> <select id="depotEdit"
														class="form-control">
														<option value="0">True</option>
														<option value="1">False</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>IS Warehouse</label> <select id="wareEdit"
														class="form-control">
														<option value="0">True</option>
														<option value="1">False</option>
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Status</label> <select id="statusEdit"
														class="form-control">
														<option value="A">Active</option>
														<option value="I">In-Active</option>
													</select>
												</div>
											</div>
										</div>
										<div id="addressEdit" class="tab-pane fade">
											<div class="modal-body" style="height: 430px">
												<div class="form-group col-md-4">
													<label class="control-label">House No</label> <input
														type="text" class="form-control" id="houseNoEdit"
														maxlength="10">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street</label> <input
														type="text" class="form-control" id="streetEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street 1</label> <input
														type="text" class="form-control" id="street1Edit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Street 2</label> <input
														type="text" class="form-control" id="street2Edit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Landmark</label> <input
														type="text" class="form-control" id="landmarkEdit"
														maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">City</label> <input
														type="text" class="form-control" id="cityEdit"
														maxlength="50">
												</div>
												<div class="form-group col-md-4">
													<label>Country</label> <select id="countryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>State</label> <select id="stateEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Pin Code</label> <input
														type="text" class="form-control" id="pincodeEdit"
														maxlength="6"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Mobile Number</label> <input
														type="text" class="form-control" id="mobileEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Telephone Number</label> <input
														type="text" class="form-control" id="telephoneEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Telephone Extension</label> <input
														type="text" class="form-control" id="teleExtenEdit"
														maxlength="4"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Email</label> <input
														type="text" class="form-control" id="emailEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Web Site</label> <input
														type="text" class="form-control" id="websiteEdit"
														maxlength="100">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Alternate Mobile</label> <input
														type="text" class="form-control" id="altMobEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label class="control-label">Alternate Telephone</label> <input
														type="text" class="form-control" id="altTelEdit"
														maxlength="15"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
											</div>
										</div>

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="branchUpdate" type="button" style="margin-right: 5px">Update</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</section>