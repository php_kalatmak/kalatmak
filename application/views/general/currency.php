
<script type="text/javascript">
	$(document).ready(function() {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-currency').addClass('active');
	$('#Currency').hide();
	$('#CurrencyEdit').hide();

	$('#currencyAdd').click(function() {
	$('#Currency').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function() {
	$('#currencycode').val('');
	$('#currencyname').val('');
	$('#intercode').val('');
	$('#intername').val('');
	$('#decimalLimit').vla('');	

	});

});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Currency</li>
				</ol>
			</div>
			<?php $currency = $this->db->query("select * from erpgcrc order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CURRENCY</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="currencyAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th>Currency Code</th>
														<th>Currency Name</th>
														<th>International Code</th>
														<th>International Name</th>
														<th>Decimal Limit</th>
														<th>Currency Symbol</th>
														<th>Edit</th>
													</tr>
												</thead>
												<tbody>
													<?php $i=0; foreach($currency as $row) { ?>
													<tr>
													<td><?php echo $row->currency_code; ?></td>
													<td><?php echo $row->currency_name; ?></td>
													<td><?php echo $row->international_code; ?></td>
													<td><?php echo $row->international_name; ?></td>
													<td><?php echo $row->international_name; ?></td>
													<td><?php echo $row->decimal_limit; ?></td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#CurrencyEdit<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													
						<div class="country_info modal " id="CurrencyEdit<?php echo $i; ?>" tabindex="-1" 
						role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/update_currency')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CURRENCY</h4>
									</div>
									<div class="modal-body" style="height: 250px">

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Currency
												Code</label> <input type="text" class="form-control" name="currencycode"
												id="currencycodeEdit" maxlength="3" readonly="readonly" value="<?php echo $row->currency_code; ?>"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Currency
												Name</label> <input type="text" class="form-control" name="currencyname"
												id="currencynameEdit" maxlength="30" value="<?php echo $row->currency_name; ?>"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">International
												Code</label> <input type="text" class="form-control" name="internationalcode"
												id="intercodeEdit" maxlength="3" value="<?php echo $row->international_code; ?>"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">International
												Name</label> <input type="text" class="form-control" name="internationalname"
												id="internameEdit" maxlength="30" value="<?php echo $row->international_name; ?>"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Decimal
												Limit </label> <input type="text" class="form-control" name="decimallimit"
												id="decimalLimitEdit" maxlength="1" value="<?php echo $row->decimal_limit; ?>"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Currency
												Symbol </label> <input type="text" class="form-control" name="decimallimit"
												id="decimalLimitEdit" maxlength="1" value="<?php echo $row->decimal_limit; ?>"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
											</tbody>
												<?php $i++; } ?>	
													
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
							

						<div class="country_info modal " id="Currency" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/add_currency'), array('id'=>'currency','name'=>'currency','autocomplete'=>'off')); ?>
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CURRENCY</h4>
									</div>
									<div class="modal-body" style="height: 250px">

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Currency
												Code</label> <input type="text" class="form-control"
												id="currencycode" maxlength="3" name="currency_code"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Currency
												Name</label> <input type="text" class="form-control"
												id="currencyname" maxlength="30" name ="currency_name"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">International
												Code</label> <input type="text" class="form-control" id="intercode"
												maxlength="3" name="international_code"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">International
												Name</label> <input type="text" class="form-control" id="intername"
												maxlength="30" name="international_name"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Decimal
												Limit </label> <input type="text" class="form-control"
												id="decimalLimit" maxlength="1" name="decimal_limit"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Currency
												Symbol </label> <input type="text" class="form-control" name="decimallimit"
												id="decimalLimitEdit" maxlength="1" value="<?php echo $row->decimal_limit; ?>"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
											<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">	

										</div>

									</div>
									</form>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</section>