
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionTableCreation').addClass('active');


	$('#conditionTableCreationAdd').click(function() {
	$('#ConditionTableCreation').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function() {
	$('#tab').val('');	
	})
});	
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Condition Table Creation</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CONDITION TABLE CREATION</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success"
												id="conditionTableCreationAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="tab">Tab</th>
														<th data-field="country">Country</th>
														<th data-field="state">State</th>
														<th data-field="business">Business Partner</th>
														<th data-field="company">Company</th>
														<th data-field="hsn">HSN / SAC</th>
														<th data-field="item">Item</th>
														<th data-field="itemGrp">Item Group</th>
														<th data-field="uom">UOM</th>
														<th data-field="amtper">Amount / Percentage</th>
														<!-- <th data-field="fromDate">From Date</th>
														<th data-field="toDate">To Date</th> -->
														<th data-field="status">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionTableCreation"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TABLE CREATION</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Tab Code</label> <input type="text" id="tab"
														maxlength="3" class="form-control"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="country"><b>
														Country </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="state"><b> State </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="business"><b>
														Business Partner </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="company"><b>
														Company </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="hsn"><b> HSN / SAC
													</b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="item"><b> Item </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="itemGrp"><b> Item
														Group </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="uom"><b> UOM </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="amtper"><b> Amount
														/ Percentage </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="fromDate"><b> From
														Date </b>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="toDate"><b> To
														Date </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="status"><b> Status
													</b><br>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTableCreationSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionTableCreationEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TABLE CREATION</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Tab Code</label> <input type="text" id="tabEdit"
														maxlength="3" class="form-control" readonly="readonly"
														onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="countryEdit"><b>
														Country </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="stateEdit"><b>
														State </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="businessEdit"><b>
														Business Partner </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="companyEdit"><b>
														Company </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="hsnEdit"><b> HSN /
														SAC </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="itemEdit"><b> Item
													</b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="itemGrpEdit"><b>
														Item Group </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="uomEdit"><b> UOM </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="amtperEdit"><b>
														Amount / Percentage </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="fromDateEdit"><b>
														From Date </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="toDateEdit"><b> To
														Date </b><br>
												</div>
												<div class="form-group col-md-6">
													<input type="checkbox" name="statusEdit"><b>
														Status </b><br>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTableCreationUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>