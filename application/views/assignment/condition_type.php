
<script type="text/javascript">
	
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionType').addClass('active');


	$('#conditionTypeAdd').click(function() {
		$('#ConditionType').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	$('#country_Reset').click(function(){
		$('#condType').val('');
		$('#condName').val('');
	})
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Condition Type</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CONDITION TYPE</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="conditionTypeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="condType">Condition Type</th>
														<th data-field="condName">Condition Name</th>
														<th data-field="condSign">Condition Sign</th>
														<th data-field="scaleBasic">Scale Basic</th>
														<th data-field="headerItem">Header Item</th>
														<th data-field="strtSlab">Straight Slab</th>
														<th data-field="amtper">Amount / Percentage</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionType" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Condition Type</label> <input type="text" maxlength="4"
														id="condType" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Name</label> <input type="text" maxlength="20"
														id="condName" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Sign</label> <select id="condSign"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Scale Basic</label> <select id="scaleBasic"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Header Item</label> <select id="headerItem"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Straight Slab</label> <select id="strtSlab"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount / Percentage</label> <select id="amtper"
														class="form-control">
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTypeSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionTypeEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Condition Type</label> <input type="text" maxlength="4"
														readonly="readonly" id="condTypeEdit" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Name</label> <input type="text" maxlength="20"
														id="condNameEdit" class="form-control">
												</div>
												<div class="form-group col-md-6">
													<label>Condition Sign</label> <select id="condSignEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Scale Basic</label> <select id="scaleBasicEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Header Item</label> <select id="headerItemEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Straight Slab</label> <select id="strtSlabEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Amount / Percentage</label> <select id="amtperEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-6">
													<label>Status</label> <select id="statusEdit"
														class="form-control">
														<option value="-1">Select Status</option>
														<option value="L">Active</option>
														<option value="X">In-Active</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionTypeUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>