
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-sequenceMapping').addClass('active');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Sequence Mapping</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">SEQUENCE MAPPING</div>
										<div class="panel-body">
											<div class="form-group col-md-6">
												<label>Condition</label> <select id="condition"
													class="form-control">
												</select>
											</div>
											<div class="form-group col-md-6">
												<label class="col-md-12">Tab</label>
												<div class="col-md-12">
													<select id="tab_map" multiple="multiple" style=""
														class="multiselect-ui form-control">
													</select>
												</div>
											</div>
										</div>
										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="country_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="saveAssgn" type="button" style="margin-right: 5px">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>