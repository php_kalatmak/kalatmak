<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionMaster').addClass('active');


	$('#conditionMasterAdd').click(function() {
		$('#ConditionMaster').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	$('#country_Reset').click(function(){
		$('#hsn').val('');
		$('#value').val('');
	})
});
</script>	


<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Assignments</a></li>
					<li class="active">Condition Master</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">CONDITION MASTER</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success"
												id="conditionMasterAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="condType">Condition Type</th>
														<th data-field="tab">Tab</th>
														<th data-field="country">Country</th>
														<th data-field="state">State</th>
														<th data-field="business">Business Partner</th>
														<th data-field="company">Company</th>
														<th data-field="hsn">HSN / SAC</th>
														<th data-field="item">Item</th>
														<th data-field="itemGrp">Item Group</th>
														<th data-field="uom">UOM</th>
														<th data-field="amtper">Amount / Percentage</th>
														<th data-field="fromDate">From Date</th>
														<th data-field="toDate">To Date</th>
														<th data-field="status">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionMaster"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											MASTER</h4>
									</div>
									<div class="modal-body" style="height: 400px">
										<form>
											<div class="row">
												<div class="form-group col-md-4">
													<label>Condition Type</label> <select id="condType"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Tab</label> <select id="tab" class="form-control">
														<option value="-1">Select Tab</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="countryDiv">
													<label>Country</label> <select id="country"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="stateDiv">
													<label>State</label> <select id="state"
														class="form-control">
														<option value="-1">Select State</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="businessDiv">
													<label>Business Partner</label> <select id="business"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="compDiv">
													<label>Company</label> <select id="comp"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="hsnDiv">
													<label>HSN /SAC</label> <input type="text" id="hsn"
														maxlength="19" class="form-control">
												</div>
												<div class="form-group col-md-4" id="itemDiv">
													<label>Item</label> <select id="item" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="itemGrpDiv">
													<label>Item Group</label> <select id="itemGrp"
														class="form-control">
													</select>
												</div>

												<div class="form-group col-md-4" id="uomDiv">
													<label>UOM</label> <select id="uom" class="form-control">
													</select>
												</div>
												<!-- <div class="form-group col-md-4" id="amtperDiv">
													<label>Amount / Percentage</label> <select id="amtper"
														class="form-control">
														<option value="-1">Select Amount / Percentage</option>
														<option value="A">Amount</option>
														<option value="P">Percentage</option>
													</select>
												</div> -->
												<div class="form-group col-md-4" id="valueDiv">
													<label>Amount / Percentage</label> <input type="text"
														id="value" maxlength="9" class="form-control"
														onkeyup="this.value = this.value.replace(/[^0-9 . ]/, '')">
												</div>
												<div class="form-group col-md-4" id="fromDateDiv">
													<label>From Date</label> <input type="text" id="fromDate"
														class="form-control">
												</div>
												<div class="form-group col-md-4" id="toDateDiv">
													<label>To Date</label> <input type="text" id="toDate"
														class="form-control">
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionMasterSave" type="button"
												style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="ConditionMasterEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">CONDITION
											MASTER</h4>
									</div>
									<div class="modal-body" style="height: 400px">
										<form>
											<div class="row">
												<div class="form-group col-md-4">
													<label>Condition Type</label> <select id="condTypeEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Tab</label> <select id="tabEdit"
														class="form-control">
														<option value="-1">Select Tab</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="countryDiv">
													<label>Country</label> <select id="countryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="stateDiv">
													<label>State</label> <select id="stateEdit"
														class="form-control">
														<option value="-1">Select State</option>
													</select>
												</div>
												<div class="form-group col-md-4" id="businessDiv">
													<label>Business Partner</label> <select id="businessEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="compDiv">
													<label>Company</label> <select id="compEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="hsnDiv">
													<label>HSN /SAC</label> <input type="text" id="hsnEdit"
														maxlength="19" class="form-control">
												</div>
												<div class="form-group col-md-4" id="itemDiv">
													<label>Item</label> <select id="itemEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4" id="itemGrpDiv">
													<label>Item Group</label> <select id="itemGrpEdit"
														class="form-control">
													</select>
												</div>

												<div class="form-group col-md-4" id="uomDiv">
													<label>UOM</label> <select id="uomEdit"
														class="form-control">
													</select>
												</div>
												<!-- <div class="form-group col-md-4" id="amtperDiv">
													<label>Amount / Percentage</label> <select id="amtperEdit"
														class="form-control">
														<option value="-1">Select Amount / Percentage</option>
														<option value="A">Amount</option>
														<option value="P">Percentage</option>
													</select>
												</div> -->
												<div class="form-group col-md-4" id="fromDateDiv">
													<label>From Date</label> <input type="text"
														id="fromDateEdit" class="form-control">
												</div>
												<div class="form-group col-md-4" id="toDateDiv">
													<label>To Date</label> <input type="text" id="toDateEdit"
														class="form-control">
												</div>
												<div class="form-group col-md-4" id="valueDiv">
													<label>Amount / Percentage</label> <input type="text"
														id="valueEdit" maxlength="9" class="form-control"
														onkeyup="this.value = this.value.replace(/[^0-9 . ]/, '')">
												</div>
												<div class="form-group col-md-4">
													<label>Status</label> <select id="statusEdit"
														class="form-control">
														<option value="-1">Select Status</option>
														<option value="L">Active</option>
														<option value="X">In-Active</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ConditionMasterUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>