<script type="text/javascript">
	$('#menu-financial').addClass('active');
	$('#menu-financial > a').attr('aria-expanded', 'true');
	$('#menu-financial > ul').addClass('in');

	$('#menu-chartofaccounts').addClass('active');
	$('#menu-chartofaccounts > a').attr('aria-expanded', 'true');
	$('#menu-chartofaccounts > ul').addClass('in');
</script>
<style type="text/css">
/*!
 * bootstrap-vertical-tabs - v1.1.0
 * https://dbtek.github.io/bootstrap-vertical-tabs
 * 2014-06-06
 * Copyright (c) 2014 Ä°smail Demirbilek
 * License: MIT
 */
.tabs-right {
  border-bottom: none;
  padding-top: 2px;
}
.tabs-right {
  border-left: 1px solid #ddd;
}
.tabs-right>li {
  float: none;
  margin-bottom: 2px;
}
.tabs-right>li {
  margin-left: -1px;
      float: none;
    margin-bottom: 2px;
    background: linen;
    width: 120px;
}
.tabs-right>li.active>a,
.tabs-right>li.active>a:hover,
.tabs-right>li.active>a:focus {
  border-bottom: 1px solid #ddd;
  border-left-color: transparent;
}
.tabs-right>li>a {
  border-radius: 0 4px 4px 0;
  margin-right: 0;
}
.vertical-text {
  margin-top:50px;
  border: none;
  position: relative;
}
.vertical-text>li {
  height: 20px;
  width: 120px;
  margin-bottom: 100px;
}
.vertical-text>li>a {
  border-bottom: 1px solid #ddd;
  border-right-color: transparent;
  text-align: center;
  border-radius: 4px 4px 0px 0px;
}
.vertical-text>li.active>a,
.vertical-text>li.active>a:hover,
.vertical-text>li.active>a:focus {
  border-bottom-color: transparent;
  border-right-color: #ddd;
  border-left-color: #ddd;
}
.vertical-text.tabs-left {
  left: -50px;
}
.vertical-text.tabs-right {
  right: -50px;
}
.vertical-text.tabs-right>li {
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  -o-transform: rotate(90deg);
  transform: rotate(90deg);
}
.vertical-text.tabs-left>li {
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  -o-transform: rotate(-90deg);
  transform: rotate(-90deg);
}
</style>
</style>
<section class="content">
			<div class="page-heading">
				<h1>FINANCIAL</h1>
				<ol class="breadcrumb">
					<li><a href="">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">BOM</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Chart Of Accounts</div>
										<div class="panel-body">
											<form class="form-vertical">
												<div class="form-group">
													<div class="col-md-4">
													</div>
												    <div class="col-md-8">
														<div class="row">
														    <div  class="col-sm-12">
														        <div class="col-xs-6 col-xs-offset-4">
														            <!-- Tab panes -->
														            <div class="tab-content">
														                <div class="tab-pane active" id="1">
													                	<ul>
													                		<li>Asset 1
														                		<ul>
														                			<li>Sub Asset 1</li>
														                			<li>Sub Asset 2</li>
														                			<li>Sub Asset 3</li>
														                		</ul>
														                	</li>
													                		<li>Asset 2
													                			<ul>
																					<li>Sub Asset 2-1</li>
														                			<li>Sub Asset 2-2</li>
														                			<li>Sub Asset 2-3</li>
														                		</ul>
													                		</li>
													                		<li>Asset 3
														                		<ul>
														                			<li>Sub Asset 3-1</li>
														                			<li>Sub Asset 3-2</li>
														                			<li>Sub Asset 3-3</li>
														                		</ul>
														                	</li>
													                	</ul>
														                </div>
														                <div class="tab-pane" id="2">
													                	<ul>
													                		<li>Liability 1
														                		<ul>
														                			<li>Liability 1</li>
														                			<li>Liability 2</li>
														                			<li>Liability 3</li>
														                		</ul>
														                	</li>
													                		<li>Liability 2
													                			<ul>
																					<li>Liability 2-1</li>
														                			<li>Liability 2-2</li>
														                			<li>Liability 2-3</li>
														                		</ul>
													                		</li>
													                		<li>Liability 3
														                		<ul>
														                			<li>Liability 3-1</li>
														                			<li>Liability 3-2</li>
														                			<li>Liability 3-3</li>
														                		</ul>
														                	</li>
													                	</ul>
														                </div>
														                <div class="tab-pane" id="3">
													                	<ul>
													                		<li>Asset 1
														                		<ul>
														                			<li>Sub Asset 1</li>
														                			<li>Sub Asset 2</li>
														                			<li>Sub Asset 3</li>
														                		</ul>
														                	</li>
													                		<li>Asset 2
													                			<ul>
																					<li>Sub Asset 2-1</li>
														                			<li>Sub Asset 2-2</li>
														                			<li>Sub Asset 2-3</li>
														                		</ul>
													                		</li>
													                		<li>Asset 3
														                		<ul>
														                			<li>Sub Asset 3-1</li>
														                			<li>Sub Asset 3-2</li>
														                			<li>Sub Asset 3-3</li>
														                		</ul>
														                	</li>
													                	</ul>	
														                </div>
														                <div class="tab-pane" id="4">
													                	<ul>
													                		<li>Asset 1
														                		<ul>
														                			<li>Sub Asset 1</li>
														                			<li>Sub Asset 2</li>
														                			<li>Sub Asset 3</li>
														                		</ul>
														                	</li>
													                		<li>Asset 2
													                			<ul>
																					<li>Sub Asset 2-1</li>
														                			<li>Sub Asset 2-2</li>
														                			<li>Sub Asset 2-3</li>
														                		</ul>
													                		</li>
													                		<li>Asset 3
														                		<ul>
														                			<li>Sub Asset 3-1</li>
														                			<li>Sub Asset 3-2</li>
														                			<li>Sub Asset 3-3</li>
														                		</ul>
														                	</li>
													                	</ul>    	
														                </div>
														                <div class="tab-pane" id="5">
													                	<ul>
													                		<li>Asset 1
														                		<ul>
														                			<li>Sub Asset 1</li>
														                			<li>Sub Asset 2</li>
														                			<li>Sub Asset 3</li>
														                		</ul>
														                	</li>
													                		<li>Asset 2
													                			<ul>
																					<li>Sub Asset 2-1</li>
														                			<li>Sub Asset 2-2</li>
														                			<li>Sub Asset 2-3</li>
														                		</ul>
													                		</li>
													                		<li>Asset 3
														                		<ul>
														                			<li>Sub Asset 3-1</li>
														                			<li>Sub Asset 3-2</li>
														                			<li>Sub Asset 3-3</li>
														                		</ul>
														                	</li>
													                	</ul>
														                </div>
														            </div>
														        </div>
														        <div class="col-xs-2">
													            	<!-- required for floating -->
													            	<!-- Nav tabs -->
													            	<ul class="nav nav-tabs tabs-right">
													                	<li class="active"><a href="#1" data-toggle="tab">Asset</a></li>
													                	<li><a href="#2" data-toggle="tab">Liability</a></li>
													                	<li><a href="#3" data-toggle="tab">Equity</a></li>
													                	<li><a href="#4" data-toggle="tab">Revenue</a></li>
													                	<li><a href="#5" data-toggle="tab">Expenditure</a></li>
													            	</ul>
														        </div>
														    </div>
														</div>
												    </div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>