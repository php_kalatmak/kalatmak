
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-gl').addClass('active');
	$('#menu-gl > a').attr('aria-expanded', 'true');
	$('#menu-gl > ul').addClass('in');

	$('#menu-glDeterRule').addClass('active');
	$('#GLRule').hide();

	$('#GLRuleAdd').click(function() {
		$('#GLRule').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>	
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">GL Determination</a></li>
					<li class="active">GL Determination Rule</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">GL DETERMINATION RULE</div>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab"
												href="#salesDisplay">Sales</a></li>
											<li><a data-toggle="tab" href="#purchaseDisplay">Purchase</a></li>
											<li><a data-toggle="tab" href="#generalDisplay">General</a></li>
											<li><a data-toggle="tab" href="#inventoryDisplay">Inventory</a></li>
											<li><a data-toggle="tab" href="#resourceDisplay">Resource</a></li>
											<li
												style="float: right; padding-top: 5px; margin-right: 16px;"><button
													class="btn btn-sm btn-success" id="GLRuleAdd">New</button></li>
										</ul>
										<div class="tab-content">
											<div id="salesDisplay" class="tab-pane fade in active">
												<div class="panel-body table-responsive">

													<br clear="left">
													<table
														class="table table-striped table-hover js-exportable dataTable"
														id="salesDisplayTable" data-search=true>
														<thead>
															<tr>
																<th data-field="company">Company</th>
																<th data-field="branch">Branch</th>
																<th data-field="ordType">Order Type</th>
																<th data-field="procType">Procedure Type</th>
																<th data-field="action" data-formatter="actionFormatter"
																	data-events="actionEvents">Edit</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td>OrderType1</td>
																<td>PT1</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td>OrderType2</td>
																<td>PT2</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															
															</tbody>
													</table>
														<div class="country_info modal " id="StateEdit" tabindex="-1"
															role="dialog" aria-labelledby="EmployementTypeLabel">
															<div class="modal-dialog">
																<div class="modal-content" style="margin-top: 25%">
																	<div class="modal-header">
																		<button type="button" class="close " data-dismiss="modal">
																			<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																		</button>
																		<h4 class="modal-title" id="EmployementTypeLabel">GL DETERMINATION RULE
																		</h4>
																	</div>
																	<div class="modal-body" style="height: 250px">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th scope="col">Key Code</th>
																					<th scope="col">Key Name</th>
																					<th scope="col">Condition</th>
																					<th scope="col">Account Code</th>
																				</tr>
																				<tr>
																					<td>10430</td>
																					<td>Domestic</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>102340</td>
																					<td>International</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>1012430</td>
																					<td>Forign</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>10023</td>
																					<td>Domestic</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																			</thead>
																		</table>
																	</div>
																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="salepurTypesave" type="button" style="margin-right: 5px">Save</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
												</div>
											</div>
											<div id="purchaseDisplay" class="tab-pane fade">
												<div class="panel-body table-responsive">
													<br clear="left">
													<table
														class="table table-striped table-hover js-exportable dataTable"
														id="purchaseDisplayTable" data-search=true>
														<thead>
															<tr>
																<th data-field="company">Company</th>
																<th data-field="branch">Branch</th>
																<th data-field="ordType">Order Type</th>
																<th data-field="procType">Procedure Type</th>
																<th data-field="action"
																	data-formatter="actionFormatter1"
																	data-events="actionEvents">Edit</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td>OrderType1</td>
																<td>PT1</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit1">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td>OrderType2</td>
																<td>PT2</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit1">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
											<div id="generalDisplay" class="tab-pane fade">
												<div class="panel-body table-responsive">
													<br clear="left">
													<table
														class="table table-striped table-hover js-exportable dataTable"
														id="generalDisplayTable" data-search=true>
														<thead>
															<tr>
																<th data-field="company">Company</th>
																<th data-field="branch">Branch</th>
																<th data-field="action"
																	data-formatter="actionFormatter2"
																	data-events="actionEvents">Edit</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit2">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit2">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
											<div id="inventoryDisplay" class="tab-pane fade">
												<div class="panel-body table-responsive">
													<br clear="left">
													<table
														class="table table-striped table-hover js-exportable dataTable"
														id="inventoryDisplayTable" data-search=true>
														<thead>
															<tr>
																<th data-field="company">Company</th>
																<th data-field="itemGrp">Item Group</th>
																<th data-field="action"
																	data-formatter="actionFormatter4"
																	data-events="actionEvents">Edit</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Quadraerp</td>
																<td>IG1</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit3">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															<tr>
																<td>Quadraerp</td>
																<td>IG2</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit3">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
											<div id="resourceDisplay" class="tab-pane fade">
												<div class="panel-body table-responsive">
													<br clear="left">
													<table
														class="table table-striped table-hover js-exportable dataTable"
														id="resourceDisplayTable" data-search=true>
														<thead>
															<tr>
																<th data-field="company">Company</th>
																<th data-field="branch">Branch</th>
																<th data-field="action"
																	data-formatter="actionFormatter3"
																	data-events="actionEvents">Edit</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit4">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															<tr>
																<td>Quadraerp</td>
																<td>General</td>
																<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit4">
															                <i class="glyphicon glyphicon-pencil"></i></a></td>
															</tr>
															
														</tbody>
													</table>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="GLRule" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
										<ul class="nav nav-tabs">
											<li class="active"><a data-toggle="tab" href="#sales">Sales</a></li>
											<li><a data-toggle="tab" href="#purchase">Purchase</a></li>
											<li><a data-toggle="tab" href="#general">General</a></li>
											<li><a data-toggle="tab" href="#inventory">Inventory</a></li>
											<li><a data-toggle="tab" href="#resource">Resource</a></li>
										</ul>
									</div>
									<div class="tab-content">

										<div id="sales" class="tab-pane fade in active">
											<div class="modal-body" style="height: 400px">
												<div class="form-group col-md-3">
													<label>Company</label> <select id="companySales"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Branch</label> <select id="branchSales"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Order Type</label> <select id="ordTypeSales"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Procedure Type</label> <select id="procedureSales"
														class="form-control">
													</select>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>Condition Type</label> <select id="condType"
														class="form-control">
													</select>
												</div> -->
												<div class="form-group col-md-12">
													<div id="salesTable"></div>
												</div>
											</div>
										</div>
										<div id="purchase" class="tab-pane fade">
											<div class="modal-body" style="height: 400px">
												<div class="form-group col-md-3">
													<label>Company</label> <select id="companyPurchase"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Branch</label> <select id="branchPurchase"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Order Type</label> <select id="ordTypePurchase"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Procedure Type</label> <select
														id="procedurePurchase" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-12">
													<div id="purchaseTable"></div>
												</div>
											</div>
										</div>
										<div id="general" class="tab-pane fade">
											<div class="modal-body" style="height: 400px">
												<div class="form-group col-md-4">
													<label>Company</label> <select id="companyGeneral"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchGeneral"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-12">
													<div id="generalTable"></div>
												</div>
											</div>
										</div>
										<div id="inventory" class="tab-pane fade">
											<div class="modal-body" style="height: 400px">
												<div class="form-group col-md-3">
													<label>Company</label> <select id="companyInventory"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Branch / Item Group</label> <select
														id="branchORItemGrpInventory" class="form-control">
														<option value='-1'>Select</option>
														<option value='B'>Branch</option>
														<option value='I'>Item Group</option>
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Branch</label> <select id="branchInventory"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-3">
													<label>Item Group</label> <select id="itemGrpInventory"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-12">
													<div id="inventoryTable"></div>
												</div>
											</div>
										</div>
										<div id="resource" class="tab-pane fade">
											<div class="modal-body" style="height: 400px">
												<div class="form-group col-md-4">
													<label>Company</label> <select id="companyResource"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchResource"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-12">
													<div id="resourceTable"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="saveGLRule" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="GLRuleSalesEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
									</div>
									<div class="modal-body" style="height: 350px">
										<form>
											<div class="row">
												<!-- <div class="form-group col-md-4">
													<label>Company</label> <select id="companySalesEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchSalesEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Order Type</label> <select id="ordTypeSalesEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Procedure Type</label> <select
														id="procedureSalesEdit" class="form-control">
													</select>
												</div> -->
												<div class="form-group col-md-12">
													<div id="salesTableEdit"></div>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>Condition Type</label> <select
														id="condTypeSalesEdit" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Key</label> <select id="keyCodeSalesEdit"
														class="form-control">
													</select>
												</div>

												<div class="form-group col-md-4">
													<label>GL Code</label> <select id="glCodeSalesEdit"
														class="form-control">
													</select>
												</div> -->
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="salesUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="GLRulePurchaseEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
									</div>
									<div class="modal-body" style="height: 350px">
										<form>
											<div class="row">
												<div class="form-group col-md-12">
													<div id="purchaseTableEdit"></div>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>Company</label> <select id="companyPurchaseEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchPurchaseEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Order Type</label> <select id="ordTypePurchaseEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Procedure Type</label> <select
														id="procedurePurchaseEdit" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Condition Type</label> <select
														id="condTypePurchaseEdit" class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Key</label> <select id="keyCodePurchaseEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>GL Code</label> <select id="glCodePurchaseEdit"
														class="form-control">
													</select>
												</div> -->
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="purchaseUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="GLRuleGeneralEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-12">
													<div id="generalTableEdit"></div>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>Company</label> <select id="companyGeneralEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchGeneralEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Key</label> <select id="keyCodeGeneralEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>GL Code</label> <select id="glCodeGeneralEdit"
														class="form-control">
													</select>
												</div> -->
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="generalUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="GLRuleResourceEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-12">
													<div id="resourceTableEdit"></div>
												</div>
												<!-- <div class="form-group col-md-4">
													<label>Company</label> <select id="companyResourceEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Branch</label> <select id="branchResourceEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Key</label> <select id="keyCodeResourceEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>GL Code</label> <select id="glCodeResourceEdit"
														class="form-control">
													</select>
												</div> -->
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="resourceUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="country_info modal " id="GLRuleInventoryEdit"
							tabindex="-1" role="dialog"
							aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">GL
											DETERMINATION RULE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-12">
													<div id="inventoryTableEdit"></div>
												</div>
												<!-- div class="form-group col-md-4">
													<label>Company</label> <select id="companyInventoryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Item Group</label> <select id="itemGrpInventoryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>Key</label> <select id="keyCodeInventoryEdit"
														class="form-control">
													</select>
												</div>
												<div class="form-group col-md-4">
													<label>GL Code</label> <select id="glCodeInventoryEdit"
														class="form-control">
													</select>
												</div> -->
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="inventoryUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>



		<div class="country_info modal " id="StateEdit1" tabindex="-1"
															role="dialog" aria-labelledby="EmployementTypeLabel">
															<div class="modal-dialog">
																<div class="modal-content" style="margin-top: 25%">
																	<div class="modal-header">
																		<button type="button" class="close " data-dismiss="modal">
																			<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																		</button>
																		<h4 class="modal-title" id="EmployementTypeLabel">GL DETERMINATION RULE
																		</h4>
																	</div>
																	<div class="modal-body" style="height: 250px">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th scope="col">Key Code</th>
																					<th scope="col">Key Name</th>
																					<th scope="col">Condition</th>
																					<th scope="col">Account Code</th>
																				</tr>
																				<tr>
																					<td>10430</td>
																					<td>Domestic</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>102340</td>
																					<td>International</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>1012430</td>
																					<td>Forign</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>10023</td>
																					<td>Domestic</td>
																					<td> 	
																					<select class=" form-control select2" name="cityid">
																					<option value="select country">Select Discount</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																			</thead>
																		</table>
																	</div>
																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="salepurTypesave" type="button" style="margin-right: 5px">Save</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
												</div>


												<div class="country_info modal " id="StateEdit2" tabindex="-1"
															role="dialog" aria-labelledby="EmployementTypeLabel">
															<div class="modal-dialog">
																<div class="modal-content" style="margin-top: 25%">
																	<div class="modal-header">
																		<button type="button" class="close " data-dismiss="modal">
																			<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																		</button>
																		<h4 class="modal-title" id="EmployementTypeLabel">GL DETERMINATION RULE
																		</h4>
																	</div>
																	<div class="modal-body" style="height: 250px">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th scope="col">Key Code</th>
																					<th scope="col">Key Name</th>
																					<th scope="col">Account Code</th>
																				</tr>
																				<tr>
																					<td>10430</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>102340</td>
																					<td>International</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>1012430</td>
																					<td>Forign</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>10023</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																			</thead>
																		</table>
																	</div>
																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="salepurTypesave" type="button" style="margin-right: 5px">Save</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
												</div>



												<div class="country_info modal " id="StateEdit3" tabindex="-1"
															role="dialog" aria-labelledby="EmployementTypeLabel">
															<div class="modal-dialog">
																<div class="modal-content" style="margin-top: 25%">
																	<div class="modal-header">
																		<button type="button" class="close " data-dismiss="modal">
																			<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																		</button>
																		<h4 class="modal-title" id="EmployementTypeLabel">GL DETERMINATION RULE
																		</h4>
																	</div>
																	<div class="modal-body" style="height: 250px">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th scope="col">Key Code</th>
																					<th scope="col">Key Name</th>
																					<th scope="col">Account Code</th>
																				</tr>
																				<tr>
																					<td>10430</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>102340</td>
																					<td>International</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>1012430</td>
																					<td>Forign</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>10023</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																			</thead>
																		</table>
																	</div>
																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="salepurTypesave" type="button" style="margin-right: 5px">Save</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
												</div>


												<div class="country_info modal " id="StateEdit4" tabindex="-1"
															role="dialog" aria-labelledby="EmployementTypeLabel">
															<div class="modal-dialog">
																<div class="modal-content" style="margin-top: 25%">
																	<div class="modal-header">
																		<button type="button" class="close " data-dismiss="modal">
																			<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
																		</button>
																		<h4 class="modal-title" id="EmployementTypeLabel">GL DETERMINATION RULE
																		</h4>
																	</div>
																	<div class="modal-body" style="height: 250px">
																		<table class="table table-bordered table-responsive">
																			<thead>
																				<tr>
																					<th scope="col">Key Code</th>
																					<th scope="col">Key Name</th>
																					<th scope="col">Account Code</th>
																				</tr>
																				<tr>
																					<td>10430</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>102340</td>
																					<td>International</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>1012430</td>
																					<td>Forign</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select Code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																				<tr>
																					<td>10023</td>
																					<td>Domestic</td>
																					<td><select class=" form-control select2" name="cityid">
																					<option value="select country">Select code</option>
																					<option>Percentage</option>
																					<option>Purchase</option>
																					</select></td>
																				</tr>
																			</thead>
																		</table>
																	</div>
																	<div class="modal-footer clearfix">
																		<div class="form-group" style="">
																			<button class="btn btn-danger pull-right btn-sm RbtnMargin"
																				id="country_Reset" type="button">Reset</button>
																			<button class="btn btn-success pull-right btn-sm"
																				id="salepurTypesave" type="button" style="margin-right: 5px">Save</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
												</div>



