
<script type="text/javascript">
	
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-gl').addClass('active');
	$('#menu-gl > a').attr('aria-expanded', 'true');
	$('#menu-gl > ul').addClass('in');

	$('#menu-ordType').addClass('active');
	$('#orderType').hide();

	$('#orderTypeAdd').click(function() {
	$('#orderType').show();
	});

	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#country_Reset').click(function(){
	$('#ordId').val('');
	$('#ordName').val('');	
	});
	
});

</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">GL Determination</a></li>
					<li class="active">OrderType</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">ORDER TYPE</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="orderTypeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="ordId">Order Type ID</th>
														<th data-field="ordName">Order Type Name</th>
														<th data-field="salepur">Sales/ Purchase</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>YBBR</td>
														<td>Branch Sale</td>
														<td>Sales</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>YBXP</td>
														<td>Export Sale</td>
														<td>Sales</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>ZIMP</td>
														<td>Import Purchase</td>
														<td>Purchase</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ORDER
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Order Type Id</label> <input type="text" readonly="" 
														class="form-control" id="ordId" maxlength="4">
												</div>
												<div class="form-group col-md-6">
													<label>Order Type Name</label> <input type="text"
														class="form-control" id="ordName" maxlength="30">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Sales / Purchase </label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Sales / Purchase </option>
													<option>Sales</option>
													<option>Purchase</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ordertypesave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
										
						<div class="country_info modal " id="orderType" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ORDER
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Order Type Id</label> <input type="text"
														class="form-control" id="ordId" maxlength="4">
												</div>
												<div class="form-group col-md-6">
													<label>Order Type Name</label> <input type="text"
														class="form-control" id="ordName" maxlength="30">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Sales / Purchase </label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Sales / Purchase </option>
													<option>Sales</option>
													<option>Purchase</option>
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ordertypesave" type="button" style="margin-right: 5px">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
							
						<div class="country_info modal " id="orderTypeEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ORDER
											TYPE</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label>Order Type Id</label> <input type="text"
														class="form-control" id="ordIdEdit" maxlength="4"
														readonly="readonly">
												</div>
												<div class="form-group col-md-6">
													<label>Order Type Name</label> <input type="text"
														class="form-control" id="ordNameEdit" maxlength="30">
												</div>
												<div class="form-group col-md-6">
													<label>Sales / Purchase</label> <select id="salepurEdit"
														class="form-control">
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="ordertypeupdate" type="button" style="margin-right: 5px">Update</button>
																				
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>