

<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-product_categories').addClass('active');
	$('#City').hide();

	$('#cityAdd').click(function() {
		$('#City').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Product Categories</li>
				</ol>
			</div>
			
			<?php $state = $this->db->query("SELECT b.id,b.country_id,a.country_name,b.state_code,b.state_name,b.gst_code,b.union_territory,b.status from erpgcy a ,erpgste b WHERE a.id = b.country_id and b.status = '1' order by id DESC")->result(); ?>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Product Categories</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="cityAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="country">Product Catogory Code</th>
														<th data-field="stateId">Product Catogory Name</th>
														<th data-field="action" data-formatter="actionFormatter"	data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>HW</td>
														<td>Hardware</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Comp</td>
														<td>Components</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>Raw</td>
														<td>Raw Material</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>PJ</td>
														<td>Project Specific</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Product Categories</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Name</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>																			
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												<!-- <?php $i=0; foreach($state as $row) { ?>
												<tr>
												<td><?php echo $row->country_name ?></td>
												<td><?php echo $row->state_code?></td>
												<td><a class="edit ml10" title="" data-toggle="modal" data-target="#CityEdit<?php echo $i; ?>">
												                <i class="glyphicon glyphicon-pencil"></i></a></td>
												
						    <div class="country_info modal " id="CityEdit<?php echo $i; ?>" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
							 	<?php echo form_open(base_url('general/update_state')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Product Categories</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Name</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>																			
											</div>
										
									</div>
									
									
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>
											
												
												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
												
										    </div>
									    </div>
									</form>
								</div>
							</div>
						</div>
												
												
												</tr>
												<?php $i++; } ?> -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="country_info modal " id="City" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
							
							<?php echo form_open(base_url('general/add_state'), array('id'=>'state','name'=>'state','autocomplete'=>'off')); ?>
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Product Categories</h4>
									</div>
									<div class="modal-body" style="height: 250px">
										<form>
											<div class="row">
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>												
												<div class="form-group col-md-6">
													<label class="control-label">Product Catogory Code</label> <input
														type="text" class="form-control" id="statecodeEdit" name="statecode"
														maxlength="3" value="<?php echo $row->state_code?>"
														onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
												</div>												
											</div>
										</form>
									</div>
									<div class="modal-footer clearfix">
										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button>

												<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>


					</div>
				</div>
			</div>
		</section>