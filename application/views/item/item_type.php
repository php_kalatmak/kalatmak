
<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-itemType').addClass('active');
	$('#ItemType').hide();
	$('#ItemTypeEdit').hide();

	$('#itemTypeAdd').click(function() {
		$('#ItemType').show();
	});

	$('#itemType_Reset').click(function() {
		$('#itemTypeCode').val('');
		$('#itemTypeName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Item</a></li>
					<li class="active">Item Type</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Item Type</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="itemTypeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="itemTypeId">Item Type ID</th>
														<th data-field="itemTypeName">Item Type Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>TypeID001</td>
													<td>Item</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>TypeID002</td>
													<td>Service</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>TypeID003</td>
													<td>Labour</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>TypeID004</td>
													<td>Travel</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>TypeID005</td>
													<td>Fixed Asset</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="itemType_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="itemTypeCode" class="control-label">Item Type ID
												</label> <input type="text" class="form-control" readonly name="Item Type ID"
												id="itemTypeCode" maxlength="1"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="itemTypeName" class="control-label">Item Type
												Name</label> <input type="text" class="form-control"
												id="itemTypeName" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="itemType_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="itemTypeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="itemType_info modal " id="ItemType" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="itemTypeCode" class="control-label">Item Type ID
												</label> <input type="text" class="form-control"
												id="itemTypeCode" maxlength="1"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="itemTypeName" class="control-label">Item Type
												Name</label> <input type="text" class="form-control"
												id="itemTypeName" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="itemType_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="itemTypeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="valuationType_info modal " id="ItemTypeEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="valuationcode" class="control-label">Item Type ID
												</label> <input type="text" class="form-control"
												id="itemTypeCodeEdit" maxlength="1" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="valuationname" class="control-label">Item Type
												Name</label> <input type="text" class="form-control"
												id="itemTypeNameEdit" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="itemTypeUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>