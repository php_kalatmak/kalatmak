
<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-division').addClass('active');

	$('#Divisionadd').hide();
	$('#divisionEdit').hide();

	$('#divisionAdd').click(function() {
	$('#Divisionadd').show();
	});
	
	$(".close").click(function() {
	$(".modal").hide();
	});

	$('#remarks').click(function(){
	$('#divCode').val('');
	$('#divName').val('');	
	});
	
	});
</script>

<section class="content">
		<div class="page-heading">
			<h1>Masters</h1>
			<ol class="breadcrumb">
				<li><a href="../../erp/home">Master</a></li>
				<li><a href="javascript:void(0);">General</a></li>
				<li class="active">Division</li>
			</ol>
		</div>
		<?php $division = $this->db->query("select * from erpgdiv order by id DESC")->result(); ?>
		<div class="page-body">
			<div class="row">
				<div class="col-lg-12">

					<div class="page-body" id="dtab">
						<div class="row clearfix margin_full">
							<!-- <div class="col-lg-8 col-md-offset-2"> -->
								<div class="panel panel-default">
									<div class="panel-heading">Division</div>
									<div class="panel-body table-responsive">
										<button class="btn btn-sm btn-success" id="divisionAdd">New</button>
										<br clear="left">
										<table
											class="table table-striped table-hover js-exportable dataTable"
											data-search=true>

											<thead>
												<tr>
													<th data-field="divTblId">Division Code</th>
													<th data-field="divTblDesc">Division Name</th>
													<th data-field="action" data-formatter="actionFormatter"
														data-events="actionEvents">Edit</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>DIV0001</td>
													<td>Carpentry</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>DIV0002</td>
													<td>Metal</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												
												<div class="Division_info modal " id="StateEdit" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 10%">
							<?php echo form_open(base_url('general/add_division'),array('id'=>'division','name'=>'division','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Division</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Division
											Code</label> <input type="text" class="form-control" readonly name="divisioncode" maxlength="2" id="divCode" name="division_code">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Division
											Name</label> <input type="text" class="form-control" id="divName" name="division_name"
											maxlength="30"
											onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="division_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

									</div>
								</div>
								
							</div>
						</div>
					</div>
											<!-- <?php $i=0; foreach($division as $row) { ?>
											<tr>
											<td><?php echo $row->division_code; ?></td>
											<td><?php echo $row->division_name; ?></td>
											<td><a class="edit ml10" title="" data-toggle="modal" data-target="#divisionEdit<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-pencil"></i></a></td>
											</tr> -->
											
						<div class="uom_info modal" id="divisionEdit<?php echo $i; ?>" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">

							<div class="modal-content" style="margin-top: 10%">
							<?php echo form_open(base_url('general/update_division')); ?>
							<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Division</h4>
								</div>
								<div class="modal-body" style="height: 150px">
								
									<div class="form-group col-md-6">
										<label>Division Code</label> 
										
										 <input type="text" class="form-control" readonly name="divisioncode"
											id="divCodeEdit" value="<?php echo $row->division_code; ?>">
									</div>

									<div class="form-group col-md-6">
										<label for="description" class="control-label"> Division
											Name</label> <input type="text" class="form-control"  name="divisionname" value="<?php echo $row->division_name; ?>"
											id="divDescNewEdit">
									</div>

									
								</div>
							</div>
							<div class="modal-footer clearfix">

								<div class="form-group" style="">
									<button class="btn btn-danger pull-right btn-sm RbtnMargin" id="uom_Reset" type="button">Reset</button>
									<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
									
								</div>

							</div>
							
						</div>
					</div>
											</tbody>
											<?php $i++; } ?>

										</table>
									</div>
								</div>
						
						</div>
					</div>






					<div class="Division_info modal " id="Divisionadd" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 10%">
							<?php echo form_open(base_url('general/add_division'),array('id'=>'division','name'=>'division','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">Division</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">Division
											Code</label> <input type="text" class="form-control" maxlength="2" id="divCode" name="division_code">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">Division
											Name</label> <input type="text" class="form-control" id="divName" name="division_name"
											maxlength="30"
											onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="division_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

									</div>
								</div>
								
							</div>
						</div>
					</div>


					

				</div>
			</div>
		</div>
		</section>