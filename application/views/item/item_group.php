<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-itemGroup').addClass('active');
	$('#ItemGroup').hide();
	$('#ItemGroupEdit').hide();

	$('#itemGroupAdd').click(function() {
		$('#ItemGroup').show();
	});

	$('#itemGroup_Reset').click(function() {
		$('#itemGroupCode').val('');
		$('#itemGroupName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Item</a></li>
					<li class="active">Item Group</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Item Group</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="itemGroupAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="itemGroupId">Item Group ID</th>
														<th data-field="itemGroupName">Item Group Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>GroupID001</td>
													<td>General</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>GroupID002</td>
													<td>Project Specific</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="itemGroup_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Group</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="itemGroupCode" class="control-label">Item Group ID
												</label> <input type="text" class="form-control" readonly name="Item Group ID"
												id="itemGroupCode" maxlength="4"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="itemGroupName" class="control-label">Item Group
												Name</label> <input type="text" class="form-control"
												id="itemGroupName" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="itemGroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="itemGroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="itemGroup_info modal " id="ItemGroup" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Group</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="itemGroupCode" class="control-label">Item Group ID
												</label> <input type="text" class="form-control"
												id="itemGroupCode" maxlength="4"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="itemGroupName" class="control-label">Item Group
												Name</label> <input type="text" class="form-control"
												id="itemGroupName" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="itemGroup_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="itemGroupSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="itemGroup_info modal " id="ItemGroupEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Item Group</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="ItemGroupIdEdit" class="control-label">Item Group ID
												</label> <input type="text" class="form-control"
												id="ItemGroupIdEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="itemGroupNameEdit" class="control-label">Item Type
												Name</label> <input type="text" class="form-control"
												id="itemGroupNameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="itemGroupUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>