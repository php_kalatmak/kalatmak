<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-itemMaster').addClass('active');
	//$('#Branch').hide();

	$('#itemMasterAdd').click(function() {
		$('#ItemMaster').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Company</a></li>
					<li class="active">ItemMaster</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">ItemMaster</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="itemMasterAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="itemCodeTbl">Item Code</th>
														<th data-field="itemNameTbl">Item Name</th>
														<th data-field="itemCodeTbl">Item Type</th>
														<th data-field="itemNameTbl">Item Group</th>
														<th data-field="itemCodeTbl">Valuation</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
													<tbody>
														<tr>
															<td>Code01</td>
															<td>Table</td>
															<td>Item</td>
															<td>Raw Material</td>
															<td>Own Product</td>
															<td><a class="edit ml10" title="">
														                <i class="glyphicon glyphicon-pencil"></i></a></td>
														</tr>
														<tr>
															<td>Code02</td>
															<td>Chair</td>
															<td>Item</td>
															<td>Raw Material</td>
															<td>Own Product</td>
															<td><a class="edit ml10" title="">
														                <i class="glyphicon glyphicon-pencil"></i></a></td>
														</tr>
													
													<div class="country_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">			
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ItemMaster
										</h4>
									</div>
									
									<div class="modal-body" style="height: auto; margin-bottom: 20px">
										<div class="row">
									<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">
												Item Code </label> <input type="text" class="form-control"
												id="itemCode" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Item Name</label> <input type="text" class="form-control"
												id="itemDesc" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Item Type</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item Type</option>
													<option>Item</option>
													<option>Service</option>
													<option>Labour</option>
													<option>Travel</option>
													<option>Fixed Asset</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Item Group</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item Group</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">UOM Group</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select UOM Group Code</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Valuation Code</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Valuation Code</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Bar Code</label> <input type="text" class="form-control"
												id="barCode" maxlength="50">
												
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Payment Term Code</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Payment Term Code</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Brand</label> <input type="text" class="form-control"
												id="field1" maxlength="50"
												>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Short Text</label> <input type="text" class="form-control"
												id="field2" maxlength="50">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Legacy Code</label> <input type="text" class="form-control"
												id="field2" maxlength="50">
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-4">
													<label for="countrycode" class="control-label">Item
														Image </label> <input type="file" class="form-control"
														id="itemGrpImage" onchange="readURL(this);" />
												</div>
												<div class="form-group col-md-4">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
											</div>
										<br clear="left">
										
										
										    <ul class="nav nav-tabs" id="tabs">
										        <li><a data-toggle="tab" href="#general1">General</a></li>
												<li><a data-toggle="tab" href="#purchase1">Purchase</a></li>
												<li><a data-toggle="tab" href="#sales1">Sales</a></li>
												<li><a data-toggle="tab" href="#inventory1">Inventory</a></li>
												<li><a data-toggle="tab" href="#planning1">Planning</a></li>
												<li><a data-toggle="tab" href="#production1">Production</a></li>
												<li><a data-toggle="tab" href="#remarks1">Remarks</a></li>
												<li><a data-toggle="tab" href="#classification1">Classification</a></li>
										    </ul>
										    <div class="tab-content">
										        <div class="tab-pane active" id="general1">
										                <ul class="nav nav-tabs" id="repoTabs">
															<li><a data-toggle="tab" href="#generalSub1">General Sub</a></li>
															<li><a data-toggle="tab" href="#manuCapacity1">Manufacture Capacity</a></li>
															<li><a data-toggle="tab" href="#itemExtend1">Item Extend</a></li>
															<li><a data-toggle="tab" href="#itemVendorMap1">Item Vendor Mapping</a></li>                
										                </ul>
										              <div class="tab-content">
										                <div class="tab-pane active" id="generalSub1">
										                <div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Shipping Code</label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Shipping Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Manage ItemType Code</label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Manage ItemType Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
															</div>	
															<div class="form-group col-md-3">
																	<label class="control-label">Charecter ID Cap</label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Manage ItemType Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Status ID</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status ID</option>
																<option>code1</option>
																<option>code2</option>
																</select>
															</div> 
										                </div>          
										                </div>
										                
										          		<div class="tab-pane" id="manuCapacity1">
											          		<div class="row">
																<div class="form-group col-md-3">
																	<label class="control-label">Item mf Seq</label>
																	<input type="text" class="form-control"
																		id="manCapitemMfseq" maxlength="10">
																</div>
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemcode" maxlength="10">
																</div> -->
																<div class="form-group col-md-3">
																	<label class="control-label">Branch Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Capacity</label>
																	<input type="text" class="form-control"
																		id="manCapcapacity" maxlength="10">
																</div>	
																<div class="form-group col-md-3">
																	<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status</option>
																<option>Active</option>
																<option>InActive</option>
																</select>
																</div>
											          		</div>
										          		</div>
										          		
														<div class="tab-pane" id="itemExtend1">
															<div class="row">
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemCode" maxlength="10">
																</div> -->
																<div class="form-group col-md-3">
																	<label class="control-label">Company Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Company Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Branch Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Cost Center ID</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Cost Center ID</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Rol Qty</label>
																	<input type="text" class="form-control"
																		id="itemExrolQty" maxlength="10">
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Status ID</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status ID</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Allowed Or Blocked</label>
																	<input type="checkbox" name="loadItem" id="itemExallowedOrblocked">
																	
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Division Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
															</div>
														</div>
														
														<div class="tab-pane" id="itemVendorMap1">
														  <div class="row">
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemCode" maxlength="10">
																</div> -->
																<div class="form-group col-md-3">
																	<label class="control-label">Bp Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Item Alias Name</label>
																	<input type="text" class="form-control"
																		id="itemAliasName" maxlength="10">
																</div>
														</div>
														</div>
										              </div>
										        </div>
										      	<div class="tab-pane" id="purchase1">
													<div class="row">
														<!-- <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="shippingCode" class="form-control"></select>
														</div> -->
														<div class="form-group col-md-3">
															<label class="control-label">Bp Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>	
														<div class="form-group col-md-3">
																<label class="control-label">Purchase UOM Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Purchase UOM Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>
														<div class="form-group col-md-3">
															<label class="control-label">Minimum Purchase Qty
															</label>
															<input type="text" class="form-control"
																	id="purMinPurchaseQty" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Packing Size</label>
																<input type="text" class="form-control"
																	id="purPackingSze" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Length</label>
																<input type="text" class="form-control"
																	id="purLength" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Width</label>
																<input type="text" class="form-control"
																	id="purWidth" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Height</label>
																<input type="text" class="form-control"
																	id="purHeight" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Volume</label>
																<input type="text" class="form-control"
																	id="purVolume" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Weight</label>
																<input type="text" class="form-control"
																	id="purWeight" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="purField1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="purField2" maxlength="10">
														</div> 
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 1</label>
																<input type="text" class="form-control"
																	id="purParameter1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 2</label>
																<input type="text" class="form-control"
																	id="purParameter2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 3</label>
																<input type="text" class="form-control"
																	id="purParameter3" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 4</label>
																<input type="text" class="form-control"
																	id="purParameter4" maxlength="10">
														</div>
													</div>
										        </div>
										      	<div class="tab-pane" id="sales1">
										          <div class="row">
										          
														<!-- <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="shippingCode" class="form-control"></select>
														</div> -->
														
														<div class="form-group col-md-3">
																<label class="control-label">Sales UOM Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Sales UOM Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>
														
														<div class="form-group col-md-3">
																<label class="control-label">Packing Size</label>
																<input type="text" class="form-control"
																	id="salesPcksze" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Length</label>
																<input type="text" class="form-control"
																	id="salesLnth" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Width</label>
																<input type="text" class="form-control"
																	id="salesWidth" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Height</label>
																<input type="text" class="form-control"
																	id="salesHght" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Volume</label>
																<input type="text" class="form-control"
																	id="salesVolume" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Weight</label>
																<input type="text" class="form-control"
																	id="salesWeight" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="salesFld1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="salesFld2" maxlength="10">
														</div> 
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 1</label>
																<input type="text" class="form-control"
																	id="salesPrmtr1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 2</label>
																<input type="text" class="form-control"
																	id="salesPrmtr2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 3</label>
																<input type="text" class="form-control"
																	id="salesPrmtr3" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 4</label>
																<input type="text" class="form-control"
																	id="salesPrmtr4" maxlength="10">
														</div>
													</div>
										        </div>
										      	<div class="tab-pane" id="inventory1">
										      	<div class="row">
										         <!--  <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="Code" class="form-control"></select>
														</div> -->
														<div class="form-group col-md-3">
															<label class="control-label">Valuation Method Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Valuation Method Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="invFld1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="invFld2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="invFld3" maxlength="10">
														</div>
														</div>
										        </div>
										        
										      	<div class="tab-pane" id="planning1">
										          <div class="row">
										          <div class="form-group col-md-3">
															<label class="control-label">Plan Method Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Plan Method Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>
														<div class="form-group col-md-3">
															<label class="control-label">Procurement Method Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Procurement Method Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Order Interval</label>
																<input type="text" class="form-control"
																	id="orderInterval" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Min Qty</label>
																<input type="text" class="form-control"
																	id="minQty" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Lead Time</label>
																<input type="text" class="form-control"
																	id="leadTimePlan" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Tolerance Days</label>
																<input type="text" class="form-control"
																	id="tolDays" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="planfield1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="planfield2" maxlength="10">
														</div>
														
														</div>
										        </div>
										      	<div class="tab-pane" id="production1">
										         <div class="row">
										          
														<div class="form-group col-md-3">
															<label class="control-label">Issue Method Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Issue Method Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Bom Type Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bom Type Code</option>
																<option>code1</option>
																<option>code2</option>
																</select>
														</div>
														</div>
										        </div>
										      	<div class="tab-pane" id="remarks1">
										          <div class="row">
										          <div class="form-group col-md-3">
															<label class="control-label">Item Remarks</label>
															<input type="text" class="form-control"
																	id="itemRemarks" maxlength="10">
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="remfield1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="remfield2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="remfield3" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 4</label>
																<input type="text" class="form-control"
																	id="remfield4" maxlength="10">
														</div>
										          </div>
										        </div>
										        <div class="tab-pane" id="classification1">
										        	<div class="row">
										        	<div class="form-group col-md-3">
										        	<label for="comment">Item Classification</label>
      												<textarea class="form-control" rows="5" id="comment"></textarea>
      												</div>
      											</div>
										        </div>
										    </div>		
										    
										    <div class="modal-footer clearfix">

											<div class="form-group">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="itemMaster_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="itemMasterSave" type="button"
													style="margin-right: 5px">Save</button>
											</div>
										</div>  											
										</div>
									</div>
								</div>
							</div>
													</tbody>
												</thead>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
	
	


						
						
						
						<div class="country_info modal " id="ItemMaster" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">			
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ItemMaster
										</h4>
									</div>
									
									<div class="modal-body" style="height: 500px !important; margin-bottom: 20px">
									<div class="form-group col-lg-10">
									<div class="row">
										<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">
												Item Code </label> <input type="text" class="form-control"
												id="itemCode" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Item Name</label> <input type="text" class="form-control"
												id="itemDesc" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Item Type</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item Type</option>
													<option>Item</option>
													<option>Service</option>
													<option>Labour</option>
													<option>Travel</option>
													<option>Fixed Asset</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Item Group</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Item Group</option>
													<option>Raw Material</option>	
													<option>Finished Goods</option>	
													<option>Semi Finished Goods</option>	
													<option>Packing Material</option>	
													<option>Tools</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">UOM Group</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select UOM Group</option>
													<option>Kg</option>
													<option>Lt</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Valuation class</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Valuation </option>
													<option>Own Product</option>	
													<option>Trade Product</option>	
													<option>Free Product</option>	
													<option>Import Product</option>
													</select>
										</div>
										
									</div>
									<div class="row">
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Bar Code</label> <input type="text" class="form-control"
												id="barCode" maxlength="50">
												
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Payment Term</label> 
														
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Payment Term </option>
													<option>PS21</option>
													<option>PS45</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label class="control-label">Brand</label> 
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Brand</option>
													<option>Mobile</option>
													<option>laptop</option>
													</select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Short Text</label> <input type="text" class="form-control"
												id="field2" maxlength="50">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Legacy Code</label> <input type="text" class="form-control"
												id="field2" maxlength="50">
										</div>

												<div class="form-group col-md-2">
													<img id="blah" src="resources/images/avatars/def.png"
														alt="..." />
												</div>
									</div>
									<div class="row">
										<div class="form-group col-md-2">
													<label for="countrycode" class="control-label">Item
														Image </label> <input type="file" class="form-control"
														id="itemGrpImage" onchange="readURL(this);" />
												</div>
												
										</div>
									</div>
									<div class="form-group col-lg-2">
										<div class="col-md-12">
											<input type="checkbox">
											<label for="countrycode" class="control-label">Inventry Item</label> 
										</div>
										<div class="col-md-12">
											<input type="checkbox">
											<label for="countrycode" class="control-label">Purchase Item</label> 
											
										</div>
										<div class="col-md-12">
											<input type="checkbox">
											<label for="countrycode" class="control-label">Salary Item</label> 
											
										</div>
										<div class="col-md-12">
											<input type="checkbox">
											<label for="countrycode" class="control-label">Asset Item</label> 
											
										</div>
										<div class="col-md-12">
											<input type="checkbox">
											<label for="countrycode" class="control-label">Ecommerce Item</label> 
											
										</div>
									</div>
								
										<br clear="left">
										
										
										    <ul class="nav nav-tabs" id="tabs">
										        <li><a data-toggle="tab" href="#general">General</a></li>
												<li><a data-toggle="tab" href="#purchase">Purchase</a></li>
												<li><a data-toggle="tab" href="#sales">Sales</a></li>
												<li><a data-toggle="tab" href="#inventory">Inventory</a></li>
												<li><a data-toggle="tab" href="#planning">Planning</a></li>
												<li><a data-toggle="tab" href="#production">Production</a></li>
												<li><a data-toggle="tab" href="#remarks">Remarks</a></li>
												<li><a data-toggle="tab" href="#classification">Classification</a></li>
										    </ul>
										    <div class="tab-content">
										        <div class="tab-pane active" id="general">
										                <ul class="nav nav-tabs" id="repoTabs">
															<li><a data-toggle="tab" href="#generalSub">General Sub</a></li>
															<li><a data-toggle="tab" href="#manuCapacity">Manufacture Capacity</a></li>
															<li><a data-toggle="tab" href="#itemExtend">Item Extend</a></li>
															<li><a data-toggle="tab" href="#itemVendorMap">Item Alias Name</a></li> 
															<li><a data-toggle="tab" href="#storagelocation">Storage Location</a></li>                 
										                </ul>
										              <div class="tab-content">
										                <div class="tab-pane active" id="generalSub">
										                <div class="row">
															<div class="form-group col-md-3">
																<label class="control-label">Shipping </label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Shipping </option>
																<option>Transport</option>
																<option>Courier</option>
																</select>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Item Type </label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Item Type </option>
																<option>Item</option>	
																<option>Service</option>	
																<option>Labour</option>	
																<option>Travel</option>	
																<option>Fixed Asset</option>
																</select>
															</div>	
															<div class="form-group col-md-3">
																	<label class="control-label">Character ID Cap</label> 
														
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Character Id Cap</option>
																<option>HSN-25.12.01.06</option>
																<option>SAC-36251007</option>
																</select>
															</div>
															<div class="form-group col-md-3">
																<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status </option>
																<option>Active</option>
																<option>In-Active</option>
																</select>
															</div> 
										                </div>          
										                </div>
										                
										          		<div class="tab-pane" id="manuCapacity">
											          		<div class="row">
                                                            	<a class="btn btn-sm btn-primary" onClick="add_mcap()">Add More</a>
                                                            	<table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Item Manufacturing Sequence</th>
                                                                            <th>Branch Code</th>
                                                                            <th>Capacity</th>
                                                                            <th>Status</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="append_mcap">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" class="form-control" id="manCapitemMfseqEdit" maxlength="10" placeholder="Item Mf Sequence">
                                                                            </td>
                                                                            <td>
                                                                                <select id="manCapbranchCodeEdit " class="form-control select2">
                                                                                    <option>--- Select Branch Code ---</option>
                                                                                    <option>1000</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control" id="manCapcapacityEdit" maxlength="10" placeholder="Capacity">
                                                                            </td>
                                                                            <td>
                                                                                <select id="manCapbranchCodeEdit1 " class="form-control select2">
                                                                                    <option>Active</option>
                                                                                    <option>In Active</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm btn-danger" onClick="delete_mcap(this,0)"><i class="material-icons">delete</i></a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Item mf Seq</label>
																	<input type="text" class="form-control"
																		id="manCapitemMfseq" maxlength="10">
																</div>-->
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemcode" maxlength="10">
																</div> -->
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Branch Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code</option>
																<option>1000</option>
																</select>
																</div>-->
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Capacity</label>
																	<input type="text" class="form-control"
																		id="manCapcapacity" maxlength="10">
																</div>-->	
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status</option>
																<option>Active</option>
																<option>InActive</option>
																</select>
																</div>-->
											          		</div>
										          		</div>
										          		
														<div class="tab-pane" id="itemExtend">
															<div class="row">
                                                            	<a class="btn btn-sm btn-primary" onClick="add_itex()">Add More</a>
                                                            	<table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Company Code</th>
                                                                            <th>Branch Code</th>
                                                                            <th>Cost Center</th>
                                                                            <th>Rol Qty</th>
                                                                            <th>Status</th>
                                                                            <th style="width:5% !important;">Allowed Or Blocked</th>
                                                                            <th>Division</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="append_itex">
                                                                        <tr>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                	<option value="select country">Select Company Code</option>
                                                                                	<option>1000</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select id="manCapbranchCodeEdit" class="form-control select2">
                                                                                    <option>--- Select Branch Code ---</option>
                                                                                    <option>1000</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Cost Center</option>
                                                                                    <option>Purchase</option>
                                                                                    <option>Production</option>
                                                                                    <option>Sales</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                            	<input type="text" class="form-control" id="itemExrolQty" maxlength="10" placeholder="ROL Quantity">
                                                                            </td>
                                                                            <td>
                                                                                <select id="manCapstatusEdit" class="form-control select2">
                                                                                    <option>Active</option>
                                                                                    <option>In-Active</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                            	<input type="checkbox">
                                                                            </td>
                                                                            <td>
                                                                            	<select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Division</option>
                                                                                    <option>Carpentry</option>
                                                                                    <option>Metal</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm btn-danger" onClick="delete_itex(this,0)"><i class="material-icons">delete</i></a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemCode" maxlength="10">
																</div> -->
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Company Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Company Code</option>
																<option>1000</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Branch Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Branch Code</option>
																<option>1000</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Cost Center</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Cost Center</option>
																<option>Purchase</option>
																<option>Production</option>
																<option>Sales</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Rol Qty</label>
																	<input type="text" class="form-control"
																		id="itemExrolQty" maxlength="10">
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Status</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Status</option>
																<option>Active</option>
																<option>In-Active</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Allowed Or Blocked</label>
																	<input type="checkbox" name="loadItem" id="itemExallowedOrblocked">
																	
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Division</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Division</option>
																<option>Carpentry</option>
																<option>Metal</option>
																</select>
																</div>-->
															</div>
														</div>
														
														<div class="tab-pane" id="itemVendorMap">
														  <div class="row">
																<!-- <div class="form-group col-md-4">
																	<label class="control-label">Item Code</label>
																	<input type="text" class="form-control"
																		id="itemCode" maxlength="10">
																</div> -->
                                                                <a class="btn btn-sm btn-primary" onClick="add_itve()">Add More</a>
                                                            	<table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>BP Code</th>
                                                                            <th>Item Alias Name</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="append_itve">
                                                                        <tr>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Bp Code</option>
                                                                                    <option>20001256</option>
                                                                                    <option>20011298</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                               <input type="text" class="form-control" id="itemAliasName" maxlength="10" placeholder="Alias Name">
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm btn-danger" onClick="delete_itve(this,0)"><i class="material-icons">delete</i></a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Bp Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bp Code</option>
																<option>20001256</option>
																<option>20011298</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Item Alias Name</label>
																	<input type="text" class="form-control"
																		id="itemAliasName" maxlength="10">
																</div>-->
														</div>
														</div>
														<div class="tab-pane" id="storagelocation">
														  <div class="row">
                                                                <a class="btn btn-sm btn-primary" onClick="add_sto()">Add More</a>
                                                            	<table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Company Code</th>
                                                                            <th>Branch code</th>
                                                                            <th>Storage Loaction</th>
                                                                            <th>Bin Loaction</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="append_sto">
                                                                        <tr>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Company Code</option>
                                                                                    <option>20001256</option>
                                                                                    <option>20011298</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Branch Code</option>
                                                                                    <option>200056</option>
                                                                                    <option>2001298</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Storage Loaction</option>
                                                                                    <option>loc1</option>
                                                                                    <option>loc2</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <select class=" form-control select2" name="cityid">
                                                                                    <option value="select country">Select Bin Loaction</option>
                                                                                    <option>loc1</option>
                                                                                    <option>loc2</option>
                                                                                </select>
                                                                            </td>
                                                                            <td>
                                                                                <a class="btn btn-sm btn-danger" onClick="delete_sto(this,0)"><i class="material-icons">delete</i></a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
																<!--<div class="form-group col-md-3">
																	<label class="control-label">Bp Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bp Code</option>
																<option>20001256</option>
																<option>20011298</option>
																</select>
																</div>
																<div class="form-group col-md-3">
																	<label class="control-label">Item Alias Name</label>
																	<input type="text" class="form-control"
																		id="itemAliasName" maxlength="10">
																</div>-->
														</div>
														</div>
										              </div>
										        </div>
										      	<div class="tab-pane" id="purchase">
													<div class="row">
														<div class="col-md-3">
														<div class="form-group col-md-12">
															<label class="control-label">Bp Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bp Code</option>
																<option>20001256</option>
																<option>20001256</option>
																</select>
														</div>	
														<div class="form-group col-md-122">
																<label class="control-label">Purchase UOM Code</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Purchase UOM Code</option>
																<option>Kg</option>
																<option>Lt</option>
																</select>
														</div>
														<div class="form-group col-md-12">
															<label class="control-label">Minimum Purchase Qty
															</label>
															<input type="text" class="form-control"
																	id="purMinPurchaseQty" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Packing Size</label>
																<input type="text" class="form-control"
																	id="purPackingSze" maxlength="10">
														</div>
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Length</label>
																<input type="text" class="form-control"
																	id="purLength" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Width</label>
																<input type="text" class="form-control"
																	id="purWidth" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Height</label>
																<input type="text" class="form-control"
																	id="purHeight" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Volume</label>
																<input type="text" class="form-control"
																	id="purVolume" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Weight</label>
																<input type="text" class="form-control"
																	id="purWeight" maxlength="10">
														</div>
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="purField1" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="purField2" maxlength="10">
														</div>
														</div> 
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 1</label>
																<input type="text" class="form-control"
																	id="purParameter1" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 2</label>
																<input type="text" class="form-control"
																	id="purParameter2" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 3</label>
																<input type="text" class="form-control"
																	id="purParameter3" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 4</label>
																<input type="text" class="form-control"
																	id="purParameter4" maxlength="10">
														</div>
													</div>
													</div>
										        </div>
										      	<div class="tab-pane" id="sales">
										          <div class="row">
										          	<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Sales UOM</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Sales UOM</option>
																<option>kg</option>
																<option>Lt</option>
																</select>
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Packing Size</label>
																<input type="text" class="form-control"
																	id="salesPcksze" maxlength="10">
														</div>
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Length</label>
																<input type="text" class="form-control"
																	id="salesLnth" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Width</label>
																<input type="text" class="form-control"
																	id="salesWidth" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Height</label>
																<input type="text" class="form-control"
																	id="salesHght" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Volume</label>
																<input type="text" class="form-control"
																	id="salesVolume" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Weight</label>
																<input type="text" class="form-control"
																	id="salesWeight" maxlength="10">
														</div>
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="salesFld1" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="salesFld2" maxlength="10">
														</div> 
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 1</label>
																<input type="text" class="form-control"
																	id="salesPrmtr1" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 2</label>
																<input type="text" class="form-control"
																	id="salesPrmtr2" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 3</label>
																<input type="text" class="form-control"
																	id="salesPrmtr3" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Parameter 4</label>
																<input type="text" class="form-control"
																	id="salesPrmtr4" maxlength="10">
														</div>
														</div>
													</div>
										        </div>
										      	<div class="tab-pane" id="inventory">
										      	<div class="row">
										         <!--  <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="Code" class="form-control"></select>
														</div> -->
														<div class="form-group col-md-3">
															<label class="control-label">Valuation Method</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Valuation Method</option>
																<option>Moving Average</option>	
																<option>FIFO</option>	
																<option>Standard</option>	
																<option>Serial / Batch</option>
																</select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="invFld1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="invFld2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="invFld3" maxlength="10">
														</div>
														</div>
										        </div>
										        
										      	<div class="tab-pane" id="planning">
										          <div class="row">
										          	<div class="col-md-1">
													</div>
										          <div class="col-md-3">
										          <div class="form-group col-md-12">
															<label class="control-label">Planning Method </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Planning Method</option>
																<option>MRP</option>
																<option>None</option>
																</select>
														</div>
														<div class="form-group col-md-12">
															<label class="control-label">Procurement Method</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Procurement Method</option>
																<option>Make</option>
																<option>Buy</option>
																</select>
														</div>
														</div>	
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Order Interval</label>
																<input type="text" class="form-control"
																	id="orderInterval" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Min Qty</label>
																<input type="text" class="form-control"
																	id="minQty" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Lead Time</label>
																<input type="text" class="form-control"
																	id="leadTimePlan" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Tolerance Days</label>
																<input type="text" class="form-control"
																	id="tolDays" maxlength="10">
														</div>
														</div>
														<div class="col-md-3">
														</div>
														<div class="col-md-3">
														<div class="form-group col-md-12">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="planfield1" maxlength="10">
														</div>
														<div class="form-group col-md-12">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="planfield2" maxlength="10">
														</div>
														</div>
														</div>
										        </div>
										      	<div class="tab-pane" id="production">
										         <div class="row">
										          
														<div class="form-group col-md-3">
															<label class="control-label">Issue Method</label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Issue Method</option>
																<option>Blackflush</option>
																<option>Manual</option>
																</select>
														</div>	
														<div class="form-group col-md-3">
																<label class="control-label">Bom Type </label> 
																<select class=" form-control select2" name="cityid">
																<option value="select country">Select Bom Type </option>
																<option>Standard</option>
																<option>Template</option>
																<option>Assembly</option>
																<option>Special</option>
																</select>
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Production Standard Cost</label> 
																<input type="text" name="" class="form-control">
														</div>
														</div>
										        </div>
										      	<div class="tab-pane" id="remarks">
										          <div class="row">
										          <div class="form-group col-md-3">
															<label class="control-label">Item Remarks</label>
															<input type="text" class="form-control"
																	id="itemRemarks" maxlength="10">
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="remfield1" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="remfield2" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="remfield3" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 4</label>
																<input type="text" class="form-control"
																	id="remfield4" maxlength="10">
														</div>
										          </div>
										        </div>
										        <div class="tab-pane" id="classification">
										        	<div class="row">
										        	<div class="form-group col-md-3">
										        	<label for="comment">Item Classification</label>
      												<textarea class="form-control" rows="5" id="comment"></textarea>
      												</div>
      											</div>
										        </div>
										    </div>		
										    
										    											
										</div>
										 <div class="modal-footer clearfix">

											<div class="form-group">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="itemMaster_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="itemMasterSave" type="button"
													style="margin-right: 5px">Save</button>
											</div>
										</div> 
									</div>
								</div>
							</div>
						</div>







						<div class="country_info modal " id="itemMasterEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">ItemMaster
										</h4>
										<!-- ------------------------------ -->
										<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">
												Item Code </label> <input type="text" class="form-control"
												id="itemCodeEdit" maxlength="8" readonly
												onkeyup="this.value = this.value.replace(/[^a-zA-Z0-9 ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Item Name</label> <input type="text" class="form-control"
												id="itemDescEdit" maxlength="50"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">Item
												Type Id </label> 
												<select id="itemTypeIdEdit" class="form-control"></select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">Item
												Group Id</label> <select id="itemGroupIdEdit" class="form-control"></select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">UOM
												Group Code </label> <select id="uomGroupCodeEdit" class="form-control"></select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Valuation Code</label> <select id="valuationCodeEdit" class="form-control"></select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Bar Code</label> <input type="text" class="form-control"
												id="barCodeEdit" maxlength="50">
												
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnercode" class="control-label">Payment
												Term Code </label> <select id="paytermCodeEdit" class="form-control"></select>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Field 1</label> <input type="text" class="form-control"
												id="field1Edit" maxlength="50"
												>
										</div>
										<div class="form-group col-md-2">
											<label for="businesspartnername" class="control-label">
												Field 2</label> <input type="text" class="form-control"
												id="field2Edit" maxlength="50"
												>
										</div>
										
										<br clear="left">
										
										<!-- --------------------------------------- -->
										<ul class="nav nav-tabs" id="tabs">
											<li><a data-toggle="tab" href="#generalEdit">General</a></li>
											<li><a data-toggle="tab" href="#purchaseEdit">Purchase</a></li>
											<li><a data-toggle="tab" href="#salesEdit">Sales</a></li>
											<li><a data-toggle="tab" href="#inventoryEdit">Inventory</a></li>
											<li><a data-toggle="tab" href="#planningEdit">Planning</a></li>
											<li><a data-toggle="tab" href="#productionEdit">Production</a></li>
											<li><a data-toggle="tab" href="#remarksEdit">Remarks</a></li>
										</ul>
									</div>
									<div class="tab-content">
										
										<div class="tab-pane active" id="generalEdit">
										<ul class="nav nav-tabs" id="repoTabs">
												<li><a data-toggle="tab" href="#generalSubEdit">General Sub</a></li>
												<li><a data-toggle="tab" href="#manuCapacityEdit">Manufacture Capacity</a></li>
												<li><a data-toggle="tab" href="#itemExtendEdit">Item Extend</a></li>
												<li><a data-toggle="tab" href="#itemVendorMapEdit">Item Vendor Mapping</a></li>                
									   </ul>
										
										<div class="tab-content">
											<div class="tab-pane active" id="generalSubEdit">
											<div class="row">
												<div class="form-group col-md-3">
													<label class="control-label">Shipping Code</label>
													<select id="shippingCodeEdit" class="form-control"></select>
												</div>
												<div class="form-group col-md-3">
													<label class="control-label">Manage ItemType Code</label>
													<select id="manItemTypeEdit" class="form-control"></select>
												</div>	
												<div class="form-group col-md-3">
														<label class="control-label">Character Id Cap</label>
														<input type="text" class="form-control"
															id="characterIdcapEdit" maxlength="10">
												</div>
												<div class="form-group col-md-3">
													<label class="control-label">Status Id</label>
													<select id="statusIdEdit" class="form-control"></select>
												</div> 
											</div>          
										</div>
										
											
										<div class="tab-pane" id="manuCapacityEdit">
										<div class="row">
                                        
												<div class="form-group col-md-3">
													<label class="control-label">Item mf Seq</label>
													<input type="text" class="form-control"
														id="manCapitemMfseqEdit" maxlength="10">
												</div>
												
												<div class="form-group col-md-3">
													<label class="control-label">Branch Code</label>
													<select id="manCapbranchCodeEdit" class="form-control"></select>
												</div>
												<div class="form-group col-md-3">
													<label class="control-label">Capacity</label>
													<input type="text" class="form-control"
														id="manCapcapacityEdit" maxlength="10">
												</div>	
												<div class="form-group col-md-3">
													<label class="control-label">Status</label>
													<select id="manCapstatusEdit" class="form-control"></select>
												</div>
										</div>
									</div>	
									
									<div class="tab-pane" id="itemExtendEdit">
										<div class="row">
											<!-- <div class="form-group col-md-4">
												<label class="control-label">Item Code</label>
												<input type="text" class="form-control"
													id="itemCode" maxlength="10">
											</div> -->
											<div class="form-group col-md-3">
												<label class="control-label">Company Code</label>
												<select id="itemExcompCodeEdit" class="form-control"></select>
										
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Branch Code</label>
												<select id="itemExbranchCodeEdit" class="form-control"></select>
										
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Cost Center Id</label>
												<select id="itemExcostCenterIdEdit" class="form-control"></select>
										
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Rol Qty</label>
												<input type="text" class="form-control"
													id="itemExrolQtyEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Status Id</label>
												<select id="itemExstatusEdit" class="form-control"></select>
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Allowed Or Blocked</label>
												<input type="checkbox" name="loadItem" id="itemExallowedOrblocked">
										
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Division Code</label>
												<select id="itemExdivisionCodeEdit" class="form-control"></select>
										
											</div>
										</div>
										</div>
										
										<div class="tab-pane" id="itemVendorMapEdit">
										<div class="row">
											<!-- <div class="form-group col-md-4">
												<label class="control-label">Item Code</label>
												<input type="text" class="form-control"
													id="itemCode" maxlength="10">
											</div> -->
											<div class="form-group col-md-3">
												<label class="control-label">Bp Code</label>
												<select id="itemVenBpCodeEdit" class="form-control"></select>
										
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Item Alias Name</label>
												<input type="text" class="form-control"
													id="itemAliasNameEdit" maxlength="10">
											</div>
										</div>
										</div>
								</div>
								</div>
								
								
									<div class="tab-pane" id="purchaseEdit">
										<div class="row">
											<!-- <div class="form-group col-md-3">
												<label class="control-label">Item Code</label>
												<select id="shippingCode" class="form-control"></select>
											</div> -->
											<div class="form-group col-md-3">
												<label class="control-label">Bp Code</label>
												<select id="purBpCodeEdit" class="form-control"></select>
											</div>	
											<div class="form-group col-md-3">
													<label class="control-label">Purchase UOM Code</label>
													<select id="purUomCodeEdit" class="form-control"></select>
								
											</div>
											<div class="form-group col-md-3">
												<label class="control-label">Minimum Purchase Qty
												</label>
												<input type="text" class="form-control"
														id="purMinPurchaseQtyEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Packing Size</label>
													<input type="text" class="form-control"
														id="purPackingSzeEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Length</label>
													<input type="text" class="form-control"
														id="purLengthEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Width</label>
													<input type="text" class="form-control"
														id="purWidthEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Height</label>
													<input type="text" class="form-control"
														id="purHeightEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Volume</label>
													<input type="text" class="form-control"
														id="purVolumeEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Weight</label>
													<input type="text" class="form-control"
														id="purWeightEdit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Field 1</label>
													<input type="text" class="form-control"
														id="purField1Edit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Field 2</label>
													<input type="text" class="form-control"
														id="purField2Edit" maxlength="10">
											</div> 
											<div class="form-group col-md-3">
													<label class="control-label">Parameter 1</label>
													<input type="text" class="form-control"
														id="purParameter1Edit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Parameter 2</label>
													<input type="text" class="form-control"
														id="purParameter2Edit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Parameter 3</label>
													<input type="text" class="form-control"
														id="purParameter3Edit" maxlength="10">
											</div>
											<div class="form-group col-md-3">
													<label class="control-label">Parameter 4</label>
													<input type="text" class="form-control"
														id="purParameter4Edit" maxlength="10">
											</div>
										</div>
								</div>
								
								<div class="tab-pane" id="salesEdit">
										          <div class="row">
										          
														<!-- <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="shippingCode" class="form-control"></select>
														</div> -->
														
														<div class="form-group col-md-3">
																<label class="control-label">Sales UOM Code</label>
																<select id="salesUomCodeEdit" class="form-control"></select>
														</div>
														
														<div class="form-group col-md-3">
																<label class="control-label">Packing Size</label>
																<input type="text" class="form-control"
																	id="salesPckszeEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Length</label>
																<input type="text" class="form-control"
																	id="salesLnthEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Width</label>
																<input type="text" class="form-control"
																	id="salesWidthEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Height</label>
																<input type="text" class="form-control"
																	id="salesHghtEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Volume</label>
																<input type="text" class="form-control"
																	id="salesVolumeEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Weight</label>
																<input type="text" class="form-control"
																	id="salesWeightEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="salesFld1Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="salesFld2Edit" maxlength="10">
														</div> 
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 1</label>
																<input type="text" class="form-control"
																	id="salesPrmtr1Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 2</label>
																<input type="text" class="form-control"
																	id="salesPrmtr2Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 3</label>
																<input type="text" class="form-control"
																	id="salesPrmtr3Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Parameter 4</label>
																<input type="text" class="form-control"
																	id="salesPrmtr4Edit" maxlength="10">
														</div>
													</div>
										        </div>
										      	<div class="tab-pane" id="inventoryEdit">
										      	<div class="row">
										         <!--  <div class="form-group col-md-3">
															<label class="control-label">Item Code</label>
															<select id="Code" class="form-control"></select>
														</div> -->
														<div class="form-group col-md-3">
															<label class="control-label">Valuation Method Code</label>
															<select id="valMethodcodeEdit" class="form-control"></select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="invFld1Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="invFld2Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="invFld3Edit" maxlength="10">
														</div>
														</div>
										        </div>
										        
										        <div class="tab-pane" id="planningEdit">
										          <div class="row">
										          <div class="form-group col-md-3">
															<label class="control-label">Plan Method Code</label>
															<select id="planMethCodeEdit" class="form-control"></select>
														</div>
														<div class="form-group col-md-3">
															<label class="control-label">Procurement Method Code</label>
															<select id="procMethTypeEdit" class="form-control"></select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Order Interval</label>
																<input type="text" class="form-control"
																	id="orderIntervalEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Min Qty</label>
																<input type="text" class="form-control"
																	id="minQtyEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Lead Time</label>
																<input type="text" class="form-control"
																	id="leadTimePlanEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Tolerance Days</label>
																<input type="text" class="form-control"
																	id="tolDaysEdit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="planfield1Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="planfield2Edit" maxlength="10">
														</div>
														
														</div>
										        </div>
										      	<div class="tab-pane" id="productionEdit">
										         <div class="row">
										          
														<div class="form-group col-md-3">
															<label class="control-label">Issue Method Code</label>
															<select id="issueMethodcodeEdit" class="form-control"></select>
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Bom Type Code</label>
																<select id="bomTypecodeEdit" class="form-control"></select>
														</div>
														</div>
										        </div>
										      	<div class="tab-pane" id="remarksEdit">
										          <div class="row">
										          <div class="form-group col-md-3">
															<label class="control-label">Item Remarks</label>
															<input type="text" class="form-control"
																	id="itemRemarksEdit" maxlength="10">
														</div>	
														
														<div class="form-group col-md-3">
																<label class="control-label">Field 1</label>
																<input type="text" class="form-control"
																	id="remfield1Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 2</label>
																<input type="text" class="form-control"
																	id="remfield2Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 3</label>
																<input type="text" class="form-control"
																	id="remfield3Edit" maxlength="10">
														</div>
														<div class="form-group col-md-3">
																<label class="control-label">Field 4</label>
																<input type="text" class="form-control"
																	id="remfield4Edit" maxlength="10">
														</div>
										          </div>
										        </div>
										    </div>
								

										<div class="modal-footer clearfix">
											<div class="form-group" style="">
												<button class="btn btn-danger pull-right btn-sm RbtnMargin"
													id="itemMaster_Reset" type="button">Reset</button>
												<button class="btn btn-success pull-right btn-sm"
													id="itemMasterUpdate" type="button" style="margin-right: 5px">Update</button>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<style>
			.modal-dialog{
				width: 82% !important;
			}
		</style>
        <script>
			function add_mcap()
{
	var txt =  '<tr><td><input type="text" class="form-control" id="manCapitemMfseqEdit" maxlength="10" placeholder="Item Mf Sequence"></td><td><select id="manCapbranchCodeEdit" class="form-control"><option>--- Select Branch Code ---</option><option>1000</option></select></td><td><input type="text" class="form-control" id="manCapcapacityEdit" maxlength="10" placeholder="Capacity"></td><td><select id="manCapstatusEdit" class="form-control"><option>Active</option><option>In-Active</option></select></td><td><a class="btn btn-sm btn-danger" onClick="delete_mcap(this,0)"><i class="material-icons">delete</i></a></td></tr>';
	

	$("#append_mcap").append(txt);
	
}
function delete_mcap(row, id)
{
	//if(id > 0)
//	{
//		 $.ajax({
//		url: site_url + "admin/project/delete_contact", 
//		data:{id:id},
//		type: 'post',
//		success: function(result){
//			 
//		}
//	});
//	}	

	$(row).parents('tr').addClass('animated fadeOut');
        setTimeout(function() {
            $(row).parents('tr').remove();
           //calc_total();
        }, 50);
   
	
}
function add_itex()
{
	var txt =  '<tr><td><select class=" form-control select2" name="cityid"><option value="select country">Select Company Code</option><option>1000</option></select></td><td><select id="manCapbranchCodeEdit" class="form-control"><option>--- Select Branch Code ---</option><option>1000</option></select></td><td><select class=" form-control select2" name="cityid"><option value="select country">Select Cost Center</option><option>Purchase</option><option>Production</option><option>Sales</option></select></td><td><input type="text" class="form-control" id="itemExrolQty" maxlength="10" placeholder="ROL Quantity"></td><td><select id="manCapstatusEdit" class="form-control"><option>Active</option><option>In-Active</option></select></td><td><input type="checkbox"></td><td><select class=" form-control select2" name="cityid"><option> Select Division</option><option>Carpentry</option><option>Metal</option></select></td><td><a class="btn btn-sm btn-danger" onClick="delete_itex(this,0)"><i class="material-icons">delete</i></a></td></tr>';
	

	$("#append_itex").append(txt);
	
}
function delete_itex(row, id)
{
	//if(id > 0)
//	{
//		 $.ajax({
//		url: site_url + "admin/project/delete_contact", 
//		data:{id:id},
//		type: 'post',
//		success: function(result){
//			 
//		}
//	});
//	}	

	$(row).parents('tr').addClass('animated fadeOut');
        setTimeout(function() {
            $(row).parents('tr').remove();
           //calc_total();
        }, 50);
   
	
}
function add_itve()
{
	var txt =  '<tr><td><select class=" form-control select2" name="cityid"><option value="select country">Select Bp Code</option><option>20001256</option><option>20011298</option></select></td><td><input type="text" class="form-control" id="itemAliasName" maxlength="10" placeholder="Alias Name"></td><td><a class="btn btn-sm btn-danger" onClick="delete_itve(this,0)"><i class="material-icons">delete</i></a></td></tr>';
	

	$("#append_itve").append(txt);
	
}
function delete_itve(row, id)
{
	//if(id > 0)
//	{
//		 $.ajax({
//		url: site_url + "admin/project/delete_contact", 
//		data:{id:id},
//		type: 'post',
//		success: function(result){
//			 
//		}
//	});
//	}	

	$(row).parents('tr').addClass('animated fadeOut');
        setTimeout(function() {
            $(row).parents('tr').remove();
           //calc_total();
        }, 50);
   
	
}
function add_sto()
{
	var txt =  '<tr><td><select class=" form-control select2" name="cityid"><option value="select country">Select Company Code</option><option>20001256</option><option>20011298</option></select></td><td><select class=" form-control select2" name="cityid"><option value="select country">Select Branch Code</option><option>200056</option><option>2001298</option></select></td><td><select class=" form-control select2" name="cityid"><option value="select country">Select Storage Loaction</option><option>loc1</option><option>loc2</option></select></td><td><select class=" form-control select2" name="cityid"><option value="select country">Select Bin Loaction</option><option>loc1</option><option>loc2</option></select></td><td><a class="btn btn-sm btn-danger" onClick="delete_sto(this,0)"><i class="material-icons">delete</i></a></td></tr>';
	

	$("#append_sto").append(txt);
	
}
function delete_sto(row, id)
{
	//if(id > 0)
//	{
//		 $.ajax({
//		url: site_url + "admin/project/delete_contact", 
//		data:{id:id},
//		type: 'post',
//		success: function(result){
//			 
//		}
//	});
//	}	

	$(row).parents('tr').addClass('animated fadeOut');
        setTimeout(function() {
            $(row).parents('tr').remove();
           //calc_total();
        }, 50);
   
	
}
		</script>