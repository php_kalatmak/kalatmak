<script type="text/javascript">
	$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-item').addClass('active');
					$('#menu-item > a').attr('aria-expanded', 'true');
					$('#menu-item > ul').addClass('in');

					$('#menu-hsnSacCode').addClass('active');

					$('#HsnSacadd').hide();
					$('#HsnSacEdit').hide();

					$('#hsnSacAdd').click(function() {
					$('#HsnSacadd').show();
					});
													
					$(".close").click(function() {
					$(".modal").hide();
					});

					$('#hsnSac_Reset').click(function(){
					$('#chapterId').val('');
					$('#categories').val('');
					$('#subCategories').val('');
					$('#subSubCategories').val('');
					$('#remarks').val('');
					})
				});
</script>

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">HSN/SAC</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">

						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">HSN/SAC Code</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="hsnSacAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="compCodeTbl">Company</th>
														<th data-field="hsnSacTbl">HSN/SAC</th>
														<th data-field="chapterIdTbl">Chapter Id</th>
														<th data-field="categoryTbl">Categories</th>
														<th data-field="subCategoryTbl">Sub Categories</th>
														<th data-field="subSubCateTbl">Sub Sub Categories</th>
														<th data-field="remarksTbl">Remarks</th>
														<th data-field="chapterIdCapTbl">Chapter_Id_Cap</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Quadraerp</td>
														<td>HSN</td>
														<td>25</td>
														<td>12</td>
														<td>01</td>
														<td>06</td>
														<td>Mobile</td>
														<td>25120106</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												    </tr>
												    <tr>
														<td>Quadraerp</td>
														<td>SAC</td>
														<td>36</td>
														<td>25</td>
														<td>10</td>
														<td>07</td>
														<td>Laptop</td>
														<td>36251007</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												    </tr>
												    <div class="Payterm_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">HSN/SAC</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<div class="form-group col-md-6">
											<label class="control-label">Company Code</label> 	
											<select class=" form-control select2" name="cityid">
												<option value="select country">Select Company Code</option>
												<option>Quadraerp</option>
											</select>
										</div>

										<div class="form-group col-md-6">
											<label class="control-label">HSN/SAC Code </label> 
											<select class=" form-control select2" name="cityid">
												<option value="select country">Select HSN/SAC Code </option>
												<option>HSN</option>
												<option>SAC</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter
												Id</label>
												 <input type="text" class="form-control" id="chapterId" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Categories
												 </label> <input type="text" class="form-control" id="categories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Categories
												 </label> <input type="text" class="form-control"
												id="subCategories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Sub Categories
												 </label> <input type="text" class="form-control"
												id="subSubCategories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Remarks
												 </label> <input type="text" class="form-control"
												id="remarks">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter Id Cap
												 </label> <input type="text" class="form-control"  maxlength="16" readonly
												id="chapterIdCap">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="hsnSac_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="hsnSacSave" type="button" style="margin-right: 5px">Save</button>

										</div>
									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>






						<div class="Payterm_info modal " id="HsnSacadd" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">HSN/SAC</h4>
									</div>
									<div class="modal-body" style="height: 300px">
										<div class="form-group col-md-6">
											<label class="control-label">Company Code</label> 	
											<select class=" form-control select2" name="cityid">
												<option value="select country">Select Company Code</option>
												<option>quadraerp</option>
											</select>
										</div>

										<div class="form-group col-md-6">
											<label class="control-label">HSN/SAC Code </label> 
											<select class=" form-control select2" name="cityid">
												<option value="select country">Select HSN/SAC Code </option>
												<option>HSN</option>
												<option>SAC</option>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter
												Id</label>
												 <input type="text" class="form-control" id="chapterId" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Categories
												 </label> <input type="text" class="form-control" id="categories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Categories
												 </label> <input type="text" class="form-control"
												id="subCategories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Sub Categories
												 </label> <input type="text" class="form-control"
												id="subSubCategories" maxlength="3" onkeypress="return isNumberKey(event);">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Remarks
												 </label> <input type="text" class="form-control"
												id="remarks">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter Id Cap
												 </label> <input type="text" class="form-control"  maxlength="16" readonly
												id="chapterIdCap">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="hsnSac_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="hsnSacSave" type="button" style="margin-right: 5px">Save</button>

										</div>
									</div>
								</div>
							</div>
						</div>


						<!-- <div class="payterm_info modal" id="HsnSacEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">HSN/SAC</h4>
									</div>
									<div class="modal-body" style="height: 300px">

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Company
												Code</label> <select class="form-control" id="compCodeEdit"></select>
										</div>

										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">HSN/SAC
												Code</label> <select class="form-control" id="hsnSacEdit"></select>
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter
												Id</label>
												 <input type="text" class="form-control" id="chapterIdEdit">
										</div>
										

										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Categories
												 </label> <input type="text" class="form-control" id="categoriesEdit">
										</div>
										
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Categories
												 </label> <input type="text" class="form-control"
												id="subCategoryEdit">
										</div>
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Sub Sub Categories
												 </label> <input type="text" class="form-control"
												id="subSubCategoryEdit">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Remarks
												 </label> <input type="text" class="form-control"
												id="remarksEdit">
										</div>
                                        <div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Chapter Id Cap
												 </label> <input type="text" class="form-control"
												id="chapterIdCapEdit">
										</div>
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										 <button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="payterm_Reset" type="button">Reset</button> -->
										<button class="btn btn-success pull-right btn-sm"
											id="hsnSacUpdate" type="button" style="margin-right: 5px">Save</button>
									</div>

								</div>
							</div>
						</div> -->

					</div>
				</div>
			</div>
		</section>