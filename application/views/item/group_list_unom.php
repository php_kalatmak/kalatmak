
<script type="text/javascript">
	$(document)
		.ready(
				function() {
					var grpCode;
					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-item').addClass('active');
					$('#menu-item > a').attr('aria-expanded', 'true');
					$('#menu-item > ul').addClass('in');

					$('#menu-grpListUom').addClass('active');

					$('#GrpListUomadd').hide();
					$('#grplistUomEdit').hide();
					$('#grplistUomEditQQ').hide();

					$('#grpuomAdd').click(function() {
					$('#GrpListUomadd').show();
					});

					$('#grpuom_Reset').click(function() {
					$("#grpuomCode").val('');
					$("#grpuomName").val('');
					});

					$(".close").click(function() {
					$(".modal").hide();
					});
				});
</script>	

<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">Group List UOM</li>
				</ol>
			</div>
			<!-- <?php $grpluo = $this->db->query("select * from erpggluo order by id DESC")->result(); ?> -->
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">

						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Group List Of UOM</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="grpuomAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th>Group Code</th>
														<th>Group Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
														<th data-field="action1" data-formatter="actionFormatter2"
															data-events="actionEvents">Grp Defn</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>KG</td>
														<td>Kilo Gram</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEdit">
												    		<i class="glyphicon glyphicon-pencil"></i></a></td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEditQQ">
												   			<i class="glyphicon glyphicon-modal-window"></i></a></td>
													</tr>
													<tr>
														<td>lt</td>
														<td>Litre</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEdit">
												    		<i class="glyphicon glyphicon-pencil"></i></a></td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEditQQ">
												    		<i class="glyphicon glyphicon-modal-window"></i></a></td>
													</tr>
												<?php //$i=0; foreach($grpluo as $row) { ?>
											<!-- <tr>
											<td><?php echo $row->group_code; ?></td>
											<td><?php echo $row->group_name; ?></td>
											<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEdit<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-pencil"></i></a></td>
											<td><a class="edit ml10" title="" data-toggle="modal" data-target="#grplistUomEditQQ<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-modal-window"></i></a></td>		
											</tr> -->	

						<div class="uom_info modal" id="grplistUomEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/update_grpuom')); ?>
								<input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">UOM</h4>
									</div>
									<div class="modal-body" style="height: 200px">

										<div class="form-group col-md-6">
											<label>Group Code</label>
											<!-- <select id="uomCodeEdit" class="form-control"></select> -->
											<input type="text" class="form-control" readonly name="groupcode" value="<?php echo $row->group_code; ?>"
												id="grpuomCodeEdit">
										</div>

										<div class="form-group col-md-6">
											<label for="description" class="control-label"> Group
												Name</label> <input type="text" class="form-control" name="groupname" value="<?php echo $row->group_name; ?>"
												id="grpuomDescNewEdit">
										</div>

										<div class="form-group col-md-6">
										<label class="control-label">Status</label>
										<select  class="form-control select2" name="stateid"  >
										<option value="select country">Select Status</option>
										    <option value ="Active">Active</option>
											<option value ="InActive">InActive</option>
										</select>
									</div>
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="grpuom_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
									</div>

								</div>
								</form>
							</div>
						</div>
					</tbody>
				</table>
						
						
						<div class="uom_info modal" id="grplistUomEditQQ" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
								
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">UOM</h4>
									</div>
									<div class="modal-body" style="height: 500px">

										<div class="form-group col-md-3">
											<label>Alt. Qty</label>
											<!-- <select id="uomCodeEdit" class="form-control"></select> -->
											<input type="text" class="form-control" id="altQtyGrpUom">
										</div>

										<div class="form-group col-md-3">
											<label class="control-label">Alt. Uom</label>
														
													<select  class=" form-control select2" name="stateid"  >
													<option value="select country">Select Department Name</option>
													<option value="select country">Sales</option>
													<option value="select country">Marketing</option>
													<option value="select country">Medical</option>
													<option value="select country">Accounts</option>
													</select>
										</div>

										<div class="form-group col-md-3">
											<label for="description" class="control-label"> Base
												Qty</label> <input type="text" class="form-control"
												id="baseQtyGrpUom">
										</div>

										<div class="form-group col-md-3">
											<label class="control-label">Base Uom</label>
														
													<select  class=" form-control select2" name="stateid"  >
													<option value="select country">Select Department Name</option>
													<option value="select country">Sales</option>
													<option value="select country">Marketing</option>
													<option value="select country">Medical</option>
													<option value="select country">Accounts</option>
													</select>
										</div>

										<table class="table table-striped table-hover js-exportable dataTable example display newt" id="table_id" >

											<thead>
												<tr>
													<th data-field="grpdefn_uomGrpId">UoM Group Code</th>
													<th data-field="grpdefn_altQty">Alt. Qty</th>
													<th data-field="grpdefn_altUom">Alt. UoM</th>
													<th data-field="grpdefn_baseQty">Base. Qty</th>
													<th data-field="grpdefn_baseUom">Base. UoM</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>UGC0001</td>
													<td>100</td>
													<td>UOM1</td>
													<td>10</td>
													<td>BUOM</td>
												</tr>
												<tr>
													<td>UGC0002</td>
													<td>100</td>
													<td>UOM2</td>
													<td>10</td>
													<td>BUOM</td>
												</tr>
												<tr>
													<td>UGC0003</td>
													<td>100</td>
													<td>UOM3</td>
													<td>10</td>
													<td>BUOM</td>
												</tr>
												<tr>
													<td>UGC0004</td>
													<td>100</td>
													<td>UOM4</td>
													<td>10</td>
													<td>BUOM</td>
												</tr>
												<tr>
													<td>UGC0005</td>
													<td>100</td>
													<td>UOM5</td>
													<td>10</td>
													<td>BUOM</td>
												</tr>
											</tbody>

										</table>



									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="grpDefn_Reset" type="button">Reset</button>
										<button class="btn btn-success pull-right btn-sm"
											id="grpDefnuomEditUpdate" type="button"
											style="margin-right: 5px">Save</button>
									</div>

								</div>
							</div>
						</div>
												
												<?php //$i++; } ?>	

											
										</div>
									</div>
								</div>
							</div>
						</div>






						<div class="Uom_info modal " id="GrpListUomadd" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 10%">
								<?php echo form_open(base_url('general/add_grpuom'),array('id'=>'guom','name'=>'guom','autocomplete'=>'off')); ?>
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">UOM</h4>
									</div>
									<div class="modal-body" style="height: 150px">
										<div class="form-group col-md-6">
											<label for="countrycode" class="control-label">Group
												Code</label> <input type="text" class="form-control" name="group_code" id="grpuomCode">
										</div>

										<div class="form-group col-md-6">
											<label for="countryname" class="control-label">Group
												Name</label> <input type="text" class="form-control" name="group_name" id="grpuomName"
												maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="grpuom_Reset" type="button">Reset</button>
											<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

										</div>
									</div>
									</form>
								</div>
							</div>
						</div>

						
						
						<!-- <div class="uom_info modal" id="grplistUomEditQQ" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">

								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Group Defn</h4>
										<table id="grpDefnTbl"
												class="table table-striped table-hover js-exportable dataTable sample1"
												data-search=true>
												<thead>
													<tr>
														<th width="25%" data-field="grpDefnAltQtyTbId">Alt. Qty</th>
														<th width="25%" data-field="grpDefnAltUomTbDesc">Alt. UoM</th>
														<th width="25%" data-field="grpDefnBaseQtyTbId">Base Qty</th>
														<th width="25%" data-field="grpDefnBaseUomTbDesc">Base UoM</th>
													</tr>
												</thead>
											</table>
									</div>
								</div>
							</div>
						</div> -->




					</div>
				</div>
			</div>
		</section>
		<script type="text/javascript">
			$(".newt").dataTable();
		</script>