<script type="text/javascript">
	$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-bomType').addClass('active');
	$('#BOMTypeAdd').hide();
	$('#BOMTypeEdit').hide();

	$('#bomTypeAdd').click(function() {
		$('#BOMTypeAdd').show();
	});

	$('#bomType_Reset').click(function() {
		$('#bomTypeCode').val('');
		$('#bomTypeName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Master</a></li>
					<li><a href="javascript:void(0);">Item</a></li>
					<li class="active">BOM Type</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">BOM Type</div>
										<div class="panel-body table-responsive">
											<button class="btn btn-sm btn-success" id="bomTypeAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="bomTypeCode">BOM Type Code</th>
														<th data-field="bomTypeName">BOM Type Name</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>Code001</td>
													<td>Standard
															</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>Code002</td>
													<td>Template
																</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>Code003</td>
													<td>Assembly
																</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<tr>
													<td>Code004</td>
													<td>Special</td>
													<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
												</tr>
												<div class="bomType_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BOM Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="bomTypeCode" class="control-label">BOM Type Code
												</label> <input type="text" class="form-control" readonly name="BOM Type Code"
												id="bomTypeCode" maxlength="2"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="bomTypeName" class="control-label">BOM Type
												Name</label> <input type="text" class="form-control"
												id="bomTypeName" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="bomType_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="bomTypeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
											</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="bomType_info modal " id="BOMTypeAdd" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BOM Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="bomTypeCode" class="control-label">BOM Type Code
												</label> <input type="text" class="form-control"
												id="bomTypeCode" maxlength="2"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="bomTypeName" class="control-label">BOM Type
												Name</label> <input type="text" class="form-control"
												id="bomTypeName" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="bomType_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="bomTypeSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="bomType_info modal " id="BOMTypeEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">BOM Type</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="bomTypeEditCode" class="control-label">BOM Type Code
												</label> <input type="text" class="form-control"
												id="bomTypeEditCode" maxlength="2" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="bomTypeEditName" class="control-label">BOM Type
												Name</label> <input type="text" class="form-control"
												id="bomTypeEditName" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="country_Reset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="bomTypeUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>