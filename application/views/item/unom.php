
<script type="text/javascript">
	$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-item').addClass('active');
					$('#menu-item > a').attr('aria-expanded', 'true');
					$('#menu-item > ul').addClass('in');

					$('#menu-uom').addClass('active');

					$('#Uomadd').hide();
					$('#uomEdit').hide();

					$('#uomAdd').click(function() {
						$('#Uomadd').show();
					});

					
					  $('#uom_Reset').click(function() {
						  // alert("Reset :::: ");
						  $("#uomCode").val(''); 
						  $("#uomName").val('');
						  });
					 

					$(".close").click(function() {
						$(".modal").hide();
					});
				});	
</script>
<section class="content">
		<div class="page-heading">
			<h1>Masters</h1>
			<ol class="breadcrumb">
				<li><a href="../../erp/home">Master</a></li>
				<li><a href="javascript:void(0);">General</a></li>
				<li class="active">UOM</li>
			</ol>
		</div>
		<?php $uom = $this->db->query("select * from erpguo order by id DESC")->result(); ?>
		<div class="page-body">
			<div class="row">
				<div class="col-lg-12">

					<div class="page-body" id="dtab">
						<div class="row clearfix margin_full">
							<div class="col-lg-8">
								<div class="panel panel-default">
									<div class="panel-heading">UOM</div>
									<div class="panel-body">
										<button class="btn btn-sm btn-success" id="uomAdd">New</button>
										<br clear="left">
										<table
											class="table table-striped table-hover js-exportable dataTable"
											data-search=true>

											<thead>
												<tr>
													<th>UOM Code</th>
													<th>UOM Name</th>
													<th data-field="action" data-formatter="actionFormatter"
														data-events="actionEvents">Edit</th>
												</tr>
											</thead>
											<tbody>
											<?php $i=0; foreach($uom as $row) { ?>
											<tr>
											<td><?php echo $row->uom_code; ?></td>
											<td><?php echo $row->uom_name; ?></td>
											<td><a class="edit ml10" title="" data-toggle="modal" data-target="#uomEdit<?php echo $i; ?>">
												    <i class="glyphicon glyphicon-pencil"></i></a></td>
											</tr>
											
						<div class="uom_info modal" id="uomEdit<?php echo $i; ?>" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                     
							<div class="modal-content" style="margin-top: 10%">
							  <?php echo form_open(base_url('general/update_uom')); ?>							
							  <input type="hidden" name="edit_id" value="<?php echo $row->id; ?>">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">UOM</h4>
								</div>
								<div class="modal-body" style="height: 180px">
								
									<div class="form-group col-md-6">
										<label>UOM Code</label> 
										<!-- <select id="uomCodeEdit" class="form-control"></select> -->
										 <input type="text" class="form-control" readonly name="uomcode" value="<?php echo $row->uom_code; ?>"
											id="uomCodeEdit">
									</div>

									<div class="form-group col-md-6">
										<label for="description" class="control-label"> UOM
											Name</label> <input type="text" class="form-control" name="uomname" value="<?php echo $row->uom_name; ?>"
											id="uomDescNewEdit">
									</div>

									<div class="form-group col-md-6">
										<label class="control-label">Status</label>
										<select  class="form-control select2" name="stateid"  >
										<option value="select country">Select Status</option>
										    <option value ="Active">Active</option>
											<option value ="InActive">InActive</option>
										</select>
									</div>
								</div>
							</div>
							<div class="modal-footer clearfix">

								<div class="form-group" style="">
									<button class="btn btn-danger pull-right btn-sm RbtnMargin" id="uom_Reset" type="button">Reset</button>
									<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Update">
								</div>

							</div>
							</form>
						</div>
					</div>
											</tbody>
										    <?php $i++; } ?>	

										</table>
									</div>
								</div>
							</div>
						</div>
					</div>






					<div class="Uom_info modal " id="Uomadd" tabindex="-1"
						role="dialog" aria-labelledby="EmployementTypeLabel">
						<div class="modal-dialog">
                            
							<div class="modal-content" style="margin-top: 10%">
							<?php echo form_open(base_url('general/add_uom'), array('id'=>'uom','name'=>'uom','autocomplete'=>'off')); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
									</button>
									<h4 class="modal-title" id="EmployementTypeLabel">UOM</h4>
								</div>
								<div class="modal-body" style="height: 150px">
									<div class="form-group col-md-6">
										<label for="countrycode" class="control-label">UOM
											Code</label> <input type="text" class="form-control" name="uom_code" id="uomCode">
									</div>

									<div class="form-group col-md-6">
										<label for="countryname" class="control-label">UOM
											Name</label> <input type="text" class="form-control" name="uom_name" id="uomName"
											maxlength="30"
											onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
									</div>
								</div>
								<div class="modal-footer clearfix">

									<div class="form-group" style="">
										<button class="btn btn-danger pull-right btn-sm RbtnMargin"
											id="uom_Reset" type="button">Reset</button>
										<input type="submit" name="submit" class="btn btn-success pull-right btn-sm" value="Save">

									</div>
								</div>
							</div>
						</div>
					</div>


					

				</div>
			</div>
		</div>
		</section>