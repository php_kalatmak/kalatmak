
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">


<link href="<?php echo base_url(); ?>assets/css/parsley.css"	rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/font.css"	rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/css/icon.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/css/bootstrap.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/iCheck/skins/flat/_all.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/switchery/dist/switchery.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/metisMenu/dist/metisMenu.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/plugins/pace/themes/white/pace-theme-flash.css"	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/css/themes/allthemes.css" rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/css/demo/setting-box.css" rel="stylesheet" />
<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/css/bootstrap-table.css">
<link	href="<?php echo base_url(); ?>assets/plugins/DataTables/media/css/dataTables.bootstrap.css" rel="stylesheet" />

<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/plugins/imageUpload/media/css/bootstrap-imgupload.css">
<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/plugins/imageUpload/media/css/bootstrap-imgupload.min.css">
<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/plugins/imageUpload/media/css/bootstrap-imguploadn.css">
<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/plugins/imageUpload/media/css/bootstrap-imguploadn.min.css">
<link rel="stylesheet"	href="<?php echo base_url(); ?>assets/css/bootstrap/css/bootstrap-multiselect.css">
<link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet" />

<script src="<?php echo base_url(); ?>assets/plugins/jquery/dist/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/metisMenu/dist/metisMenu.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/screenfull/src/screenfull.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/switchery/dist/switchery.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/jquery-countTo/jquery.countTo.js"></script>

<script	src="<?php echo base_url(); ?>assets/plugins/jquery-sparkline/dist/jquery.sparkline.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-table.js"></script>
<script src="<?php echo base_url(); ?>assets/js/admin.js"></script>
<script src="<?php echo base_url(); ?>assets/js/pages/widgets/infobox.js"></script>

<script src="<?php echo base_url(); ?>assets/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/dataTables.buttons.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/buttons.bootstrap.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/buttons.flash.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/jszip.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/pdfmake.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/vfs_fonts.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/buttons.html5.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/export/buttons.print.min.js"></script>


<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-imgupload.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-imgupload.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-imguploadn.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-imguploadn.min.js"></script>
<script	src="<?php echo base_url(); ?>assets/plugins/bootstrap/dist/js/bootstrap-multiselect.js"></script>




	




<style>
table {
	table-layout: black;
}

table .fixed-table-header {
	table-layout: fixed;
	word-wrap: break-word;
	width: 100%;
}

.modal-dialog {
	width: 900px !important;
}
.select2{
	width:100% !important;
	border-radius:none !important;
}
</style>
<script type="text/javascript">
	function actionFormatter(value, row, index) {
		return [
			'<a class="edit ml10" title=""  data-toggle="modal" data-target="#ConditionMasterEdit" >',
			'<i class="glyphicon glyphicon-pencil"></i>',
			'</a>'
		].join('');
	}
	//$('select').select2();

</script>
