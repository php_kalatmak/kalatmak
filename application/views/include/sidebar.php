<div class="all-content-wrapper">
		<header>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar-collapse">
					<i class="material-icons">swap_vert</i>
				</button>
				<a href="javascript:void(0);"
					class="left-toggle-left-sidebar js-left-toggle-left-sidebar"> <i
					class="material-icons">menu</i>
				</a> <a class="navbar-brand" href="home"> <span
					class="logo-minimized">QERP</span> <span class="logo">Quadra ERP</span>
				</a>

			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="javascript:void(0);"
						class="toggle-left-sidebar js-toggle-left-sidebar"> <i
							class="material-icons">menu</i>
					</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">

					<li><a href="javascript:void(0);"
						class="fullscreen js-fullscreen"> <i class="material-icons">fullscreen</i>
					</a></li>

					<li class="dropdown user-menu"><a href="javascript:void(0);"
						class="dropdown-toggle" data-toggle="dropdown"> <img
							src="<?php echo base_url(); ?>/assets/images/avatars/face2.jpg" alt="User Avatar" /> <span
							class="hidden-xs">Admin</span>
					</a>
						<ul class="dropdown-menu">
							<li class="header"><img
								src="<?php echo base_url(); ?>/assets/images/avatars/face2.jpg" alt="User Avatar" />
								<div class="user">
									Admin
									<div class="title">Front-end Developer</div>
								</div></li>
							<li class="body">
								<ul>
									<li><a href="../../miscellaneous/profile.html"> <i
											class="material-icons">account_circle</i> Profile
									</a></li>
									<li><a href="javascript:void(0);"> <i
											class="material-icons">lock_open</i> Change Password
									</a></li>
								</ul>
							</li>
							<li class="footer">
								<div class="row clearfix">
									<div class="col-xs-2"></div>
									<div class="col-xs-5">
										<a href="logout" class="btn btn-default btn-sm btn-block">Log
											Out </a>
									</div>
								</div>
							</li>
						</ul></li>


				</ul>
			</div>
		</div>
	</nav>
</header>

<aside class="sidebar">

	<nav class="sidebar-nav">
		<ul class="metismenu">
			<li class="title">MAIN NAVIGATION</li>
			<li id="menu-master"><a href="javascript:void(0);"
				class="menu-toggle"> <i class="material-icons">widgets</i> <span
					class="nav-label">Masters</span>
			</a>
				<ul>
                	<li id="menu-general"><a href="javascript:void(0);"
						class="menu-toggle"> <span>General</span>
					</a>
						<ul>
							<li id="menu-companyGroup"><a href="<?php echo base_url('general/company_group')?>">Company Group</a></li>
                            <li id="menu-Company"><a href="<?php echo base_url('general/company')?>">Company</a></li>
                            <li id="menu-compAssignment"><a href="<?php echo base_url('general/company_assignment')?>">Company Assignment</a></li>
                            <li id="menu-branch"><a href="<?php echo base_url('general/branch')?>">Branch</a></li>
                            <li id="menu-branchAssignment"><a href="<?php echo base_url('general/branch_assignment')?>">Branch Assignment</a></li>
                            <li id="menu-currency"><a href="<?php echo base_url('general/currency')?>">Currency</a></li>
                            <li id="menu-country"><a href="<?php echo base_url('general/country')?>">Country</a></li>
                            <li id="menu-state"><a href="<?php echo base_url('general/state')?>">State</a></li>
							<li id="menu-city"><a href="<?php echo base_url('general/city')?>">City</a></li>
							<li id="menu-area"><a href="<?php echo base_url('general/area')?>">Area</a></li>
							
							
							
                            
							
                            <!--<li id="menu-country"><a href="<?php echo base_url('general/country')?>">Country</a></li>
							<li id="menu-state"><a href="<?php echo base_url('general/state')?>">State</a></li>
                            <li id="menu-city"><a href="<?php echo base_url('general/city')?>">City</a></li>
							<li id="menu-currency"><a href="<?php echo base_url('general/currency')?>">Currency</a></li>
							<li id="menu-uom"><a href="<?php echo base_url('general/unom')?>">UOM</a></li>
							<li id="menu-bank"><a href="<?php echo base_url('general/bank')?>">Bank</a></li>
							<li id="menu-grpListUom"><a href="<?php echo base_url('general/group_list_unom')?>">Group List
									UOM</a></li>
							<li id="menu-paymentTerm"><a href="<?php echo base_url('general/payment_term')?>">Payment
									Term</a></li>
							<li id="menu-payTermSlab"><a href="<?php echo base_url('general/payment_term_slab')?>">PaymentTerm
									Slab</a></li>
							<li id="menu-postingPeriod"><a href="<?php echo base_url('general/posting_period')?>">Posting
									Period </a></li>
							<li id="menu-hsnSacCode"><a href="<?php echo base_url('general/hsnsac')?>">HSN/SAC
									Code </a></li>
							<li id="menu-division"><a href="<?php echo base_url('general/division')?>">Division </a></li>-->
						</ul></li>
                        <li id="menu-user"><a href="javascript:void(0);"
						class="menu-toggle"> <span>User</span>
					</a>
						<ul>
							<li id="menu-User"><a href="<?php echo base_url('user/user') ?>">User
									</a></li>
							<li id="menu-rolecreation"><a href="<?php echo base_url('user/role_creation') ?>">Role
									Creation</a></li>
							<li id="menu-moduleCreation"><a href="<?php echo base_url('user/module_creation') ?>">Module
									Creation</a></li>
							<li id="menu-formRightsCreation"><a href="<?php echo base_url('user/form_rights') ?>">Form
									Rights </a></li>
							<li id="menu-roleVsRights"><a href="<?php echo base_url('user/role_vs_rights') ?>">Role Vs
									Rights </a></li>
							<li id="menu-department"><a href="<?php echo base_url('user/department')?>">Department</a></li>		
							<li id="menu-designation"><a href="<?php echo base_url('user/designation')?>">Designation</a></li>
							
							
						</ul></li>
                        <li id="menu-item"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Item</span>
					</a>
						<ul>
                        	<li id="menu-uom"><a href="<?php echo base_url('item/unom')?>">UOM</a></li>
							<li id="menu-grpListUom"><a href="<?php echo base_url('item/group_list_unom')?>">Group List
									UOM</a></li>
                        	<li id="menu-hsnSacCode"><a href="<?php echo base_url('item/hsnsac')?>">HSN/SAC
									Code </a></li>
							<li id="menu-division"><a href="<?php echo base_url('item/division')?>">Division </a></li>
							<li id="menu-valuationType"><a href="<?php echo base_url('item/valuation_type')?>">Valuation
									Type </a></li>
							<li id="menu-itemType"><a href="<?php echo base_url('item/item_type')?>">Item Type </a></li>
							<li id="menu-itemGroup"><a href="<?php echo base_url('item/item_group')?>">Item Group </a></li>
							<li id="menu-mangeItemType"><a href="<?php echo base_url('item/manage_item_type')?>">Manage
									Item Type </a></li>
							<li id="menu-shippingType"><a href="<?php echo base_url('item/shipping_type')?>">Shipping
									Type</a></li>
							<li id="menu-valuationMethod"><a href="<?php echo base_url('item/valuation_method')?>">Valuation
									Method</a></li>
							<li id="menu-itemMaster"><a href="<?php echo base_url('item/item_master')?>">Item
									Master</a></li>
							<li id="menu-bomType"><a href="<?php echo base_url('item/bom_type')?>">BOM Type</a></li>
							<li id="menu-product_categories"><a href="<?php echo base_url('item/product_categories')?>">Product Categories</a></li>
							<li id="menu-brand"><a href="<?php echo base_url('item/brand')?>">Brand</a></li>
						</ul></li>
					<li id="menu-businesspartner"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Business Partner</span> </a>
                      <ul>
                        <li id="menu-salesgroup"><a href="<?php echo base_url('business/sales_group')?>">Sales
                          Group</a></li>
                        <li id="menu-grade"><a href="<?php echo base_url('business/grade')?>">Grade</a></li>
                        <li id="menu-bnusinessPartnerType"><a
								href="<?php echo base_url('business/business_partnertype')?>"> Business Partner Type</a></li>
                        <li id="menu-businessPartnergroup"><a
								href="<?php echo base_url('business/business_partner_group')?>"> Business Partner Group</a></li>
                        <li id="menu-businessPartnerMaster"><a
								href="<?php echo base_url('business/business_partner_master')?>"> Business Partner Master</a></li>
                      </ul>
					</li>
					<li id="menu-accounts"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Accounts</span>
				    </a>
					  <ul>
                        <li id="menu-exchangeRate"><a href="<?php echo base_url('accounts/exchange_rate') ?>">Exchange
                        Rate</a></li>
                        <li id="menu-bank"><a href="<?php echo base_url('accounts/bank')?>">Bank</a></li>
                        <li id="menu-paymentTerm"><a href="<?php echo base_url('accounts/payment_term')?>">Payment
                        Term</a></li>
                        <!-- <li id="menu-payTermSlab"><a href="<?php echo base_url('accounts/payment_term_slab')?>">PaymentTerm
                        Slab</a></li> -->
                        <li id="menu-postingPeriod"><a href="<?php echo base_url('accounts/posting_period')?>">Posting
                        Period </a></li>
						<li id="menu-mode_of_payment"><a href="<?php echo base_url('accounts/mode_of_payment')?>">Mode Of Payment</a></li>
					    </ul>
				  </li>
                  <li id="menu-storage"><a href="javascript:void(0);" class="menu-toggle"><span>Storage</span> </a>
                    <ul>
                      <li id="menu-storageLocation"><a href="<?php echo base_url('storage/storage_location') ?>">Storage Location</a></li>
                      <li id="menu-storagelocationassignment"><a href="<?php echo base_url('storage/storage_location_assignment') ?>">Storage Location Assignment</a></li>
                      <li id="menu-binlocation"><a href="<?php echo base_url('storage/bin_location') ?>">Bin Location</a></li>
                    </ul>
                  </li>
                    <li id="menu-gl"><a href="javascript:void(0);"
						class="menu-toggle"><span>GL Determination</span> </a>
                      <ul>
                        <li id="menu-ordType"><a href="<?php echo base_url('gldetermination/order_type')?>">Order Type</a></li>
                        <!-- <li id="menu-salepurType"><a href="<?php echo base_url('gldetermination/sales_purchase_type')?>">Sales
                          Purchase Type</a></li> -->
                        <li id="menu-glDeterRule"><a href="<?php echo base_url('gldetermination/gl_determination_rule')?>">GL
                          Determination Rule</a></li>
                      </ul>
                    </li>
                    <li id="menu-condition"><a href="javascript:void(0);"
						class="menu-toggle"><span>Conditions</span>
                    </a>
                      <ul>
                        <!--<li id="menu-compAssignment"><a href="<?php echo base_url('condition/company_assignment') ?>">Company
                          Assignment</a></li>
                        <li id="menu-branchAssignment"><a href="<?php echo base_url('condition/branch_assignment') ?>">Branch
                          Assignment</a></li>
                        <li id="menu-exchangeRate"><a href="<?php echo base_url('condition/exchange_rate') ?>">Exchange
                          Rate</a></li>-->
                        <li id="menu-conditionType"><a href="<?php echo base_url('condition/condition_type') ?>">Condition
                          Type </a></li>
                        <li id="menu-conditionMaster"><a href="<?php echo base_url('condition/condition_master') ?>">Condition
                          Master </a></li>
                        <li id="menu-conditionTableCreation"><a
								href="<?php echo base_url('condition/condition_table_creation') ?>">Condition Table Creation</a></li>
                        <li id="menu-sequenceMapping"><a href="<?php echo base_url('condition/sequence_mapping') ?>">Sequence
                          Tab Mapping</a></li>
                        </ul>
                    </li>
					<!--<li id="menu-company"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Company</span>
					</a>
						<ul>

							<li id="menu-companyGroup"><a href="<?php echo base_url('company/company_group')?>">Company
									Group</a></li>
							<li id="menu-Company"><a href="<?php echo base_url('company/company')?>">Company</a></li>
							<li id="menu-branch"><a href="<?php echo base_url('company/branch')?>">Branch</a></li>
							<li id="menu-User"><a href="<?php echo base_url('company/user')?>">User</a></li>
						</ul></li>
					<li id="menu-finance"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Finance</span>
					</a>
						<ul>
							<li id="menu-ag"><a href="accountgroup">Accounts Group</a></li>
							<li><a href="../panels/colored.html">Accounts Head</a></li>
						</ul></li>

					

					<li id="menu-rights"><a href="javascript:void(0);"
						class="menu-toggle"> <span>Rights</span>
					</a>
						<ul>
							<li id="menu-rolecreation"><a href="<?php echo base_url('rights/role_creation')?>">Role
									Creation </a></li>
							<li id="menu-moduleCreation"><a href="<?php echo base_url('rights/module_creation')?>">Module
									Creation </a></li>
							<li id="menu-formRightsCreation"><a
								href="<?php echo base_url('rights/form_rights')?>">Form Rights </a></li>
							<li id="menu-roleVsRights"><a href="<?php echo base_url('rights/role_vs_rights')?>">RoleVsRights
							</a></li>

						</ul></li>-->
		  </ul>
		</li>

			<li id="menu-production">
				<a href="javascript:void(0);" class="menu-toggle"> <i class="material-icons">poll</i> <span class="nav-label">Production</span></a>
				<ul>
					<li id="menu-bom"><a href="<?php echo base_url('production/bom')?>">Bill Of Material</a></li>
					<li id="menu-resource"><a href="<?php echo base_url('production/resource')?>">Resource</a></li>
					<li id="menu-route"><a href="<?php echo base_url('production/route')?>">Route Stage</a></li>
					<li id="menu-productionorder"><a href="<?php echo base_url('production/productionorder')?>">Production Order</a></li>
					<li id="menu-issueproduction"><a href="<?php echo base_url('production/issueproduction')?>">Issue For Production</a></li>
					<li id="menu-receivefromproduction"><a href="<?php echo base_url('production/receivefromproduction')?>">Receive From Production</a></li>
					<li id="menu-productionsettings"><a href="<?php echo base_url('production/productionsettings')?>">Production Settings </a></li>
				</ul>
			</li>

			<li id="menu-purchase">
				<a href="javascript:void(0);" class="menu-toggle"> <i class="material-icons">shopping_cart</i> <span class="nav-label">Purchase</span></a>
				<ul>
					<li id="menu-purchaserequest"><a href="<?php echo base_url('purchase/purchaserequest')?>">Purchase Request</a></li>
					<li id="menu-purchaseorder"><a href="<?php echo base_url('purchase/purchaseorder')?>">Purchase Order</a></li>
					<li id="menu-goodsreceiptpo"><a href="<?php echo base_url('purchase/goodsreceiptpo')?>">Goods Receipt PO</a></li>
					<li id="menu-apinvoice"><a href="<?php echo base_url('purchase/apinvoice')?>">AP Invoice</a></li>
					<li id="menu-goodsissue"><a href="<?php echo base_url('purchase/goodsissue')?>">Goods Issue</a></li>
					<li id="menu-goodsreceipt"><a href="<?php echo base_url('purchase/goodsreceipt')?>">Goods Receipt</a></li>
					<li id="menu-landedcost"><a href="<?php echo base_url('purchase/landedcost')?>">Landed Cost</a></li>
					<li id="menu-apcreditmemo"><a href="<?php echo base_url('purchase/apcreditmemo')?>">AP Credit Memo</a></li>
					<li id="menu-freightmasterform"><a href="<?php echo base_url('purchase/freightmasterform')?>">Freight Master Form</a></li>
					<li id="menu-landedcostmasterform"><a href="<?php echo base_url('purchase/landedcostmasterform')?>">Landed Cost Master Form</a></li>

					<!-- <li id="menu-grpopage"><a href="<?php echo base_url('purchase/grpopage')?>">GRPO Batch & Serial page</a></li> -->
					<!-- <li id="menu-batchserialtranspage"><a href="<?php echo base_url('purchase/batchserialtranspage')?>">Transfer GR Batch & Serial page</a></li> -->
					<!-- <li id="menu-apdownpayment"><a href="<?php echo base_url('purchase/apdownpayment')?>">AP Downpayment page</a></li> -->
				</ul>
			</li>

			<li id="menu-finance">
				<a href="javascript:void(0);" class="menu-toggle"> <i class="material-icons">monetization_on</i> <span class="nav-label">Finance</span></a>
				<ul>
					<li id="menu-chartofaccounts"><a href="<?php echo base_url('finance/chartofaccounts')?>">Chart Of Accounts</a></li>
					<!-- <li id="menu-glmasters"><a href="<?php echo base_url('finance/glmasters')?>">GL Master</a></li> -->
					<li id="menu-journalentry"><a href="<?php echo base_url('finance/journalentry')?>">Journal Entry</a></li>
					<li id="menu-journalvoucher"><a href="<?php echo base_url('finance/journalvoucher')?>">Journal Voucher</a></li>
					<!-- <li id="menu-recurringposting"><a href="<?php echo base_url('finance/recurringposting')?>">Recurring Posting</a></li>
					<li id="menu-reversallist"><a href="<?php echo base_url('finance/reversallist')?>">Reversal List</a></li>
					<li id="menu-reconcilation"><a href="<?php echo base_url('finance/reconcilation')?>">Reconcilation</a></li>
					<li id="menu-costcenter"><a href="<?php echo base_url('finance/costcenter')?>">Cost Center</a></li>
					<li id="menu-costingreport"><a href="<?php echo base_url('finance/costingreport')?>">Costing Report</a></li> -->
				</ul>
			</li>

			<li id="menu-banking">
				<a href="javascript:void(0);" class="menu-toggle"> <i class="material-icons">account_balance</i> <span class="nav-label">Banking</span></a>
				<ul>
					<li id="menu-incomingpayment"><a href="<?php echo base_url('banking/incomingpayment')?>">Incoming Payment</a></li>
					<li id="menu-outgoingpayment"><a href="<?php echo base_url('banking/outgoingpayment')?>">Outgoing Payment</a></li>
					<li id="menu-deposit"><a href="<?php echo base_url('banking/deposit')?>">Deposit</a></li>
					<li id="menu-manualreconsilation"><a href="<?php echo base_url('banking/manualreconsilation')?>">Manual Reconsilation</a></li>
					<li id="menu-taxpayment"><a href="<?php echo base_url('banking/taxpayment')?>">Tax Payment</a></li>
					<li id="menu-paymentorder"><a href="<?php echo base_url('banking/paymentorder')?>">Payment Order</a></li>
					<li id="menu-autopayment"><a href="<?php echo base_url('banking/autopayment')?>">Auto Payment</a></li>
				</ul>
			</li>
		</ul>
	</nav>
</aside>