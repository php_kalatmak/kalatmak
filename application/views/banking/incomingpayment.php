<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-banking').addClass('active');
	$('#menu-banking > a').attr('aria-expanded', 'true');
	$('#menu-banking> ul').addClass('in');

	$('#menu-incomingpayment').addClass('active');
	$('#menu-incomingpayment > a').attr('aria-expanded', 'true');
	$('#menu-incomingpayment > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>Banking</h1>
		<ol class="breadcrumb">
			<li><a href="">Banking</a></li>
			<li class="active">Incoming Payment</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab" >
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Incoming Payment</div>
								<div class="panel-body">



								<div class="row">
									<div class="col-sm-2">
									</div>
									<div class="col-sm-3">
										<label class="radio-inline"><input type="radio" value="click1" id="radiobtn" name="optradio" checked>Customer</label>
									</div>
									<div class="col-sm-3">
										<label class="radio-inline"><input type="radio" value="click2" id="radiobtn1" name="optradio">Vendor</label>
									</div>
									<div class="col-sm-3">
										<label class="radio-inline"><input type="radio" value="click3" id="radiobtn2" name="optradio">GL Account</label>
									</div>
									<div class="col-sm-2">
									</div>
								</div>
								<div id="CV">
								<h2 id="Customer">Customer</h2>
								<h2 id="Vendor">Vendor</h2>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Company</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Company</option>
											<option>Quadraerp</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Doc_No</label> 
								    	 <div class="col-sm-8"> 
											<input type="text" class="form-control" id="" placeholder="Doc_No" readonly="">
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Posting Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Branch</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Branch</option>
											<option>general</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Project</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Project</option>
											<option>Project1</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Due Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">BP Code</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select BP Code</option>
											<option>BP00054</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Currency</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Currency</option>
											<option>INR</option>
											<option>USD</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Document Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">BP Name</label> 
								    	 <div class="col-sm-8"> 
											<input type="text" class="form-control" id="" readonly="">
										 </div>
										</div>
									</div>
								</div>
								<br/>
							

								<div class="row">
									<div class="col-md-12 table-responsive">
										<table class="table table-bordered" >
											<thead>
												<tr>
													<th>Select</th>
													<th>Document_No</th>
													<th>Instalment</th>
													<th>Document_Type</th>
													<th>Date</th>
													<th>Due_Date</th>
													<th>Over_Due_Days</th>
													<th>Doc_Remarks</th>
													<th>Document_Amount</th>
													<th>Paid_Amount</th>
													<th>Balance_Amount</th>
												</tr>
											</thead>
											<tbody id="service_tab1">
												<tr>
													<td>
														<input type="checkbox">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Document_No" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Instalment" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Document_Type" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="date" class="form-control" id="" readonly="">
													</td>
													<td>
														<input type="date" class="form-control" id="" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Over_Due_Days" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Doc_Remarks" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Document_Amount" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Paid_Amount" readonly="" style="width: 100px;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Balance_Amount" readonly="" style="width: 100px;">
													</td>
												</tr>
											</tbody>
										</table>
									</div>									
								</div>
							</div>


						
							<div id="GLAccount">
								<h1>GL Account</h1>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Company</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Company</option>
											<option>Quadraerp</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Doc_No</label> 
								    	 <div class="col-sm-8"> 
											<input type="text" class="form-control" id="" placeholder="Doc_No" readonly="">
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Posting Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Branch</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Branch</option>
											<option>general</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Project</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Project</option>
											<option>Project1</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Due Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Currency</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Currency</option>
											<option>INR</option>
											<option>USD</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Document Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<br/>
									<div class="col-md-12 table-responsive">
										<div class="col-md-1 ">
												<button class="form-control btn btn-lg btn-success" style="line-height: 0.333px" onclick="addmore()">Add</button> 
										<div style="padding: 5px"></div>
										</div>
										<table class="table table-bordered" >
											<thead>
												<tr>
													<th>GL_Account</th>
													<th>Account_Name</th>
													<th>Doc_Remarks</th>
													<th>Amount</th>
													<th>Delete</th>
												</tr>
											</thead>
											<tbody id="service_tab">
												<tr>
													<td>
														<input type="text" name="" class="form-control" placeholder="GL_Account" ">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Account_Name" ">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Doc_Remarks" ;">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Amount" ">
													</td>
													<td>
														<a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a>
													</td>
												</tr>
											</tbody>
										</table>
									</div>						
								</div>


								<div class="row">
									<div class="col-sm-6" >
										<div style="padding: 10px !important;"></div>
										<div class="col-md-12 ">
											<div class="form-group">
												<label class="control-label col-sm-4" for="comment">Remarks</label>
												<div class="col-sm-8"> 
			  									<textarea class="textarea form-control" style="height: 120px!important" placeholder=" Journal_Remarks" id="comment"></textarea>
			  									</div>
		  									</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
									    	<label class="control-label col-sm-4" for="">Control Account</label> 
									    	 <div class="col-sm-8"> 
												<select class=" form-control select2" name="cityid">
												<option value="select country">Select Control Account</option>
												<option>bank</option>
												</select>
											 </div>
											</div>
										</div>
									</div>
									<div class="col-sm-2" >
									</div>
									<div class="col-sm-4" >
										<div style="padding: 20px !important;"></div>
										<div class="col-lg-12">
									    	<label class="control-label col-sm-5" for="">Total Amount Due</label> 
									    	 <div class="col-sm-7"> 
												<input type="text" class="form-control" id="" >
											 </div>
										</div>
									</div>
								</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<script>

function addmore(){
var txt = '<tr><td><input type="text" name="" class="form-control" placeholder="GL_Account" "></td><td><input type="text" name="" class="form-control" placeholder="Account_Name" "></td><td><input type="text" name="" class="form-control" placeholder="Doc_Remarks" ;"></td><td><input type="text" name="" class="form-control" placeholder="Amount" "></td><td><a class="btn btn-danger" onclick="delete_contact(this,0)"><i class="material-icons">delete</i></a></td></tr>';
$("#service_tab").append(txt);

}

function delete_contact(row, id)
{
if(id > 0)
{
$.ajax({
url: site_url + "admin/project/delete_contract", 
data:{id:id},
type: 'post',
success: function(result){
}
});
}
$(row).parents('tr').addClass('animated fadeOut');
setTimeout(function() {
$(row).parents('tr').remove();
//calc_total();
}, 50);

}

	$("#Customer").show();
	$("#Vendor").hide();
	$("#GLAccount").hide();
	$("#radiobtn").on("change",function(){
		var type = $("#radiobtn").val();
		if(type == 'click1'){
			$("#Customer").show();
			$("#CV").show();
			$("#Vendor").hide();
			$("#GLAccount").hide();
		} 
	});
	$("#radiobtn1").on("change",function(){
		var type = $("#radiobtn1").val();
		if(type == 'click2'){
			$("#Customer").hide();
			$("#CV").show();
			$("#Vendor").show();
			$("#GLAccount").hide();
		}
	});
	$("#radiobtn2").on("change",function(){
		var type = $("#radiobtn2").val();
		if(type == 'click3'){
			$("#CV").hide();
			$("#GLAccount").show();
		}
	});
	

</script>