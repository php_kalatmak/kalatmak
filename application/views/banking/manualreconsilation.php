<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-banking').addClass('active');
	$('#menu-banking > a').attr('aria-expanded', 'true');
	$('#menu-banking> ul').addClass('in');

	$('#menu-manualreconsilation').addClass('active');
	$('#menu-manualreconsilation > a').attr('aria-expanded', 'true');
	$('#menu-manualreconsilation > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>Banking</h1>
		<ol class="breadcrumb">
			<li><a href="">Banking</a></li>
			<li class="active">Manual Reconsilation</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab" >
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Manual Reconsilation</div>
								<div class="panel-body">


									<h2><b>Selection Screen</b></h2>
									<div class="row">
										<div class="col-lg-4">
											<form class="form-horizontal" action="/action_page.php">
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Account Code</label>
										      <div class="col-sm-8">
										        <input type="text" class="form-control" id="" placeholder="Account Code" name="" readonly="">
										      </div>
										    </div>
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Currency</label>
										      <div class="col-sm-8">          
										        <input type="text" class="form-control" id="" placeholder="Currency" name="pwd" readonly="">
										      </div>
										    </div>
										  	</form>
										</div>
									</div>

									<h2><b>Bank Statement</b></h2>
									<div class="row">
									<div class="col-lg-4">
										<form class="form-horizontal" action="/action_page.php">
									    <div class="form-group">
									      <label class="control-label col-sm-4" for="">Last Balance</label>
									      <div class="col-sm-8">
									        <input type="text" class="form-control" id="" placeholder="Last Balance" name="" readonly="">
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-sm-4" for="">Ending Balance</label>
									      <div class="col-sm-8">          
									        <input type="text" class="form-control" id="" placeholder="Ending Balance" name="pwd" >
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-sm-4" for="">End Date</label>
									      <div class="col-sm-8">          
									        <input type="Date" class="form-control" id="" placeholder="End Date" name="pwd" >
									      </div>
									    </div>
									  	</form>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-lg-4">
										<form class="form-horizontal" action="/action_page.php">
									    <div class="form-group">
									      <label class="control-label col-sm-4" for="">Account Code</label>
									      <div class="col-sm-8">          
									        <input type="text" class="form-control" id="" placeholder="Account Code" name="pwd" >
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-sm-4" for="">Find</label>
									      <div class="col-sm-8">          
									        <input type="text" class="form-control" id="" placeholder="Find" name="pwd" >
									      </div>
									    </div>
									  	</form>
									</div>
									<div class="col-lg-4">
									</div>
									<div class="col-lg-4">
										<form class="form-horizontal" action="/action_page.php">
											<div class="form-group">
									      <label class="control-label col-sm-4" for="">Statement No</label>
									      <div class="col-sm-8">          
									        <input type="text" class="form-control" id="" placeholder="Statement No" name="pwd" >
									      </div>
									    </div>
										</form>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-md-12 table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Cleared</th>
													<th>Transaction_Type</th>
													<th>Date</th>
													<th>Transaction_No</th>
													<th>Doc_Remarks</th>
													<th>Ref</th>
													<th>Payment</th>
													<th>Deposit</th>
													<th>Cleared_Amount</th>
												</tr>
											</thead>
											<tbody id="service_tab">
												<tr>
													<td>
														<input type="checkbox">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Transaction_Type" readonly="">
													</td>
													<td>
														<input type="Date" name="" class="form-control" placeholder="Date" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Transaction_No" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Doc_Remarks" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Ref" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Payment" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Deposit" readonly="">
													</td>
													<td>
														<input type="text" name="" class="form-control" placeholder="Cleared_Amount" readonly="">
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-lg-4">
										<form class="form-horizontal" action="/action_page.php">
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Payment</label>
										      <div class="col-sm-8">          
										        <input type="text" class="form-control" id="" placeholder="Payment" name="pwd" readonly="">
										      </div>
										    </div>
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Deposit</label>
										      <div class="col-sm-8">          
										        <input type="text" class="form-control" id="" placeholder="Deposit" name="pwd" readonly="">
										      </div>
										    </div>
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Total No</label>
										      <div class="col-sm-8">          
										        <input type="text" class="form-control" id="" placeholder="Total No" name="pwd" readonly="">
										      </div>
										    </div>
										    <div class="form-group">
										      <label class="control-label col-sm-4" for="">Total Amount</label>
										      <div class="col-sm-8">          
										        <input type="text" class="form-control" id="" placeholder="Total Amount" name="pwd" readonly="">
										      </div>
										    </div>
										    <br>
										    <div class="form-group">
										  <div class="row">
										  	<div class=" col-sm-12" style="text-align: right; padding-right: 30px;">
										      <button type="button" class="btn btn-success">Reconcile</button>
										       <button type="button" class="btn btn-success">Cancel</button>
										    </div>
										    </div> 
										  </div>
									  	</form>
									</div>
									<div class="col-lg-4">
									</div>
									<div class="col-lg-4">
										<form class="form-horizontal" action="/action_page.php">
										<div class="form-group">
									      <label class="control-label col-sm-5" for="">Clear Book Balance</label>
									      <div class="col-sm-7">          
									        <input type="text" class="form-control" id="" placeholder="Clear Book Balance" name="pwd" >
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-sm-5" for="">Statement Balance</label>
									      <div class="col-sm-7">          
									        <input type="text" class="form-control" id="" placeholder="Statement Balance" name="pwd" >
									      </div>
									    </div>
									    <div class="form-group">
									      <label class="control-label col-sm-5" for="">Difference</label>
									      <div class="col-sm-7">          
									        <input type="text" class="form-control" id="" placeholder="Difference" name="pwd" >
									      </div>
									    </div>
									    <br>
										 <div class="form-group">
										  <div class="row">
										  	<div class=" col-sm-12" style="text-align: right; padding-right: 30px;">
										      <button type="button" class="btn btn-success">Adjustment</button>
										    </div>
										    </div> 
										  </div>
										</form>
									</div>
								</div>


											
										

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

