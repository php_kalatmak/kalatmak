<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-banking').addClass('active');
	$('#menu-banking > a').attr('aria-expanded', 'true');
	$('#menu-banking> ul').addClass('in');

	$('#menu-deposit').addClass('active');
	$('#menu-deposit > a').attr('aria-expanded', 'true');
	$('#menu-deposit > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>Banking</h1>
		<ol class="breadcrumb">
			<li><a href="">Banking</a></li>
			<li class="active">Deposit</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab" >
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Deposit</div>
								<div class="panel-body">



								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Deposit No</label> 
								    	 <div class="col-sm-8"> 
											<input type="text" class="form-control" id="" placeholder="Deposit No" readonly="">
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Deposit Date</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Considered Until</label> 
								    	 <div class="col-sm-8"> 
											<input type="date" class="form-control" id="" >
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Deposit_Currency</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Deposit Currency</option>
											<option>INR</option>
											<option>USD</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">GL Account</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select GL Account</option>
											<option>Acc1</option>
											<option>Acc2</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
								    	<label class="control-label col-sm-4" for="">Branch</label> 
								    	 <div class="col-sm-8"> 
											<select class=" form-control select2" name="cityid">
											<option value="select country">Select Branch</option>
											<option>General</option>
											</select>
										 </div>
										</div>
									</div>
									<div class="col-sm-4">
									</div>
									<div class="col-sm-4">
									</div>
								</div>

						<div class="row form-group">
							<div class="tab-content">
									<ul class="nav nav-tabs" id="tabs">
										<div style="padding: 10px;"></div>
										<li><a data-toggle="tab" href="#check">Check</a></li>
										<li><a data-toggle="tab" href="#bank">Cash</a></li>
									</ul>
									<div class="tab-pane active" id="check">
										<div id="tab">
											<div class="col-sm-6">
												<div class="form-group">
											    	<label class="control-label col-sm-3" for="">Search Check No</label> 
											    	 <div class="col-sm-5"> 
														<input type="text" class="form-control" id="" placeholder="Search Check No">
													 </div>
												</div>
											</div>
											<div style="padding: 20px;"></div>
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Select</th>
																	<th>Date</th>
																	<th>Check</th>
																	<th>Bank</th>
																	<th>Branch</th>
																	<th>Account_No</th>
																	<th>BP/Account_Code</th>
																	<th>BP/Account_Name</th>
																	<th>Check_Amount</th>
																	<th>Project</th>
																	<th>Incoming_Payment</th>
																</tr>
															</thead>
															<tbody id="service_tab">
																<tr>
																	<td>
																		<input type="checkbox">
																	</td>
																	<td>
																		<input type="Date" name="" class="form-control" placeholder="Check" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Oty" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Bank" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Branch" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Account_No" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="BP/Account_Code" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="BP/Account_Name" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Check_Amount" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Project" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Incoming_Payment" readonly="">
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											
										</div>
									</div>

								     <div class="tab-pane " id="bank">
										<div id="tab">
												<div class="row">
													<div class="col-sm-4">
														<div class="form-group">
												    	<label class="control-label col-sm-4" for="">GL Account</label> 
												    	 <div class="col-sm-8"> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select GL Account</option>
															<option>Acc1</option>
															<option>Acc2</option>
															</select>
														 </div>
														</div>
													</div>
													<div class="col-sm-4">
													</div>
													<div class="col-sm-4">
														<div class="form-group">
												    	<label class="control-label col-sm-4" for="">GL Balance</label> 
												    	 <div class="col-sm-8"> 
															<input type="text" class="form-control" id="" readonly="">
														 </div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-4">
														<div class="form-group">
												    	<label class="control-label col-sm-4" for="">Amount</label> 
												    	 <div class="col-sm-8"> 
															<input type="text" class="form-control" id="">
														 </div>
														</div>
													</div>
													<div class="col-sm-4">
													</div>
													<div class="col-sm-4">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


							<div class="row">
									<div style="padding: 10px !important;"></div>
									<div class="row col-sm-6" >
										<div class="col-md-12 ">
											<div class="form-group">
												<label class="control-label col-sm-3" for="comment">Remarks</label>
												<div class="col-sm-8"> 
			  									<textarea class="textarea form-control" style="height: 120px!important"  placeholder="Journal Remarks" id="comment"></textarea>
			  									</div>
		  									</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group">
									    	<label class="control-label col-sm-3" for="">Trans.No</label> 
									    	 <div class="col-sm-8"> 
												<input type="text" class="form-control" id="" >
											 </div>
											</div>
										</div>

										<div class="col-sm-12">
											<div style="padding: 5px !important;"></div>
									    	 <div class="col-sm-6"> 
												<button type="button" class="btn btn-success">Add</button>
											 </div>
											 <div class="col-sm-6"> 
												<button type="button" class="btn btn-danger">Cancel</button>
											 </div>
										</div>
									</div>
									<div class="row col-sm-4" >
									</div>
								</div>

													
										

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

