<script type="text/javascript">
	$(document).ready(function(){
		
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-binlocation').addClass('active');
	$('#BinLocation').hide();
	$('#binlocationEdit').hide();

	$('#binlocationAdd').click(function() {
		$('#BinLocation').show();
	});
	$(".close").click(function () {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Storage</a></li>
					<li class="active">Bin Location</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Bin Location</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="binlocationAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="branchDesc">Branch Name</th>
														<th data-field="storageDesc">Storage Name</th>
														<th data-field="binId">Bin Code</th>
														<th data-field="Binname">Bin Name</th>
														<th data-field="capacity">Capacity <br/><b>Cu.Mt</b></th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>General</td>
														<td>Storage Location 1</td>
														<td>BIN0001</td>
														<td>Rack 1</td>
														<td>150</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Storage Location 2</td>
														<td>BIN0002</td>
														<td>Rack 2</td>
														<td>120</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Metallic</td>
														<td>BIN0003</td>
														<td>Rack 3</td>
														<td>120</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Carpentry</td>
														<td>BIN0004</td>
														<td>Rack 4</td>
														<td>100</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Packing</td>
														<td>BIN0005</td>
														<td>Rack 5</td>
														<td>80</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Bin Location</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label class="control-label">Branch Name</label> 	
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Branch Name</option>
													<option>General</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label class="control-label">Storage Name</label> 	
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Storage Name</option>
													<option>Matalic</option>
													<option>Packing</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label for="bincode" class="control-label">Bin Code
												</label> <input type="text" class="form-control"
												id="bincode"maxlength="4"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="binname" class="control-label">Bin Name
												</label> <input type="text" class="form-control"
												id="binname"maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="capacity" class="control-label">Capacity
												</label> <input type="text" class="form-control"
												id="capacity" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
										<label class="control-label">Status</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>Active</option>
													<option>Inactive</option>
													</select>
												</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="binlocation_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="BinlocationSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="BinLocation" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Bin Location</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label class="control-label">Branch Name</label> 	
													<select class=" form-control select2" name="cityid" >
													<option value="select country">Select Branch Name</option>
													<option>General</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label class="control-label">Storage Name</label> 	
													<select class=" form-control select2" name="cityid" >
													<option value="select country">Select Storage Name</option>
													<option>Matalic</option>
													<option>Packing</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label for="bincode" class="control-label">Bin Code
												</label> <input type="text" class="form-control"
												id="bincode"maxlength="4"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="binname" class="control-label">Bin Name
												</label> <input type="text" class="form-control"
												id="binname"maxlength="8"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="capacity" class="control-label">Capacity
												</label> <input type="text" class="form-control"
												id="capacity" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="binlocation_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="BinlocationSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div> 
						<div class="storage_info modal " id="binlocationEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Bin Location</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										
										<div class="form-group col-md-6">
											<label for="branchname" class="control-label">Branch
												Name</label> <input type="text" class="form-control"
												id="branchnameEdit" maxlength="2" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="storagenames" class="control-label">Storage
												Name</label> <input type="text" class="form-control"
												id="storagenameEdit" maxlength="2" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="Bincode" class="control-label">Bin
												Code</label> <input type="text" class="form-control"
												id="BincodeEdit" maxlength="2" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										
										<div class="form-group col-md-6">
											<label for="BinName" class="control-label">Bin
												Name</label> <input type="text" class="form-control"
												id="BinNameEdit" maxlength="30" 
												onkeyup="this.value = this.value.replace(/[^a-zA-Z]/, '')">
										</div>
										
											<div class="form-group col-md-6">
											<label for="capacity" class="control-label">Capacity 
												</label> <input type="text" class="form-control"
												id="CapacityEdit" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
									
																												
										<div class="form-group col-md-6">
													<label>Status</label> <select id="statusBinEdit"
														class="form-control">

													</select>
												</div>
												<div class="form-group col-md-6">
											<label for="countryname" class="control-label">
												</label> <input type="text" hidden="true"
												id="BranchId" maxlength="30">
										</div>
											<div class="form-group col-md-6">
											<label for="countryname" class="control-label">
												</label> <input type="text" hidden="true"
												id="getStorageId" maxlength="30">
										</div>
												
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-success pull-right btn-sm"
												id="binlocationUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>