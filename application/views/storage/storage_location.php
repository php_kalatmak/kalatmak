

<script type="text/javascript">
	$(document).ready(function () {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-storageLocation').addClass('active');
	$('#Storage').hide();
	$('#storageEdit').hide();

	$('#storageAdd').click(function () {
		$('#Storage').show();
	});

	$('#storage_Reset').click(function () {
		$('#storagecode').val('');
		$('#storagename').val('');
	});
   $('#storageEdit_Reset').click(function () {
		$('#storagenameEdit').val('');
		$('#statusEdit').val('-1');
	});
	$(".close").click(function () {
		$(".modal").hide();
	});
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Storage</a></li>
					<li class="active">Storage Location</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8 ">
									<div class="panel panel-default">
										<div class="panel-heading">Storage Location</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="storageAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="storageId">Storage Code</th>
														<th data-field="storageDesc">Storage Name</th>
														<th data-field="status">Status</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>STO0001</td>
														<td>Storage Location 1</td>
														<td>Acive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>STO0002</td>
														<td>Storage Location 2</td>
														<td>Acive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>STO0003</td>
														<td>Metallic</td>
														<td>Acive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>STO0004</td>
														<td>Carpentry</td>
														<td>Acive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>STO0005</td>
														<td>Packing</td>
														<td>Acive</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Storage Location</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="storagecode" class="control-label">Storage
												Code</label> <input type="text" class="form-control" readonly="" 
												id="storagecode"  maxlength="3"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="storagename" class="control-label">Storage
												Name</label> <input type="text" class="form-control"
												id="storagename" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
										<label class="control-label">Status</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>Active</option>
													<option>InActive</option>
													</select>
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storage_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="storageSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="Storage" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Storage Location</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="storagecode" class="control-label">Storage
												Code</label> <input type="text" class="form-control"
												id="storagecode"  maxlength="3"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="storagename" class="control-label">Storage
												Name</label> <input type="text" class="form-control"
												id="storagename" maxlength="20"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storage_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="storageSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="country_info modal " id="storageEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">STORAGE LOCATION</h4>
									</div>
									<div class="modal-body" style="height: 150px">

									<!-- 	<div class="form-group col-md-6">
											<label for="storagecode" class="control-label">Storage
												Code</label> <select class="form-control"
												id="storagecodeEdit">
												
												</select>
										</div> -->
										<div class="form-group col-md-6">
											<label for="storagecodeEdit" class="control-label">Storage Code
												</label> <input type="text" class="form-control"
												id="storagecodeEdit" maxlength="4" readonly="readonly"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
											<label for="storagename" class="control-label">Storage
												Name</label> <input type="text" class="form-control"
												id="storagenameEdit" maxlength="30"
												onkeyup="this.value = this.value.replace(/[^a-zA-Z ]/, '')">
										</div>
										<div class="form-group col-md-6">
													<label>Status</label> <select id="statusEdit"
														class="form-control">

													</select>
												</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storageEdit_Reset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="storageUpdate" type="button" style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>