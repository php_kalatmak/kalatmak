<script type="text/javascript">
	$(document).ready(function () {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-storagelocationassignment').addClass('active');
	$('#StorageAss').hide();
	$('#storageAssEdit').hide();

	$('#storageAssAdd').click(function () {
		$('#StorageAss').show();
	});

	$('#storageAss_Reset').click(function () {
		$('#branchname').val('-1');
		$('#storagenameass').val('-1');
		$('#capacity').val('');
	});
	$('#storageAssEdit_Reset').click(function () {

		$('#statusAssEdit').val('-1');
		$('#capacityEdit').val('');
	});

	$(".close").click(function () {
		$(".modal").hide();
	});
});

</script>	
<section class="content">
			<div class="page-heading">
				<h1>Masters</h1>
				<ol class="breadcrumb">
					<li><a href="../../erp/home">Masters</a></li>
					<li><a href="javascript:void(0);">Storage</a></li>
					<li class="active">Storage Location Assignment</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Storage Location Assignment</div>
										<div class="panel-body">
											<button class="btn btn-sm btn-success" id="storageAssAdd">New</button>
											<br clear="left">
											<table
												class="table table-striped table-hover js-exportable dataTable"
												data-search=true>

												<thead>
													<tr>
														<th data-field="branchdesc">Branch Name</th>
														<!-- <th data-field="branchId" hidden="true">Branch Code</th> -->
														<th data-field="storagedesc">Storage Name</th>
														<th data-field="capacity">Capacity</th>
														<th data-field="action" data-formatter="actionFormatter"
															data-events="actionEvents">Edit</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>General</td>
														<td>Storage Location 1</td>
														<td>1500 Cu.Mt</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Storage Location 2</td>
														<td>1200 Cu.Mt</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Carpentry</td>
														<td>1000 Cu.Mt</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Metallic</td>
														<td>1200 Cu.Mt</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<tr>
														<td>General</td>
														<td>Packing</td>
														<td>1000 Cu.Mt</td>
														<td><a class="edit ml10" title="" data-toggle="modal" data-target="#StateEdit">
													                <i class="glyphicon glyphicon-pencil"></i></a></td>
													</tr>
													<div class="storage_info modal " id="StateEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Storage
											Location Assignment</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label class="control-label">Branch Name</label> 	
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Branch Name</option>
													<option>General</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label class="control-label">Storage Name</label> 	
													<select class=" form-control select2" name="cityid" disabled="">
													<option value="select country">Select Storage Name</option>
													<option>Matalic</option>
													<option>Packing</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label for="capacity" class="control-label">Capacity
											</label> <input type="text" class="form-control" id="capacity"
												 maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div class="form-group col-md-6">
										<label class="control-label">Status</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Status</option>
													<option>INR</option>
													<option>EUR</option>
													</select>
									    </div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storageAss_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="storageAssSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
												</tbody>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="storage_info modal " id="StorageAss" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 10%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">Storage
											Location Assignment</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label class="control-label">Branch Name</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Branch Name</option>
													<option>General</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label class="control-label">Storage Name</label> 	
													<select class=" form-control select2" name="cityid">
													<option value="select country">Select Storage Name</option>
													<option>Matalic</option>
													<option>Packing</option>
													</select>
										</div>
										<div class="form-group col-md-6">
											<label for="capacity" class="control-label">Capacity
											</label> <input type="text" class="form-control" id="capacity"
												 maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storageAss_Reset" type="button">Reset</button>
											<button class="btn btn-success pull-right btn-sm"
												id="storageAssSave" type="button" style="margin-right: 5px">Save</button>

										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="storage_info modal " id="storageAssEdit" tabindex="-1"
							role="dialog" aria-labelledby="EmployementTypeLabel">
							<div class="modal-dialog">
								<div class="modal-content" style="margin-top: 25%">
									<div class="modal-header">
										<button type="button" class="close " data-dismiss="modal">
											<span aria-hidden="true">&times;</span> <span class="sr-only">Close</span>
										</button>
										<h4 class="modal-title" id="EmployementTypeLabel">STORAGE
											LOCATION ASSIGNMENT</h4>
									</div>
									<div class="modal-body" style="height: 150px">

										<div class="form-group col-md-6">
											<label for="branchnameEdit" class="control-label">Branch
												Name</label> <input type="text" class="form-control" id="branchnameEdit" readonly="readonly">
										</div>
										
										<div class="form-group col-md-6">
											<label for="storagenames" class="control-label">Storage
												Name</label> <input type="text" class="form-control" id="storagenameEdit" readonly="readonly">
										</div>

								       									
										<div class="form-group col-md-6">
											<label for="capacity" class="control-label">
												Capacity</label> <input type="text" class="form-control"
												id="Capacity" maxlength="8"
												onkeyup="this.value = this.value.replace(/[^0-9 ]/, '')">
										</div>
										<div>
											<label for="BranchId" class="control-label"> </label> <input
												type="hidden" class="form-control" id="BranchId"
												maxlength="8">
										</div>
										<div>
											<input type="hidden" class="form-control" id="StoregeId"
												maxlength="8"> <label for="StoregeId"
												class="control-label"> </label>
										</div>
										<div class="form-group col-md-6">
											<label>Status</label> <select id="statusAssEdit"
												class="form-control">

											</select>
											<div class="form-group col-md-6">
											<label for="countryname" class="control-label">
												</label> <input type="text" hidden="true"
												id="BranchIdhidden" maxlength="30">
										</div>
											<div class="form-group col-md-6">
											<label for="countryname" class="control-label">
												</label> <input type="text" hidden="true"
												id="getStorageIdhidden" maxlength="30">
										</div>
										</div>
									</div>
									<div class="modal-footer clearfix">

										<div class="form-group" style="">
											<!-- <button class="btn btn-danger pull-right btn-sm RbtnMargin"
												id="storageAssEdit_Reset" type="button">Reset</button> -->
											<button class="btn btn-success pull-right btn-sm"
												id="storageAssUpdate" type="button"
												style="margin-right: 5px">Update</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>