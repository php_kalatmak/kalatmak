<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-productionsettings').addClass('active');
	$('#menu-productionsettings > a').attr('aria-expanded', 'true');
	$('#menu-productionsettings > ul').addClass('in');
});
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masters/common.js"></script>
<section class="content">
	<div class="page-heading">
		<h1>PRODUCTION</h1>
		<ol class="breadcrumb">
			<li><a href="">Master</a></li>
			<li><a href="javascript:void(0);">General</a></li>
			<li class="active">BOM</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Production Setting</div>
								<div class="panel-body">
									<div class="form-group">
										<div class="col-md-6">
											<input type="radio" name="" id="proSettingPO1">
											<label>Production Order -- > Issue for Production -- > Receive from Production</label>
										</div>
										<div class="col-md-6">
											<input type="radio" name="" id="proSettingPO2">
											<label>Production Order -- > Receive from Production -- > Auto Issue for Production</label>
										</div>
										<div class="col-md-6" id="hidden_Check1">
											<input type="checkbox" name="">Auto Purchase Request Generation<br> 
											<input type="checkbox" name="">Open Purchase Order<br> 
											<input type="checkbox" name="">Stock<br>
											<input type="checkbox" name="">Open Purchase Request
										</div>
										<div class="col-md-6" id="hidden_Check2">
											<input type="checkbox" name="">Material Issue Parent Production Order Based<br> 
											<input type="checkbox" name="">Auto Child Production Order Generation <br> 
											<input type="checkbox" name="">Batch
										</div>
									</div>
									<div style="padding: 20px"></div>
									<div class="col-md-12">
										<div class="form-group text-center">
											<button class="btn btn-info">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>
