<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-productionorder').addClass('active');
	$('#menu-productionorder > a').attr('aria-expanded', 'true');
	$('#menu-productionorder > ul').addClass('in');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>PRODUCTION</h1>
				<ol class="breadcrumb">
					<li><a href="">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">BOM</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Production Order</div>
										<div class="panel-body">
											<div class="form-group">
												<form class="form-vertical">
													<div class="form-group">
													    <div class="col-md-4">
													    	<label class="control-label">Production Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Production Type</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label">Production Status</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Production Status</option>
															<option>Status1</option>
															<option>Status2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="itmdesc">Production Number</label>
													    	<input type="text" class="form-control" id="" placeholder="Production Number">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="itmdesc">Post Date</label>
													    	<input type="date" class="form-control" id="">
													    </div>
													</div>
													<div class="form-group">
													    <div class="col-md-4">
													    	<label class="control-label">Company Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label">Factory Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Factory Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="bom">Sales Order Number</label>
															<input type="text" name="" class="form-control" placeholder="Sales Order Number">
													    </div>
													 </div>
													 <div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="compcode">Start Date</label>
															<input type="date" name="" class="form-control">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="manfplanet">Product Material Code</label>
													    	<input type="text" name="" class="form-control" placeholder="Product Material Code">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="manfplanet">Product Material Description</label>
													    	<textarea class="form-control" rows="2" placeholder="Product Material Description"></textarea>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="">Planned Quantity</label>
															<input type="text" class="form-control" name="fdate" placeholder="Planned Quantity">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="todate">Due Date</label>
															<input type="date" class="form-control" name="todate">
													    </div>
													</div>
												</form>
											</div>
											<div style="padding: 160px"></div>
											<div class="col-md-12 table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>S.No</th>
															<th>Bar Code</th>
															<th>Type</th>
															<th>Mat.Code</th>
															<th>Description</th>
															<th>Base Quantity</th>
															<th>Additional Qty</th>
															<th>Planned Qty</th>
															<th>Issed Qty</th>
															<th>Available Qty</th>
															<th>UOM</th>
															<th>Location</th>
															<th>Issue Method</th>
															<th>Start Date</th>
															<th>End Date</th>
															<th>Sequence</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>
																<input type="text" name="" class="form-control  col-sm-1" placeholder="Bar C.." style="width: 50px;">
															</td>
															<td>
																<select class="form-control" style="width: 100px;">
																	<option>Select</option>
																	<option>Material</option>
																	<option>Route</option>
																</select>
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Mat Code">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Mat Des..">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="B QTY" style="width: 50px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="A Qty" style="width: 60px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="P Qty" style="width: 60px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="I Qty" style="width: 60px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Av Qty">
															</td>
															<td>
																<select class="form-control" style="width: 50px">
																	<option>select</option>
																	<option>UOM</option>
																</select>
															</td>
															<td>
																<select class="form-control" style="width: 50px">
																	<option>select</option>
																	<option>001F</option>
																	<option>001A</option>
																</select>
															</td>
															<td>
																<select class="form-control">
																	<option>select</option>
																	<option>Manual</option>
																	<option>BackFlush</option>
																</select>
															</td>
															<td><input type="date" class="form-control" name="" style="width: 150px"></td>
															<td><input type="date" class="form-control" name="" style="width: 150px"></td>
															<td><input type="date" class="form-control" name="" style="width: 150px"></td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-12">
												<div class="form-group text-center">
													<button class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>