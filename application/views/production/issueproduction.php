<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-issueproduction').addClass('active');
	$('#menu-issueproduction > a').attr('aria-expanded', 'true');
	$('#menu-issueproduction > ul').addClass('in');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>PRODUCTION</h1>
				<ol class="breadcrumb">
					<li><a href="">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">BOM</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Bill Of Material</div>
										<div class="panel-body">
											<div class="form-group">
												<form class="form-vertical">
													<div class="form-group">
													    <div class="col-md-3">
													    	<label class="control-label">Company Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label">Factory Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Factory Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label">Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Type</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">
													    <div class="col-md-3">
													    	<label class="control-label" for="lotsiz">Document Number</label>
													    	<input type="text" class="form-control" id="lotsiz" placeholder="Document Number">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label">Reference Document Number</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Reference Document Number</option>
															<option>No1</option>
															<option>No2</option>
															</select>
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label" for="bom">Doc Number</label>
													    	<input type="text" class="form-control" name="" placeholder="Doc Number">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label" for="compcode">Fiscal Year</label>
													    	<input type="date" class="form-control" name="">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-3"> 
													    	<label class="control-label" for="manfplanet">Post Date</label>
															<input type="date" class="form-control" name="">
													    </div>
													</div>
												</form>
											</div>
											<div style="padding: 120px"></div>
											<div class="col-md-12 table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>S.No</th>
															<th>Bar Code</th>
															<th>Item Type</th>
															<th>Mat.Code</th>
															<th>Description</th>
															<th>Balance Qty</th>
															<th>Issue Qty</th>
															<th>Batch No</th>
															<th>Return Qty</th>
															<th>Location</th>
															<th>planned</th>
															<th>Issued Qty</th>
															<th>Available Qty</th>
															<th>UOM</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>
																<input type="text" name="" class="form-control  col-sm-1" placeholder="Bar C.." style="width: 50px;">
															</td>
															<td>
																<select class="form-control" style="width: 100px;">
																	<option>Select</option>
																	<option>Material</option>
																	<option>Resource</option>
																</select>
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Mat Code">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Mat Des..">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="QTY" style="width: 50px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Qty" style="width: 60px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Batch No" style="width: 60px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Ret Qty" style="width: 60px">
															</td>
															<td>
																<select class="form-control" style="width: 100px;">
																	<option>Select</option>
																	<option>001H</option>
																	<option>003A</option>
																</select>
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder=" Planned">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Qty">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Qty">
															</td>
															<td>
																<select class="form-control" style="width: 70px;">
																	<option>Select</option>
																	<option>UOM</option>
																</select>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-12">
												<div class="form-group text-center">
													<button class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>