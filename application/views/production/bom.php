<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-bom').addClass('active');
	$('#menu-bom > a').attr('aria-expanded', 'true');
	$('#menu-bom > ul').addClass('in');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>PRODUCTION</h1>
				<ol class="breadcrumb">
					<li><a href="">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">BOM</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Bill Of Material</div>
										<div class="panel-body">
											<div class="form-group">
												<form class="form-vertical">
													<div class="row">
													    <div class="col-md-4">
													    	<label class="control-label" for="itemcod">Item Code</label>
													      	<input type="text" class="form-control" id="itmcode" placeholder="Item Code">
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="itmqty">Default Quantity</label>
													      	<input type="text" class="form-control" id="qty" placeholder="Default Quantity">
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="itmdesc">Item Description</label>
													      	<textarea class="form-control" rows="2" id="itmdesc" placeholder="Item Description"></textarea>
													    </div>
													</div>
													<div class="row">
													    <div class="col-md-4">
													    	<label class="control-label" for="lotsiz">LOT Size</label>
													    	<input type="text" class="form-control" id="lotsiz" placeholder="LOT Size">
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label">Direction Code </label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Direction Code </option>
															<option>Code1</option>
															<option>code2</option>
															</select>
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label">BOM Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select BOM Type</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
													    </div>
												</div>
													<div class="row">													    
													    <div class="col-md-4"> 
													    	<label class="control-label">Company Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company Code</option>
															<option>Code1</option>
															<option>code2</option>
															</select>
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="fdate">From</label>
															<input class="form-control" type="date" name="fdate">
													    </div>													    
													    <div class="col-md-4"> 
													    	<label class="control-label" for="todate">To</label>
															<input class="form-control" type="date" name="todate">
													    </div>
													</div>
												</form>
											</div>
											<div style="padding: 20px"></div>
											<div class="col-md-12 table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>S.No</th>
															<th>Bar_Code</th>
															<th>Type</th>
															<th>Item</th>
															<th>Description</th>
															<th>Quantity</th>
															<th>UOM</th>
															<th>Location</th>
															<th>Issed Method</th>
															<th>Per Price</th>
															<th>Total Price</th>
															<th>Stage</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>
																<input type="text" name="" class="form-control  col-sm-1" placeholder="Bar C.." >
															</td>
															<td>
																<select class="form-control select2" style="width: 100px;">
																	<option>Select</option>
																	<option>Resource</option>
																	<option>Route Stage</option>
																	<option>Text</option>
																</select>
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Item Code">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder=" Item Des..">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="QTY" style="width: 50px">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="UOM" style="width: 60px">
															</td>
															<td>
																<select class="form-control select2" style="width: 100px;">
																	<option>Select</option>
																	<option>#006</option>
																	<option>#003</option>
																	<option>#008</option>
																</select>
															</td>
															<td>
																<select class="form-control select2" style="width: 100px;">
																	<option>Select</option>
																	<option>Manual</option>
																</select>
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Unit P..">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Total P">
															</td>
															<td>
																<input type="text" name="" class="form-control" placeholder="Sequence">
															</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-12">
												<div class="form-group text-center">
													<button class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>