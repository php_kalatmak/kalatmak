<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-route').addClass('active');
	$('#menu-route > a').attr('aria-expanded', 'true');
	$('#menu-route > ul').addClass('in');
});
</script>
<section class="content">
			<div class="page-heading">
				<h1>PRODUCTION</h1>
				<ol class="breadcrumb">
					<li><a href="">Master</a></li>
					<li><a href="javascript:void(0);">General</a></li>
					<li class="active">BOM</li>
				</ol>
			</div>
			<div class="page-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="page-body" id="dtab">
							<div class="row clearfix margin_full">
								<div class="col-lg-8">
									<div class="panel panel-default">
										<div class="panel-heading">Route Stage Master</div>
										<div class="panel-body">
											<div class="form-group">
												<form class="form-vertical">
												  	<div class="form-group">													    
													    <div class="col-md-6"> 
													    	<label class="control-label">Company Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
												  	</div>
												  	<div class="form-group">													    
													    <div class="col-md-6"> 
													    	<label class="control-label">Factory Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Factory Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
													    </div>
												  	</div>
													<div class="form-group">
													    <div class="col-md-6">
													    	<label class="control-label" for="lotsiz">Direction Code</label>
													    	<input type="text" class="form-control" id="lotsiz" placeholder="Direction Code">
													    </div>
													</div>
													<div class="form-group">													    
													    <div class="col-md-6"> 
													    	<label class="control-label" for="itmdesc">Direction Description</label>
													      <textarea class="form-control" rows="2" id="itmdesc" placeholder="Direction Description"></textarea>
													    </div>
													</div>
												</form>
											</div>
											<div style="padding: 100px"></div>
											<div class="col-md-12 table-responsive">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th>Sequence Code</th>
															<th>Stage Code</th>
															<th>Stage Description</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>												
															<td>cutting</td>												
															<td>cutting</td>													
														</tr>
														<tr>
															<td>1</td>												
															<td>pending</td>												
															<td>pending</td>													
														</tr>
														<tr>
															<td>1</td>												
															<td>pending</td>												
															<td>pending</td>													
														</tr>
													</tbody>
												</table>
											</div>
											<div class="col-md-12">
												<div class="form-group text-center">
													<button class="btn btn-info">Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>