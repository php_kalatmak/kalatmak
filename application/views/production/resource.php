<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-production').addClass('active');
	$('#menu-production > a').attr('aria-expanded', 'true');
	$('#menu-production> ul').addClass('in');

	$('#menu-resource').addClass('active');
	$('#menu-resource > a').attr('aria-expanded', 'true');
	$('#menu-resource > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PRODUCTION</h1>
		<ol class="breadcrumb">
			<li><a href="">Master</a></li>
			<li><a href="javascript:void(0);">General</a></li>
			<li class="active">BOM</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Production Resource</div>
								<div class="panel-body">
									<div class="form-group">
										<form class="form-vertical">
											<div class="form-group">
											    <div class="col-md-4">
											    	<label class="control-label">Resource Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Resource Type</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
											    </div>
											</div>
											<div class="form-group">									    
											    <div class="col-md-4"> 
											    	<label class="control-label">Company Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>
											</div>
											<div class="form-group">											    
											    <div class="col-md-4"> 
											    	<label class="control-label">Factory Code</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Factory Code</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>
											</div>
											<div class="form-group">
											    <div class="col-md-4">
											    	<label class="control-label" for="lotsiz">Resource Code</label>
											    	<input type="text" class="form-control" id="lotsiz" placeholder="Resource Code">
											    </div>
											  </div>
											<div class="form-group">											    
											    <div class="col-md-4"> 
											    	<label class="control-label" for="direction">Resource Name</label>
											    	<input type="text" class="form-control" id="lotsiz" placeholder="Resource Name">
											    </div>
											</div>
											<div class="form-group">											    
											    <div class="col-md-2"> 
											    	<label class="control-label" for="fdate">From</label>
											    	<input type="text" class="form-control" id="lotsiz" placeholder="Resource Per Minute">
											    </div>
											</div>
											<div class="form-group">											    
											    <div class="col-md-2"> 
											    	<label class="control-label" for="todate">To</label>
											    	<input type="text" class="form-control" id="lotsiz" placeholder="Resource Cost">
											    </div>
											</div>
										</form>
									</div>

									<div style="padding: 80px"></div>
									<div id="tab">	
										<ul class="nav nav-tabs">
											<li class="active"><a href="#1" data-toggle="tab">General</a></li>
											<li><a href="#2" data-toggle="tab">Fixed Assets</a></li>
											<li><a href="#3" data-toggle="tab">Employee Mapping</a></li>
											<li><a href="#4" data-toggle="tab">Planning</a></li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="1">
												<div class="row">
													<div class="form-group">
														<form class="form-vertical">
														  	<div class="form-group">										    
															    <div class="col-md-4"> 
															    	<label class="control-label">Issue Method</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Issue Method</option>
															<option>IM1</option>
															<option>IM2</option>
															</select>
															    </div>
														  	</div>
														  	<div class="form-group">											    
															    <div class="col-md-2"> 
															    	<label class="control-label" for="fdate">Valid From</label>
																	<input type="date" name="fdate">
															    </div>
														  	</div>
														  	<div class="form-group">											    
															    <div class="col-md-2"> 
															    	<label class="control-label" for="todate">Valid To</label>
																	<input type="date" name="todate">
															    </div>
														  	</div>
														</form>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="2">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>S.No</th>
																	<th>Asset Code</th>
																	<th>Asset Name</th>
																	<th>Asset Status</th>
																	<th>Employee Code</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="">
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="3">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>S.No</th>
																	<th>Employee Code</th>
																	<th>Employee Name</th>
																	<th>Employee Status</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="">
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="4">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>Day</th>
																	<th>Monday</th>
																	<th>Tuesday</th>
																	<th>Wednesday</th>
																	<th>Thursday</th>
																	<th>Friday</th>
																	<th>Saturday</th>
																	<th>Sunday</th>
																</tr>
															</thead>
															<tbody>
																<tr class="text-center">
																	<td>1</td>
																	<td>8</td>
																	<td>8</td>
																	<td>8</td>
																	<td>8</td>
																	<td>8</td>
																	<td>8</td>
																	<td>8</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>		
										</div>
									</div>
									<div style="padding: 20px"></div>
									<div class="col-md-12">
										<div class="form-group text-center">
											<button class="btn btn-info">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>
