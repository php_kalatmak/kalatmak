<script type="text/javascript">
	$(document).ready(function(){
	$('#menu-purchase').addClass('active');
	$('#menu-purchase > a').attr('aria-expanded', 'true');
	$('#menu-purchase> ul').addClass('in');

	$('#menu-purchaseorder').addClass('active');
	$('#menu-purchaseorder > a').attr('aria-expanded', 'true');
	$('#menu-purchaseorder > ul').addClass('in');
});
</script>
<section class="content">
	<div class="page-heading">
		<h1>PRODUCTION</h1>
		<ol class="breadcrumb">
			<li><a href="">Master</a></li>
			<li><a href="javascript:void(0);">General</a></li>
			<li class="active">BOM</li>
		</ol>
	</div>
	<div class="page-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-body" id="dtab">
					<div class="row clearfix margin_full">
						<div class="col-lg-8">
							<div class="panel panel-default">
								<div class="panel-heading">Purchase Order</div>
								<div class="panel-body">
									<div class="form-group">
										<form class="form-vertical">
											<div class="row">
											    <div class="col-md-3">
											    	<label class="control-label">Order Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Order Type</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>								    
											    <div class="col-md-3"> 
											    	<label class="control-label">Company</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Company</option>
															<option>Company1</option>
															<option>Company2</option>
															</select>
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="todate">Document Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label">Status</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Status</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>
										</div>
										<div class="row ">
											<div class="form-group">											    
											    <div class="col-md-3">
											    	   <div class=" col-md-6">
											    	      <label class="control-label">Reference Type</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Ref Type</option>
															<option>Type1</option>
															<option>type2</option>
															</select>
											          </div>
											    <div class=" col-md-6 ">
											    	<label class="control-label" for="itmqty">Ref Number</label>
													<input type="text" name="" class="form-control" placeholder="Ref Number">
											    </div>
											    </div>
											</div>								    
											    <div class="col-md-3"> 
											    	<label class="control-label">Factory/Branch</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Factory/Branch</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Delivery Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label">Ship To</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Ship To</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
											    </div>
										</div>
										<div class="row ">							    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="itmqty">Document Number</label>
													<input type="text" name="" class="form-control" placeholder="Document Number">
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label">Vendor</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Vendor</option>
															<option>Type1</option>
															<option>Type2</option>
															</select>
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Post Date</label>
											    	<input type="date" class="form-control" id="">
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label">Approval Status</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Status</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>
										</div>
										<div class="row">							    
											    <div class="col-md-3"> 
											    	<label class="control-label">Fiscal Year</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Fiscal Year</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label">Currency</label> 
															<select class=" form-control select2" name="cityid">
															<option value="select country">Select Currency</option>
															<option>Code1</option>
															<option>Code2</option>
															</select>
											    </div>									    
											    <div class="col-md-3"> 
											    	<label class="control-label" for="fdate">Valid Until</label>
											    	<input type="date" class="form-control" id="">
											    </div>
											</div>
											<div class="form-group col-lg-8">											    
											</div>										
										</form>
									</div>

									<div style="padding: 20px"></div>
									<div id="tab">	
										<ul class="nav nav-tabs">
											<li class="active"><a href="#1" data-toggle="tab">Material</a></li>
											<li><a href="#2" data-toggle="tab">Service</a></li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="1">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>S.No</th>
																	<th>Bar Code</th>
																	<th>Item Code</th>
																	<th>Description</th>
																	<th>Oty</th>
																	<th>UOM</th>
																	<th>Price</th>
																	<th>Discount</th>
																	<th>Frieght</th>
																	<th>Tax</th>
																	<th>Withholding Tax</th>
																	<th>Before Tax</th>
																	<th>Gross Total</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Referance Type</th>
																	<th>Ref.No</th>
																	<th>Cost Center</th>
																	<th>Storage Location</th>
																	<th>Pin Location</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>
																		<input type="text" name="" class="form-control  col-sm-1" placeholder="Bar Code">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Qty">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Price" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Frieght" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Tax" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Before Tax" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gross Total" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref No">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost Centre" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Storage Location" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="2">
												<div class="row">
													<div class="col-md-12 table-responsive">
														<table class="table table-bordered">
															<thead>
																<tr>
																	<th>S.No</th>
																	<th>SAC</th>
																	<th>Gl Account</th>
																	<th>Gl Name</th>
																	<th>Description</th>
																	<th>Amount</th>
																	<th>Discount</th>
																	<th>TDS</th>
																	<th>Frieght</th>
																	<th>Tax</th>
																	<th>Withholding Tax</th>
																	<th>Before Tax</th>
																	<th>Gross Total</th>
																	<th>Total</th>
																	<th>Project</th>
																	<th>Reference Type</th>
																	<th>Ref.No</th>
																	<th>Cost Center</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td>1</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gl Name" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Description">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Amount">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Discount">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="TDS">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Frieght">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Tax" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Before Tax" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Gross Total" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Total" readonly="">
																	</td>
																	<td>
																		<select class="form-control">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<select class="form-control" disabled="">
																			<option>select</option>
																			<option>1002E</option>
																			<option>201T</option>
																		</select>
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Ref No" readonly="">
																	</td>
																	<td>
																		<input type="text" name="" class="form-control" placeholder="Cost Center" readonly="">
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>		
										</div>
									</div>
									<div style="padding: 20px"></div>
									<div class="col-md-12">
										<div class="form-group text-center">
											<button class="btn btn-info">Save</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</section>
