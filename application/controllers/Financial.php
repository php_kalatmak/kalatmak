<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Financial extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('login_model');
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
   public function chartofaccounts($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Financial";
        $data['content'] 	= $this->load->view('financial/chartofaccounts', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function glmasters($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "GL Master";
        $data['content'] 	= $this->load->view('financial/glmasters', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function journalentry($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/journalentry', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function journalvoucher($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/journalvoucher', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function recurringposting($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/recurringposting', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function reversallist($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/reversallist', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function reconcilation($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/reconcilation', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function costcenter($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/costcenter', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
   public function costingreport($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Journal Entry";
        $data['content'] 	= $this->load->view('financial/costingreport', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
}

?>