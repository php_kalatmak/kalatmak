<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Rights extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function role_vs_rights($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Role vs Rights";
        $data['content'] 	= $this->load->view('rights/role_vs_rights', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function role_creation($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Role Creation";
        $data['content'] 	= $this->load->view('rights/role_creation', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function module_creation($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Module Creation";
        $data['content'] 	= $this->load->view('rights/module_creation', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function form_rights($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Form Rights";
        $data['content'] 	= $this->load->view('rights/form_rights', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}


}

?>