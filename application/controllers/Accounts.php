<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Accounts extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function exchange_rate($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Exchange Rate";
        $data['content'] 	= $this->load->view('accounts/exchange_rate', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function mode_of_payment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Mode Of Payment";
        $data['content'] 	= $this->load->view('accounts/mode_of_payment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
public function bank($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Bank";
        $data['content'] 	= $this->load->view('accounts/bank', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function posting_period($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Posting Period";
        $data['content'] 	= $this->load->view('accounts/posting_period', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function payment_term_slab($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - PayTermSlab";
        $data['content'] 	= $this->load->view('accounts/pay_term_slab', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function payment_term($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Payment Term";
        $data['content'] 	= $this->load->view('accounts/payment_term', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

}

?>