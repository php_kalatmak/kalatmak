<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Login extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('login_model');
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function index($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Quadraerp";
        $data['content'] 	= $this->load->view('home/index', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
  
	

}

?>