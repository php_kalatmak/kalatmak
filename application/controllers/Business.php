<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Business extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function sales_group($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Sales Group";
        $data['content'] 	= $this->load->view('business/sales_group', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function grade($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Grade";
        $data['content'] 	= $this->load->view('business/grade', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function business_partnertype($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Business Partner Type";
        $data['content'] 	= $this->load->view('business/business_partnertype', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
  
  public function business_partner_master($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Business Partner Master";
        $data['content'] 	= $this->load->view('business/business_partner_master', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function business_partner_group($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Business Partner Group";
        $data['content'] 	= $this->load->view('business/business_partner_group', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	

}

?>