<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Banking extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}

public function incomingpayment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - incomingpayment";
        $data['content'] 	= $this->load->view('banking/incomingpayment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function outgoingpayment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - outgoingpayment";
        $data['content'] 	= $this->load->view('banking/outgoingpayment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function deposit($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - deposit";
        $data['content'] 	= $this->load->view('banking/deposit', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function manualreconsilation($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - manualreconsilation";
        $data['content'] 	= $this->load->view('banking/manualreconsilation', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function taxpayment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - taxpayment";
        $data['content'] 	= $this->load->view('banking/taxpayment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function paymentorder($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - paymentorder";
        $data['content'] 	= $this->load->view('banking/paymentorder', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}	
public function autopayment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - autopayment";
        $data['content'] 	= $this->load->view('banking/autopayment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}			



}

?>