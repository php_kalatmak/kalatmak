<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Finance extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}

public function chartofaccounts($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - chartofaccounts";
        $data['content'] 	= $this->load->view('finance/chartofaccounts', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function journalentry($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - journalentry";
        $data['content'] 	= $this->load->view('finance/journalentry', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function journalvoucher($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - journalvoucher";
        $data['content'] 	= $this->load->view('finance/journalvoucher', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}


}

?>