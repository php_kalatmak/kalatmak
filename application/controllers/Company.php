<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Company extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function user($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - User";
        $data['content'] 	= $this->load->view('company/user', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function company_group($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company Group";
        $data['content'] 	= $this->load->view('company/company_group', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function company($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company";
        $data['content'] 	= $this->load->view('company/company', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function branch($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Branch";
        $data['content'] 	= $this->load->view('company/branch', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}


}

?>