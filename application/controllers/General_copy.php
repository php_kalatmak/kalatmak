<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class General extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function unom($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Unom";
        $data['content'] 	= $this->load->view('general/unom', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function storage_location_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP -Storage Location Assignment";
        $data['content'] 	= $this->load->view('general/storage_location_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function storage_location($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Storage Location";
        $data['content'] 	= $this->load->view('general/storage_location', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function state($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - State";
        $data['content'] 	= $this->load->view('general/state', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function city($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - City";
        $data['content'] 	= $this->load->view('general/city', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function area($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Area";
        $data['content'] 	= $this->load->view('general/area', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	

	public function posting_period($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Posting Period";
        $data['content'] 	= $this->load->view('general/posting_period', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function payment_term_slab($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - PayTermSlab";
        $data['content'] 	= $this->load->view('general/pay_term_slab', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function payment_term($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Payment Term";
        $data['content'] 	= $this->load->view('general/payment_term', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function hsnsac($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - HsnSac";
        $data['content'] 	= $this->load->view('general/hsnsac', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function group_list_unom($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - GroupList Unom";
        $data['content'] 	= $this->load->view('general/group_list_unom', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function division($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Division";
        $data['content'] 	= $this->load->view('general/division', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function currency($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Currency";
        $data['content'] 	= $this->load->view('general/currency', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function country($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Country";
        $data['content'] 	= $this->load->view('general/country', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function bin_location($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Bin Location";
        $data['content'] 	= $this->load->view('general/bin_location', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function bank($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Bank";
        $data['content'] 	= $this->load->view('general/bank', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function branch_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Branch Assignment";
        $data['content'] 	= $this->load->view('general/branch_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function branch($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Branch";
        $data['content'] 	= $this->load->view('general/branch', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	 public function company_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company Assignment";
        $data['content'] 	= $this->load->view('general/company_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function company($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company";
        $data['content'] 	= $this->load->view('general/company', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function company_group($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company Group";
        $data['content'] 	= $this->load->view('general/company_group', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	
	public function add_country()
	{
		 $data = $this->input->post();

		 $c_code = $data['country_code'];
		 $c_name = $data['country_name'];

		 
		 $num = $this->db->query("select * from country where name='".$c_name."' and code = '".$c_code."'")->num_rows();
		
		if($num > 0)
		{
			echo "";
			
			redirect(base_url('general/country'));
		}
		else{
			$country['name'] = $c_name;
			$country['code'] = $c_code;
			$country['status'] = '1';

			$this->db->insert("country", $country);

			echo "<script>alert('This country added successfully')</script>";
			
			redirect(base_url('general/country'));
		}
		
		
	}
	
	public function update_country()
	{
		$data['country_code'] = $this->input->post('countrycode');
		$data['country_name'] = $this->input->post('countryname');
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgcy",$data);
		if($this->db->affected_rows())
		{
			redirect(base_url('general/country'));
		}
	}
	public function add_state()
	{
		$data['country_id'] = $this->input->post('countryid');
		$data['state_code'] = $this->input->post('statecode');
		$data['state_name'] = $this->input->post('statename');
		$data['gst_code'] = $this->input->post('gstcode');
		$data['union_territory'] = $this->input->post('load');
		$data['status'] = '1';
		$this->db->insert("erpgste",$data);
		if($this->db->affected_rows())
		{
			redirect(base_url('general/state'));
		}
	}

	public function update_state()	
	{
		//print_r($this->input->post());
		$data['country_id'] = $this->input->post('country_id');
		$data['state_code'] = $this->input->post('statecode');
		$data['state_name'] = $this->input->post('statename');
		$data['gst_code'] = $this->input->post('gstcode');
		$data['union_territory'] = $this->input->post('load');
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgste",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/state'));
		}
	}
	
	public function add_currency()
	{
		$data['currency_code'] = $this->input->post('currency_code');
		$data['currency_name'] = $this->input->post('currency_name');
		$data['international_code'] = $this->input->post('international_code');
		$data['international_name'] = $this->input->post('international_name');
		$data['decimal_limit'] = $this->input->post('decimal_limit');
		$data['status'] = '1';
		$this->db->insert("erpgcrc",$data);
		if($this->db->affected_rows())
		{
			redirect(base_url('general/currency'));
		}
	}
	
	public function update_currency()
	{
		$data['currency_code'] = $this->input->post('currencycode');
		$data['currency_name'] = $this->input->post('currencyname');
		$data['international_code'] = $this->input->post('internationalcode');
		$data['international_name'] = $this->input->post('internationalname');
		$data['decimal_limit'] = $this->input->post('decimallimit');
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgcrc",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/currency'));
		}
	}

	public function add_uom()
	{
		$data['uom_code'] = $this->input->post('uom_code');
		$data['uom_name'] = $this->input->post('uom_name');
		$data['status'] = '1';
		$this->db->insert("erpguo",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/unom'));
		}
		
	}
	
	public function update_uom()
	{
		
		$data['uom_code'] = $this->input->post('uomcode');
		$data['uom_name'] = $this->input->post('uomname');
		$data['status'] = $this->input->post('uom_status');
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpguo",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/unom'));
		}
	}
	public function add_bank()
	{
		$data['bank_code'] = $this->input->post('bank_code');
		$data['bank_name'] = $this->input->post('bank_name');
		$data['status'] = '1';
		$this->db->insert("erpgbk",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/bank'));
		}
		
	}
	
	public function update_bank()
	{
		
		$data['bank_code'] = $this->input->post('bankcode');
		$data['bank_name'] = $this->input->post('bankname');	
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgbk",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/bank'));
		}
	}
	
	public function add_grpuom()
	{
		$data['group_code'] = $this->input->post('group_code');
		$data['group_name'] = $this->input->post('group_name');
		$data['status'] = '1';
		$this->db->insert("erpggluo",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/group_list_unom'));
		}
		
	}
	
	public function update_grpuom()
	{
		
		$data['group_code'] = $this->input->post('groupcode');
		$data['group_name'] = $this->input->post('groupname');
        $data['status'] = $this->input->post('grpuomstatus');		
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpggluo",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/group_list_unom'));
		}
	}
	
	public function add_paytermslab()
	{
		$data['payterm_code'] = $this->input->post('payterm_code');
		$data['slab_seqid'] = $this->input->post('slab_seqid');
		$data['credit_days'] = $this->input->post('credit_days');
		$data['percentage'] = $this->input->post('percentage');
		$data['status'] = '1';
		$this->db->insert("erpgpts",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/payment_term_slab'));
		}
		
	}
	
	public function update_paytermslab()
	{
		
		$data['payterm_code'] = $this->input->post('paytermcode');
		$data['slab_seqid'] = $this->input->post('slabseqid');
		$data['credit_days'] = $this->input->post('creditdays');
		$data['percentage'] = $this->input->post('percentage');
        		
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgpts",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/payment_term_slab'));
		}
	}
	
	public function add_division()
	{
		$data['division_code'] = $this->input->post('division_code');
		$data['division_name'] = $this->input->post('division_name');
		$data['status'] = '1';
		$this->db->insert("erpgdiv",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/division'));
		}
		
	}
	
	public function update_division()
	{
		
		$data['division_code'] = $this->input->post('divisioncode');
		$data['division_name'] = $this->input->post('divisionname');
		
        		
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgdiv",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/division'));
		}
	}
	
	public function add_payterm()
	{
		$data['payterm_code'] = $this->input->post('payterm_code');
		$data['payterm_name'] = $this->input->post('payterm_name');
		$data['credit_days'] = $this->input->post('credit_days');
		
		if($this->input->post(load)=='on')
		{
			$data['is_slab'] = 'ture';
			$data['slab_level'] = $this->input->post('slab_level');
			
		}
		else 
		{
			$data['is_slab'] = 'false';
		}
		
		$data['tolerance_days'] = $this->input->post('tolerance_days');
		$data['status'] = '1';
		
		$this->db->insert("erpgpt",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/payment_term'));
		}
		
	}
	
	public function update_payterm()
	{
		
		$data['payterm_code'] = $this->input->post('paytermcode');
		$data['payterm_name'] = $this->input->post('paytermname');
		$data['credit_days'] = $this->input->post('creditdays');
		
		if($this->input->post('load') == 'on')
		{
			$data['is_slab'] = 'true';
			$data['slab_level'] = $this->input->post('slableveledit');
			
		}
		else 
		{
			$data['is_slab'] = 'false';
			$data['slab_level'] = '';
		}
		
		$data['tolerance_days'] = $this->input->post('tolerancedays');
		$data['status'] = $this->input->post('status');
        	
		$this->db->where('id',$this->input->post('edit_id'));
		$this->db->update("erpgpt",$data);
		
		if($this->db->affected_rows())
		{
			redirect(base_url('general/payment_term'));
		}
	}
	
}

?>