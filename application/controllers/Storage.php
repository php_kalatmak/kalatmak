<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Storage extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function storage_location($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Storage Location";
        $data['content'] 	= $this->load->view('storage/storage_location', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function storage_location_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Storage Location Assignment";
        $data['content'] 	= $this->load->view('storage/storage_location_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function bin_location($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Bin Location";
        $data['content'] 	= $this->load->view('storage/bin_location', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	
}

?>