<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Assignment extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function sequence_mapping($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Sequence Mapping";
        $data['content'] 	= $this->load->view('assignment/sequence_mapping', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function exchange_rate($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Exchange Rate";
        $data['content'] 	= $this->load->view('assignment/exchange_rate', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function condition_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Condition Type";
        $data['content'] 	= $this->load->view('assignment/condition_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function condition_master($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Condition Master";
        $data['content'] 	= $this->load->view('assignment/condition_master', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
  
  public function company_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Company Assignment";
        $data['content'] 	= $this->load->view('assignment/company_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function branch_assignment($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Branch Assignment";
        $data['content'] 	= $this->load->view('assignment/branch_assignment', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function condition_table_creation($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Condition Table Creation";
        $data['content'] 	= $this->load->view('assignment/condition_table_creation', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}


}

?>