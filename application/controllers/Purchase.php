<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Purchase extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('login_model');
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}


public function purchaserequest($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "purchaserequest";
        $data['content'] 	= $this->load->view('purchase/purchaserequest', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function purchaseorder($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "purchaseorder";
        $data['content'] 	= $this->load->view('purchase/purchaseorder', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function landedcost($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "landedcost";
        $data['content']    = $this->load->view('purchase/landedcost', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
public function goodsreceiptpo($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "goodsreceiptpo";
        $data['content']    = $this->load->view('purchase/goodsreceiptpo', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
public function apinvoice($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "apinvoice";
        $data['content']    = $this->load->view('purchase/apinvoice', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }

public function apcreditmemo($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "apcreditmemo";
        $data['content']    = $this->load->view('purchase/apcreditmemo', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }

public function freightmasterform($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "freightmasterform";
        $data['content']    = $this->load->view('purchase/freightmasterform', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }  

public function landedcostmasterform($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "landedcostmasterform";
        $data['content']    = $this->load->view('purchase/landedcostmasterform', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }       
    
public function goodsissue($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "goodsissue";
        $data['content']    = $this->load->view('purchase/goodsissue', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
    public function goodsreceipt($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "goodsreceipt";
        $data['content']    = $this->load->view('purchase/goodsreceipt', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
     public function apdownpayment($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "apdownpayment";
        $data['content']    = $this->load->view('purchase/apdownpayment', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }

public function grpopage($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "grpopage";
        $data['content']    = $this->load->view('purchase/grpopage', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
public function batchserialtranspage($message = '')
    { 
        $data                   = $this->includeheader();
        $data['message']        = $message;
        $data['meta_title'] = "batchserialtranspage";
        $data['content']    = $this->load->view('purchase/batchserialtranspage', $data, TRUE);        
        $this->load->view('viewpage', $data);
    }
    
}

?>