<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Item extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function valuation_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Valuation Type";
        $data['content'] 	= $this->load->view('item/valuation_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function product_categories($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - product categories";
        $data['content'] 	= $this->load->view('item/product_categories', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function brand($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - brand";
        $data['content'] 	= $this->load->view('item/brand', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function valuation_method($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Valuation Method";
        $data['content'] 	= $this->load->view('item/valuation_method', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function manage_item_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - ManageItem Type";
        $data['content'] 	= $this->load->view('item/manage_item_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function item_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Item Type";
        $data['content'] 	= $this->load->view('item/item_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function item_master($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Item Master";
        $data['content'] 	= $this->load->view('item/item_master', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function item_group($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Item group";
        $data['content'] 	= $this->load->view('item/item_group', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function bom_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - BOM Type";
        $data['content'] 	= $this->load->view('item/bom_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function shipping_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Shipping Type";
        $data['content'] 	= $this->load->view('item/shipping_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

public function unom($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Unom";
        $data['content'] 	= $this->load->view('item/unom', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	
	public function hsnsac($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - HsnSac";
        $data['content'] 	= $this->load->view('item/hsnsac', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function group_list_unom($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - GroupList Unom";
        $data['content'] 	= $this->load->view('item/group_list_unom', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function division($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "ERP - Division";
        $data['content'] 	= $this->load->view('item/division', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
}

?>