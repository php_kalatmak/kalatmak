<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Gldetermination extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	    = $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function sales_purchase_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	    = "ERP - Sales Purchase Type";
        $data['content'] 	    = $this->load->view('gldetermination/sales_purchase_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
	public function order_type($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	    = "ERP - Order Type";
        $data['content'] 	    = $this->load->view('gldetermination/order_type', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	public function gl_determination_rule($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	    = "ERP - Gldetermination Rule";
        $data['content'] 	    = $this->load->view('gldetermination/gl_determination_rule', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}

	
}

?>