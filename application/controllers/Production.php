<?php if (!defined('BASEPATH')) { exit('No direct script access allowed'); }

class Production extends DOT_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('login_model');
		
    }
	public function includeheader()
	{
		$data 					= array();
		$data['header'] 		= $this->load->view('include/header','',TRUE);
		$data['sidebar'] 		= $this->load->view('include/sidebar', $data, TRUE);
		$data['footer'] 	= $this->load->view('include/footer', '', TRUE);
		return $data;
	}
public function bom($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "BOM";
        $data['content'] 	= $this->load->view('production/bom', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function resource($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Resource";
        $data['content'] 	= $this->load->view('production/resource', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function route($message = '')
    { 

        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Route";
        $data['content'] 	= $this->load->view('production/route', $data, TRUE);        
        $this->load->view('viewpage', $data);
		
	}
public function productionorder($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Productionorder";
        $data['content'] 	= $this->load->view('production/productionorder', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function issueproduction($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Issueproduction";
        $data['content'] 	= $this->load->view('production/issueproduction', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function receivefromproduction($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Issueproduction";
        $data['content'] 	= $this->load->view('production/receivefromproduction', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function purchaserequest($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Issueproduction";
        $data['content'] 	= $this->load->view('production/purchaserequest', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function purchaseorder($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Issueproduction";
        $data['content'] 	= $this->load->view('production/purchaseorder', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
public function productionsettings($message = '')
    { 
        $data					= $this->includeheader();
		$data['message'] 		= $message;
        $data['meta_title']	= "Issueproduction";
        $data['content'] 	= $this->load->view('production/productionsettings', $data, TRUE);        
        $this->load->view('viewpage', $data);
	}
}

?>