$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-company').addClass('active');
	$('#menu-company > a').attr('aria-expanded', 'true');
	$('#menu-company > ul').addClass('in');

	$('#menu-companyGroup').addClass('active');
	$('#CompanyGroup').hide();

	$('#CompanyGroupAdd').click(function() {
		$('#CompanyGroup').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});


	var result = $.ajax({
		type : "POST",
		url : "getCurrencyListInGroupCompany",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#compGrpCurr").get(0).options.length = 0;
			$("#compGrpCurr").get(0).options[0] = new Option("Select Currency", "-1");
			$("#compGrpCurrEdit").get(0).options.length = 0;
			$("#compGrpCurrEdit").get(0).options[0] = new Option("Select Currency", "-1");
			$.each(msg, function(index, item) {
				$("#compGrpCurr").get(0).options[$("#compGrpCurr").get(0).options.length] = new Option(item.currDesc, item.currCode);
				$("#compGrpCurrEdit").get(0).options[$("#compGrpCurrEdit").get(0).options.length] = new Option(item.currDesc, item.currCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getCountryInGroupComp",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#country").get(0).options.length = 0;
			$("#country").get(0).options[0] = new Option("Select Country", "-1");
			$("#countryEdit").get(0).options.length = 0;
			$("#countryEdit").get(0).options[0] = new Option("Select Country", "-1");
			$.each(msg, function(index, item) {
				$("#country").get(0).options[$("#country").get(0).options.length] = new Option(item.countryDesc, item.countryId);
				$("#countryEdit").get(0).options[$("#countryEdit").get(0).options.length] = new Option(item.countryDesc, item.countryId);
			});
		},
	});

	$("#country").change(function() {
		var country = $("#country").val();
		var result = $.ajax({
			type : "POST",
			url : "getStateINGroupCompany?country=" + country,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#state").get(0).options.length = 0;
				$("#state").get(0).options[0] = new Option("Select State", "-1");
				$.each(msg, function(index, item) {
					$("#state").get(0).options[$("#state").get(0).options.length] = new Option(item.stateDesc, item.stateId);
				});
			},
		});
	});

	$("#countryEdit").change(function() {
		var country = $("#countryEdit").val();
		var result = $.ajax({
			type : "POST",
			url : "getStateINGroupCompany?country=" + country,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#stateEdit").get(0).options.length = 0;
				$("#stateEdit").get(0).options[0] = new Option("Select State", "-1");
				$.each(msg, function(index, item) {
					$("#stateEdit").get(0).options[$("#stateEdit").get(0).options.length] = new Option(item.stateDesc, item.stateId);
				});
			},
		});
	});
	var result = $.ajax({
		type : "POST",
		url : "getStateINGroupCompanyAlone",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#stateEdit").get(0).options.length = 0;
			$("#stateEdit").get(0).options[0] = new Option("Select State", "-1");
			$.each(msg, function(index, item) {
				$("#stateEdit").get(0).options[$("#stateEdit").get(0).options.length] = new Option(item.stateDesc, item.stateId);
			});
		},
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getCompGroupList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "compGrpCode"
					},
					{
						"data" : "compGrpName"
					},
					{
						"data" : "compGrpCurr"
					},
					{
						"data" : "compGrpLang"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#compGrpCodeEdit').val(tableData.compGrpCode);
		$('#compGrpNameEdit').val(tableData.compGrpName);
		$('#compGrpCurrEdit').val(tableData.compGrpCurr);
		$('#compGrpLangEdit').val(tableData.compGrpLang);

		$.ajax({
			type : "POST",
			url : "getCompanyGroupAddressDetails?compGrpCode=" + tableData.compGrpCode,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {

				$('#houseNoEdit').val(msg[0].houseNo);
				$('#streetEdit').val(msg[0].street);
				$('#street1Edit').val(msg[0].street1);
				$('#street2Edit').val(msg[0].street2);
				$('#landmarkEdit').val(msg[0].landmark);
				$('#cityEdit').val(msg[0].city);
				$('#countryEdit').val(msg[0].country);
				$('#stateEdit').val(msg[0].state);
				$('#pincodeEdit').val(msg[0].pincode);
				$('#mobileEdit').val(msg[0].mobile);
				$('#telephoneEdit').val(msg[0].telephone);
				$('#teleExtenEdit').val(msg[0].telExtension);
				$('#emailEdit').val(msg[0].email);
				$('#websiteEdit').val(msg[0].website);
				$('#altMobEdit').val(msg[0].altMob);
				$('#altTelEdit').val(msg[0].altTel);

				$("#blah1").attr("src", msg[0].Empimg);
			},
		});

	});

	$('#compGrpSave').click(function() {

		var compGrpCode = $("#compGrpCode").val();
		var compGrpName = $('#compGrpName').val();
		var grpCurr = $('#compGrpCurr').val();
		var grpLang = $('#compGrpLang').val();
		var houseNo = $('#houseNo').val();
		var street = $('#street').val();
		var street1 = $('#street1').val();
		var street2 = $('#street2').val();
		var landmark = $('#landmark').val();
		var city = $('#city').val();
		var country = $('#country').val();
		var state = $('#state').val();
		var pinCode = $('#pincode').val();
		var mobile = $('#mobile').val();
		var telephone = $('#telephone').val();
		var telExt = $('#teleExten').val();
		var email = $('#email').val();
		var website = $('#website').val();
		var altMob = $('#altMob').val();
		var altTel = $('#altTel').val();
		if (compGrpCode == ' ' || compGrpCode == 0 || compGrpCode == '-1') {
			alert("Enter Company Group Code");
			$('#compGrpCode').focus();
			return false;
		} else if (compGrpName == ' ' || compGrpName == 0 || compGrpName == '-1') {
			alert("Enter Company Group Name");
			$('#compGrpName').focus();
			return false;
		} else if (grpCurr == ' ' || grpCurr == 0 || grpCurr == '-1') {
			alert("Enter Company Group Currency");
			$('#compGrpCurr').focus();
			return false;
		} else if (grpLang == '' || grpLang == null || grpLang == '-1') {
			alert("Enter Company Group Language");
			$('#compGrpLang').focus();
			return false;
		} else if (houseNo == ' ' || houseNo == 0) {
			alert("Enter House No.");
			$('#houseNo').focus();
			return false;
		} else if (street == '' || street == null) {
			alert("Enter Street");
			$('#street').focus();
			return false;
		} else if (street1 == ' ' || street1 == 0) {
			alert("Enter Street 1");
			$('#street1').focus();
			return false;
		} else if (street2 == '' || street2 == null) {
			alert("Enter Street 2");
			$('#street2').focus();
			return false;
		} else if (landmark == ' ' || landmark == 0) {
			alert("Enter Landmark");
			$('#landmark').focus();
			return false;
		} else if (city == '' || city == null) {
			alert("Enter City");
			$('#city').focus();
			return false;
		} else if (country == ' ' || country == 0 || country == '-1') {
			alert("Select Country");
			$('#country').focus();
			return false;
		} else if (state == ' ' || state == 0 || state == '-1') {
			alert("Select State");
			$('#state').focus();
			return false;
		} else if (pinCode == '' || pinCode == null) {
			alert("Enter PinCode");
			$('#pincode').focus();
			return false;
		} else if (mobile == ' ' || mobile == 0) {
			alert("Enter Mobile");
			$('#mobile').focus();
			return false;
		} else if (telephone == '' || telephone == null) {
			alert("Enter Telephone");
			$('#telephone').focus();
			return false;
		} else if (telExt == '' || telExt == null) {
			alert("Enter Telephone Extension");
			$('#teleExten').focus();
			return false;
		} else if (email == '' || email == null) {
			alert("Enter Email");
			$('#email').focus();
			return false;
		} else if (website == '' || website == null) {
			alert("Enter Website");
			$('#website').focus();
			return false;
		} else if (altMob == '' || altMob == null) {
			alert("Enter Alternate Mobile No.");
			$('#altMob').focus();
			return false;
		} else if (altTel == '' || altTel == null) {
			alert("Enter Alternate Telephone No.");
			$('#altTel').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveGroupCompany?compGrpCode=" + compGrpCode + "&compGrpName=" + compGrpName + "&grpCurr=" + grpCurr
					+ "&grpLang=" + grpLang + "&houseNo=" + houseNo + "&street=" + street + "&street1=" + street1 + "&street2=" + street2 +
					"&landmark=" + landmark + "&city=" + city + "&country=" + country + "&state=" + state + "&pinCode=" + pinCode +
					"&mobile=" + mobile + "&telephone=" + telephone + "&telExt=" + telExt + "&email=" + email + "&website="
					+ website + "&altMob=" + altMob + "&altTel=" + altTel,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			processFileUpload(result.split(":")[1]);
			if (result.split(":")[0] == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Company Group Code Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#compGrpUpdate').click(function() {

		var compGrpCode = $("#compGrpCodeEdit").val();
		var compGrpName = $('#compGrpNameEdit').val();
		var grpCurr = $('#compGrpCurrEdit').val();
		var grpLang = $('#compGrpLangEdit').val();
		var houseNo = $('#houseNoEdit').val();
		var street = $('#streetEdit').val();
		var street1 = $('#street1Edit').val();
		var street2 = $('#street2Edit').val();
		var landmark = $('#landmarkEdit').val();
		var city = $('#cityEdit').val();
		var country = $('#countryEdit').val();
		var state = $('#stateEdit').val();
		var pinCode = $('#pincodeEdit').val();
		var mobile = $('#mobileEdit').val();
		var telephone = $('#telephoneEdit').val();
		var telExt = $('#teleExtenEdit').val();
		var email = $('#emailEdit').val();
		var website = $('#websiteEdit').val();
		var altMob = $('#altMobEdit').val();
		var altTel = $('#altTelEdit').val();
		if (compGrpCode == ' ' || compGrpCode == 0 || compGrpCode == '-1') {
			alert("Enter Company Group Code");
			$('#compGrpCodeEdit').focus();
			return false;
		} else if (compGrpName == ' ' || compGrpName == 0 || compGrpName == '-1') {
			alert("Enter Company Group Name");
			$('#compGrpNameEdit').focus();
			return false;
		} else if (grpCurr == ' ' || grpCurr == 0 || grpCurr == '-1') {
			alert("Enter Company Group Currency");
			$('#compGrpCurrEdit').focus();
			return false;
		} else if (grpLang == '' || grpLang == null || grpLang == '-1') {
			alert("Enter Company Group Language");
			$('#compGrpLangEdit').focus();
			return false;
		} else if (houseNo == ' ' || houseNo == 0) {
			alert("Enter House No.");
			$('#houseNoEdit').focus();
			return false;
		} else if (street == '' || street == null) {
			alert("Enter Street");
			$('#streetEdit').focus();
			return false;
		} else if (street1 == ' ' || street1 == 0) {
			alert("Enter Street 1");
			$('#street1Edit').focus();
			return false;
		} else if (street2 == '' || street2 == null) {
			alert("Enter Street 2");
			$('#street2Edit').focus();
			return false;
		} else if (landmark == ' ' || landmark == 0) {
			alert("Enter Landmark");
			$('#landmarkEdit').focus();
			return false;
		} else if (city == '' || city == null) {
			alert("Enter City");
			$('#cityEdit').focus();
			return false;
		} else if (country == ' ' || country == 0 || country == '-1') {
			alert("Select Country");
			$('#countryEdit').focus();
			return false;
		} else if (state == ' ' || state == 0 || state == '-1') {
			alert("Select State");
			$('#stateEdit').focus();
			return false;
		} else if (pinCode == '' || pinCode == null) {
			alert("Enter PinCode");
			$('#pincodeEdit').focus();
			return false;
		} else if (mobile == ' ' || mobile == 0) {
			alert("Enter Mobile");
			$('#mobileEdit').focus();
			return false;
		} else if (telephone == '' || telephone == null) {
			alert("Enter Telephone");
			$('#telephoneEdit').focus();
			return false;
		} else if (telExt == '' || telExt == null) {
			alert("Enter Telephone Extension");
			$('#teleExtenEdit').focus();
			return false;
		} else if (email == '' || email == null) {
			alert("Enter Email");
			$('#emailEdit').focus();
			return false;
		} else if (website == '' || website == null) {
			alert("Enter Website");
			$('#websiteEdit').focus();
			return false;
		} else if (altMob == '' || altMob == null) {
			alert("Enter Alternate Mobile No.");
			$('#altMobEdit').focus();
			return false;
		} else if (altTel == '' || altTel == null) {
			alert("Enter Alternate Telephone No.");
			$('#altTelEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateGroupCompany?compGrpCode=" + compGrpCode + "&compGrpName=" + compGrpName + "&grpCurr=" + grpCurr
					+ "&grpLang=" + grpLang + "&houseNo=" + houseNo + "&street=" + street + "&street1=" + street1 + "&street2=" + street2 +
					"&landmark=" + landmark + "&city=" + city + "&country=" + country + "&state=" + state + "&pinCode=" + pinCode +
					"&mobile=" + mobile + "&telephone=" + telephone + "&telExt=" + telExt + "&email=" + email + "&website="
					+ website + "&altMob=" + altMob + "&altTel=" + altTel,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			processFileUpload(result.split(":")[1]);
			if (result.split(":")[0] == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("State Code Exists");
			} else {
				alert("failed");

			}
		}
	});
	var fileClick = 0;
	$("#compGrpImage").on('change', prepareLoad);
	$("#compGrpImageEdit").on('change', prepareLoad);
	var files;
	function prepareLoad(event) {
		fileClick++;
		console.log(' event fired' + event.target.files[0].name);
		files = event.target.files;

	}
	function processFileUpload(compGrpCode) {
		if (fileClick > 0) {
			var oMyForm = new FormData();
			oMyForm.append("file", files[0]);

			var result = $.ajax({
				dataType : 'json',
				url : "uploadCompGrpImage?compGrpCode=" + compGrpCode,
				data : oMyForm,
				type : "POST",
				enctype : 'multipart/form-data',
				processData : false,
				contentType : false,
				async : false,
			}).responseText;
			var result1 = result.split(':');
			if (result == "S") {
			} else {
			}
		}
	}
	function clearData23() {
		$("#compGrpCodeEdit").val('');
		$('#compGrpNameEdit').val('');
		$('#compGrpCurrEdit').val('-1');
		$('#compGrpLangEdit').val('-1');
		$('#houseNoEdit').val('');
		$('#streetEdit').val('');
		$('#street1Edit').val('');
		$('#street2Edit').val('');
		$('#landmarkEdit').val('');
		$('#cityEdit').val('');
		$('#countryEdit').val('-1');
		$('#stateEdit').val('-1');
		$('#pincodeEdit').val('');
		$('#mobileEdit').val('');
		$('#telephoneEdit').val('');
		$('#teleExtenEdit').val('');
		$('#emailEdit').val('');
		$('#websiteEdit').val('');
		$('#altMobEdit').val('');
		$('#altTelEdit').val('');

		$("#compGrpCode").val('');
		$('#compGrpName').val('');
		$('#compGrpCurr').val('-1');
		$('#compGrpLang').val('-1');
		$('#houseNo').val('');
		$('#street').val('');
		$('#street1').val('');
		$('#street2').val('');
		$('#landmark').val('');
		$('#city').val('');
		$('#country').val('-1');
		$('#state').val('-1');
		$('#pincode').val('');
		$('#mobile').val('');
		$('#telephone').val('');
		$('#teleExten').val('');
		$('#email').val('');
		$('#website').val('');
		$('#altMob').val('');
		$('#altTel').val('');
	}
});