$(document).ready(function () {

$('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-rights').addClass('active');
    $('#menu-rights > a').attr('aria-expanded', 'true');
    $('#menu-rights > ul').addClass('in');

    $('#menu-formRightsCreation').addClass('active');
    $("#FormRightsAdd").hide();

  $('#formRightsAdd').click(function () {
        $('#FormRightsAdd').show();
    });

    $('.close').click(function () {
        $('#FormRightsAdd').hide();
    });

     $('#formRightsReset').click(function () {
        $('#rightsName').val('');
        $('#moduleRightsId').val('-1');
        $('#rightsTypeId').val('-1');
    });


 $.ajax({
        type: "POST",
        url: "getFormRightsCount",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {

            $("#rightsId").val(msg[0].count);
        },
    });


    $.ajax({
        type: "POST",
        url: "getModuleFormRightsList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#moduleRightsId").get(0).options.length = 0;
            $("#moduleRightsId").get(0).options[0] = new Option("Select Module", "-1");
            $("#moduleRightsEditId").get(0).options.length = 0;
           $("#moduleRightsEditId").get(0).options[0] = new Option("Select Parent Module", "-1");
            $.each(msg, function (index, data) {
                $("#moduleRightsId").get(0).options[$("#moduleRightsId").get(0).options.length] = new Option(data.moduleName, data.moduleId);
               $("#moduleRightsEditId").get(0).options[$("#moduleRightsEditId").get(0).options.length] = new Option(data.moduleName, data.moduleId);
            });
        },
    });


      $.ajax({
        type: "POST",
        url: "getRightsTypeList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#rightsTypeId").get(0).options.length = 0;
            $("#rightsTypeId").get(0).options[0] = new Option("Select RightsType", "-1");
            $("#rightsTypeEditId").get(0).options.length = 0;
            $("#rightsTypeEditId").get(0).options[0] = new Option("Select Parent Module", "-1");
            $.each(msg, function (index, data) {
                $("#rightsTypeId").get(0).options[$("#rightsTypeId").get(0).options.length] = new Option(data.rightsTypeName, data.rightsTypeId);
                $("#rightsTypeEditId").get(0).options[$("#rightsTypeEditId").get(0).options.length] = new Option(data.rightsTypeName, data.rightsTypeId);
            });
        },
    });

     $.ajax({
        type: "POST",
        url: "getStatusList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#statusEditId").get(0).options.length = 0;
            $("#statusEditId").get(0).options[0] = new Option("Select Status", "-1");     
            $.each(msg, function (index, data) {
                $("#statusEditId").get(0).options[$("#statusEditId").get(0).options.length] = new Option(data.statusDesc, data.statusCode);
            });
        },
    });




    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                ajax: {
                    type: "POST",
                    url: "getFormRightsList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                },
                "columns": [
                    {
                        "data": "rightsId"
                    },
                    {
                        "data": "rightsName"
                    },
                    {
                        "data": "moduleName"
                    },
                    {
                        "data": "rightsTypeName"
                    },
                    {
                        "data": "statusName"
                    },
                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))
    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
        $('#rightsEditId').val(tableData.rightsId);
        $('#rightsEditName').val(tableData.rightsName);
        $('#moduleRightsEditId').val(tableData.moduleId); 
        $('#rightsTypeEditId').val(tableData.rightsTypeId);
        $('#statusEditId').val(tableData.statusId);     
    });


    $('#formRightsSave').click(function () {

        var rightsId = $('#rightsId').val();
        var rightsName = $('#rightsName').val();
        var moduleRightsId = $('#moduleRightsId').val();
        var rightsTypeId = $('#rightsTypeId').val();


        if (rightsId == ' ' || rightsId == 0) {
            alert("Enter Rights ID");
            $('#rightsId').focus();
            return false;
        } else if (rightsName == '' || rightsName == null) {
            alert("Enter Rights Name");
            $('#rightsName').focus();
            return false;
        } else if (moduleRightsId == '' || moduleRightsId == 0 || moduleRightsId=='-1') {
            alert("Select Module Name");
            $('#moduleRightsId').focus();
            return false;
        }else if (rightsTypeId == '' || rightsTypeId == 0 || rightsTypeId=='-1') {
            alert("Select RightsType");
            $('#rightsTypeId').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "saveFormRightsCreation?rightsId=" + rightsId + "&rightsName=" + rightsName
                    + "&moduleRightsId=" + moduleRightsId  + "&rightsTypeId=" + rightsTypeId,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;
            if (result == "S") {
                alert("Saved Successfully");
                clearData();
                location.reload();
            } else if (result == "A") {
                alert("Form Name Already Exists");
            } else {
                alert("failed");

            }
        }
    });


     $('#formRightsEditUpdate').click(function() {

		var rightsEditId = $('#rightsEditId').val();
        var rightsEditName = $('#rightsEditName').val();
        var moduleRightsEditId = $('#moduleRightsEditId').val();
        var rightsTypeEditId = $('#rightsTypeEditId').val();
        var statusEditId = $('#statusEditId').val();
	
		if (rightsEditId == ' ' || rightsEditId == 0) {
			alert("Select Rights Id");
			$('#rightsEditId').focus();
			return false;
		} else if (rightsEditName == '' || rightsEditName == null) {
			alert("Enter Rights Name");
			$('#rightsEditName').focus();
			return false;
		}else if (moduleRightsEditId == ' ' || moduleRightsEditId == 0 || moduleRightsEditId=='-1') {
			alert("Select Module");
			$('#moduleRightsEditId').focus();
			return false;
		} else if (rightsTypeEditId == ' ' || rightsTypeEditId == 0 || rightsTypeEditId=='-1') {
			alert("Select RightsType");
			$('#rightsTypeEditId').focus();
			return false;
		} else if (statusEditId == ' ' || statusEditId == 0 || statusEditId=='-1') {
			alert("Select Status");
			$('#statusEditId').focus();
			return false;
		}  else {
			var result = $.ajax({
				type : "POST",
				url : "updateFormRightsEdit?rightsEditId=" + rightsEditId + "&rightsEditName=" + rightsEditName
                    + "&moduleRightsEditId=" + moduleRightsEditId+ "&rightsTypeEditId=" + rightsTypeEditId
                    + "&statusEditId=" + statusEditId,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				//clearEditData();
				location.reload();
			} else if (result == "A") {
				alert("Form Name Already Exists");
			} else {
				alert("failed");

			}
		}
    });
    

    




    function clearData(){
        $('#rightsId').val('');
        $('#rightsName').val('');
        $('#moduleRightsId').val('-1');
        $('#rightsTypeId').val('-1');
    }

});