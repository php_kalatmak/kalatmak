var editor;
$(document).ready(function () {


	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-businesspartner').addClass('active');
	$('#menu-businesspartner > a').attr('aria-expanded', 'true');
	$('#menu-businesspartner > ul').addClass('in');

	$('#menu-salesgroup').addClass('active');
	$('#salesgroup').hide();
	$('#salesgroupEdit').hide();

	$('#salesgroupAdd').click(function () {
		$('#salesgroup').show();
	});

	$('#sales_Reset').click(function () {
		$('#salesgroupid').val('');
		$('#salesgroupname').val('');
	});
  
	$(".close").click(function () {
		$(".modal").hide();
	});

var table;
	(function ($) {
		'use strict';
		$(function () {
			table = $('.js-exportable').DataTable({
				responsive: true,
				"iDisplayLength": 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax: {
					type: "POST",
					url: "getSalesTypeIdList",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					async: false,
					//	success : function(data) {
					//		alert(data);

					//	},	
				},
				"columns": [
					{
						"data": "salesgroupId"
					},
					{
						"data": "salesDesc"
					},
					
					{
						"data": "action",
						render: actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function () {
		tableData = table.row(this).data();
		$('#salesgroupidEdit').val(tableData.salesgroupId);
		$('#salesnameEdit').val(tableData.salesDesc);
		
	});

//var result = $.ajax({
		//type: "POST",
		//url: "getSalesgroupEdit",
		//contentType: "application/json; charset=utf-8",
		//dataType: "json",
		//async: false,
		//success: function (msg) {
			

		//	$.each(msg, function (index, item) {

				//$("#salesgroupidEdit").get(0).options[$("#salesgroupidEdit").get(0).options.length] = new Option(item.Salesgroupid, item.Salesgroupid);


			//});
		//},
	//});

	$('#salesgroupSave').click(function () {

		var salesgroupid = $('#salesgroupid').val();
		var salesgroupname = $('#salesgroupname').val();
		
        	if (salesgroupid == null || salesgroupid == undefined || salesgroupid == "") {
			alert("Enter Sales Group Id");
			$('#salesgroupid').focus();
			return false;
		} else if (salesgroupname == null || salesgroupname == undefined || salesgroupname == "") {
			alert("Enter Sales Group  Name");
			$('#salesgroupname').focus();
			return false;
		} else {
			var result = $.ajax({
				type: "POST",
				url: "saveSalesgroup?salesgroupid=" + salesgroupid + "&salesgroupname=" + salesgroupname,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;



			if (result == "S") {
				alert("Saved Successfully");
				$('#salesgroupid').val('');
				$('#salesgroupname').val('');
				location.reload();
				$("#salesgroup").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Sales Group Already Exists");
			} else {
				alert("failed");

			}
		}
	});
	$('#salesgroupUpdate').click(function () {

		var salesgroupid = $('#salesgroupidEdit').val();
		var salesgroupname = $('#salesnameEdit').val();
		
        	if (salesgroupid == null || salesgroupid == undefined || salesgroupid == "") {
			alert("Enter Sales Group Id");
			$('#salesgroupidEdit').focus();
			return false;
		} else if (salesgroupname == null || salesgroupname == undefined || salesgroupname == "") {
			alert("Enter Sales Group  Name");
			$('#salesnameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type: "POST",
				url: "UpdateSalesgroup?salesgroupid=" + salesgroupid + "&salesgroupname=" + salesgroupname,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;

			if (result == "S") {
				alert("Update Successfully");
				$('#salesgroupidEdit').val('');
				$('#salesnameEdit').val('');
				location.reload();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Sales Group Already Exists");
			} else {
				alert("failed");

			}
		}
	});

});