$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-grade').addClass('active');


    $('#grade').hide();
    $('#gradeEdit').hide();

    $('#gradeAdd').click(function () {
        $('#grade').show();
    });

    $('#grade_Reset').click(function () {
        $('#gradeid').val('');
        $('#gradename').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });


    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                //processing : true,
                //serverSide : true,
                //dom : '<"html5buttons"B>lTfgtip',
                //buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
                ajax: {
                    type: "POST",
                    url: "getgradeTypeIdList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    //	success : function(data) {
                    //		alert(data);

                    //	},	
                },
                "columns": [
                    {
                        "data": "gradeId"
                    },
                    {
                        "data": "gradedesc"
                    },

                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))

    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
        $('#gradeIdEdit').val(tableData.gradeId);
        $('#gradenameEdit').val(tableData.gradedesc);

    });

   // var result = $.ajax({
     //   type: "POST",
       // url: "getgradeEdit",
       // contentType: "application/json; charset=utf-8",
       // dataType: "json",
       // async: false,
       // success: function (msg) {


          //  $.each(msg, function (index, item) {

            //    $("#gradeIdEdit").get(0).options[$("#gradeIdEdit").get(0).options.length] = new Option(item.gradeid, item.gradeid);


           // });
       // },
   // });

    $('#gradeSave').click(function () {

        var gradeid = $('#gradeid').val();
        var gradename = $('#gradename').val();

        if (gradeid == null || gradeid == undefined || gradeid == "") {
            alert("Enter Grade Id");
            $('#gradeid').focus();
            return false;
        } else if (gradename == null || gradename == undefined || gradename == "") {
            alert("Enter Grade Name");
            $('#gradename').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "savegrade?gradeid=" + gradeid + "&gradename=" + gradename,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;



            if (result == "S") {
                alert("Saved Successfully");
                $('#gradeid').val('');
                $('#gradename').val('');
                location.reload();
                $("#grade").hide();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
    $('#gradeUpdate').click(function () {

        var gradeid = $('#gradeIdEdit').val();
        var gradename = $('#gradenameEdit').val();

        if (gradeIdEdit == null || gradeIdEdit == undefined || gradeIdEdit == "") {
            alert("Enter Grade Id");
            $('#gradeIdEdit').focus();
            return false;
        } else if (gradenameEdit == null || gradenameEdit == undefined || gradenameEdit == "") {
            alert("Enter Grade Name");
            $('#gradenameEdit').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "Updategrade?gradeid=" + gradeid + "&gradename=" + gradename,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;

            if (result == "S") {
                alert("Update Successfully");
                $('#gradeIdEdit').val('');
                $('#gradenameEdit').val('');
                location.reload();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
});