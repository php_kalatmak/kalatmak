$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-bnusinessPartnerType').addClass('active');
    $('#businesssave').hide();
    $('#businessEdit').hide();

    $('#businessAdd').click(function () {
        $('#businesssave').show();
    });

    $('#businesspartner_Reset').click(function () {
        $('#businesspartnerId').val('');
        $('#businesspartnername').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });


    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                //processing : true,
                //serverSide : true,
                //dom : '<"html5buttons"B>lTfgtip',
                //buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
                ajax: {
                    type: "POST",
                    url: "getbusinessTypeIdList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    //	success : function(data) {
                    //		alert(data);

                    //	},	
                },
                "columns": [
                    {
                        "data": "bptypeId"
                    },
                    {
                        "data": "bptypeDesc"
                    },

                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))

    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
        $('#businesspartnerIdEdit').val(tableData.bptypeId);
        $('#businesspartnernameEdit').val(tableData.bptypeDesc);

    });

  //  var result = $.ajax({
       // type: "POST",
       // url: "getbptyEdit",
      //  contentType: "application/json; charset=utf-8",
       // dataType: "json",
       // async: false,
      //  success: function (msg) {


           // $.each(msg, function (index, item) {

               // $("#businesspartnerIdEdit").get(0).options[$("#businesspartnerIdEdit").get(0).options.length] = new Option(item.bptypeId, item.bptypeId);


          //  });
       // },
   // });

    $('#businesspartnerSave').click(function () {

        var businesspaId = $('#businesspartnerId').val();
        var businesspartname = $('#businesspartnername').val();


        alert("businesspaId  "+businesspaId)
           alert("businesspartname  "+businesspartname)
        if (businesspaId == null || businesspaId == undefined || businesspaId == "") {
            alert("Enter Business Partner Id");
            $('#businesspartnerId').focus();
            return false;
        } else if (businesspartname == null || businesspartname == undefined || businesspartname == "") {
            alert("Enter Business Partner Name");
            $('#businesspartnername').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "businesspartnername?businesspaId=" + businesspaId + "&businesspartname=" + businesspartname,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;



            if (result == "S") {
                alert("Saved Successfully");
                $('#businesspartnerId').val('');
                $('#businesspartnername').val('');
                location.reload();
                $("#grade").hide();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
    $('#BusinesspartnerUpdate').click(function () {

         var businesspaId = $('#businesspartnerIdEdit').val();
        var businesspartname = $('#businesspartnernameEdit').val();


      
        if (businesspaId == null || businesspaId == undefined || businesspaId == "") {
            alert("Enter Business Partner Id");
            $('#businesspartnerIdEdit').focus();
            return false;
        } else if (businesspartname == null || businesspartname == undefined || businesspartname == "") {
            alert("Enter Business Partner Name");
            $('#businesspartnernameEdit').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "businesspartnernameUpdate?businesspaId=" + businesspaId + "&businesspartname=" + businesspartname,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;

            if (result == "S") {
                alert("Update Successfully");
                $('#businesspartnerIdEdit').val('');
                $('#businesspartnernameEdit').val('');
                location.reload();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
});