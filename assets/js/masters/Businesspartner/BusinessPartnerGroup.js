$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-businesspartner').addClass('active');
    $('#menu-businesspartner > a').attr('aria-expanded', 'true');
    $('#menu-businesspartner > ul').addClass('in');

    $('#menu-businessPartnergroup').addClass('active');
    $('#businessgroupsave').hide();
    $('#businessgroupEdit').hide();

    $('#businessgroupAdd').click(function () {
        $('#businessgroupsave').show();
    });

    $('#businesspartnergroup_Reset').click(function () {
        $('#businesspartnergroupId').val('');
        $('#businesspartnergroupname').val('');
    });

    $(".close").click(function () {
        $(".modal").hide();
    });


    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                //processing : true,
                //serverSide : true,
                //dom : '<"html5buttons"B>lTfgtip',
                //buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
                ajax: {
                    type: "POST",
                    url: "getbusinessgroupTypeIdList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    //	success : function(data) {
                    //		alert(data);

                    //	},	
                },
                "columns": [
                    {
                        "data": "businessgroupId"
                    },
                    {
                        "data": "businessgroupDesc"
                    },

                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))

    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
        $('#businesspartnergroupIdEdit').val(tableData.businessgroupId);
        $('#businesspartnergroupnameEdit').val(tableData.businessgroupDesc);

    });

   // var result = $.ajax({
       // type: "POST",
       // url: "getbptygroupEdit",
       // contentType: "application/json; charset=utf-8",
       // dataType: "json",
       // async: false,
       // success: function (msg) {


         //   $.each(msg, function (index, item) {

            //    $("#businesspartnergroupIdEdit").get(0).options[$("#businesspartnergroupIdEdit").get(0).options.length] = new Option(item.businessgroupId, item.businessgroupId);


           // });
       // },
  //  });

    $('#businesspartnergroupSave').click(function () {

        var businesspartnergroupId = $('#businesspartnergroupId').val();
        var businesspartnergroupname = $('#businesspartnergroupname').val();


  
        if (businesspartnergroupId == null || businesspartnergroupId == undefined || businesspartnergroupId == "") {
            alert("Enter Business Partner Group Id");
            $('#businesspartnergroupId').focus();
            return false;
        } else if (businesspartnergroupname == null || businesspartnergroupname == undefined || businesspartnergroupname == "") {
            alert("Enter Business Partner Group Name");
            $('#businesspartnergroupname').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "savebusinesspartnergroupname?businesspartnergroupId=" + businesspartnergroupId + "&businesspartnergroupname=" + businesspartnergroupname,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;



            if (result == "S") {
                alert("Saved Successfully");
                $('#businesspartnergroupId').val('');
                $('#businesspartnergroupname').val('');
                location.reload();
                $("#grade").hide();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
    $('#BusinesspartnerGroupUpdate').click(function () {

         var businesspartnergroupId = $('#businesspartnergroupIdEdit').val();
        var businesspartnergroupname = $('#businesspartnergroupnameEdit').val();


  
        if (businesspartnergroupId == null || businesspartnergroupId == undefined || businesspartnergroupId == "") {
            alert("Enter Business Partner Group Id");
            $('#businesspartnerIdEdit').focus();
            return false;
        } else if (businesspartnergroupname == null || businesspartnergroupname == undefined || businesspartnergroupname == "") {
            alert("Enter Business Partner Group Name");
            $('#businesspartnernameEdit').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "Updatebusinesspartnergroupname?businesspartnergroupId=" + businesspartnergroupId + "&businesspartnergroupname=" + businesspartnergroupname,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;

            if (result == "S") {
                alert("Update Successfully");
                $('#businesspartnergroupIdEdit').val('');
                $('#businesspartnergroupnameEdit').val('');
                location.reload();
                $("#dtab").show();
            } else if (result == "A") {
                alert(" Already Exists");
            } else {
                alert("failed");

            }
        }
    });
});