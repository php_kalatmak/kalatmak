$(document).ready(function(){
		
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-binlocation').addClass('active');
	$('#BinLocation').hide();
	$('#binlocationEdit').hide();

	$('#binlocationAdd').click(function() {
		$('#BinLocation').show();
	});

	$('#storage_Reset').click(function() {
		      $('#branchnameEdit').val('-1');
				$('#storagenameEdit').val('-1');				
			    $('#BinnewEdit').val('');
				 $('#BinnewName').val('');				
				   $('#CapacityNewEdit').val('')
				    $('#statusBinEdit').val('-1')
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax : {
					type : "POST",
					url : "getBinLocationTypeIdList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
					//	success : function(data) {
					//		alert(data);

				//	},	//capacity
				},
				"columns" : [
					{
						"data" : "branchDesc"
					},
					{
						"data" : "storageDesc"
					},
					{
						"data" : "binId"
					},
					{
						"data" : "Binname"
					},
					{
						"data" : "capacity"
					},
					
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#branchnameEdit').val(tableData.branchDesc);
		$('#storagenameEdit').val(tableData.storageDesc);
		$('#BincodeEdit').val(tableData.binId);
		$('#BinNameEdit').val(tableData.Binname);
		$('#CapacityEdit').val(tableData.capacity);
		$('#BranchId').val(tableData.branchId);
		$('#getStorageId').val(tableData.storageId);
			
				
	});

	$.ajax({
			type : "POST",
			url : "getBranchCodesbinloc",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#Branchname").get(0).options.length = 0;
				$("#Branchname").get(0).options[0] = new Option("Select Branch", "-1");
				
				$.each(msg, function(index, item) {
					
					$("#Branchname").get(0).options[$("#Branchname").get(0).options.length] = new Option(item.branchName,item.branchcode);
					
				});
			},
		});

		$('#Branchname').on('change', function(event) {
			$.ajax({
				type : "POST",
				url : "getStorageCodesbinloc",
				contentType : "application/json; charset=utf-8",
				dataType : "json",
				async : false,
				success : function(msg) {
					$("#storagenamebin").get(0).options.length = 0;
					$("#storagenamebin").get(0).options[0] = new Option("Select Storage", "-1");

					$.each(msg, function(index, item) {

						$("#storagenamebin").get(0).options[$("#storagenamebin").get(0).options.length] = new Option(item.StorageDesc,item.StorageId);
						
						
					});
				},
			});

		});

		$('#BinlocationSave').click(function() {

		var BranchName = $('#Branchname').val();
		var StorageName = $('#storagenamebin').val();
		var Capacity = $('#capacity').val();
		var BinCode = $('#bincode').val();
		var BinName = $('#binname').val();
		
		
	
		if (BranchName == null || BranchName == undefined || BranchName == "" || BranchName == "-1") {
			alert("Select  Branch Name");
			$('#branchname').focus();
			return false;
		}else if (StorageName == null || StorageName == undefined || StorageName == "" || StorageName == "-1") {
			alert("Select  Storage Name");
			$('#storagenameass').focus();
			return false;
		}else if (BinCode == '' || BinCode == null) {
			alert("Enter Bin code ");
			$('#bincode').focus();
			return false;
		}else if (BinName == '' || BinName == null) {
			alert("Enter Bin Name ");
			$('#binname').focus();
			return false;
			} else if (Capacity == '' || Capacity == null) {
			alert("Enter capacity ");
			$('#capacity').focus();
			return false;
				}else{ 
			var result = $.ajax({
				type : "POST",
			url : "saveBinLocation?BranchName=" +BranchName+ "&StorageName=" +StorageName+ "&Capacity=" +Capacity+ "&BinCode=" +BinCode + "&BinName=" +BinName,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
		
			if (result == "S") {
				alert("Saved Successfully");
				$('#Branchname').val('-1');
				$('#storagenamebin').val('-1');
				$('#bincode').val('');
			    $('#capacity').val('');
				$('#binname').val('');
				location.reload();
				$("#StorageAss").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Storage Code Already Exists");
			}else {
				alert("failed");

			}
		}
	});

	

			
		
		
		

		
var result = $.ajax({
			type : "POST",
			url : "getStatusFromEdit",
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#statusBinEdit").get(0).options.length = 0;
				$("#statusBinEdit").get(0).options[0] = new Option("Select Status", "-1");

				$.each(msg, function(index, item) {

					$("#statusBinEdit").get(0).options[$("#statusBinEdit").get(0).options.length] = new Option(item.statusDesc, item.statusId);
					
					
				});
			},
		});
			$('#binlocationUpdate').click(function() {

			var BranchCode = $('#branchnameEdit').val();
			var StorageCode = $('#storagenameEdit').val();
			var BinCode = $('#BincodeEdit').val();
			var BinNameEdit = $('#BinNameEdit').val();			
			var capacityEdit =$('#CapacityEdit').val();			
			var statusBinEdit =$('#statusBinEdit').val();
			var branchId =$('#BranchId').val();
			var StorageId =$('#getStorageId').val();

			if (statusBinEdit == null || statusBinEdit == undefined || statusBinEdit == "" || statusBinEdit == "-1") {
				alert("Select Status");
				$('#statusBinEdit').focus();
				return false;
		        
               } else{
				var result = $.ajax({
					type : "POST",
					url : "BinlocationUpdates?BranchName=" +branchId+ "&StorageName=" +StorageId+ "&BinCode=" +BinCode+ "&BinNameEdit=" +BinNameEdit + "&capacityEdit=" +capacityEdit+"&statusBinEdit=" +statusBinEdit,
					dataType : "json",
					contentType : "application/json",
					processData : false,
					async : false
				}).responseText;
				
				if (result == "S") {
					alert("Updated Successfully");
				$('#branchnameEdit').val('-1');
				$('#storagenameEdit').val('-1');
				$('#BincodeEdit').val('-1');
			    $('#BinnewEdit').val('');
				$('#BinNameEdit').val('-1');
				 $('#BinnewName').val('');//capacityEdit
				  $('#capacityEdit').val('-1')
				   $('#CapacityNewEdit').val('')
				    $('#statusBinEdit').val('-1')
				location.reload();
				$("#binlocationEdit").hide();
				$("#dtab").show();
					
				} else if (result == "D") {
					alert("Already Exists");
				} else {
					alert("failed");
				}
			
			}
		});
		
});


