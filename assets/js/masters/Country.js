var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-country').addClass('active');
	$('#Country').hide();
	$('#countryEdit').hide();

	$('#countryAdd').click(function() {
		$('#Country').show();
	});

	$('#country_Reset').click(function() {
		$('#countrycode').val('');
		$('#countryname').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax : {
					type : "POST",
					url : "getCountryTypeIdList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
					//	success : function(data) {
					//		alert(data);

				//	},	
				},
				"columns" : [
					{
						"data" : "countryId"
					},
					{
						"data" : "countryDesc"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#countrycodeEdit').val(tableData.countryId);
		$('#countrynameEdit').val(tableData.countryDesc);
	});

	/*	$('#dtab').on("click", '#dtab tbody', function(e) {
			var parent = $(this).closest('#dtab').parents('tr').index();
			var parentIndex = $('tbody tr:nth-child(' + (parent) + ')').attr('index');
			var currentIndex = $(this).closest('tr').index();
			var data = sections[parentIndex][currentIndex];
			console.log(data);
		});
	*/ /*$('.js-exportable').on('click', 'a.fa fa-pencil', function(e) {
		e.preventDefault();
		alert(HI)
		editor.edit($(this).closest('tr'), {
			title : 'Edit record',
			buttons : 'Update'
		});
	});*/

	$('#countrySave').click(function() {

		var countryCode = $('#countrycode').val();
		var countryName = $('#countryname').val();
		if (countryCode == ' ' || countryCode == 0) {
			alert("Enter Country Code");
			$('#countrycode').focus();
			return false;
		} else if (countryName == '' || countryName == null) {
			alert("Enter Country Name");
			$('#countryname').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveCountry?countryCode=" + $("#countrycode").val() + "&countryName=" + $("#countryname").val(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#countrycode').val('');
				$('#countryname').val('');
				location.reload();
				$("#Country").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Country Code Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#countryUpdate').click(function() {
		var countryCode = $('#countrycodeEdit').val();
		var countryName = $('#countrynameEdit').val();

		if (countryCode == null || countryCode == undefined || countryCode == "" || countryCode == "-1") {
			alert("Select Country");
			$('#countrycodeEdit').focus();
			return false;
		} else if (countryName == '' || countryName == null) {
			alert("Enter Description");
			$('#countrynameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateCountry?countryCode=" + countryCode + "&countryName=" + countryName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#countrycodeEdit').val('');
				$('#countrynameEdit').val('');
				location.reload();
			} else if (result == "D") {
				alert("Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});