$(document).ready(function () {

    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-rights').addClass('active');
    $('#menu-rights > a').attr('aria-expanded', 'true');
    $('#menu-rights > ul').addClass('in');

    $('#menu-moduleCreation').addClass('active');
    $("#ModuleAdd").hide();

    $('#moduleAdd').click(function () {
        $('#ModuleAdd').show();
    });

    $('.close').click(function () {
        $('#ModuleAdd').hide();
    });

    $('#moduleReset').click(function () {
        $('#moduleName').val('');
        $('#parentModuleId').val('');
    });

     $('#moduleEditReset').click(function () {
        $('#modulenameEdit').val('');
        $('#parentModuleIdEdit').val('-1');
    });


    $.ajax({
        type: "POST",
        url: "getModuleIdCount",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {

            $("#moduleId").val(msg[0].count);
        },
    });

    $.ajax({
        type: "POST",
        url: "getParentModuleIdList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#parentModuleId").get(0).options.length = 0;
            $("#parentModuleId").get(0).options[0] = new Option("Select Parent Module", "-1");
            $("#parentModuleIdEdit").get(0).options.length = 0;
            $("#parentModuleIdEdit").get(0).options[0] = new Option("Select Parent Module", "-1");
            $.each(msg, function (index, data) {
                $("#parentModuleId").get(0).options[$("#parentModuleId").get(0).options.length] = new Option(data.moduleName, data.moduleId);
                $("#parentModuleIdEdit").get(0).options[$("#parentModuleIdEdit").get(0).options.length] = new Option(data.moduleName, data.moduleId);
            });
        },
    });




    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                ajax: {
                    type: "POST",
                    url: "getModuleTableList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                },
                "columns": [
                    {
                        "data": "moduleId"
                    },
                    {
                        "data": "moduleName"
                    },

                    {
                        "data": "parentModuleName"
                    },
                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))
    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
        $('#moduleIdEdit').val(tableData.moduleId);
        $('#modulenameEdit').val(tableData.moduleName);
       // alert(tableData.parentModule);
        if (tableData.parentModule == "" || tableData.parentModule == null) {
           // alert(-1)
            $('#parentModuleIdEdit').val('-1');
        } else {
            $('#parentModuleIdEdit').val(tableData.parentModule);
        }

    });



    $('#moduleSave').click(function () {

        var moduleId = $('#moduleId').val();
        var moduleName = $('#moduleName').val();
        var parentModuleId = $('#parentModuleId').val();


        if (moduleId == ' ' || moduleId == 0) {
            alert("Enter Module ID");
            $('#moduleId').focus();
            return false;
        } else if (moduleName == '' || moduleName == null) {
            alert("Enter Module Name");
            $('#moduleName').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "saveModuleCreation?moduleId=" + moduleId + "&moduleName=" + moduleName
                    + "&parentModuleId=" + parentModuleId,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;
            if (result == "S") {
                alert("Saved Successfully");
                clearData();
                location.reload();
            } else if (result == "A") {
                alert("Role Name Already Exists");
            } else {
                alert("failed");

            }
        }
    });



    $('#moduleEditUpdate').click(function() {

		var moduleIdEdit = $('#moduleIdEdit').val();		
        var moduleNameEdit = $('#modulenameEdit').val();
        var parentModuleIdEdit = $('#parentModuleIdEdit').val();
	
		if (moduleIdEdit == ' ' || moduleIdEdit == 0) {
			alert("Select Module ID");
			$('#moduleIdEdit').focus();
			return false;
		} else if (moduleNameEdit == '' || moduleNameEdit == null) {
			alert("Enter Module Name");
			$('#modulenameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateRoleEditCreation?moduleIdEdit=" + moduleIdEdit + "&moduleNameEdit=" + moduleNameEdit
					+ "&parentModuleIdEdit=" + parentModuleIdEdit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				//clearEditData();
				location.reload();
			} else if (result == "A") {
				alert("Role Name Already Exists");
			} else {
				alert("failed");

			}
		}
	});






    function clearData() {
        $('#moduleId').val('');
        $('#moduleName').val('');
        $('#parentModuleId').val('-1');
    }


});