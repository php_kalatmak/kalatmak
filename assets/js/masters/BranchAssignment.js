$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-branchAssignment').addClass('active');

	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#comp").get(0).options.length = 0;
			$("#comp").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, item) {
				$("#comp").get(0).options[$("#comp").get(0).options.length] = new Option(item.compName, item.compCode);
			});
		},
	});
	var result = $.ajax({
		type : "POST",
		url : "getBranchListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$.each(msg, function(index, item) {
				$("#Branch_Map").get(0).options[$("#Branch_Map").get(0).options.length] = new Option(item.branchCode + '-' + item.branchName, item.branchCode);
			});
			$('#Branch_Map').multiselect({
			});
		},
	});

	$('#comp').on('change', function() {
		$("#Branch_Map").val('');
		$("#Branch_Map").multiselect('deselectAll', true);
		var compId = $("#comp").val();

		var result = $.ajax({
			type : "POST",
			url : "getBranchListinAssign?compId=" + compId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#Branch_Map").multiselect('deselectAll', true);
				$("#Branch_Map").multiselect('select', msg);
			}
		});
	});

	$('#saveAssgn').click(function() {
		var compId = $('#comp').val();
		var branchId = $('#Branch_Map').val();
		if (compId == null || compId == undefined || compId == "" || compId == "-1") {
			alert("Select Company");
			$('#comp').focus();
			return false;
		} else if (branchId == null || branchId == undefined || branchId == "" || branchId == "-1") {
			alert("Select Branch");
			$('#Branch_Map').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveBranchAssgn?compId=" + compId + "&branchId=" + branchId,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");

			}
		}
	});
});