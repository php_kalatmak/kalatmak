var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-mangeItemType').addClass('active');
	$('#ManageItemType').hide();
	$('#ManageItemTypeEdit').hide();

	$('#manageItemTypeAdd').click(function() {
		$('#ManageItemType').show();
	});

	$('#manageItemType_Reset').click(function() {
		$('#manageItemTypeCode').val('');
		$('#manageItemTypeName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getManageItemTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "manageItemTypeId"
					},
					{
						"data" : "manageItemTypeName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#manageItemTypeCodeEdit').val(tableData.manageItemTypeId);
		$('#manageItemTypeNameEdit').val(tableData.manageItemTypeName);
	});

	
	$('#manageItemTypeSave').click(function() {

		var manageItemTypeCode = $('#manageItemTypeCode').val();
		var manageItemTypeName = $('#manageItemTypeName').val();
		if (manageItemTypeCode == ' ' || manageItemTypeCode == 0) {
			alert("Enter Manage Item Type Code");
			$('#manageItemTypeCode').focus();
			return false;
		} else if (manageItemTypeName == '' || manageItemTypeName == null) {
			alert("Enter Manage Item Type Name");
			$('#manageItemTypeName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveManageItemType?manageItemTypeCode=" + manageItemTypeCode + "&manageItemTypeName=" + manageItemTypeName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#manageItemTypeCode').val('');
				$('#manageItemTypeName').val('');
				location.reload();
				//$("#Country").hide();
				//$("#dtab").show();
			} else if (result == "A") {
				alert("Manage Item Type Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#manageItemTypeUpdate').click(function() {
		var manageItemTypeEditCode = $('#manageItemTypeCodeEdit').val();
		var manageItemTypeEditName = $('#manageItemTypeNameEdit').val();

		if (manageItemTypeEditCode == null || manageItemTypeEditCode == undefined || manageItemTypeEditCode == "") {
			alert("Enter Manage Item Type Code");
			$('#manageItemTypeEditCode').focus();
			return false;
		} else if (manageItemTypeEditName == '' || manageItemTypeEditName == null) {
			alert("Enter Manage Item Type Name");
			$('#manageItemTypeNameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

                url : "updateManageItemType?manageItemTypeEditCode=" + manageItemTypeEditCode 
                + "&manageItemTypeEditName=" + manageItemTypeEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#manageItemTypeEditCode').val('');
				$('#manageItemTypeNameEdit').val('');
				location.reload();
			} else if (result == "A") {
				alert("Manage Item Type Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});