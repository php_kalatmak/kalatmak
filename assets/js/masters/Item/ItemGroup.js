var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-itemGroup').addClass('active');
	$('#ItemGroup').hide();
	$('#ItemGroupEdit').hide();

	$('#itemGroupAdd').click(function() {
		$('#ItemGroup').show();
	});

	$('#itemGroup_Reset').click(function() {
		$('#itemGroupCode').val('');
		$('#itemGroupName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getItemGroupList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "itemGroupId"
					},
					{
						"data" : "itemGroupName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#ItemGroupIdEdit').val(tableData.itemGroupId);
		$('#itemGroupNameEdit').val(tableData.itemGroupName);
	});

	
	$('#itemGroupSave').click(function() {

		var itemGroupCode = $('#itemGroupCode').val();
		var itemGroupName = $('#itemGroupName').val();
		if (itemGroupCode == ' ' || itemGroupCode == 0) {
			alert("Enter Item Group Code");
			$('#itemGroupCode').focus();
			return false;
		} else if (itemGroupName == '' || itemGroupName == null) {
			alert("Enter Item Group Name");
			$('#itemGroupName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveItemGroup?itemGroupCode=" + itemGroupCode + "&itemGroupName=" + itemGroupName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#itemGroupCode').val('');
				$('#itemGroupName').val('');
				location.reload();
				//$("#Country").hide();
				//$("#dtab").show();
			} else if (result == "A") {
				alert("Item Group Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#itemGroupUpdate').click(function() {
		var itemGroupEditCode = $('#ItemGroupIdEdit').val();
		var itemGroupEditName = $('#itemGroupNameEdit').val();

		if (itemGroupEditCode == null || itemGroupEditCode == undefined || itemGroupEditCode == "") {
			alert("Enter Item Group Code");
			$('#ItemGroupIdEdit').focus();
			return false;
		} else if (itemGroupEditName == '' || itemGroupEditName == null) {
			alert("Enter Item Group Name");
			$('#itemGroupNameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateItemGroup?itemGroupEditCode=" + itemGroupEditCode + "&itemGroupEditName=" + itemGroupEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#ItemGroupIdEdit').val('');
				$('#itemGroupNameEdit').val('');
				location.reload();
			} else if (result == "A") {
				alert("Item Group Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});