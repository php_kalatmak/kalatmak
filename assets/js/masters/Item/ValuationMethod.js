var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-valuationMethod').addClass('active');
	$('#ValuationMethodAdd').hide();
	$('#ValuationMethodEdit').hide();

	$('#valuationMethodAdd').click(function() {
		$('#ValuationMethodAdd').show();
	});

	$('#valuationMethod_Reset').click(function() {
		$('#valuationMethodCode').val('');
		$('#valuationMethodName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getValuationMethodTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "valuationMethodCode"
					},
					{
						"data" : "valuationMethodName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#valuationMethodEditCode').val(tableData.valuationMethodCode);
		$('#valuationMethodEditName').val(tableData.valuationMethodName);
	});

	
	$('#valuationMethodSave').click(function() {

		var valuationMethodCode = $('#valuationMethodCode').val();
		var valuationMethodName = $('#valuationMethodName').val();
		if (valuationMethodCode == ' ' || valuationMethodCode == 0) {
			alert("Enter Valuation Method Code");
			$('#valuationMethodCode').focus();
			return false;
		} else if (valuationMethodName == '' || valuationMethodName == null) {
			alert("Enter Valuation Method Name");
			$('#valuationMethodName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
                url : "saveValuationMethod?valuationMethodCode=" + valuationMethodCode
                 + "&valuationMethodName=" + valuationMethodName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#valuationMethodCode').val('');
				$('#valuationMethodName').val('');
				location.reload();
			} else if (result == "A") {
				alert("Valuation Method Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#valuationMethodUpdate').click(function() {
		var valuationMethodEditCode = $('#valuationMethodEditCode').val();
		var valuationMethodEditName = $('#valuationMethodEditName').val();

		if (valuationMethodEditCode == null || valuationMethodEditCode == undefined || valuationMethodEditCode == "") {
			alert("Enter Valuation Method Code");
			$('#valuationMethodEditCode').focus();
			return false;
		} else if (valuationMethodEditName == '' || valuationMethodEditName == null) {
			alert("Enter Valuation Method Name");
			$('#valuationMethodEditName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

                url : "updateValuationMethod?valuationMethodEditCode=" + valuationMethodEditCode 
                + "&valuationMethodEditName=" + valuationMethodEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#valuationMethodEditCode').val('');
				$('#valuationMethodEditName').val('');
				location.reload();
			} else if (result == "A") {
				alert("Valuation Method Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});