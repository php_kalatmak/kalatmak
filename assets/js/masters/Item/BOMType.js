var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-bomType').addClass('active');
	$('#BOMTypeAdd').hide();
	$('#BOMTypeEdit').hide();

	$('#bomTypeAdd').click(function() {
		$('#BOMTypeAdd').show();
	});

	$('#bomType_Reset').click(function() {
		$('#bomTypeCode').val('');
		$('#bomTypeName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getBOMTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "bomTypeCode"
					},
					{
						"data" : "bomTypeName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#bomTypeEditCode').val(tableData.bomTypeCode);
		$('#bomTypeEditName').val(tableData.bomTypeName);
	});

	
	$('#bomTypeSave').click(function() {

		var bomTypeCode = $('#bomTypeCode').val();
		var bomTypeName = $('#bomTypeName').val();
		if (bomTypeCode == ' ' || bomTypeCode == 0) {
			alert("Enter BOM Type Code");
			$('#bomTypeCode').focus();
			return false;
		} else if (bomTypeName == '' || bomTypeName == null) {
			alert("Enter BOM Type Name");
			$('#bomTypeName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveBOMType?bomTypeCode=" + bomTypeCode + "&bomTypeName=" + bomTypeName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#bomTypeCode').val('');
				$('#bomTypeName').val('');
				location.reload();
			} else if (result == "A") {
				alert("BOM Type Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#bomTypeUpdate').click(function() {
		var bomTypeEditCode = $('#bomTypeEditCode').val();
		var bomTypeEditName = $('#bomTypeEditName').val();

		if (bomTypeEditCode == null || bomTypeEditCode == undefined || bomTypeEditCode == "") {
			alert("Enter BOM Type Code");
			$('#bomTypeEditCode').focus();
			return false;
		} else if (bomTypeEditName == '' || bomTypeEditName == null) {
			alert("Enter BOM Type Name");
			$('#bomTypeEditName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateBOMType?bomTypeEditCode=" + bomTypeEditCode + "&bomTypeEditName=" + bomTypeEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#bomTypeEditCode').val('');
				$('#bomTypeEditName').val('');
				location.reload();
			} else if (result == "A") {
				alert("BOM Type Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});