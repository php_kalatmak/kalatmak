var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-shippingType').addClass('active');
	$('#ShippingTypeAdd').hide();
	$('#ShippingTypeEdit').hide();

	$('#shippingTypeAdd').click(function() {
		$('#ShippingTypeAdd').show();
	});

	$('#shippingType_Reset').click(function() {
		$('#shippingCode').val('');
		$('#shippingName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getShippingTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "shippingCode"
					},
					{
						"data" : "shippingName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#shippingEditCode').val(tableData.shippingCode);
		$('#shippingEditName').val(tableData.shippingName);
	});

	
	$('#shippingTypeSave').click(function() {

		var shippingCode = $('#shippingCode').val();
		var shippingName = $('#shippingName').val();
		if (shippingCode == ' ' || shippingCode == 0) {
			alert("Enter Shipping Code");
			$('#shippingCode').focus();
			return false;
		} else if (shippingName == '' || shippingName == null) {
			alert("Enter Shipping Name");
			$('#shippingName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveShippingType?shippingCode=" + shippingCode + "&shippingName=" + shippingName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#shippingCode').val('');
				$('#shippingName').val('');
				location.reload();
				//$("#Country").hide();
				//$("#dtab").show();
			} else if (result == "A") {
				alert("Shipping Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#shippingTypeUpdate').click(function() {
		var shippingEditCode = $('#shippingEditCode').val();
		var shippingEditName = $('#shippingEditName').val();

		if (shippingEditCode == null || shippingEditCode == undefined || shippingEditCode == "") {
			alert("Enter Shipping Code");
			$('#shippingEditCode').focus();
			return false;
		} else if (shippingEditName == '' || shippingEditName == null) {
			alert("Enter Shipping Name");
			$('#shippingEditName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateShippingType?shippingEditCode=" + shippingEditCode + "&shippingEditName=" + shippingEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#shippingEditCode').val('');
				$('#shippingEditName').val('');
				location.reload();
			} else if (result == "A") {
				alert("Shipping Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});