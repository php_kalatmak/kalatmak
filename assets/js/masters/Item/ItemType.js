var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-itemType').addClass('active');
	$('#ItemType').hide();
	$('#ItemTypeEdit').hide();

	$('#itemTypeAdd').click(function() {
		$('#ItemType').show();
	});

	$('#itemType_Reset').click(function() {
		$('#itemTypeCode').val('');
		$('#itemTypeName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getItemTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,				
				},
				"columns" : [
					{
						"data" : "itemTypeId"
					},
					{
						"data" : "itemTypeName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#itemTypeCodeEdit').val(tableData.itemTypeId);
		$('#itemTypeNameEdit').val(tableData.itemTypeName);
	});

	
	$('#itemTypeSave').click(function() {

		var itemTypeCode = $('#itemTypeCode').val();
		var itemTypeName = $('#itemTypeName').val();
		if (itemTypeCode == ' ' || itemTypeCode == 0) {
			alert("Enter Item Type Code");
			$('#itemTypeCode').focus();
			return false;
		} else if (itemTypeName == '' || itemTypeName == null) {
			alert("Enter Item Type Name");
			$('#itemTypeName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveItemType?itemTypeCode=" + itemTypeCode + "&itemTypeName=" + itemTypeName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#itemTypeCode').val('');
				$('#itemTypeName').val('');
				location.reload();
				//$("#Country").hide();
				//$("#dtab").show();
			} else if (result == "A") {
				alert("Item Type Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#itemTypeUpdate').click(function() {
		var itemTypeEditCode = $('#itemTypeCodeEdit').val();
		var itemTypeEditName = $('#itemTypeNameEdit').val();

		if (itemTypeEditCode == null || itemTypeEditCode == undefined || itemTypeEditCode == "") {
			alert("Enter Item Type Code");
			$('#itemTypeCodeEdit').focus();
			return false;
		} else if (itemTypeEditName == '' || itemTypeEditName == null) {
			alert("Enter Item Type Name");
			$('#itemTypeNameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateItemType?itemTypeEditCode=" + itemTypeEditCode + "&itemTypeEditName=" + itemTypeEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#itemTypeCodeEdit').val('');
				$('#itemTypeNameEdit').val('');
				location.reload();
			} else if (result == "A") {
				alert("Item Type Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});