var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-item').addClass('active');
	$('#menu-item > a').attr('aria-expanded', 'true');
	$('#menu-item > ul').addClass('in');

	$('#menu-valuationType').addClass('active');
	$('#ValuationType').hide();
	$('#valuationTypeEdit').hide();

	$('#valuationTypeAdd').click(function() {
		$('#ValuationType').show();
	});

	$('#valuationType_Reset').click(function() {
		$('#valuationCode').val('');
		$('#valuationName').val('');
	});

	$(".close").click(function() {
		$(".modal").hide();
	});
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax : {
					type : "POST",
					url : "getValuationTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
					//	success : function(data) {
					//		alert(data);

				//	},	
				},
				"columns" : [
					{
						"data" : "valuationCode"
					},
					{
						"data" : "valuationName"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#valuationCodeEdit').val(tableData.valuationCode);
		$('#valuationNameEdit').val(tableData.valuationName);
	});

	
	$('#valuationTypeSave').click(function() {

		var valuationCode = $('#valuationCode').val();
		var valuationName = $('#valuationName').val();
		if (valuationCode == ' ' || valuationCode == 0) {
			alert("Enter Valuation Code");
			$('#valuationCode').focus();
			return false;
		} else if (valuationName == '' || valuationName == null) {
			alert("Enter valuation Name");
			$('#valuationName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveValuationType?valuationCode=" + valuationCode + "&valuationName=" + valuationName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				$('#valuationCode').val('');
				$('#valuationName').val('');
				location.reload();
				//$("#Country").hide();
				//$("#dtab").show();
			} else if (result == "A") {
				alert("Valuation Type Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#valuationTypeUpdate').click(function() {
		var valuationEditCode = $('#valuationCodeEdit').val();
		var valuationEditName = $('#valuationNameEdit').val();

		if (valuationEditCode == null || valuationEditCode == undefined || valuationEditCode == "") {
			alert("Enter Valuation Type");
			$('#valuationCodeEdit').focus();
			return false;
		} else if (valuationEditName == '' || valuationEditName == null) {
			alert("Enter Valuation Name");
			$('#valuationNameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",

				url : "updateValuationType?valuationEditCode=" + valuationEditCode + "&valuationEditName=" + valuationEditName.trim(),
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				$('#valuationCodeEdit').val('');
				$('#valuationNameEdit').val('');
				location.reload();
			} else if (result == "A") {
				alert("Valuation Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});