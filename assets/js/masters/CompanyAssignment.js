$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-compAssignment').addClass('active');


	var result = $.ajax({
		type : "POST",
		url : "getCompGroupListInComp",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#compGrp").get(0).options.length = 0;
			$("#compGrp").get(0).options[0] = new Option("Select Company Group", "-1");
			$.each(msg, function(index, item) {
				$("#compGrp").get(0).options[$("#compGrp").get(0).options.length] = new Option(item.compGrpName, item.compGrpCode);

			});
		},
	});

	
	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$.each(msg, function(index, item) {

				$("#Comp_Map").get(0).options[$("#Comp_Map").get(0).options.length] = new Option(item.compCode + '-' + item.compName, item.compCode);
		
			});
			$('#Comp_Map').multiselect({
			});
		},
	});

	$('#compGrp').on('change', function() {
		$("#Comp_Map").val('');
		$("#Comp_Map").multiselect('deselectAll', true);
		var grpId = $("#compGrp").val();

		var result = $.ajax({
			type : "POST",
			url : "getCompListForGroup?grpId=" + grpId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#Comp_Map").multiselect('deselectAll', true);
				$("#Comp_Map").multiselect('select', msg);
			}
		});
	});

	$('#compGrp').on('change',function() {
		$("#Comp_Map").val('');
		$("#Comp_May").multiselect('deselectAll',true);
		var grpId = $("#compGrp").val();

		var result = $.ajax({
			type : "POST",
			url : "getCompListForGroup?grpId="
		})

	});



	$('#saveAssgn').click(function() {
		var grpId = $('#compGrp').val();
		var compId = $('#Comp_Map').val();
		if (grpId == null || grpId == undefined || grpId == "" || grpId == "-1") {
			alert("Select Company Group");
			$('#compGrp').focus();
			return false;
		} else if (compId == null || compId == undefined || compId == "" || compId == "-1") {
			alert("Select Company");
			$('#Comp_Map').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "savecompanyAssgn?grpId=" + grpId + "&compId=" + compId,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");

			}
		}
	});
});