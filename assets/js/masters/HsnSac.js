$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-general').addClass('active');
					$('#menu-general > a').attr('aria-expanded', 'true');
					$('#menu-general > ul').addClass('in');

					$('#menu-hsnSacCode').addClass('active');

					$('#HsnSacadd').hide();
					$('#HsnSacEdit').hide();

					$('#hsnSacAdd').click(function() {
						$('#HsnSacadd').show();
					});

					$(".close").click(function() {
						$(".modal").hide();
					});

					var result = $
							.ajax({
								type : "POST",
								url : "getCompListinAssignment",
								contentType : "application/json; charset=utf-8",
								dataType : "json",
								async : false,
								success : function(msg) {
									$("#compCode").get(0).options.length = 0;
									$("#compCode").get(0).options[0] = new Option(
											"Select Company", "-1");
									$("#compCodeEdit").get(0).options.length = 0;
									$("#compCodeEdit").get(0).options[0] = new Option(
											"Select Company", "-1");
									$
											.each(
													msg,
													function(index, item) {
														$("#compCodeEdit").get(
																0).options[$(
																"#compCodeEdit")
																.get(0).options.length] = new Option(
																item.compName,
																item.compCode);
														$("#compCode").get(0).options[$(
																"#compCode")
																.get(0).options.length] = new Option(
																item.compName,
																item.compCode);
													});
								},
							});

					var result = $
							.ajax({
								type : "POST",
								url : "getHsnSacList",
								contentType : "application/json; charset=utf-8",
								dataType : "json",
								async : false,
								success : function(msg) {
									$("#hsnSac").get(0).options.length = 0;
									$("#hsnSac").get(0).options[0] = new Option("Select HSN/SAC", "-1");
									 $("#hsnSacEdit").get(0).options.length = 0;
									 $("#hsnSacEdit").get(0).options[0] = new Option("Select HSN/SAC", "-1");
									$
											.each(
													msg,
													function(index, item) {
														$("#hsnSacEdit").get(0).options[$("#hsnSacEdit").get(0).options.length] = new Option(item.hsnSacDesc, item.hsnSacCode);
														$("#hsnSac").get(0).options[$("#hsnSac").get(0).options.length] = new Option(item.hsnSacDesc,item.hsnSacCode);
													});
								},
							});

					$('#chapterId').on('blur', function(event) {
						var chapId = $('#chapterId').val();
						$("#chapterIdCap").val(chapId);
					});

					$('#categories').on(
							'blur',
							function(event) {
								var id = $("#chapterId").val();
								var categ = $('#categories').val();
								/*var subCateg = $('#subCategories').val();
								var subSubCateg = $('#subSubCategories').val();*/
								
								$("#chapterIdCap").val('');
								$("#chapterIdCap").val(id + "." + categ);

							});

					$('#subCategories').on(
							'blur',
							function(event) {
								$("#chapterIdCap").val('');
								var id = $("#chapterId").val();
								var categ = $('#categories').val();
								var subCateg = $('#subCategories').val();
								//var subSubCateg = $('#subSubCategories').val();
								$("#chapterIdCap").val(id + "." + categ + "." + subCateg);
							});

					$('#subSubCategories').on(
							'blur',
							function(event) {
								$("#chapterIdCap").val('');
								var id = $("#chapterId").val();
								var categ = $('#categories').val();
								var subCateg = $('#subCategories').val();
								var subSubCateg = $('#subSubCategories').val();

								$("#chapterIdCap").val(
										id + "." + categ + "." + subCateg + "."
												+ subSubCateg);
							});
					
					
					
					$('#hsnSacSave').click(function() {

						var compCode = $('#compCode').val();
						var hsnSac = $('#hsnSac').val();
						var chapterId = $('#chapterId').val();
						var categories = $('#categories').val();
						var subCategories = $('#subCategories').val();
						var subSubCategories = $("#subSubCategories").val();
						var remarks = $('#remarks').val();
						var chapterIdCap = $('#chapterIdCap').val();
						if (compCode == ' ' || compCode == 0 || compCode == '-1') {
							alert("Select Company");
							$('#compCode').focus();
							return false;
						} else if (hsnSac == ' ' || hsnSac == 0 || hsnSac == '-1') {
							alert("Select HSN/SAC Code");
							$('#hsnSac').focus();
							return false;
						} else if (chapterId == '' || chapterId == null) {
							alert("Enter Period Name");
							$('#chapterId').focus();
							return false;
						} else if (categories == '' || categories == null || categories == '-1') {
							alert("Select Sub Period");
							$('#categories').focus();
							return false;
						} else if (subCategories == '' || subCategories == null) {
							alert("Select Date");
							$('#subCategories').focus();
							return false;
						}else if (subSubCategories == '' || subSubCategories == null) {
							alert("Select Date");
							$('#subCategories').focus();
							return false;
						} else if (remarks == '' || remarks == null) {
							alert("Select Date");
							$('#subCategories').focus();
							return false;
						}else if (chapterIdCap == '' || chapterIdCap == null) {
							alert("Select Date");
							$('#chapterIdCap').focus();
							return false;
						}  else {
							var result = $.ajax({
								type : "POST",
								url : "saveHsnSac?compCode=" + compCode + "&hsnSac=" + hsnSac+ "&chapterId=" + chapterId 
								+ "&categories=" + categories + "&subCategories=" + subCategories + "&subSubCategories=" 
								+ subSubCategories+"&remarks="+remarks+"&chapterIdCap="+chapterIdCap,
								dataType : "json",
								contentType : "application/json",
								processData : false,
								async : false
							}).responseText;
							if (result == "S") {
								alert("Saved Successfully");
								clearDataHsn();
								location.reload();
							} else if (result == "A") {
								alert("HSN/SAC Exists");
							} else {
								alert("failed");
							}
						}
					});
					
					
	function clearDataHsn(){
		 $('#compCode').val('-1');
		 $('#hsnSac').val('-1');
		 $('#chapterId').val('');
		 $('#categories').val('');
		 $('#subCategories').val('');
		 $("#subSubCategories").val('');
		 $('#remarks').val('');
		 $('#chapterIdCap').val('');
	}		
	//hsnSac_Reset
	
	$("hsnSac_Reset").click(function() {
		$('#compCode').val('-1');
		 $('#hsnSac').val('-1');
		 $('#chapterId').val('');
		 $('#categories').val('');
		 $('#subCategories').val('');
		 $("#subSubCategories").val('');
		 $('#remarks').val('');
		 $('#chapterIdCap').val('');
		
	});
	
	
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable')
					.DataTable(
							{
								responsive : true,
								"iDisplayLength" : 5,
								ajax : {
									type : "POST",
									url : "getHsnsacIdList",
									contentType : "application/json; charset=utf-8",
									dataType : "json",
									async : false,
								},
								"columns" : [ {
									"data" : "compCodeTbl"
								}, {
									"data" : "hsnSacTbl"
								}, {
									"data" : "chapterIdTbl"
								}, {
									"data" : "categoryTbl"
								}, {
									"data" : "subCategoryTbl"
								}, {
									"data" : "subSubCateTbl"
								},{
									"data" : "remarksTbl"
								}, {
									"data" : "chapterIdCapTbl"
								}, {
									"data" : "action",
									render : actionFormatter
								}, ]

							});
		});

	}(jQuery))
					
		$('#dtab').on("click", 'tr', function() {
			tableData = table.row(this).data();
			$('#compCodeEdit').val(tableData.compCodeTbl);
			alert(tableData.hsnSacTbl);
			$('#hsnSacEdit').val(tableData.hsnSacTbl);
			$('#chapterIdEdit').val(tableData.chapterIdTbl);
			
			$('#categoriesEdit').val(tableData.categoryTbl);
			$('#subCategoryEdit').val(tableData.subCategoryTbl);
			$('#subSubCategoryEdit').val(tableData.subSubCateTbl);
            $('#remarksEdit').val(tableData.remarksTbl);
			$('#chapterIdCapEdit').val(tableData.chapterIdCapTbl);
					
					
		});		
	
	
	
	$('#hsnSacUpdate').click(function() {
		var compCodeEdit = $('#compCodeEdit').val();
		var hsnSacEdit = $('#hsnSacEdit').val();
		var chapterIdEdit = $('#chapterIdEdit').val();
		var categoriesEdit = $('#categoriesEdit').val();
		var subCategoryEdit = $("#subCategoryEdit").val();
		var subSubCategoryEdit = $('#subSubCategoryEdit').val();
		var remarksEdit = $('#remarksEdit').val();
		var chapterIdCapEdit = $('#chapterIdCapEdit').val();
		
		//alert("compCodeEdit :"+compCodeEdit+" hsnSacEdit :"+hsnSacEdit+" chapterIdEdit :"+chapterIdEdit+" categoriesEdit :"+categoriesEdit
				//+" subCategoryEdit :"+subCategoryEdit+" subSubCategoryEdit :"+subSubCategoryEdit+" remarksEdit :"+remarksEdit+" chapterIdCapEdit :"+chapterIdCapEdit);
		
		if (compCodeEdit == ' ' || compCodeEdit == 0 || compCodeEdit == '-1') {
			alert("Select Company");
			$('#compCodeEdit').focus();
			return false;
		}else if (hsnSacEdit == ' ' || hsnSacEdit == 0 || hsnSacEdit == '-1') {
			alert("Select HSN/SAC");
			$('#hsnSacEdit').focus();
			return false;
		} else if (chapterIdEdit == ' ' || chapterIdEdit == 0) {
			alert("Enter Chapter Id");
			$('#chapterIdEdit').focus();
			return false;
		} else if (categoriesEdit == '' || categoriesEdit == null) {
			alert("Enter Category ");
			$('#categoriesEdit').focus();
			return false;
		}/* else if (subCategoryEdit == '' || subCategoryEdit == null) {
			alert("Enter Sub Category");
			$('#subCategoryEdit').focus();
			return false;
		}*/else if (subSubCategoryEdit == '' || subSubCategoryEdit == null) {
			alert("Enter Sub Sub Category");
			$('#subSubCategoryEdit').focus();
			return false;
		} else if (remarksEdit == '' || remarksEdit == null) {
			alert("Enter Remarks");
			$('#remarksEdit').focus();
			return false;
		}else if (chapterIdCapEdit == '' || chapterIdCapEdit == null) {
			alert("Enter Chapter Id Cap");
			$('#chapterIdCapEdit').focus();
			return false;
		}else {
			var result = $.ajax({
				type : "POST",
				url : "updateHsnSac?compCodeEdit=" + compCodeEdit + "&hsnSacEdit=" + hsnSacEdit
					+ "&chapterIdEdit=" + chapterIdEdit +"&categoriesEdit="+categoriesEdit+"&subCategoryEdit="+subCategoryEdit
					+"&subSubCategoryEdit=" + subSubCategoryEdit+"&remarksEdit="+remarksEdit+"&chapterIdCapEdit="+chapterIdCapEdit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearUpdateHsnSac();
				location.reload();
				
			} else if (result == "A") {
				alert("Payterm Already Exists");
			} else {
				alert("failed");
			}
		}
	});	
	
	
	function clearUpdateHsnSac(){
		
		 $('#compCodeEdit').val('-1');
		 $('#hsnSacEdit').val('-1');
		 $('#chapterIdEdit').val('');
		 $('#categoriesEdit').val('');
		 $('#subCategoryEdit').val('');
		 $("#subSubCategoryEdit").val('');
		 $('#remarksEdit').val('');
		 $('#chapterIdCapEdit').val('');
	}
	
	
	
	$('#chapterIdEdit').on('blur', function(event) {
		var chapId = $('#chapterIdEdit').val();
		$("#chapterIdCapEdit").val(chapId);
	});

	$('#categoriesEdit').on(
			'blur',
			function(event) {
				var id = $("#chapterIdEdit").val();
				var categ = $('#categoriesEdit').val();
				/*var subCateg = $('#subCategories').val();
				var subSubCateg = $('#subSubCategories').val();*/
				
				$("#chapterIdCapEdit").val('');
				$("#chapterIdCapEdit").val(id + "." + categ);

			});

	$('#subCategoryEdit').on(
			'blur',
			function(event) {
				$("#chapterIdCapEdit").val('');
				var id = $("#chapterIdEdit").val();
				var categ = $('#categoriesEdit').val();
				var subCateg = $('#subCategoryEdit').val();
				//var subSubCateg = $('#subSubCategories').val();
				$("#chapterIdCapEdit").val(id + "." + categ + "." + subCateg);
			});

	$('#subSubCategoryEdit').on(
			'blur',
			function(event) {
				$("#chapterIdCapEdit").val('');
				var id = $("#chapterIdEdit").val();
				var categ = $('#categoriesEdit').val();
				var subCateg = $('#subCategoryEdit').val();
				var subSubCateg = $('#subSubCategoryEdit').val();

				$("#chapterIdCapEdit").val(
						id + "." + categ + "." + subCateg + "."
								+ subSubCateg);
			});
					
				});