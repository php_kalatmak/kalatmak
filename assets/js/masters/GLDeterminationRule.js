$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-glDetermination').addClass('active');
	$('#menu-glDetermination > a').attr('aria-expanded', 'true');
	$('#menu-glDetermination > ul').addClass('in');

	$('#menu-glDeterRule').addClass('active');
	$('#GLRule').hide();

	$('#GLRuleAdd').click(function() {
		$('#GLRule').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	$('#branchInventory').prop('disabled', true);
	$('#itemGrpInventory').prop('disabled', true);

	$("#branchORItemGrpInventory").change(function() {
		var branchORItemGrpInventory = $("#branchORItemGrpInventory").val();
		if (branchORItemGrpInventory == 'B') {
			$('#branchInventory').prop('disabled', false);
			$('#itemGrpInventory').prop('disabled', true);
			$('#itemGrpInventory').val('-1');
		} else {
			$('#itemGrpInventory').prop('disabled', false);
			$('#branchInventory').prop('disabled', true);
			$('#branchInventory').val('-1');
		}
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('#salesDisplayTable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getGLDeterRuleListSales",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "branch"
					},
					{
						"data" : "ordType"
					},
					{
						"data" : "procType"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData1 = table.row(this).data();

		$('#salesTableEdit').puidatatable({
			editMode : 'cell',
			columns : [
				{
					field : 'keyCode',
					headerText : 'Key Code'
				},
				{
					field : 'keyName',
					headerText : 'Key Name'
				},
				{
					field : 'condType',
					headerText : 'Condition',
					editor : 'select'
				},
				{
					field : 'accCode',
					headerText : 'Account Code',
					editor : 'select1'
				}
			],
			datasource : function(callback) {
				$.ajax({
					type : "GET",
					url : "getKeyCodeForSalesEdit?company=" + tableData1.compCode + "&branch=" + tableData1.branchCode +
						"&ordTypeCode=" + tableData1.ordTypeCode + "&procTypeCode=" + tableData1.procTypeCode,
					dataType : "json",
					context : this,
					success : function(response) {
						callback.call(this, response);
					}
				});
			},
			cellEdit : function(event, ui) {
				$('#messages').puigrowl('show', [ {
					severity : 'info',
					summary : 'Cell Edit',
					detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
				} ]);
			},
			selectionMode : 'single'
		});
		$('#salesUpdate').click(function() {
			var salesTable = $("#salesTableEdit").puidatatable();
			var salesTable1 = salesTable.data();
			var salesTable2 = salesTable1["primeui-puidatatable"];
			var salesTable3 = salesTable2["data"];
			var salesTableJSON = JSON.stringify(salesTable3);

			var data = '{"header":[{"company":"' + tableData1.compCode + '","branch":"' + tableData1.branchCode +
				'","ordType":"' + tableData1.ordTypeCode + '","procedure":"' + tableData1.procTypeCode + '" }]';
			data += ',"salesTableEdit":' + salesTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "updateGLSales",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		});

	});
	var table1;
	(function($) {
		'use strict';
		$(function() {
			table1 = $('#purchaseDisplayTable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				"autoWidth" : true,
				ajax : {
					type : "POST",
					url : "getGLDeterRuleListPurchase",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "branch"
					},
					{
						"data" : "ordType"
					},
					{
						"data" : "procType"
					},
					{
						"data" : "action",
						render : actionFormatter1
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData2 = table1.row(this).data();

		$('#purchaseTableEdit').puidatatable({
			editMode : 'cell',
			columns : [
				{
					field : 'keyCode',
					headerText : 'Key Code'
				},
				{
					field : 'keyName',
					headerText : 'Key Name'
				},
				{
					field : 'condType',
					headerText : 'Condition',
					editor : 'select'
				},
				{
					field : 'accCode',
					headerText : 'Account Code',
					editor : 'select1'
				}
			],
			datasource : function(callback) {
				$.ajax({
					type : "GET",
					url : "getKeyCodeForPurchaseEdit?company=" + tableData2.compCode + "&branch=" + tableData2.branchCode +
						"&ordTypeCode=" + tableData2.ordTypeCode + "&procTypeCode=" + tableData2.procTypeCode,
					dataType : "json",
					context : this,
					success : function(response) {
						callback.call(this, response);
					}
				});
			},
			cellEdit : function(event, ui) {
				$('#messages').puigrowl('show', [ {
					severity : 'info',
					summary : 'Cell Edit',
					detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
				} ]);
			},
			selectionMode : 'single'
		});

		$('#purchaseUpdate').click(function() {
			var purchaseTable = $("#purchaseTableEdit").puidatatable();
			var purchaseTable1 = purchaseTable.data();
			var purchaseTable2 = purchaseTable1["primeui-puidatatable"];
			var purchaseTable3 = purchaseTable2["data"];
			var purchaseTableJSON = JSON.stringify(purchaseTable3);

			var data = '{"header":[{"company":"' + tableData2.compCode + '","branch":"' + tableData2.branchCode +
				'","ordType":"' + tableData2.ordTypeCode + '","procedure":"' + tableData2.procTypeCode + '" }]';
			data += ',"purchaseTableEdit":' + purchaseTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "updateGLPurchase",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		});
	});

	var table2;
	(function($) {
		'use strict';
		$(function() {
			table2 = $('#generalDisplayTable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				"autoWidth" : true,
				ajax : {
					type : "POST",
					url : "getGLDeterRuleListGeneral",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "branch"
					},
					{
						"data" : "action",
						render : actionFormatter2
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData3 = table2.row(this).data();

		$('#generalTableEdit').puidatatable({
			editMode : 'cell',
			columns : [
				{
					field : 'keyCode',
					headerText : 'Key Code'
				},
				{
					field : 'keyName',
					headerText : 'Key Name'
				},
				{
					field : 'accCode',
					headerText : 'Account Code',
					editor : 'select1'
				}
			],
			datasource : function(callback) {
				$.ajax({
					type : "GET",
					url : "getKeyCodeForGeneralEdit?company=" + tableData3.compCode + "&branch=" + tableData3.branchCode,
					dataType : "json",
					context : this,
					success : function(response) {
						callback.call(this, response);
					}
				});
			},
			cellEdit : function(event, ui) {
				$('#messages').puigrowl('show', [ {
					severity : 'info',
					summary : 'Cell Edit',
					detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
				} ]);
			},
			selectionMode : 'single'
		});


		$('#generalUpdate').click(function() {
			var generalTable = $("#generalTableEdit").puidatatable();
			var generalTable1 = generalTable.data();
			var generalTable2 = generalTable1["primeui-puidatatable"];
			var generalTable3 = generalTable2["data"];
			var generalTableJSON = JSON.stringify(generalTable3);

			var data = '{"header":[{"company":"' + tableData3.compCode + '","branch":"' + tableData3.branchCode + '" }]';
			data += ',"generalTableEdit":' + generalTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "updateGLGeneral",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		});


	});


	var table3;
	(function($) {
		'use strict';
		$(function() {
			table3 = $('#resourceDisplayTable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				"autoWidth" : true,
				ajax : {
					type : "POST",
					url : "getGLDeterRuleListResource",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "branch"
					},
					{
						"data" : "action",
						render : actionFormatter3
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData5 = table3.row(this).data();

		$('#resourceTableEdit').puidatatable({
			editMode : 'cell',
			columns : [
				{
					field : 'keyCode',
					headerText : 'Key Code'
				},
				{
					field : 'keyName',
					headerText : 'Key Name'
				},
				{
					field : 'accCode',
					headerText : 'Account Code',
					editor : 'select1'
				}
			],
			datasource : function(callback) {
				$.ajax({
					type : "GET",
					url : "getKeyCodeForResourceEdit?company=" + tableData5.compCode + "&branch=" + tableData5.branchCode,
					dataType : "json",
					context : this,
					success : function(response) {
						callback.call(this, response);
					}
				});
			},
			cellEdit : function(event, ui) {
				$('#messages').puigrowl('show', [ {
					severity : 'info',
					summary : 'Cell Edit',
					detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
				} ]);
			},
			selectionMode : 'single'
		});


		$('#resourceUpdate').click(function() {
			var resourceTable = $("#resourceTableEdit").puidatatable();
			var resourceTable1 = resourceTable.data();
			var resourceTable2 = resourceTable1["primeui-puidatatable"];
			var resourceTable3 = resourceTable2["data"];
			var resourceTableJSON = JSON.stringify(resourceTable3);

			var data = '{"header":[{"company":"' + tableData5.compCode + '","branch":"' + tableData5.branchCode + '" }]';
			data += ',"resourceTableEdit":' + resourceTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "updateGLResource",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		});
	});


	var table4;
	(function($) {
		'use strict';
		$(function() {
			table4 = $('#inventoryDisplayTable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				"autoWidth" : true,
				ajax : {
					type : "POST",
					url : "getGLDeterRuleListInventory",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "itemGrp"
					},
					{
						"data" : "action",
						render : actionFormatter4
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData4 = table4.row(this).data();

		$('#inventoryTableEdit').puidatatable({
			editMode : 'cell',
			columns : [
				{
					field : 'keyCode',
					headerText : 'Key Code'
				},
				{
					field : 'keyName',
					headerText : 'Key Name'
				},
				{
					field : 'accCode',
					headerText : 'Account Code',
					editor : 'select1'
				}
			],
			datasource : function(callback) {
				$.ajax({
					type : "GET",
					url : "getKeyCodeForInventoryEdit?company=" + tableData4.compCode + "&itemGrpCode=" + tableData4.itemGrpCode,
					dataType : "json",
					context : this,
					success : function(response) {
						callback.call(this, response);
					}
				});
			},
			cellEdit : function(event, ui) {
				$('#messages').puigrowl('show', [ {
					severity : 'info',
					summary : 'Cell Edit',
					detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
				} ]);
			},
			selectionMode : 'single'
		});

		$('#inventoryUpdate').click(function() {
			var inventoryTable = $("#inventoryTableEdit").puidatatable();
			var inventoryTable1 = inventoryTable.data();
			var inventoryTable2 = inventoryTable1["primeui-puidatatable"];
			var inventoryTable3 = inventoryTable2["data"];
			var inventoryTableJSON = JSON.stringify(inventoryTable3);

			var data = '{"header":[{"company":"' + tableData4.compCode + '","itemGrpCode":"' + tableData4.itemGrpCode + '" }]';
			data += ',"inventoryTableEdit":' + inventoryTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "updateGLInventory",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		});

	});

	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#companySales").get(0).options.length = 0;
			$("#companySales").get(0).options[0] = new Option("Select Company", "-1");
			$("#companyPurchase").get(0).options.length = 0;
			$("#companyPurchase").get(0).options[0] = new Option("Select Company", "-1");
			$("#companyGeneral").get(0).options.length = 0;
			$("#companyGeneral").get(0).options[0] = new Option("Select Company", "-1");
			$("#companyResource").get(0).options.length = 0;
			$("#companyResource").get(0).options[0] = new Option("Select Company", "-1");
			//			$("#companySalesEdit").get(0).options.length = 0;
			//			$("#companySalesEdit").get(0).options[0] = new Option("Select Company", "-1");
			//			$("#companyPurchaseEdit").get(0).options.length = 0;
			//			$("#companyPurchaseEdit").get(0).options[0] = new Option("Select Company", "-1");
			//			$("#companyGeneralEdit").get(0).options.length = 0;
			//			$("#companyGeneralEdit").get(0).options[0] = new Option("Select Company", "-1");
			//			$("#companyResourceEdit").get(0).options.length = 0;
			//			$("#companyResourceEdit").get(0).options[0] = new Option("Select Company", "-1");
			//			$("#companyInventoryEdit").get(0).options.length = 0;
			//			$("#companyInventoryEdit").get(0).options[0] = new Option("Select Company", "-1");
			$("#companyInventory").get(0).options.length = 0;
			$("#companyInventory").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, item) {
				//				$("#companySalesEdit").get(0).options[$("#companySalesEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				//				$("#companyInventoryEdit").get(0).options[$("#companyInventoryEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				//				$("#companyResourceEdit").get(0).options[$("#companyResourceEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				//				$("#companyPurchaseEdit").get(0).options[$("#companyPurchaseEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				//				$("#companyGeneralEdit").get(0).options[$("#companyGeneralEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#companyPurchase").get(0).options[$("#companyPurchase").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#companySales").get(0).options[$("#companySales").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#companyGeneral").get(0).options[$("#companyGeneral").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#companyResource").get(0).options[$("#companyResource").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#companyInventory").get(0).options[$("#companyInventory").get(0).options.length] = new Option(item.compName, item.compCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getBranchInGLRule",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#branchSales").get(0).options.length = 0;
			$("#branchSales").get(0).options[0] = new Option("Select Branch", "-1");
			$("#branchPurchase").get(0).options.length = 0;
			$("#branchPurchase").get(0).options[0] = new Option("Select Branch", "-1");
			$("#branchGeneral").get(0).options.length = 0;
			$("#branchGeneral").get(0).options[0] = new Option("Select Branch", "-1");
			$("#branchResource").get(0).options.length = 0;
			$("#branchResource").get(0).options[0] = new Option("Select Branch", "-1");
			//			$("#branchSalesEdit").get(0).options.length = 0;
			//			$("#branchSalesEdit").get(0).options[0] = new Option("Select Branch", "-1");
			//			$("#branchPurchaseEdit").get(0).options.length = 0;
			//			$("#branchPurchaseEdit").get(0).options[0] = new Option("Select Branch", "-1");
			//			$("#branchGeneralEdit").get(0).options.length = 0;
			//			$("#branchGeneralEdit").get(0).options[0] = new Option("Select Branch", "-1");
			//			$("#branchResourceEdit").get(0).options.length = 0;
			//			$("#branchResourceEdit").get(0).options[0] = new Option("Select Branch", "-1");
			$("#branchInventory").get(0).options.length = 0;
			$("#branchInventory").get(0).options[0] = new Option("Select Branch", "-1");
			$.each(msg, function(index, item) {
				//				$("#branchResourceEdit").get(0).options[$("#branchResourceEdit").get(0).options.length] = new Option(item.branchName, item.branchCode);
				//				$("#branchPurchaseEdit").get(0).options[$("#branchPurchaseEdit").get(0).options.length] = new Option(item.branchName, item.branchCode);
				//				$("#branchSalesEdit").get(0).options[$("#branchSalesEdit").get(0).options.length] = new Option(item.branchName, item.branchCode);
				//				$("#branchGeneralEdit").get(0).options[$("#branchGeneralEdit").get(0).options.length] = new Option(item.branchName, item.branchCode);
				$("#branchPurchase").get(0).options[$("#branchPurchase").get(0).options.length] = new Option(item.branchName, item.branchCode);
				$("#branchSales").get(0).options[$("#branchSales").get(0).options.length] = new Option(item.branchName, item.branchCode);
				$("#branchGeneral").get(0).options[$("#branchGeneral").get(0).options.length] = new Option(item.branchName, item.branchCode);
				$("#branchResource").get(0).options[$("#branchResource").get(0).options.length] = new Option(item.branchName, item.branchCode);
				$("#branchInventory").get(0).options[$("#branchInventory").get(0).options.length] = new Option(item.branchName, item.branchCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getOrderTypeInGLRule",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#ordTypeSales").get(0).options.length = 0;
			$("#ordTypeSales").get(0).options[0] = new Option("Select Order Type", "-1");
			//			$("#ordTypeSalesEdit").get(0).options.length = 0;
			//			$("#ordTypeSalesEdit").get(0).options[0] = new Option("Select Order Type", "-1");
			//			$("#ordTypePurchaseEdit").get(0).options.length = 0;
			//			$("#ordTypePurchaseEdit").get(0).options[0] = new Option("Select Order Type", "-1");
			$("#ordTypePurchase").get(0).options.length = 0;
			$("#ordTypePurchase").get(0).options[0] = new Option("Select Order Type", "-1");
			$.each(msg, function(index, item) {
				$("#ordTypeSales").get(0).options[$("#ordTypeSales").get(0).options.length] = new Option(item.ordTypeName, item.ordTypeCode);
				//				$("#ordTypePurchaseEdit").get(0).options[$("#ordTypePurchaseEdit").get(0).options.length] = new Option(item.ordTypeName, item.ordTypeCode);
				$("#ordTypePurchase").get(0).options[$("#ordTypePurchase").get(0).options.length] = new Option(item.ordTypeName, item.ordTypeCode);
			//				$("#ordTypeSalesEdit").get(0).options[$("#ordTypeSalesEdit").get(0).options.length] = new Option(item.ordTypeName, item.ordTypeCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getProcedureTypeInGLRule",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#procedureSales").get(0).options.length = 0;
			$("#procedureSales").get(0).options[0] = new Option("Select Procedure", "-1");
			//			$("#procedurePurchaseEdit").get(0).options.length = 0;
			//			$("#procedurePurchaseEdit").get(0).options[0] = new Option("Select Procedure", "-1");
			//			$("#procedureSalesEdit").get(0).options.length = 0;
			//			$("#procedureSalesEdit").get(0).options[0] = new Option("Select Procedure", "-1");
			$("#procedurePurchase").get(0).options.length = 0;
			$("#procedurePurchase").get(0).options[0] = new Option("Select Procedure", "-1");
			$.each(msg, function(index, item) {
				$("#procedureSales").get(0).options[$("#procedureSales").get(0).options.length] = new Option(item.procedureName, item.procedureCode);
				//				$("#procedureSalesEdit").get(0).options[$("#procedureSalesEdit").get(0).options.length] = new Option(item.procedureName, item.procedureCode);
				//				$("#procedurePurchaseEdit").get(0).options[$("#procedurePurchaseEdit").get(0).options.length] = new Option(item.procedureName, item.procedureCode);
				$("#procedurePurchase").get(0).options[$("#procedurePurchase").get(0).options.length] = new Option(item.procedureName, item.procedureCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getItemGroupInConditionMaster",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			//			$("#itemGrpInventoryEdit").get(0).options.length = 0;
			//			$("#itemGrpInventoryEdit").get(0).options[0] = new Option("Select Item Group", "-1");
			$("#itemGrpInventory").get(0).options.length = 0;
			$("#itemGrpInventory").get(0).options[0] = new Option("Select Item Group", "-1");
			$.each(msg, function(index, item) {
				//				$("#itemGrpInventoryEdit").get(0).options[$("#itemGrpInventoryEdit").get(0).options.length] = new Option(item.itemGrp, item.itemGrpCode);
				$("#itemGrpInventory").get(0).options[$("#itemGrpInventory").get(0).options.length] = new Option(item.itemGrp, item.itemGrpCode);
			});
		},
	});

	$('#salesTable').puidatatable({
		//		caption : 'List of Cars',
		editMode : 'cell',
		/*paginator : {
			rows : 10
		},*/
		columns : [
			{
				field : 'keyCode',
				headerText : 'Key Code'
			},
			{
				field : 'keyName',
				headerText : 'Key Name'
			},
			{
				field : 'condType',
				headerText : 'Condition',
				editor : 'select'
			},
			{
				field : 'accCode',
				headerText : 'Account Code',
				editor : 'select1'
			}
		],
		datasource : function(callback) {
			$.ajax({
				type : "GET",
				url : 'getKeyCodeForSales',
				dataType : "json",
				context : this,
				success : function(response) {
					callback.call(this, response);
				}
			});
		},
		cellEdit : function(event, ui) {
			$('#messages').puigrowl('show', [ {
				severity : 'info',
				summary : 'Cell Edit',
				detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
			} ]);
		},
		selectionMode : 'single'
	});
	$('#purchaseTable').puidatatable({
		//		caption : 'List of Cars',
		editMode : 'cell',
		/*paginator : {
			rows : 10
		},*/
		columns : [
			{
				field : 'keyCode',
				headerText : 'Key Code',
				hidden : true
			},
			{
				field : 'keyName',
				headerText : 'Key Name',
			},
			{
				field : 'condType',
				headerText : 'Condition',
				editor : 'select'
			},
			{
				field : 'accCode',
				headerText : 'Account Code',
				editor : 'select1'
			}
		],
		datasource : function(callback) {
			$.ajax({
				type : "GET",
				url : 'getKeyCodeForPurchase',
				dataType : "json",
				context : this,
				success : function(response) {
					callback.call(this, response);
				}
			});
		},
		cellEdit : function(event, ui) {
			$('#messages').puigrowl('show', [ {
				severity : 'info',
				summary : 'Cell Edit',
				detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
			} ]);
		},
		selectionMode : 'single'
	});

	$('#generalTable').puidatatable({
		//		caption : 'List of Cars',
		editMode : 'cell',
		/*paginator : {
			rows : 10
		},*/
		columns : [
			{
				field : 'keyCode',
				headerText : 'Key Code',
				hidden : true
			},
			{
				field : 'keyName',
				headerText : 'Key Name',
			},
			{
				field : 'accCode',
				headerText : 'Account Code',
				editor : 'select1'
			}
		],
		datasource : function(callback) {
			$.ajax({
				type : "GET",
				url : 'getKeyCodeForGeneral',
				dataType : "json",
				context : this,
				success : function(response) {
					callback.call(this, response);
				}
			});
		},
		cellEdit : function(event, ui) {
			$('#messages').puigrowl('show', [ {
				severity : 'info',
				summary : 'Cell Edit',
				detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
			} ]);
		},
		selectionMode : 'single'
	});

	$('#inventoryTable').puidatatable({
		//		caption : 'List of Cars',
		editMode : 'cell',
		/*paginator : {
			rows : 10
		},*/
		columns : [
			{
				field : 'keyCode',
				headerText : 'Key Code',
				hidden : true
			},
			{
				field : 'keyName',
				headerText : 'Key Name',
			},
			{
				field : 'accCode',
				headerText : 'Account Code',
				editor : 'select1'
			}
		],
		datasource : function(callback) {
			$.ajax({
				type : "GET",
				url : 'getKeyCodeForInventory',
				dataType : "json",
				context : this,
				success : function(response) {
					callback.call(this, response);
				}
			});
		},
		cellEdit : function(event, ui) {
			$('#messages').puigrowl('show', [ {
				severity : 'info',
				summary : 'Cell Edit',
				detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
			} ]);
		},
		selectionMode : 'single'
	});

	$('#resourceTable').puidatatable({
		//		caption : 'List of Cars',
		editMode : 'cell',
		/*paginator : {
			rows : 10
		},*/
		columns : [
			{
				field : 'keyCode',
				headerText : 'Key Code',
				hidden : true
			},
			{
				field : 'keyName',
				headerText : 'Key Name',
			},
			{
				field : 'accCode',
				headerText : 'Account Code',
				editor : 'select1'
			}
		],
		datasource : function(callback) {
			$.ajax({
				type : "GET",
				url : 'getKeyCodeForResource',
				dataType : "json",
				context : this,
				success : function(response) {
					callback.call(this, response);
				}
			});
		},
		cellEdit : function(event, ui) {
			$('#messages').puigrowl('show', [ {
				severity : 'info',
				summary : 'Cell Edit',
				detail : 'Old Value: ' + ui.oldValue + ', New Value: ' + ui.newValue + ' for ' + ui.field
			} ]);
		},
		selectionMode : 'single'
	});




	$('#saveGLRule').click(function() {


		var companySales = $('#companySales').val();
		var companyPurchase = $('#companyPurchase').val();
		var companyGeneral = $('#companyGeneral').val();
		var companyResource = $('#companyResource').val();
		var companyInventory = $('#companyInventory').val();

		var branchSales = $('#branchSales').val();
		var branchPurchase = $('#branchPurchase').val();
		var branchGeneral = $('#branchGeneral').val();
		var branchResource = $('#branchResource').val();
		var branchInventory = $('#branchInventory').val();

		var itemGrpInventory = $('#itemGrpInventory').val();

		var ordTypeSales = $('#ordTypeSales').val();
		var ordTypePurchase = $('#ordTypePurchase').val();

		var procedureSales = $('#procedureSales').val();
		var procedurePurchase = $('#procedurePurchase').val();

		var branchORItemGrpInventory = $('#branchORItemGrpInventory').val();

		var salesTable = $("#salesTable").puidatatable();
		var salesTable1 = salesTable.data();
		var salesTable2 = salesTable1["primeui-puidatatable"];
		var salesTable3 = salesTable2["data"];
		var salesTableJSON = JSON.stringify(salesTable3);
		var sales = encodeURIComponent(salesTableJSON);

		var purchaseTable = $("#purchaseTable").puidatatable();
		var purchaseTable1 = purchaseTable.data();
		var purchaseTable2 = purchaseTable1["primeui-puidatatable"];
		var purchaseTable3 = purchaseTable2["data"];
		var purchaseTableJSON = JSON.stringify(purchaseTable3);
		var purchase = encodeURIComponent(purchaseTableJSON);

		var generalTable = $("#generalTable").puidatatable();
		var generalTable1 = generalTable.data();
		var generalTable2 = generalTable1["primeui-puidatatable"];
		var generalTable3 = generalTable2["data"];
		var generalTableJSON = JSON.stringify(generalTable3);
		var general = encodeURIComponent(generalTableJSON);

		var inventoryTable = $("#inventoryTable").puidatatable();
		var inventoryTable1 = inventoryTable.data();
		var inventoryTable2 = inventoryTable1["primeui-puidatatable"];
		var inventoryTable3 = inventoryTable2["data"];
		var inventoryTableJSON = JSON.stringify(inventoryTable3);
		var inventory = encodeURIComponent(inventoryTableJSON);

		var resourceTable = $("#resourceTable").puidatatable();
		var resourceTable1 = resourceTable.data();
		var resourceTable2 = resourceTable1["primeui-puidatatable"];
		var resourceTable3 = resourceTable2["data"];
		var resourceTableJSON = JSON.stringify(resourceTable3);
		var resource = encodeURIComponent(resourceTableJSON);

		if (companySales == null || companySales == undefined || companySales == "" || companySales == "-1") {
			alert("Select Company In Sales");
			$('#companySales').focus();
			return false;
		} else if (branchSales == null || branchSales == undefined || branchSales == "" || branchSales == "-1") {
			alert("Select Branch In Sales");
			$('#branchSales').focus();
			return false;
		} else if (ordTypeSales == null || ordTypeSales == undefined || ordTypeSales == "" || ordTypeSales == "-1") {
			alert("Select Order Type In Sales");
			$('#ordTypeSales').focus();
			return false;
		} else if (procedureSales == null || procedureSales == undefined || procedureSales == "" || procedureSales == "-1") {
			alert("Select Procedure In Sales");
			$('#procedureSales').focus();
			return false;
		} else if (companyPurchase == null || companyPurchase == undefined || companyPurchase == "" || companyPurchase == "-1") {
			alert("Select Company In Purchase");
			$('#companyPurchase').focus();
			return false;
		} else if (branchPurchase == null || branchPurchase == undefined || branchPurchase == "" || branchPurchase == "-1") {
			alert("Select Branch In Purchase");
			$('#branchPurchase').focus();
			return false;
		} else if (ordTypePurchase == null || ordTypePurchase == undefined || ordTypePurchase == "" || ordTypePurchase == "-1") {
			alert("Select Order Type In Purchase");
			$('#ordTypePurchase').focus();
			return false;
		} else if (procedurePurchase == null || procedurePurchase == undefined || procedurePurchase == "" || procedurePurchase == "-1") {
			alert("Select Procedure In Purchase");
			$('#procedurePurchase').focus();
			return false;
		} else if (companyGeneral == null || companyGeneral == undefined || companyGeneral == "" || companyGeneral == "-1") {
			alert("Select Company In General");
			$('#companyGeneral').focus();
			return false;
		} else if (branchGeneral == null || branchGeneral == undefined || branchGeneral == "" || branchGeneral == "-1") {
			alert("Select Branch In General");
			$('#branchGeneral').focus();
			return false;
		} else if (companyResource == null || companyResource == undefined || companyResource == "" || companyResource == "-1") {
			alert("Select Company In Resource");
			$('#companyResource').focus();
			return false;
		} else if (branchResource == null || branchResource == undefined || branchResource == "" || branchResource == "-1") {
			alert("Select Branch In Resource");
			$('#branchResource').focus();
			return false;
		} else if (companyInventory == null || companyInventory == undefined || companyInventory == "" || companyInventory == "-1") {
			alert("Select Company In Inventory");
			$('#companyInventory').focus();
			return false;
		} else if (branchORItemGrpInventory == null || branchORItemGrpInventory == "-1" || branchORItemGrpInventory == undefined) {
			alert("Select Branch / Item Group");
			$("#branchORItemGrpInventory").focus();
			return false;
		} else if ((branchORItemGrpInventory == 'B' || branchORItemGrpInventory == "B") &&
			(branchInventory == null || branchInventory == "-1" || branchInventory == undefined)) {
			alert("Select Branch In Inventory");
			$("#branchInventory").focus();
			return false;
		} else if ((branchORItemGrpInventory == 'I' || branchORItemGrpInventory == "I") &&
			(itemGrpInventory == null || itemGrpInventory == "-1" || itemGrpInventory == undefined)) {
			alert("Select Item Group In Inventory");
			$("#itemGrpInventory").focus();
			return false;
		} else {
			var data = '{"header":[{"companySales":"' + companySales + '","branchSales":"' + branchSales +
				'","itemGrpInventory":"' + itemGrpInventory + '","ordTypeSales":"' + ordTypeSales +
				'","procedureSales":"' + procedureSales +
				'","companyPurchase":"' + companyPurchase + '","branchPurchase":"' + branchPurchase +
				'","ordTypePurchase":"' + ordTypePurchase + '","procedurePurchase":"' + procedurePurchase +
				'","companyGeneral":"' + companyGeneral + '","branchGeneral":"' + branchGeneral +
				'","companyResource":"' + companyResource + '","branchResource":"' + branchResource +
				'","companyInventory":"' + companyInventory + '","branchORItemGrpInventory":"' + branchORItemGrpInventory +
				'","branchInventory":"' + branchInventory +
				'" }]';
			data += ',"salesTable":' + salesTableJSON;
			data += ',"purchaseTable":' + purchaseTableJSON;
			data += ',"generalTable":' + generalTableJSON;
			data += ',"resourceTable":' + resourceTableJSON;
			data += ',"inventoryTable":' + inventoryTableJSON;
			data += '}';
			var result = $.ajax({
				type : "POST",
				url : "savGLDeterrRule",
				data : data,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");
			}
		}
	});
});