$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-exchangeRate').addClass('active');
	$('#ExchangeRate').hide();

	$('#exchangerateAdd').click(function() {
	$('#ExchangeRate').show();
	});
	
	$(".close").click(function() {
	$(".modal").hide();
	});

	$("#date").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});
	$("#dateEdit").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#comp").get(0).options.length = 0;
			$("#comp").get(0).options[0] = new Option("Select Company", "-1");
			$("#compEdit").get(0).options.length = 0;
			$("#compEdit").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, item) {
				$("#comp").get(0).options[$("#comp").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#compEdit").get(0).options[$("#compEdit").get(0).options.length] = new Option(item.compName, item.compCode);
			});
		},
	});
	var result = $.ajax({
		type : "POST",
		url : "getCurrencyListInGroupCompany",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#curr").get(0).options.length = 0;
			$("#curr").get(0).options[0] = new Option("Select Currency", "-1");
			$("#currEdit").get(0).options.length = 0;
			$("#currEdit").get(0).options[0] = new Option("Select Currency", "-1");
			$.each(msg, function(index, item) {
				$("#curr").get(0).options[$("#curr").get(0).options.length] = new Option(item.currDesc, item.currCode);
				$("#currEdit").get(0).options[$("#currEdit").get(0).options.length] = new Option(item.currDesc, item.currCode);
			});
		},
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getExchangeRateList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "date"
					},
					{
						"data" : "curr"
					},
					{
						"data" : "amt"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#compEdit').val(tableData.companyCode);
		$('#dateEdit').val(tableData.date);
		$('#currEdit').val(tableData.curr);
		$('#amtEdit').val(tableData.amt);
	});


	$('#exchSave').click(function() {

		var company = $('#comp').val();
		var date = $('#date').val();
		var curr = $('#curr').val();
		var amt = $('#amt').val();
		if (company == ' ' || company == 0 || company == '-1') {
			alert("Select Company");
			$('#comp').focus();
			return false;
		} else if (date == '' || date == null) {
			alert("Select Date");
			$('#date').focus();
			return false;
		} else if (curr == '' || curr == null || curr == '-1') {
			alert("Select Currency");
			$('#curr').focus();
			return false;
		} else if (amt == '' || amt == null) {
			alert("Enter Amount");
			$('#amt').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveExchangeRate?company=" + company + "&date=" + date + "&curr=" + curr + "&amt=" + amt,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Exchange Rate Exists on Selected Date");
			} else {
				alert("failed");
			}
		}
	});

	$('#exchUpdate').click(function() {

		var company = $('#compEdit').val();
		var date = $('#dateEdit').val();
		var curr = $('#currEdit').val();
		var amt = $('#amtEdit').val();
		if (company == ' ' || company == 0 || company == '-1') {
			alert("Select Company");
			$('#comp').focus();
			return false;
		} else if (date == '' || date == null) {
			alert("Select Date");
			$('#date').focus();
			return false;
		} else if (curr == '' || curr == null || curr == '-1') {
			alert("Select Currency");
			$('#curr').focus();
			return false;
		} else if (amt == '' || amt == null) {
			alert("Enter Amount");
			$('#amt').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateExchangeRate?company=" + company + "&date=" + date + "&curr=" + curr + "&amt=" + amt,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("");
			} else {
				alert("failed");
			}
		}
	});

	function clearData23() {
		$('#comp').val('-1');
		$('#date').val('');
		$('#curr').val('-1');
		$('#amt').val('');
		$('#compEdit').val('-1');
		$('#dateEdit').val('');
		$('#currEdit').val('-1');
		$('#amtEdit').val('');
	}
});