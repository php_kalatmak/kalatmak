var editor;
$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-general').addClass('active');
					$('#menu-general > a').attr('aria-expanded', 'true');
					$('#menu-general > ul').addClass('in');

					$('#menu-bank').addClass('active');

					$('#Bankadd').hide();
					$('#bankEdit').hide();

					/*
					 * $('#Bankedit').click(function(){ $('#BankAdd').hide();
					 * $('#bankUpdate').show(); });
					 */

					/*
					 * $('#backHome_Bank').click(function(){
					 * $('#BankAdd').show(); $('#bankUpdate').hide(); });
					 */

					var table;
					(function($) {
						'use strict';
						$(function() {
							table = $('.js-exportable')
									.DataTable(
											{
												responsive : true,
												"iDisplayLength" : 5,
												ajax : {
													type : "POST",
													url : "getBankTypeIdList",
													contentType : "application/json; charset=utf-8",
													dataType : "json",
													async : false,
												},
												"columns" : [ {
													"data" : "bankTbId"
												}, {
													"data" : "bankTbDesc"
												}, {
													"data" : "action",
													render : actionFormatter
												}, ]

											});
						});

					}(jQuery))

					$('#dtab').on("click", 'tr', function() {
						tableData = table.row(this).data();
						$('#bankCodeEdit').val(tableData.bankTbId);
						$('#bankDescNewEdit').val(tableData.bankTbDesc);
					});

					$('#bankAdd').click(function() {
						$('#Bankadd').show();
					});
					
					$(".close").click(function() {
						$(".modal").hide();
					});
					
					$('#bank_Reset').click(function() {
						$('#bankCode').val('');
						$('#bankName').val('');
					})

					$('#bankSave').click(
							function() {

								alert("Bank Save");
								var bankCode = $('#bankCode').val();
								var bankName = $('#bankName').val();

								alert("bankCode :" + bankCode + " bankName :"
										+ bankName);

								if (bankCode == ' ' || bankCode == 0) {
									alert("Enter Bank Code");
									$('#bankCode').focus();
									return false;
								} else if (bankName == '' || bankName == null) {
									alert("Enter Bank Name");
									$('#bankName').focus();
									return false;
								} else {
									var result = $.ajax({
										type : "POST",
										url : "saveBank?bankCode="
												+ $("#bankCode").val()
												+ "&bankName="
												+ $("#bankName").val(),
										dataType : "json",
										contentType : "application/json",
										processData : false,
										async : false
									}).responseText;
									if (result == "S") {
										alert("Saved Successfully");
										clearDataBank();
										location.reload();
									} else if (result == "A") {
										alert("System Corrupted");
									} else if (result == "Z") {
										alert("Already Bank Code Available");
									} else if (result == "X") {
										alert("Already Bank Name Available");
									} else {
										alert("failed");

									}
								}
							});

					function clearDataBank() {
						$('#bankCode').val('');
						$('#bankName').val('');
					}

					/*$('#bankEdit').click(function() {

										var result = $
												.ajax({
													type : "POST",
													url : "getBankIdList",
													contentType : "application/json; charset=utf-8",
													dataType : "json",
													async : false,
													success : function(msg) {
														$("#bankDescEdit").get(0).options.length = 0;
														$("#bankDescEdit").get(0).options[0] = new Option("Select Bank Type","-1");
                                                        $.each(msg,	function(index,item) {
                                                        $("#bankDescEdit").get(0).options[$("#bankDescEdit").get(0).options.length] = new Option(item.bankDesc,item.bankCode);
																		});
													},
												});

									})*/

					$('#bankEditUpdate').click(function() {
                            alert("Update");
								var bankCodeEdit = $('#bankCodeEdit').val();
								var bankDescNewEdit = $('#bankDescNewEdit').val();
								

								alert("bankCodeEdit :"+bankCodeEdit);
								alert("bankDescNewEdit :"+bankDescNewEdit);

								if (bankCodeEdit == null || bankCodeEdit == undefined || bankCodeEdit == "") {
									alert("Select UOM Type");
									$('#bankCodeEdit').focus();
									return false;
								}else if(bankDescNewEdit == '' || bankDescNewEdit == null){ 
									alert("Enter Description");
									 $('#bankDescNewEdit').focus(); 
									 return false;
									 }
									 else {
									var result = $.ajax({
										type : "POST",
										url : "updateBankType?bankCodeEdit="+ bankCodeEdit+ "&bankDescNewEdit="+ bankDescNewEdit,
										dataType : "json",
										contentType : "application/json",
										processData : false,
										async : false
									}).responseText;
									if (result == "S") {
										alert("Updated Successfully");
										clearDataUpdateBank();
										location.reload();
									} else if (result == "D") {
										alert("Already Exists")
									} else {
										alert("Failed");
									}
								}
							});

					function clearDataUpdateBank() {
						$('#bankCodeEdit').val('');
						$('#bankDescNewEdit').val('');
					}

				});