var editor;
$(document).ready(function () {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-storagelocationassignment').addClass('active');
	$('#StorageAss').hide();
	$('#storageAssEdit').hide();

	$('#storageAssAdd').click(function () {
		$('#StorageAss').show();
	});

	$('#storageAss_Reset').click(function () {
		$('#branchname').val('-1');
		$('#storagenameass').val('-1');
		$('#capacity').val('');
	});
	$('#storageAssEdit_Reset').click(function () {

		$('#statusAssEdit').val('-1');
		$('#capacityEdit').val('');
	});


	$(".close").click(function () {
		$(".modal").hide();
	});
	var table;
	(function ($) {
		'use strict';
		$(function () {
			table = $('.js-exportable').DataTable({
				responsive: true,
				"iDisplayLength": 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax: {
					type: "POST",
					url: "getStorageAssTypeIdList",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					async: false,
					//	success : function(data) {
					//		alert(data);

					//	},	
				},
				"columns": [
					{
						"data": "branchdesc"
					},
					{
						"data": "storagedesc"
					},
					{
						"data": "capacity"
					},

					{
						"data": "action",
						render: actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function () {
		tableData = table.row(this).data();
		$('#branchnameEdit').val(tableData.branchdesc);
		$('#storagenameEdit').val(tableData.storagedesc);
		$('#Capacity').val(tableData.capacity);
		$('#BranchIdhidden').val(tableData.branchId);
		$('#getStorageIdhidden').val(tableData.storageId);
		

	});


	$.ajax({
		type: "POST",
		url: "getBranchCode",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function (msg) {
			$("#branchname").get(0).options.length = 0;
			$("#branchname").get(0).options[0] = new Option("Select Branch", "-1");
			
			$.each(msg, function (index, item) {

				$("#branchname").get(0).options[$("#branchname").get(0).options.length] = new Option(item.branchName, item.branchcode);

			});
		},
	});

	$('#branchname').on('change', function (event) {
		$.ajax({
			type: "POST",
			url: "getStorageCodes",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			async: false,
			success: function (msg) {
				$("#storagenameass").get(0).options.length = 0;
				$("#storagenameass").get(0).options[0] = new Option("Select Storage", "-1");
			
				$.each(msg, function (index, item) {

					$("#storagenameass").get(0).options[$("#storagenameass").get(0).options.length] = new Option(item.StorageDesc, item.StorageId);


				});
			},
		});
	});

	$('#storageAssSave').click(function () {

		var Branchcode = $('#branchname').val();
		var Storagecode = $('#storagenameass').val();
		var Capacity = $('#capacity').val();

		if (Branchcode == null || Branchcode == undefined || Branchcode == "" || Branchcode == "-1") {
			alert("Select  Branch Name");
			$('#branchname').focus();
			return false;
		} else if (Storagecode == null || Storagecode == undefined || Storagecode == "" || Storagecode == "-1") {
			alert("Select  Storage Name");
			$('#storagenameass').focus();
			return false;
		} else if (Capacity == '' || Capacity == null) {
			alert("Enter capacity");
			$('#capacity').focus();
			return false;
		} else {
			var result = $.ajax({
				type: "POST",
				url: "saveStorageLocationAssignment?Branchcode="+Branchcode+"&Storagecode="+Storagecode+"&Capacity="+Capacity,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;

			if (result == "S") {
				alert("Saved Successfully");
				$('#branchname').val('-1');
				$('#storagenameass').val('-1');
				$('#capacity').val('');
				location.reload();
				$("#StorageAss").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Storage Code Exists");
			} else {
				alert("failed");

			}
		}
	});

	var result = $.ajax({
		type: "POST",
		url: "getStatusEdits",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function (msg) {
			$("#statusAssEdit").get(0).options.length = 0;
			$("#statusAssEdit").get(0).options[0] = new Option("Select Status", "-1");

			$.each(msg, function (index, item) {

				$("#statusAssEdit").get(0).options[$("#statusAssEdit").get(0).options.length] = new Option(item.statusDesc, item.statusId);


			});
		},
	});
	
	
	
	$('#storageAssUpdate').click(function () {
		var branchnameEdit = $('#branchnameEdit').val();
		var storagenameEdit = $('#storagenameEdit').val();
		var capacityEdit = $('#Capacity').val();
		var statusAssEdit = $('#statusAssEdit').val();
			var getStorageIdhidden = $('#getStorageIdhidden').val();
				var BranchIdhidden = $('#BranchIdhidden').val();
	
		 if (statusAssEdit == null || statusAssEdit == undefined || statusAssEdit == "" || statusAssEdit == "-1") {
			alert("Select  Status");
			$('#statusAssEdit').focus();
			return false;
				} else {
			var result = $.ajax({
				type: "POST",
				url: "UpdateStorageLocationAssignment?branchnameEdit=" + BranchIdhidden + "&storagenameEdit=" + getStorageIdhidden + "&capacityEdit=" + capacityEdit + "&statusAssEdit=" + statusAssEdit,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;


			if (result == "S") {
				alert("Updated Successfully");
				$('#branchnameEdit').val('');
				$('#storagenameEdit').val('');
				$('#Capacity').val('');
				$('#capacityEdit').val('');
				$('#statusAssEdit').val('-1');

				location.reload();
			} else if (result == "D") {
				alert("Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});