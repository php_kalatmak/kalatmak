$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionTableCreation').addClass('active');


	$('#conditionTableCreationAdd').click(function() {
		$('#ConditionTableCreation').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getTabList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "tab"
					},
					{
						"data" : "country"
					},
					{
						"data" : "state"
					},
					{
						"data" : "business"
					},
					{
						"data" : "company"
					},
					{
						"data" : "hsn"
					},
					{
						"data" : "item"
					},
					{
						"data" : "itemGrp"
					},
					{
						"data" : "uom"
					},
					{
						"data" : "amtper"
					},
					/*{
						"data" : "fromDate"
					},
					{
						"data" : "toDate"
					},*/
					{
						"data" : "status"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#tabEdit').val(tableData.tab);
		if (tableData.country == true) {
			$('input[name=countryEdit]').prop('checked', true);
		} else {
			$('input[name=countryEdit]').prop('checked', false);
		}
		if (tableData.state == true) {
			$('input[name=stateEdit]').prop('checked', true);
		} else {
			$('input[name=stateEdit]').prop('checked', false);
		}
		if (tableData.business == true) {
			$('input[name=businessEdit]').prop('checked', true);
		} else {
			$('input[name=businessEdit]').prop('checked', false);
		}
		if (tableData.company == true) {
			$('input[name=companyEdit]').prop('checked', true);
		} else {
			$('input[name=companyEdit]').prop('checked', false);
		}

		if (tableData.hsn == true) {
			$('input[name=hsnEdit]').prop('checked', true);
		} else {
			$('input[name=hsnEdit]').prop('checked', false);
		}

		if (tableData.item == true) {
			$('input[name=itemEdit]').prop('checked', true);
		} else {
			$('input[name=itemEdit]').prop('checked', false);
		}

		if (tableData.itemGrp == true) {
			$('input[name=itemGrpEdit]').prop('checked', true);
		} else {
			$('input[name=itemGrpEdit]').prop('checked', false);
		}
		if (tableData.uom == true) {
			$('input[name=uomEdit]').prop('checked', true);
		} else {
			$('input[name=uomEdit]').prop('checked', false);
		}

		if (tableData.amtper == true) {
			$('input[name=amtperEdit]').prop('checked', true);
		} else {
			$('input[name=amtperEdit]').prop('checked', false);
		}

		if (tableData.fromDate == true) {
			$('input[name=fromDateEdit]').prop('checked', true);
		} else {
			$('input[name=fromDateEdit]').prop('checked', false);
		}
		if (tableData.toDate == true) {
			$('input[name=toDateEdit]').prop('checked', true);
		} else {
			$('input[name=toDateEdit]').prop('checked', false);
		}

		if (tableData.status == true) {
			$('input[name=statusEdit]').prop('checked', true);
		} else {
			$('input[name=statusEdit]').prop('checked', false);
		}

	});

	$('#ConditionTableCreationSave').click(function() {

		var tab = $('#tab').val();
		var country = $("input[name='country']:checked").val();
		var state = $("input[name='state']:checked").val();
		var company = $("input[name='company']:checked").val();
		var hsn = $("input[name='hsn']:checked").val();
		var business = $("input[name='business']:checked").val();
		var item = $("input[name='item']:checked").val();
		var itemGrp = $("input[name='itemGrp']:checked").val();
		var uom = $("input[name='uom']:checked").val();
		var amtper = $("input[name='amtper']:checked").val();
		var fromDate = $("input[name='fromDate']:checked").val();
		var toDate = $("input[name='toDate']:checked").val();
		var status = $("input[name='status']:checked").val();


		if (tab == null || tab == "") {
			alert("Enter Tab Code");
			$('#tab').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveTabDetails?tab=" + tab + "&country=" + country + "&state=" + state + "&company=" + company + "&hsn=" + hsn +
					"&business=" + business + "&item=" + item + "&itemGrp=" + itemGrp + "&uom=" + uom + "&amtper=" + amtper +
					"&fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;

			if (result == "S") {
				alert("Saved Successfully");
				clear();
				location.reload();
			} else if (result == "A") {
				alert("Tab Code Exists");
			} else {
				alert("failed");

			}
		}
	});



	$('#ConditionTableCreationUpdate').click(function() {

		var tab = $('#tabEdit').val();
		var country = $("input[name='countryEdit']:checked").val();
		var state = $("input[name='stateEdit']:checked").val();
		var company = $("input[name='companyEdit']:checked").val();
		var hsn = $("input[name='hsnEdit']:checked").val();
		var business = $("input[name='businessEdit']:checked").val();
		var item = $("input[name='itemEdit']:checked").val();
		var itemGrp = $("input[name='itemGrpEdit']:checked").val();
		var uom = $("input[name='uomEdit']:checked").val();
		var amtper = $("input[name='amtperEdit']:checked").val();
		var fromDate = $("input[name='fromDateEdit']:checked").val();
		var toDate = $("input[name='toDateEdit']:checked").val();
		var status = $("input[name='statusEdit']:checked").val();


		if (tab == null || tab == "") {
			alert("Enter Tab Code");
			$('#tab').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateTabDetails?tab=" + tab + "&country=" + country + "&state=" + state + "&company=" + company + "&hsn=" + hsn +
					"&business=" + business + "&item=" + item + "&itemGrp=" + itemGrp + "&uom=" + uom + "&amtper=" + amtper +
					"&fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;

			if (result == "S") {
				alert("Updated Successfully");
				clear();
				location.reload();
			} else {
				alert("failed");

			}
		}
	});



	function clear() {
		$('#tab').val('');
		$("input[name='country']:checked").prop('checked', false);
		$("input[name='state']:checked").prop('checked', false);
		$("input[name='company']:checked").prop('checked', false);
		$("input[name='hsn']:checked").prop('checked', false);
		$("input[name='business']:checked").prop('checked', false);
		$("input[name='item']:checked").prop('checked', false);
		$("input[name='itemGrp']:checked").prop('checked', false);
		$("input[name='uom']:checked").prop('checked', false);
		$("input[name='amtper']:checked").prop('checked', false);
		$("input[name='fromDate']:checked").prop('checked', false);
		$("input[name='toDate']:checked").prop('checked', false);
		$("input[name='status']:checked").prop('checked', false);


		$("input[name='countryEdit']:checked").prop('checked', false);
		$("input[name='stateEdit']:checked").prop('checked', false);
		$("input[name='companyEdit']:checked").prop('checked', false);
		$("input[name='hsnEdit']:checked").prop('checked', false);
		$("input[name='businessEdit']:checked").prop('checked', false);
		$("input[name='itemEdit']:checked").prop('checked', false);
		$("input[name='itemGrpEdit']:checked").prop('checked', false);
		$("input[name='uomEdit']:checked").prop('checked', false);
		$("input[name='amtperEdit']:checked").prop('checked', false);
		$("input[name='fromDateEdit']:checked").prop('checked', false);
		$("input[name='toDateEdit']:checked").prop('checked', false);
		$("input[name='statusEdit']:checked").prop('checked', false);
	}
});