$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-state').addClass('active');
	$('#State').hide();

	$('#stateAdd').click(function() {
		$('#State').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	var result = $.ajax({
		type : "POST",
		url : "getCountryTypeIdListInState",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#country").get(0).options.length = 0;
			$("#country").get(0).options[0] = new Option("Select Country", "-1");
			$("#countryEdit").get(0).options.length = 0;
			$("#countryEdit").get(0).options[0] = new Option("Select Country", "-1");
			$.each(msg, function(index, item) {
				$("#country").get(0).options[$("#country").get(0).options.length] = new Option(item.countryDesc, item.countryId);
				$("#countryEdit").get(0).options[$("#countryEdit").get(0).options.length] = new Option(item.countryDesc, item.countryId);
			});
		},
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getStateTypeIdList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "country"
					},
					{
						"data" : "stateId"
					},
					{
						"data" : "stateDesc"
					},
					{
						"data" : "gstCode"
					},
					{
						"data" : "union"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#countryEdit').val(tableData.country);
		$('#statecodeEdit').val(tableData.stateId);
		$('#statenameEdit').val(tableData.stateDesc);
		$('#gstCodeEdit').val(tableData.gstCode);
		$("#unionEdit").val(tableData.union);
	});



	$('#statesave').click(function() {

		var country = $('#country').val();
		var stateCode = $('#statecode').val();
		var stateName = $('#statename').val();
		var gstCode = $('#gstCode').val();
		var radioValue = $("input[name='load']:checked").val();
		if (country == ' ' || country == 0 || country == '-1') {
			alert("Select Country");
			$('#country').focus();
			return false;
		} else if (stateCode == ' ' || stateCode == 0) {
			alert("Enter State Code");
			$('#statecode').focus();
			return false;
		} else if (stateName == '' || stateName == null) {
			alert("Enter State Name");
			$('#statename').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveState?country=" + country + "&stateCode=" + $("#statecode").val()
					+ "&stateName=" + $("#statename").val() + "&gstCode=" + gstCode + "&radioValue=" + radioValue,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("State Code Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#stateupdate').click(function() {
		var country = $('#countryEdit').val();
		var stateCode = $('#statecodeEdit').val();
		var stateName = $('#statenameEdit').val();
		var gstCode = $('#gstCodeEdit').val();
		var unionEdit = $('#unionEdit').val();

		if (country == ' ' || country == 0 || country == '-1') {
			alert("Select Country");
			$('#countryEdit').focus();
			return false;
		} else if (stateCode == ' ' || stateCode == 0) {
			alert("Enter State Code");
			$('#statecodeEdit').focus();
			return false;
		} else if (stateName == '' || stateName == null) {
			alert("Enter State Name");
			$('#statenameEdit').focus();
			return false;
		} else if (gstCode == '' || gstCode == null) {
			alert("Enter GST Code");
			$('#gstCodeEdit').focus();
			return false;
		} else if (unionEdit == '' || unionEdit == null || unionEdit == '-1') {
			alert("Select Territory");
			$('#unionEdit').focus();
			return false;
		} else {

			var result = $.ajax({
				type : "POST",

				url : "updateState?country=" + country + "&stateCode=" + stateCode
					+ "&stateName=" + stateName + "&gstCode=" + gstCode + "&unionEdit=" + unionEdit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearDataupdatecountry();
				location.reload();
			} else if (result == "A") {
				alert("State Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});
	function clearDataupdatecountry() {
		$("#countryEdit").val('-1');
		$("#countryDescEdit").val('')

	}
	function clearData23() {
		$("#country").val('-1');
		$("#statecode").val('');
		$("#statename").val('');
		$("#gstCode").val('');
	}

	$('#state_Reset').click(function() {
		$("#country").val('-1');
		$("#statecode").val('');
		$("#statename").val('');
		$("#gstCode").val('');
	});


});