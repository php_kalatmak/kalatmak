$(document)
		.ready(
				function() {
					var grpCode;
					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-general').addClass('active');
					$('#menu-general > a').attr('aria-expanded', 'true');
					$('#menu-general > ul').addClass('in');

					$('#menu-grpListUom').addClass('active');

					$('#GrpListUomadd').hide();
					$('#grplistUomEdit').hide();
					$('#grplistUomEditQQ').hide();

					/*--------------------------------------------------*/
					var table;
					(function($) {
						'use strict';
						$(function() {
							table = $('.js-exportable')
									.DataTable(
											{
												responsive : true,
												"iDisplayLength" : 5,
												ajax : {
													type : "POST",
													url : "getGroupListUom",
													contentType : "application/json; charset=utf-8",
													dataType : "json",
													async : false,
												},
												"columns" : [ {
													"data" : "grpuomTbId"
												}, {
													"data" : "grpuomTbDesc"
												}, {
													"data" : "action",
													render : actionFormatter
												}, {
													"data" : "action1",
													render : actionFormatter2
												} ]

											});
						});

					}(jQuery))

					$('#dtab').on("click", 'tr', function() {
						tableData = table.row(this).data();
						$('#grpuomCodeEdit').val(tableData.grpuomTbId);
						grpCode = tableData.grpuomTbId;
						$('#grpuomDescNewEdit').val(tableData.grpuomTbDesc);
					});
					
					var table1;
					(function($) {
						'use strict';
						$(function() {
							//alert("Enter");
							table1 = $('.js-exportable dataTable example')
									.DataTable(
											{
												responsive : true,
												"iDisplayLength" : 5,
												ajax : {
													type : "POST",
													url : "getGroupDefnListUom",
													contentType : "application/json; charset=utf-8",
													dataType : "json",
													async : false,
												},
												"columns" : [ {
													"data" : "grpdefn_uomGrpId"
												}, {
													"data" : "grpdefn_altQty"
												}, {
													"data" : "grpdefn_altUom"
												}, {
													"data" : "grpdefn_baseQty"
												}, {
													"data" : "grpdefn_baseUom"
												} ]

											});
						});

					}(jQuery))
					/*
					 * $('#dtab').on('click', 'tr', function() { tableData =
					 * table.row(this).data();
					 * $('#GrpListUomaddExtension').show(); });
					 */

					$('#grpuomAdd').click(function() {
						$('#GrpListUomadd').show();
					});

					$('#grpuom_Reset').click(function() {
						// alert("Reset :::: ");
						$("#grpuomCode").val('');
						$("#grpuomName").val('');
					});

					$(".close").click(function() {
						$(".modal").hide();
					});

					$('#grpuomSave').click(
							function() {

								var grpuomCode = $('#grpuomCode').val();
								var grpuomName = $('#grpuomName').val();

								alert("grpuomCode :" + grpuomCode
										+ " grpuomName :" + grpuomName);

								if (grpuomCode == ' ' || grpuomCode == 0) {
									alert("Enter UOM Code");
									$('#uomCode').focus();
									return false;
								} else if (grpuomName == ''
										|| grpuomName == null) {
									alert("Enter UOM Name");
									$('#grpuomName').focus();
									return false;
								} else {
									var result = $.ajax({
										type : "POST",
										url : "saveGroupListUom?grpuomCode="
												+ $("#grpuomCode").val()
												+ "&grpuomName="
												+ $("#grpuomName").val(),
										dataType : "json",
										contentType : "application/json",
										processData : false,
										async : false
									}).responseText;
									if (result == "S") {
										alert("Saved Successfully");
										clearDataUOM();
										location.reload();
									} else if (result == "A") {
										alert("System Corrupted");
									} else if (result == "Z") {
										alert("Already Group Code Available");
									} else if (result == "X") {
										alert("Already Group Name Available");
									} else {
										alert("failed");

									}
								}
							});

					function clearDataUOM() {
						$('#grpuomCode').val('');
						$('#grpuomName').val('');
					}

					var result = $
							.ajax({
								type : "POST",
								url : "getStatusList",
								contentType : "applcation/json; charset=utf-8",
								dataType : "json",
								async : false,
								success : function(msg) {
									$("#grpuomStatusEdit").get(0).options.length = 0;
									$("#grpuomStatusEdit").get(0).options[0] = new Option(
											"Select Status", "-1");
									$
											.each(
													msg,
													function(index, item) {
														$("#grpuomStatusEdit")
																.get(0).options[$(
																"#grpuomStatusEdit")
																.get(0).options.length] = new Option(
																item.statusDesc,
																item.statusCode);
													});
								}
							});

					$('#grpuomEditUpdate')
							.click(
									function() {

										var grpuomCodeEdit = $(
												'#grpuomCodeEdit').val();
										var grpuomDescNewEdit = $(
												'#grpuomDescNewEdit').val();
										var grpuomStatusEdit = $(
												'#grpuomStatusEdit').val();

										if (grpuomCodeEdit == null
												|| grpuomCodeEdit == undefined
												|| grpuomCodeEdit == "") {
											alert("Select UOM Code");
											$('#grpuomCodeEdit').focus();
											return false;
										} else if (grpuomDescNewEdit == null
												|| grpuomDescNewEdit == undefined
												|| grpuomDescNewEdit == "") {
											alert("Enter UOM Desc");
											$('#grpuomDescNewEdit').focus();
											return false;
										} else if (grpuomStatusEdit == null
												|| grpuomStatusEdit == undefined
												|| grpuomStatusEdit == "") {
											alert("Enter UOM Status");
											$('#grpuomStatusEdit').focus();
											return false;
										} else {
											var result = $
													.ajax({
														type : "POST",
														url : "updateGroupUomType?grpuomCodeEdit="
																+ grpuomCodeEdit
																+ "&grpuomDescNewEdit="
																+ grpuomDescNewEdit
																+ "&grpuomStatusEdit="
																+ grpuomStatusEdit,
														dataType : "json",
														contentType : "application/json",
														processData : false,
														async : false
													}).responseText;
											if (result == "S") {
												alert("Updated Successfully");
												clearDataUpdateUOM();
												location.reload();
											} else if (result == "D") {
												alert("Already Exists")
											} else {
												alert("Failed");
											}
										}
									});

					function clearDataUpdateUOM() {
						// alert("Clear");
						$('#grpuomCodeEdit').val('');
						$('#grpuomDescNewEdit').val('');
						$('#grpuomStatusEdit').val('-1');
					}

					/*-------------------------------------------------------*/

					var result = $
							.ajax({
								type : "POST",
								url : "getGrpListUoM",
								contentType : "applcation/json; charset=utf-8",
								dataType : "json",
								async : false,
								success : function(msg) {
									$("#altUomGrpUom").get(0).options.length = 0;
									$("#altUomGrpUom").get(0).options[0] = new Option(
											"Select Grp List", "-1");
									$("#baseUomGrpUom").get(0).options.length = 0;
									$("#baseUomGrpUom").get(0).options[0] = new Option(
											"Select Grp List", "-1");
									$
											.each(
													msg,
													function(index, item) {
														$("#altUomGrpUom").get(
																0).options[$(
																"#altUomGrpUom")
																.get(0).options.length] = new Option(
																item.grpUomDesc,
																item.grpUomId);
														$("#baseUomGrpUom")
																.get(0).options[$(
																"#baseUomGrpUom")
																.get(0).options.length] = new Option(
																item.grpUomDesc,
																item.grpUomId);
													});
								},
							});

					$('#grpDefnuomEditUpdate')
							.click(
									function() {
										var uomGrpCode = grpCode;
										alert(uomGrpCode);
										var altQtyGrpUom = $('#altQtyGrpUom')
												.val();
										var altUomGrpUom = $('#altUomGrpUom')
												.val();
										var baseQtyGrpUom = $('#baseQtyGrpUom')
												.val();
										var baseUomGrpUom = $('#baseUomGrpUom')
												.val();
										if (altQtyGrpUom == ' '
												|| altQtyGrpUom == 0) {
											alert("Enter Alter Group Quantity");
											$('#altQtyGrpUom').focus();
											return false;
										} else if (altUomGrpUom == ''
												|| altUomGrpUom == null) {
											alert("Select Alter Group UoM");
											$('#altUomGrpUom').focus();
											return false;
										} else if (baseQtyGrpUom == ''
												|| baseQtyGrpUom == null) {
											alert("Select Alter Group UoM");
											$('#baseQtyGrpUom').focus();
											return false;
										} else if (baseUomGrpUom == ''
												|| baseUomGrpUom == null) {
											alert("Select Alter Group UoM");
											$('#baseUomGrpUom').focus();
											return false;
										} else {
											var result = $
													.ajax({
														type : "POST",
														url : "saveGroupDefnListUom?altQtyGrpUom="
																+ $(
																		"#altQtyGrpUom")
																		.val()
																+ "&altUomGrpUom="
																+ $(
																		"#altUomGrpUom")
																		.val()
																+ "&baseQtyGrpUom="
																+ $(
																		"#baseQtyGrpUom")
																		.val()
																+ "&baseUomGrpUom="
																+ $(
																		"#baseUomGrpUom")
																		.val()
																+ "&uomGrpCode="
																+ uomGrpCode,
														dataType : "json",
														contentType : "application/json",
														processData : false,
														async : false
													}).responseText;
											if (result == "S") {
												alert("Saved Successfully");
												clearDataUOM();
												location.reload();
											} else if (result == "A") {
												alert("System Corrupted");
											} else if (result == "Z") {
												alert("Already Group Code Available");
											} else if (result == "X") {
												alert("Already Group Name Available");
											} else {
												alert("failed");

											}
										}
									});

					/*-------------------------------------------------------*/
				});