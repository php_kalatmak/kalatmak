$(document).ready(function() {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-currency').addClass('active');
	$('#Currency').hide();
	$('#CurrencyEdit').hide();

	$('#currencyAdd').click(function() {
		$('#Currency').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});


	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getCurrencyList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "currCode"
					},
					{
						"data" : "currDesc"
					},
					{
						"data" : "interCode"
					},
					{
						"data" : "interDesc"
					},
					{
						"data" : "decimal"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#currencycodeEdit').val(tableData.currCode);
		$('#currencynameEdit').val(tableData.currDesc);
		$('#intercodeEdit').val(tableData.interCode);
		$('#internameEdit').val(tableData.interDesc);
		$("#decimalLimitEdit").val(tableData.decimal);
	});

	$('#currencySave').click(function() {

		var currencycode = $('#currencycode').val();
		var currencyname = $('#currencyname').val();
		var intercode = $('#intercode').val();
		var intername = $('#intername').val();
		var decimalLimit = $("#decimalLimit").val();
		if (currencycode == ' ' || currencycode == 0 || currencycode == '-1') {
			alert("Enter Currencycode");
			$('#currencycode').focus();
			return false;
		} else if (currencyname == ' ' || currencyname == 0) {
			alert("Enter Currency Name");
			$('#currencyname').focus();
			return false;
		} else if (intercode == '' || intercode == null) {
			alert("Enter International Code");
			$('#intercode').focus();
			return false;
		} else if (intername == '' || intername == null) {
			alert("Enter International Name");
			$('#intername').focus();
			return false;
		} else if (decimalLimit == '' || decimalLimit == null) {
			alert("Enter Decimal Limit");
			$('#decimalLimit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveCurrency?currencycode=" + currencycode + "&currencyname=" + currencyname
					+ "&intercode=" + intercode + "&intername=" + intername + "&decimalLimit=" + decimalLimit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Currency Code Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#currencyUpdate').click(function() {
		var currencycode = $('#currencycodeEdit').val();
		var currencyname = $('#currencynameEdit').val();
		var intercode = $('#intercodeEdit').val();
		var intername = $('#internameEdit').val();
		var decimalLimit = $("#decimalLimitEdit").val();
		if (currencycode == ' ' || currencycode == 0 || currencycode == '-1') {
			alert("Enter Currencycode");
			$('#currencycodeEdit').focus();
			return false;
		} else if (currencyname == ' ' || currencyname == 0) {
			alert("Enter Currency Name");
			$('#currencynameEdit').focus();
			return false;
		} else if (intercode == '' || intercode == null) {
			alert("Enter International Code");
			$('#intercodeEdit').focus();
			return false;
		} else if (intername == '' || intername == null) {
			alert("Enter International Name");
			$('#internameEdit').focus();
			return false;
		} else if (decimalLimit == '' || decimalLimit == null) {
			alert("Enter Decimal Limit");
			$('#decimalLimitEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateCurrency?currencycode=" + currencycode + "&currencyname=" + currencyname
					+ "&intercode=" + intercode + "&intername=" + intername + "&decimalLimit=" + decimalLimit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
				clearData23();
			} else if (result == "A") {
				alert("Currency Name Already Exists");
			} else {
				alert("failed");
			}
		}
	});

	function clearData23() {
		$('#currencycode').val('');
		$('#currencyname').val('');
		$('#intercode').val('');
		$('#intername').val('');
		$("#decimalLimit").val('');
		$('#currencycodeEdit').val('');
		$('#currencynameEdit').val('');
		$('#intercodeEdit').val('');
		$('#internameEdit').val('');
		$("#decimalLimitEdit").val('');
	}

});