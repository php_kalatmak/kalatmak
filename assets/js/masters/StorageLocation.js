var editor;
$(document).ready(function () {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-storage').addClass('active');
	$('#menu-storage > a').attr('aria-expanded', 'true');
	$('#menu-storage > ul').addClass('in');

	$('#menu-storageLocation').addClass('active');
	$('#Storage').hide();
	$('#storageEdit').hide();

	$('#storageAdd').click(function () {
		$('#Storage').show();
	});

	$('#storage_Reset').click(function () {
		$('#storagecode').val('');
		$('#storagename').val('');
	});
   $('#storageEdit_Reset').click(function () {
		$('#storagenameEdit').val('');
		$('#statusEdit').val('-1');
	});
	$(".close").click(function () {
		$(".modal").hide();
	});
	var table;
	(function ($) {
		'use strict';
		$(function () {
			table = $('.js-exportable').DataTable({
				responsive: true,
				"iDisplayLength": 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax: {
					type: "POST",
					url: "getStorageTypeIdList",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					async: false,
					//	success : function(data) {
					//		alert(data);

					//	},	
				},
				"columns": [
					{
						"data": "storageId"
					},
					{
						"data": "storageDesc"
					},
					{
						"data": "status"
					},

					{
						"data": "action",
						render: actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function () {
		tableData = table.row(this).data();
		$('#storagecodeEdit').val(tableData.storageId);
		$('#storagenameEdit').val(tableData.storageDesc);
		$('#statusEdit').val(tableData.statusId);
	});

	/*	$('#dtab').on("click", '#dtab tbody', function(e) {
			var parent = $(this).closest('#dtab').parents('tr').index();
			var parentIndex = $('tbody tr:nth-child(' + (parent) + ')').attr('index');
			var currentIndex = $(this).closest('tr').index();
			var data = sections[parentIndex][currentIndex];
			console.log(data);
		});
	*/ /*$('.js-exportable').on('click', 'a.fa fa-pencil', function(e) {
		e.preventDefault();
		alert(HI)
		editor.edit($(this).closest('tr'), {
			title : 'Edit record',
			buttons : 'Update'
		});
	});*/

	var result = $.ajax({
		type: "POST",
		url: "getStatusEdit",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		async: false,
		success: function (msg) {
			$("#statusEdit").get(0).options.length = 0;
			$("#statusEdit").get(0).options[0] = new Option("Select Status", "-1");

			$.each(msg, function (index, item) {

				$("#statusEdit").get(0).options[$("#statusEdit").get(0).options.length] = new Option(item.statusDesc, item.statusId);


			});
		},
	});


	$('#storageSave').click(function () {

		var StorageCode = $('#storagecode').val();
		var StorageName = $('#storagename').val();
		
        	if (StorageCode == null || StorageCode == undefined || StorageCode == "") {
			alert("Enter Storage Code");
			$('#storagecode').focus();
			return false;
		} else if (StorageName == null || StorageName == undefined || StorageName == "") {
			alert("Enter Storage Name");
			$('#storagename').focus();
			return false;
		} else {
			var result = $.ajax({
				type: "POST",
				url: "saveStorageLocation?StorageCode=" + StorageCode + "&StorageName=" + StorageName,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;



			if (result == "S") {
				alert("Saved Successfully");
				$('#storagecode').val('');
				$('#storagename').val('');
				location.reload();
				$("#Storage").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Storage Code Already Exists");
			} else {
				alert("failed");

			}
		}
	});
//var result = $.ajax({
		//type: "POST",
		//url: "getStorageCodeEdit",
		//contentType: "application/json; charset=utf-8",
		//dataType: "json",
	//	async: false,
		//success: function (msg) {
			

			//$.each(msg, function (index, item) {

			//	$("#storagecodeEdit").get(0).options[$("#storagecodeEdit").get(0).options.length] = new Option(item.StorageId, item.StorageId);


			//});
		///},
	//});

	$('#storageUpdate').click(function () {
		var storageCode = $('#storagecodeEdit').val();
		var storageName = $('#storagenameEdit').val();
		var Status = $('#statusEdit').val();

		if (storageCode == null || storageCode == undefined || storageCode == "" || storageCode == "-1") {
			alert("Select  StorageCode");
			$('#storagecodeEdit').focus();
			return false;
		} else if (storageName == '' || storageName == null) {
			alert("Enter Storage Name");
			$('#storagenameEdit').focus();
			return false;
		} else if (Status == null || Status == undefined || Status == "" || Status == "-1") {
			alert("Select  Status");
			$('#statusEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type: "POST",
				url: "updatetorageLocation?storageCode=" + storageCode + "&storageName=" + storageName + "&Status=" + Status,
				dataType: "json",
				contentType: "application/json",
				processData: false,
				async: false
			}).responseText;


			if (result == "S") {
				alert("Updated Successfully");
				$('#storagecodeEdit').val('');
				$('#storagenameEdit').val('');
				location.reload();
			} else if (result == "D") {
				alert("Already Exists");
			} else {
				alert("failed");
			}
		}
	});
});