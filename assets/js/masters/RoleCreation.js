$(document).ready(function () {
    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-rights').addClass('active');
    $('#menu-rights > a').attr('aria-expanded', 'true');
    $('#menu-rights > ul').addClass('in');

    $('#menu-rolecreation').addClass('active');

    var compId = $("#comp_id_hidden").val();
    $("#RoleAdd").hide();

    $('#roleAdd').click(function() {
		$('#RoleAdd').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	 $('#role_Reset').click(function() {
		$("#companyId").val('-1');
		$("#roleId").val('');
		$("#rolename").val('');
	});   


	 $('#roleEdit_Reset').click(function() {	
		$("#rolenameEdit").val('');
	}); 
    
$.ajax({
		type : "POST",
		url : "getCompIdList",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {		
			$("#companyId").get(0).options.length = 0;
			$("#companyId").get(0).options[0] = new Option("Select Company", "-1");
			//$("#companyIdEdit").get(0).options.length = 0;
			//$("#companyIdEdit").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, data) {		 		
				$("#companyId").get(0).options[$("#companyId").get(0).options.length] = new Option(data.companyName, data.companyCode);
				//$('#companyIdEdit').get(0).options[$('#companyIdEdit').get(0).options.length] = new Option(data.companyName, data.companyCode);
			});
		},
    });
    

    $("#companyId").on('change',function(){
      $.ajax({
		type : "POST",
		url : "getRoleIdCount?compId=" + $("#companyId").val(),
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {		
        
            $("#roleId").val(msg[0].count);
		},
	});


    });


    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 5,
                ajax: {
                    type: "POST",
                    url: "getRoleIdList?compId=" + compId,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                },
                "columns": [
                     {
                        "data": "companyName"
					},					
                    {
                        "data": "roleId"
                    },
                    {
                        "data": "roleName"
                    },
                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))
    $('#dtab').on("click", 'tr', function () {
        tableData = table.row(this).data();
		$('#companyIdEdit').val(tableData.companyName);
		$('#companyEdit').val(tableData.companyID);
        $('#roleIdEdit').val(tableData.roleId);
        $('#rolenameEdit').val(tableData.roleName);   
    });


$('#roleSave').click(function() {

		var companyCode = $('#companyId').val();
		var roleId = $('#roleId').val();
		var roleName = $('#rolename').val();
	
		if (companyCode == ' ' || companyCode == 0 || companyCode == '-1') {
			alert("Select Company");
			$('#companyId').focus();
			return false;
		} else if (roleId == ' ' || roleId == 0) {
			alert("Enter Role ID");
			$('#roleId').focus();
			return false;
		} else if (roleName == '' || roleName == null) {
			alert("Enter Role Name");
			$('#rolename').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveRoleCreation?companyCode=" + companyCode + "&roleId=" + roleId
					+ "&roleName=" + roleName,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData();
				location.reload();
			} else if (result == "A") {
				alert("Role Name Already Exists");
			} else {
				alert("failed");

			}
		}
	});



	$('#roleEditUpdate').click(function() {

		var companyEditCode = $('#companyEdit').val();
		var roleIdEdit = $('#roleIdEdit').val();
		var roleNameEdit = $('#rolenameEdit').val();
	
		if (companyEditCode == ' ' || companyEditCode == 0 || companyEditCode == '-1') {
			alert("Select Company");
			$('#companyIdEdit').focus();
			return false;
		} else if (roleIdEdit == ' ' || roleIdEdit == 0) {
			alert("Enter Role ID");
			$('#roleIdEdit').focus();
			return false;
		} else if (roleNameEdit == '' || roleNameEdit == null) {
			alert("Enter Role Name");
			$('#rolenameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateRoleCreation?companyEditCode=" + companyEditCode + "&roleIdEdit=" + roleIdEdit
					+ "&roleNameEdit=" + roleNameEdit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				//clearEditData();
				location.reload();
			} else if (result == "A") {
				alert("Role Name Already Exists");
			} else {
				alert("failed");

			}
		}
	});

  function clearData() {
		$("#companyId").val('-1');
		$("#roleId").val('');
		$("#rolename").val('');	
	}

	 


});