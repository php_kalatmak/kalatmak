$(document).ready(function(){
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-uom').addClass('active');

	$('#Divisionadd').hide();
	$('#divisionEdit').hide();

	$('#divisionAdd').click(function() {
		$('#Divisionadd').show();
	});
	
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	$('#divisionSave').click(
			function() {

				var divCode = $('#divCode').val();
				var divName = $('#divName').val();
				if (divCode == ' ' || divCode == 0) {
					alert("Enter Division Code");
					$('#divCode').focus();
					return false;
				} else if (divName == '' || divName == null) {
					alert("Enter Division Name");
					$('#divName').focus();
					return false;
				} else {
					var result = $.ajax({
						type : "POST",
						url : "saveDivision?divCode="
								+ $("#divCode").val()
								+ "&divName="
								+ $("#divName").val(),
						dataType : "json",
						contentType : "application/json",
						processData : false,
						async : false
					}).responseText;
					if (result == "S") {
						alert("Saved Successfully");
						clearDataDivision();
						location.reload();
					} else if (result == "A") {
						alert("System Corrupted");
					} else if (result == "Z") {
						alert("Already Division Code Available");
					} else if (result == "X") {
						alert("Already Division Name Available");
					} else {
						alert("failed");

					}
				}
			});
	
	function clearDataDivision(){
		 $('#divCode').val('');
		 $('#divName').val('');
	}
	
	$("division_Reset").click(function(){
		 $('#divCode').val('');
		 $('#divName').val('');
	});
	
	
	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable')
					.DataTable(
							{
								responsive : true,
								"iDisplayLength" : 5,
								ajax : {
									type : "POST",
									url : "getDivisionTblList",
									contentType : "application/json; charset=utf-8",
									dataType : "json",
									async : false,
								},
								"columns" : [ {
									"data" : "divTblId"
								}, {
									"data" : "divTblDesc"
								}, {
									"data" : "action",
									render : actionFormatter
								}, ]

							});
		});

	}(jQuery))
	
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#divCodeEdit').val(tableData.divTblId);
		$('#divDescNewEdit').val(tableData.divTblDesc);
	});
	
	$('#divisionUpdate').click(function() {

		var divCodeEdit = $('#divCodeEdit').val();
		var divDescNewEdit = $('#divDescNewEdit').val();
		

		if (divCodeEdit == null|| divCodeEdit == undefined|| divCodeEdit == "") {
			alert("Select Division Code");
			$('#uomCodeEdit').focus();
			return false;
		}else if (divDescNewEdit == null|| divDescNewEdit == undefined|| divDescNewEdit == "") {
			alert("Enter Division Desc");
			$('#uomDescNewEdit').focus();
			return false;
		}  else {
			var result = $
					.ajax({
						type : "POST",
						url : "updateDivision?divCodeEdit="
								+ divCodeEdit
								+ "&divDescNewEdit="
								+ divDescNewEdit,
						dataType : "json",
						contentType : "application/json",
						processData : false,
						async : false
					}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearDataUpdateDiv();
				location.reload();
			} else if (result == "D") {
				alert("Already Exists")
			} else {
				alert("Failed");
			}
		}
	});
	
	function clearDataUpdateDiv(){
		$('#divCodeEdit').val('');
		$('#divDescNewEdit').val('');
	}
	
	
});