$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-glDetermination').addClass('active');
	$('#menu-glDetermination > a').attr('aria-expanded', 'true');
	$('#menu-glDetermination > ul').addClass('in');

	$('#menu-salepurType').addClass('active');
	$('#salepurType').hide();

	$('#salepurTypeAdd').click(function() {
		$('#salepurType').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getSalespurchaseList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "salepurId"
					},
					{
						"data" : "salepurName"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#salepurIdEdit').val(tableData.salepurId);
		$('#salepurNameEdit').val(tableData.salepurName);
	});



	$('#salepurTypesave').click(function() {

		var salepurId = $('#salepurId').val();
		var salepurName = $('#salepurName').val();
		if (salepurId == ' ' || salepurId == 0 || salepurId == '-1') {
			alert("Enter Sales/Purchase Id");
			$('#salepurId').focus();
			return false;
		} else if (salepurName == ' ' || salepurName == 0) {
			alert("Enter Sales/Purchase Name");
			$('#salepurName').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "savesalespur?salepurId=" + salepurId + "&salepurName=" + salepurName,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Sales/Purchase ID Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#salepurTypeupdate').click(function() {

		var salepurId = $('#salepurIdEdit').val();
		var salepurName = $('#salepurNameEdit').val();
		if (salepurId == ' ' || salepurId == 0 || salepurId == '-1') {
			alert("Enter Sales/Purchase Id");
			$('#salepurIdEdit').focus();
			return false;
		} else if (salepurName == ' ' || salepurName == 0) {
			alert("Enter Sales/Purchase Name");
			$('#salepurNameEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updatesalespur?salepurId=" + salepurId + "&salepurName=" + salepurName,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Sales/Purchase ID Exists");
			} else {
				alert("failed");

			}
		}
	});
	function clearData23() {
		$("#salepurId").val('');
		$("#salepurName").val('');
		$("#salepurIdEdit").val('');
		$("#salepurNameEdit").val('');
	}
});