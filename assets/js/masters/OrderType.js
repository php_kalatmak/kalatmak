$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-glDetermination').addClass('active');
	$('#menu-glDetermination > a').attr('aria-expanded', 'true');
	$('#menu-glDetermination > ul').addClass('in');

	$('#menu-ordType').addClass('active');
	$('#orderType').hide();

	$('#orderTypeAdd').click(function() {
		$('#orderType').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	var result = $.ajax({
		type : "POST",
		url : "getSalesORPurchase",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#salepur").get(0).options.length = 0;
			$("#salepur").get(0).options[0] = new Option("Select Sales/Purchase", "-1");
			$("#salepurEdit").get(0).options.length = 0;
			$("#salepurEdit").get(0).options[0] = new Option("Select Sales/Purchase", "-1");
			$.each(msg, function(index, item) {
				$("#salepur").get(0).options[$("#salepur").get(0).options.length] = new Option(item.salepurDesc, item.salepurId);
				$("#salepurEdit").get(0).options[$("#salepurEdit").get(0).options.length] = new Option(item.salepurDesc, item.salepurId);
			});
		},
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getOrderTypeList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "ordId"
					},
					{
						"data" : "ordName"
					},
					{
						"data" : "salepur"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#ordIdEdit').val(tableData.ordId);
		$('#ordNameEdit').val(tableData.ordName);
		$('#salepurEdit').val(tableData.salepurCode);
	});



	$('#ordertypesave').click(function() {

		var ordId = $('#ordId').val();
		var ordName = $('#ordName').val();
		var salepur = $('#salepur').val();
		if (ordId == ' ' || ordId == 0 || ordId == '-1') {
			alert("Enter Order Type Id");
			$('#ordId').focus();
			return false;
		} else if (ordName == ' ' || ordName == 0) {
			alert("Enter Order Type Name");
			$('#ordName').focus();
			return false;
		} else if (salepur == '' || salepur == null || salepur == "-1") {
			alert("Salect Sales/ Purchase");
			$('#salepur').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveOrderType?ordId=" + ordId + "&ordName=" + ordName
					+ "&salepur=" + salepur,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Order Type ID Exists");
			} else {
				alert("failed");

			}
		}
	});

	$('#ordertypeupdate').click(function() {

		var ordId = $('#ordIdEdit').val();
		var ordName = $('#ordNameEdit').val();
		var salepur = $('#salepurEdit').val();
		if (ordId == ' ' || ordId == 0 || ordId == '-1') {
			alert("Enter Order Type Id");
			$('#ordId').focus();
			return false;
		} else if (ordName == ' ' || ordName == 0) {
			alert("Enter Order Type Name");
			$('#ordName').focus();
			return false;
		} else if (salepur == '' || salepur == null || salepur == "-1") {
			alert("Salect Sales/ Purchase");
			$('#salepur').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateOrderType?ordId=" + ordId + "&ordName=" + ordName
					+ "&salepur=" + salepur,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Order Type ID Exists");
			} else {
				alert("failed");

			}
		}
	});
	function clearData23() {
		$("#salepur").val('-1');
		$("#ordId").val('');
		$("#ordName").val('');
		$("#salepurEdit").val('-1');
		$("#ordIdEdit").val('');
		$("#ordNameEdit").val('');
	}
});