$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-sequenceMapping').addClass('active');

	var result = $.ajax({
		type : "POST",
		url : "getConditionListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#condition").get(0).options.length = 0;
			$("#condition").get(0).options[0] = new Option("Select Condition", "-1");
			$.each(msg, function(index, item) {
				$("#condition").get(0).options[$("#condition").get(0).options.length] = new Option(item.conditionName, item.conditionCode);
			});
		},
	});
	var result = $.ajax({
		type : "POST",
		url : "getTabsListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$.each(msg, function(index, item) {
				$("#tab_map").get(0).options[$("#tab_map").get(0).options.length] = new Option(item.tabCode, item.tabCode);
			});
			$('#tab_map').multiselect({
			});
		},
	});

	$('#condition').on('change', function() {
		$("#tab_map").val('');
		$("#tab_map").multiselect('deselectAll', true);
		var condition = $("#condition").val();

		var result = $.ajax({
			type : "POST",
			url : "getTabListinAssign?condition=" + condition,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#tab_map").multiselect('deselectAll', true);
				$("#tab_map").multiselect('select', msg);
			}
		});
	});

	$('#saveAssgn').click(function() {
		var condition = $('#condition').val();
		var tab = $('#tab_map').val();
		if (condition == null || condition == undefined || condition == "" || condition == "-1") {
			alert("Select Condition");
			$('#condition').focus();
			return false;
		} else if (tab == null || tab == undefined || tab == "" || tab == "-1") {
			alert("Select Tabs");
			$('#tab_map').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveSeqTabAssgn?condition=" + condition + "&tab=" + tab,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				location.reload();
			} else if (result == "D") {
				alert("Empty");
			} else {
				alert("failed");

			}
		}
	});
});