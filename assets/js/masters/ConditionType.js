$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionType').addClass('active');


	$('#conditionTypeAdd').click(function() {
		$('#ConditionType').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});



	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getConditionList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "condType"
					},
					{
						"data" : "condName"
					},
					{
						"data" : "condSign"
					},
					{
						"data" : "scaleBasic"
					},
					{
						"data" : "headerItem"
					},
					{
						"data" : "strtSlab"
					},
					{
						"data" : "amtper"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#condTypeEdit').val(tableData.condType);
		$('#condNameEdit').val(tableData.condName);
		$('#condSignEdit').val(tableData.condSignCode);
		$('#scaleBasicEdit').val(tableData.scaleBasicCode);
		$('#headerItemEdit').val(tableData.headerItemCode);
		$('#strtSlabEdit').val(tableData.strtSlabCode);
		$('#amtperEdit').val(tableData.amtperCode);
		$('#statusEdit').val(tableData.status);
	});


	var result = $.ajax({
		type : "POST",
		url : "getConditionSign",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#condSign").get(0).options.length = 0;
			$("#condSign").get(0).options[0] = new Option("Select Condition Sign", "-1");
			$("#condSignEdit").get(0).options.length = 0;
			$("#condSignEdit").get(0).options[0] = new Option("Select Condition Sign", "-1");
			$.each(msg, function(index, item) {
				$("#condSign").get(0).options[$("#condSign").get(0).options.length] = new Option(item.condSign, item.condSignCode);
				$("#condSignEdit").get(0).options[$("#condSignEdit").get(0).options.length] = new Option(item.condSign, item.condSignCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getScaleBasic",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#scaleBasic").get(0).options.length = 0;
			$("#scaleBasic").get(0).options[0] = new Option("Select Scale / Basic", "-1");
			$("#scaleBasicEdit").get(0).options.length = 0;
			$("#scaleBasicEdit").get(0).options[0] = new Option("Select Scale / Basic", "-1");
			$.each(msg, function(index, item) {
				$("#scaleBasic").get(0).options[$("#scaleBasic").get(0).options.length] = new Option(item.scaleBasic, item.scaleBasicCode);
				$("#scaleBasicEdit").get(0).options[$("#scaleBasicEdit").get(0).options.length] = new Option(item.scaleBasic, item.scaleBasicCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getHeaderItem",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#headerItem").get(0).options.length = 0;
			$("#headerItem").get(0).options[0] = new Option("Select Header / Item", "-1");
			$("#headerItemEdit").get(0).options.length = 0;
			$("#headerItemEdit").get(0).options[0] = new Option("Select Header / Item", "-1");
			$.each(msg, function(index, item) {
				$("#headerItem").get(0).options[$("#headerItem").get(0).options.length] = new Option(item.headerItem, item.headerItemCode);
				$("#headerItemEdit").get(0).options[$("#headerItemEdit").get(0).options.length] = new Option(item.headerItem, item.headerItemCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getStraightSlab",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#strtSlab").get(0).options.length = 0;
			$("#strtSlab").get(0).options[0] = new Option("Select Straight / Slab", "-1");
			$("#strtSlabEdit").get(0).options.length = 0;
			$("#strtSlabEdit").get(0).options[0] = new Option("Select Straight / Slab", "-1");
			$.each(msg, function(index, item) {
				$("#strtSlab").get(0).options[$("#strtSlab").get(0).options.length] = new Option(item.strtSlab, item.strtSlabCode);
				$("#strtSlabEdit").get(0).options[$("#strtSlabEdit").get(0).options.length] = new Option(item.strtSlab, item.strtSlabCode);
			});
		},
	});


	var result = $.ajax({
		type : "POST",
		url : "getAmtPer",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#amtper").get(0).options.length = 0;
			$("#amtper").get(0).options[0] = new Option("Select Amount / Percentage", "-1");
			$("#amtperEdit").get(0).options.length = 0;
			$("#amtperEdit").get(0).options[0] = new Option("Select Amount / Percentage", "-1");
			$.each(msg, function(index, item) {
				$("#amtper").get(0).options[$("#amtper").get(0).options.length] = new Option(item.amtper, item.amtperCode);
				$("#amtperEdit").get(0).options[$("#amtperEdit").get(0).options.length] = new Option(item.amtper, item.amtperCode);
			});
		},
	});


	$('#ConditionTypeSave').click(function() {

		var condType = $('#condType').val();
		var condName = $('#condName').val();
		var condSign = $('#condSign').val();
		var scaleBasic = $('#scaleBasic').val();
		var headerItem = $('#headerItem').val();
		var strtSlab = $('#strtSlab').val();
		var amtPer = $('#amtper').val();


		if (condType == ' ' || condType == 0 || condType == '-1') {
			alert("Enter Condition Type");
			$('#condType').focus();
			return false;
		} else if (condName == ' ' || condName == 0 || condName == '-1') {
			alert("Enter Condition Name");
			$('#impexp').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveConditionType?condType=" + condType + "&condName=" + condName
					+ "&condSign=" + condSign + "&scaleBasic=" + scaleBasic + "&headerItem=" + headerItem + "&strtSlab=" + strtSlab + "&amtPer=" + amtPer,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Condition Type Exists");
			} else {
				alert("failed");
			}
		}
	});


	$('#ConditionTypeUpdate').click(function() {

		var condType = $('#condTypeEdit').val();
		var condName = $('#condNameEdit').val();
		var condSign = $('#condSignEdit').val();
		var scaleBasic = $('#scaleBasicEdit').val();
		var headerItem = $('#headerItemEdit').val();
		var strtSlab = $('#strtSlabEdit').val();
		var amtPer = $('#amtperEdit').val();
		var status = $("#statusEdit").val();


		if (condType == ' ' || condType == 0 || condType == '-1') {
			alert("Enter Condition Type");
			$('#condType').focus();
			return false;
		} else if (condName == ' ' || condName == 0 || condName == '-1') {
			alert("Enter Condition Name");
			$('#impexp').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "ConditionTypeUpdate?condType=" + condType + "&condName=" + condName
					+ "&condSign=" + condSign + "&scaleBasic=" + scaleBasic + "&headerItem=" + headerItem + "&strtSlab=" + strtSlab + "&amtPer=" + amtPer + "&status=" + status,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else {
				alert("failed");
			}
		}
	});


	function clearData23() {
		$('#condType').val('');
		$('#condName').val('');
		$('#condSign').val('-1');
		$('#scaleBasic').val('-1');
		$('#headerItem').val('-1');
		$('#strtSlab').val('-1');
		$('#amtper').val('-1');

		$('#condTypeEdit').val('');
		$('#condNameEdit').val('');
		$('#condSignEdit').val('-1');
		$('#scaleBasicEdit').val('-1');
		$('#headerItemEdit').val('-1');
		$('#strtSlabEdit').val('-1');
		$('#amtperEdit').val('-1');
		$('#statusEdit').val('-1');
	}

});