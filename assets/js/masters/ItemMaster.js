$(document).ready(function(){
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-company').addClass('active');
	$('#menu-company > a').attr('aria-expanded', 'true');
	$('#menu-company > ul').addClass('in');

	$('#menu-itemMaster').addClass('active');
	//$('#Branch').hide();

	$('#itemMasterAdd').click(function() {
		$('#ItemMaster').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	$("#leadTimePlan").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});
	
	
	var table;
	(function ($) {
		'use strict';
		$(function () {
			table = $('.js-exportable').DataTable({
				responsive: true,
				"iDisplayLength": 5,
				
				ajax: {
					type: "POST",
					url: "getItemMasterTableList",
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					async: false,
						
				},
				"columns": [
					{
						"data": "itemCodeTbl"
					},
					{
						"data": "itemNameTbl"
					},

					{
						"data": "action",
						render: actionFormatter
					}

				],
			});
		});
	}(jQuery))
	
	
	var result = $.ajax({
		type : "POST",
		url : "getItemType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemTypeId").get(0).options.length = 0;
			$("#itemTypeId").get(0).options[0] = new Option("Select Item Type", "-1");
			$("#itemTypeIdEdit").get(0).options.length = 0;
			$("#itemTypeIdEdit").get(0).options[0] = new Option("Select Item Type", "-1");
			$.each(msg, function(index, item) {
				$("#itemTypeId").get(0).options[$("#itemTypeId").get(0).options.length] = new Option(item.itemtypeName, item.itemtypeCode);
				$("#itemTypeIdEdit").get(0).options[$("#itemTypeIdEdit").get(0).options.length] = new Option(item.itemtypeName, item.itemtypeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getGroupType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemGroupId").get(0).options.length = 0;
			$("#itemGroupId").get(0).options[0] = new Option("Select Item Type", "-1");
			$("#itemGroupIdEdit").get(0).options.length = 0;
			$("#itemGroupIdEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				$("#itemGroupId").get(0).options[$("#itemGroupId").get(0).options.length] = new Option(item.itemGrptypeName, item.itemGrptypeCode);
				$("#itemGroupIdEdit").get(0).options[$("#itemGroupIdEdit").get(0).options.length] = new Option(item.itemGrptypeName, item.itemGrptypeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getUomGroupType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#uomGroupCode").get(0).options.length = 0;
			$("#uomGroupCode").get(0).options[0] = new Option("Select Uom Group Type", "-1");
			$("#uomGroupCodeEdit").get(0).options.length = 0;
			$("#uomGroupCodeEdit").get(0).options[0] = new Option("Select Uom Group Type", "-1");
			$.each(msg, function(index, item) {
				$("#uomGroupCode").get(0).options[$("#uomGroupCode").get(0).options.length] = new Option(item.uomGrpName, item.uomGrpCode);
				$("#uomGroupCodeEdit").get(0).options[$("#uomGroupCodeEdit").get(0).options.length] = new Option(item.uomGrpName, item.uomGrpCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getValuationType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#valuationCode").get(0).options.length = 0;
			$("#valuationCode").get(0).options[0] = new Option("Select Valuation Type", "-1");
			$("#valuationCodeEdit").get(0).options.length = 0;
			$("#valuationCodeEdit").get(0).options[0] = new Option("Select Valuation Type", "-1");
			$.each(msg, function(index, item) {
				$("#valuationCode").get(0).options[$("#valuationCode").get(0).options.length] = new Option(item.valName, item.valCode);
				$("#valuationCodeEdit").get(0).options[$("#valuationCodeEdit").get(0).options.length] = new Option(item.valName, item.valCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getPaytermType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#paytermCode").get(0).options.length = 0;
			$("#paytermCode").get(0).options[0] = new Option("Select Payterm Type", "-1");
			$("#paytermCodeEdit").get(0).options.length = 0;
			$("#paytermCodeEdit").get(0).options[0] = new Option("Select Payterm Type", "-1");
			$.each(msg, function(index, item) {
				$("#paytermCode").get(0).options[$("#paytermCode").get(0).options.length] = new Option(item.paytermName, item.paytermCode);
				$("#paytermCodeEdit").get(0).options[$("#paytermCodeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getShippingType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#shippingCode").get(0).options.length = 0;
			$("#shippingCode").get(0).options[0] = new Option("Select Shipping Code", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				$("#shippingCode").get(0).options[$("#shippingCode").get(0).options.length] = new Option(item.shipName, item.shipCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getManItemTypeList",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#manItemType").get(0).options.length = 0;
			$("#manItemType").get(0).options[0] = new Option("Select Manage Item Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#manItemType").get(0).options[$("#manItemType").get(0).options.length] = new Option(item.mitName, item.mitCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getStatusEdits",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#statusId").get(0).options.length = 0;
			$("#statusId").get(0).options[0] = new Option("Select Status Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#statusId").get(0).options[$("#statusId").get(0).options.length] = new Option(item.statusDesc, item.statusId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getSalesUom",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#salesUomCode").get(0).options.length = 0;
			$("#salesUomCode").get(0).options[0] = new Option("Select Sale Uom Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#salesUomCode").get(0).options[$("#salesUomCode").get(0).options.length] = new Option(item.saleUomName, item.saleUomCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	
	var result = $.ajax({
		type : "POST",
		url : "getSalesUom",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#purUomCode").get(0).options.length = 0;
			$("#purUomCode").get(0).options[0] = new Option("Select Purchase Uom Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#purUomCode").get(0).options[$("#purUomCode").get(0).options.length] = new Option(item.saleUomName, item.saleUomCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getValMethod",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#valMethodcode").get(0).options.length = 0;
			$("#valMethodcode").get(0).options[0] = new Option("Select valuation Method Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#valMethodcode").get(0).options[$("#valMethodcode").get(0).options.length] = new Option(item.valMethName, item.valMethCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getPlanMethod",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#planMethCode").get(0).options.length = 0;
			$("#planMethCode").get(0).options[0] = new Option("Select Plan Method Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#planMethCode").get(0).options[$("#planMethCode").get(0).options.length] = new Option(item.planMethName, item.planMethCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getProcureMethodType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#procMethType").get(0).options.length = 0;
			$("#procMethType").get(0).options[0] = new Option("Select Procurement Method Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#procMethType").get(0).options[$("#procMethType").get(0).options.length] = new Option(item.procMethName, item.procMethCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getIssuemethodType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#issueMethodcode").get(0).options.length = 0;
			$("#issueMethodcode").get(0).options[0] = new Option("Select Issue Method Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#issueMethodcode").get(0).options[$("#issueMethodcode").get(0).options.length] = new Option(item.issueMethName, item.issueMethCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getBomType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#bomTypecode").get(0).options.length = 0;
			$("#bomTypecode").get(0).options[0] = new Option("Select BOM Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#bomTypecode").get(0).options[$("#bomTypecode").get(0).options.length] = new Option(item.bomTypeName, item.bomTypeCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getManCapbranch",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#manCapbranchCode").get(0).options.length = 0;
			$("#manCapbranchCode").get(0).options[0] = new Option("Select Branch Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#manCapbranchCode").get(0).options[$("#manCapbranchCode").get(0).options.length] = new Option(item.brTypeName, item.brTypeCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getStatusEdits",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#manCapstatus").get(0).options.length = 0;
			$("#manCapstatus").get(0).options[0] = new Option("Select Status Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#manCapstatus").get(0).options[$("#manCapstatus").get(0).options.length] = new Option(item.statusDesc, item.statusId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	
	var result = $.ajax({
		type : "POST",
		url : "getCompanyData",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemExcompCode").get(0).options.length = 0;
			$("#itemExcompCode").get(0).options[0] = new Option("Select Company", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemExcompCode").get(0).options[$("#itemExcompCode").get(0).options.length] = new Option(item.compTypeName, item.compTypeCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getManCapbranch",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemExbranchCode").get(0).options.length = 0;
			$("#itemExbranchCode").get(0).options[0] = new Option("Select Branch", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemExbranchCode").get(0).options[$("#itemExbranchCode").get(0).options.length] = new Option(item.brTypeName, item.brTypeCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getCostCenterType",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemExcostCenterId").get(0).options.length = 0;
			$("#itemExcostCenterId").get(0).options[0] = new Option("Select Cost Center", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemExcostCenterId").get(0).options[$("#itemExcostCenterId").get(0).options.length] = new Option(item.costCenterDesc, item.costCenterId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getStatusEdits",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemExstatus").get(0).options.length = 0;
			$("#itemExstatus").get(0).options[0] = new Option("Select Status Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemExstatus").get(0).options[$("#itemExstatus").get(0).options.length] = new Option(item.statusDesc, item.statusId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getdivisionCodebranch",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemExdivisionCode").get(0).options.length = 0;
			$("#itemExdivisionCode").get(0).options[0] = new Option("Select Status Type", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemExdivisionCode").get(0).options[$("#itemExdivisionCode").get(0).options.length] = new Option(item.diviDesc, item.diviCode);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getBpCode",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemVenBpCode").get(0).options.length = 0;
			$("#itemVenBpCode").get(0).options[0] = new Option("Select Bp Code", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#itemVenBpCode").get(0).options[$("#itemVenBpCode").get(0).options.length] = new Option(item.bpId, item.bpId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	
	var result = $.ajax({
		type : "POST",
		url : "getBpCode",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#purBpCode").get(0).options.length = 0;
			$("#purBpCode").get(0).options[0] = new Option("Select Bp Code", "-1");
			//$("#gstTypeEdit").get(0).options.length = 0;
			//$("#gstTypeEdit").get(0).options[0] = new Option("Select GST Type", "-1");
			$.each(msg, function(index, item) {
				//alert("item :"+item.data[0]);
				$("#purBpCode").get(0).options[$("#purBpCode").get(0).options.length] = new Option(item.bpId, item.bpId);
				//$("#gstTypeEdit").get(0).options[$("#gstTypeEdit").get(0).options.length] = new Option(item.typeName, item.typeCode);
			});
		},
	});
	
	

	
	$('#itemMasterSave').click(function() {
		
		alert("Item Master Save");

		//item Master
		var itemCode = $('#itemCode').val();
		var itemDesc = $('#itemDesc').val();
		var itemTypeId = $('#itemTypeId').val();
		var itemGroupId = $('#itemGroupId').val();
		var uomGroupCode = $("#uomGroupCode").val();
		var valuationCode = $('#valuationCode').val();
		var barCode = $('#barCode').val();
		var paytermCode = $('#paytermCode').val();
		var field1 = $('#field1').val();
		var field2 = $("#field2").val();
		var itemGrpImage = $('#itemGrpImage').val();
		
		//General sub
		var shippingCode = $('#shippingCode').val();
		var manItemType = $('#manItemType').val();
		var characterIdcap = $('#characterIdcap').val();
		var statusId = $("#statusId").val();
		
		//Manufacture capacity
		var manCapitemMfseq = $('#manCapitemMfseq').val();
		var manCapbranchCode = $('#manCapbranchCode').val();
		var manCapcapacity = $('#manCapcapacity').val();
		var manCapstatus = $('#manCapstatus').val();
		
		//Item Exend
		var itemExcompCode = $("#itemExcompCode").val();
		var itemExbranchCode = $('#itemExbranchCode').val();
		var itemExcostCenterId = $('#itemExcostCenterId').val();
		var itemExrolQty = $('#itemExrolQty').val();
		var itemExstatus = $("#itemExstatus").val();
		var isSlab = $("input[name='loadItem']:checked").val();///////////////////////
		var itemExdivisionCode = $("#itemExdivisionCode").val();
		
		//Item Vendor Mapping
		var itemVenBpCode = $('#itemVenBpCode').val();
		var itemAliasName = $('#itemAliasName').val();
		
		//Purchase
		var purBpCode = $('#purBpCode').val();
		var purUomCode = $("#purUomCode").val();
		var purMinPurchaseQty = $('#purMinPurchaseQty').val();
		var purPackingSze = $('#purPackingSze').val();
		var purLength = $('#purLength').val();
		var purWidth = $('#purWidth').val();
		var purHeight = $("#purHeight").val();
		var purVolume = $('#purVolume').val();
		var purWeight = $('#purWeight').val();
		var purField1 = $('#purField1').val();
		var purField2 = $('#purField2').val();
		var purParameter1 = $("#purParameter1").val();
		var purParameter2 = $('#purParameter2').val();
		var purParameter3 = $("#purParameter3").val();
		var purParameter4 = $('#purParameter4').val();
		
		//Sales
		var salesUomCode = $('#salesUomCode').val();
		var salesPcksze = $("#salesPcksze").val();
		var salesLnth = $('#salesLnth').val();
		var salesWidth = $('#salesWidth').val();
		var salesHght = $('#salesHght').val();
		var salesVolume = $('#salesVolume').val();
		var salesWeight = $("#salesWeight").val();
		var salesFld1 = $('#salesFld1').val();
		var salesFld2 = $('#salesFld2').val();
		var salesPrmtr1 = $('#salesPrmtr1').val();
		var salesPrmtr2 = $('#salesPrmtr2').val();
		var salesPrmtr3 = $("#salesPrmtr3").val();
		var salesPrmtr4 = $('#salesPrmtr4').val();
		
		//Inventory
		var valMethodcode = $("#valMethodcode").val();
		var invFld1 = $('#invFld1').val();
		var invFld2 = $('#invFld2').val();
		var invFld3 = $("#invFld3").val();
		
		//Planning
	    var planMethCode = $('#planMethCode').val();
		var procMethType = $('#procMethType').val();
		var orderInterval = $('#orderInterval').val();
		var minQty = $('#minQty').val();
		var leadTimePlan = $("#leadTimePlan").val();
		var tolDays = $('#tolDays').val();
		var planfield1 = $('#planfield1').val();
		var planfield2 = $('#planfield2').val();
		
		//Production
		var issueMethodcode = $('#issueMethodcode').val();
		var bomTypecode = $("#bomTypecode").val();
		
		//Remarks
		var itemRemarks = $('#itemRemarks').val();
		var remfield1 = $("#remfield1").val();
		var remfield2 = $('#remfield2').val();
		var remfield3 = $('#remfield3').val();
		var remfield4 = $("#remfield4").val();
		
		
		if (itemCode == null || itemCode == undefined || itemCode == '-1' || itemCode == "") {
			alert("Select Item Code");
			$('#itemCode').focus();
			return false;
		} else if (itemDesc == null || itemDesc == "") {
			alert("Enter Item Name");
			$('#itemDesc').focus();
			return false;
		} else if (itemTypeId == null || itemTypeId == undefined || itemTypeId == '-1' || itemTypeId == "") {
			alert("Select Item Type Id");
			$('#itemTypeId').focus();
			return false;
		} else if (itemGroupId == null || itemGroupId == '-1' || itemGroupId == undefined || itemGroupId == "" ) {
			alert("Select itemGroupId");
			$('#itemGroupId').focus();
			return false;
		} else if (uomGroupCode == null || uomGroupCode == '-1' ||  uomGroupCode == undefined || uomGroupCode == "") {
			alert("Select uomGroupCode");
			$('#uomGroupCode').focus();
			return false;
		}else if (valuationCode == null || valuationCode == '-1' ||  valuationCode == undefined || valuationCode == "") {
			alert("Select Valuation Code");
			$('#valuationCode').focus();
			return false;
		} else if (barCode == "" || barCode == null) {
			alert("Enter Bar Code");
			$('#barCode').focus();
			return false;
		} else if (paytermCode == null || paytermCode == '-1' ||  paytermCode == undefined || paytermCode == "") {
			alert("Select paytermCode");
			$('#paytermCode').focus();
			return false;
		} else if (field1 == '' || field1 == null) {
			alert("Enter Field 1");
			$('#field1').focus();
			return false;
		}else if (field2 == '' || field2 == null) {
			alert("Enter Field 2");
			$('#field2').focus();
			return false;
		} /*else if (itemGrpImage == '' || itemGrpImage == null) {
			alert("Select Item Image");
			$('#itemGrpImage').focus();
			return false;
		} */
		
		
		/*else if (shippingCode == null ||  shippingCode == '-1' ||  shippingCode == undefined || shippingCode == "") {
			alert("Select shippingCode");
			$('#shippingCode').focus();
			return false;
		} else if (manItemType == null ||  shippingCode == '-1' ||  shippingCode == undefined || shippingCode == "") {
			alert("Select manItemType");
			$('#manItemType').focus();
			return false;
		} else if (characterIdcap == ' ' || characterIdcap == 0) {
			alert("Enter characterIdcap");
			$('#characterIdcap').focus();
			return false;
		} else if (statusId == null ||  shippingCode == '-1' ||  shippingCode == undefined || shippingCode == "") {
			alert("Select status");
			$('#statusId').focus();
			return false;
		}
		
		
		else if (manCapitemMfseq == '' || manCapitemMfseq == null) {
			alert("Enter manCapitemMfseq");
			$('#manCapitemMfseq').focus();
			return false;
		} else if (manCapbranchCode == null || manCapbranchCode == '-1' ||  manCapbranchCode == undefined || manCapbranchCode == "" ) {
			alert("Enter manCapbranchCode");
			$('#manCapbranchCode').focus();
			return false;
		}else if (manCapcapacity == ' ' || manCapcapacity == 0) {
			alert("Enter manCapcapacity");
			$('#manCapcapacity').focus();
			return false;
		} else if (manCapstatus == null || manCapstatus == '-1' ||  manCapstatus == undefined || manCapstatus == "" ) {
			alert("Enter manCapstatus");
			$('#manCapstatus').focus();
			return false;
		}
		
		
		else if ( itemExcompCode == null || itemExcompCode == '-1' ||  itemExcompCode == undefined || itemExcompCode == "") {
			alert("Select item Extend Company Code");
			$('#itemExcompCode').focus();
			return false;
		} else if (itemExbranchCode == null || itemExbranchCode == '-1' ||  itemExbranchCode == undefined || itemExbranchCode == "") {
			alert("Select item Extend branch Code");
			$('#itemExbranchCode').focus();
			return false;
		}else if (itemExcostCenterId == null || itemExcostCenterId == '-1' ||  itemExcostCenterId == undefined || itemExcostCenterId == "") {
			alert("Select item Extend costCenterId");
			$('#itemExcostCenterId').focus();
			return false;
		} else if (itemExrolQty == '' || itemExrolQty == null) {
			alert("Enter item Extend rolQty");
			$('#itemExrolQty').focus();
			return false;
		} else if (itemExstatus == null || itemExstatus == '-1' ||  itemExstatus == undefined || itemExstatus == "") {
			alert("Enter item Extend status");
			$('#itemExstatus').focus();
			return false;
		} else if (itemExdivisionCode == null || itemExdivisionCode == '-1' ||  itemExdivisionCode == undefined || itemExdivisionCode == "") {
			alert("Enter item Extend divisionCode");
			$('#itemExdivisionCode').focus();
			return false;
		}
		
		else if (itemVenBpCode == null || itemVenBpCode == '-1' ||  itemVenBpCode == undefined || itemVenBpCode == "") {
			alert("Enter itemVenBpCode");
			$('#itemVenBpCode').focus();
			return false;
		} else if (itemAliasName == '' || itemAliasName == null) {
			alert("Enter itemAliasName");
			$('#itemAliasName').focus();
			return false;
		} 
		
		//purchase
		else if (purBpCode == null || purBpCode == '-1' ||  purBpCode == undefined || purBpCode == "") {
			alert("Select purBpCode");
			$('#purBpCode').focus();
			return false;
		} else if (purUomCode == null || purUomCode == '-1' ||  purUomCode == undefined || purUomCode == "") {
			alert("Select purUomCode");
			$('#purUomCode').focus();
			return false;
		}else if (purMinPurchaseQty == ' ' || purMinPurchaseQty == null) {
			alert("Enter purMinPurchaseQty");
			$('#purMinPurchaseQty').focus();
			return false;
		} else if (purPackingSze == '' || purPackingSze == null) {
			alert("Enter purPackingSze");
			$('#purPackingSze').focus();
			return false;
		} else if (purLength == '' || purLength == null) {
			alert("Enter purLength");
			$('#purLength').focus();
			return false;
		} else if (purWidth == '' || purWidth == null) {
			alert("Enter purWidth");
			$('#purWidth').focus();
			return false;
		}else if (purHeight == '' || purHeight == null) {
			alert("Enter purHeight");
			$('#purHeight').focus();
			return false;
		} else if (purVolume == '' || purVolume == null) {
			alert("Enter purVolume");
			$('#purVolume').focus();
			return false;
		}else if (purWeight == ' ' || purWeight == null) {
			alert("Enter purWeight");
			$('#purWeight').focus();
			return false;
		} else if (purField1 == '' || purField1 == null) {
			alert("Enter purField1");
			$('#purField1').focus();
			return false;
		} else if (purField2 == '' || purField2 == null) {
			alert("Enter purField2");
			$('#purField2').focus();
			return false;
		} else if (purParameter1 == '' || purParameter1 == null) {
			alert("Enter purParameter1");
			$('#purParameter1').focus();
			return false;
		} else if (purParameter2 == '' || purParameter2 == null) {
			alert("Enter purParameter2");
			$('#purParameter2').focus();
			return false;
		} else if (purParameter3 == '' || purParameter3 == null) {
			alert("Enter purParameter3");
			$('#purParameter3').focus();
			return false;
		} else if (purParameter4 == '' || purParameter4 == null) {
			alert("Enter purParameter4");
			$('#purParameter4').focus();
			return false;
		}
		
		//Sales
		else if (salesUomCode == null || salesUomCode == '-1' ||  salesUomCode == undefined || salesUomCode == "") {
			alert("Select salesUomCode");
			$('#salesUomCode').focus();
			return false;
		} else if (salesPcksze == '' || salesPcksze == null) {
			alert("Enter salesPcksze");
			$('#salesPcksze').focus();
			return false;
		} else if (salesLnth == '' || salesLnth == null) {
			alert("Enter salesLnth");
			$('#salesLnth').focus();
			return false;
		} else if (salesWidth == '' || salesWidth == null) {
			alert("Enter salesWidth");
			$('#salesWidth').focus();
			return false;
		}else if (salesHght == '' || salesHght == null) {
			alert("Enter salesHght");
			$('#salesHght').focus();
			return false;
		} else if (salesVolume == '' || salesVolume == null) {
			alert("Enter salesVolume");
			$('#salesVolume').focus();
			return false;
		}else if (salesWeight == ' ' || salesWeight == null) {
			alert("Enter salesWeight");
			$('#salesWeight').focus();
			return false;
		} else if (salesFld1 == '' || salesFld1 == null) {
			alert("Enter salesFld1");
			$('#salesFld1').focus();
			return false;
		} else if (salesFld2 == '' || salesFld2 == null) {
			alert("Enter salesFld2");
			$('#salesFld2').focus();
			return false;
		} else if (salesPrmtr1 == '' || salesPrmtr1 == null) {
			alert("Enter salesPrmtr1");
			$('#salesPrmtr1').focus();
			return false;
		} else if (salesPrmtr2 == '' || salesPrmtr2 == null) {
			alert("Enter salesPrmtr2");
			$('#salesPrmtr2').focus();
			return false;
		} else if (salesPrmtr3 == '' || salesPrmtr3 == null) {
			alert("Enter salesPrmtr3");
			$('#salesPrmtr3').focus();
			return false;
		} else if (salesPrmtr4 == '' || salesPrmtr4 == null) {
			alert("Enter salesPrmtr4");
			$('#salesPrmtr4').focus();
			return false;
		}
		
		//Inventory
		else if (valMethodcode == null || valMethodcode == '-1' ||  valMethodcode == undefined || valMethodcode == "") {
			alert("Enter valMethodcode");
			$('#valMethodcode').focus();
			return false;
		} else if (invFld1 == '' || invFld1 == null) {
			alert("Enter invFld1");
			$('#invFld1').focus();
			return false;
		} else if (invFld2 == '' || invFld2 == null) {
			alert("Enter invFld2");
			$('#invFld2').focus();
			return false;
		} else if (invFld3 == '' || invFld3 == null) {
			alert("Enter invFld3");
			$('#invFld3').focus();
			return false;
		}
		
		
		
		//Planning
		else if (planMethCode == null ||  valMethodcode == '-1' ||  valMethodcode == undefined || valMethodcode == "") {
			alert("Enter planMethCode");
			$('#planMethCode').focus();
			return false;
		} else if (procMethType == null || procMethType == '-1' ||  procMethType == undefined || procMethType == "") {
			alert("Enter procMethType");
			$('#procMethType').focus();
			return false;
		}else if (orderInterval == '' || orderInterval == null) {
			alert("Enter orderInterval");
			$('#orderInterval').focus();
			return false;
		} else if (minQty == '' || minQty == null) {
			alert("Enter minQty");
			$('#minQty').focus();
			return false;
		} else if (leadTimePlan == '' || leadTimePlan == null) {
			alert("Enter leadTimePlan");
			$('#leadTimePlan').focus();
			return false;
		} else if (tolDays == '' || tolDays == null) {
			alert("Enter tolDays");
			$('#tolDays').focus();
			return false;
		} else if (planfield1 == '' || planfield1 == null) {
			alert("Enter planfield1");
			$('#planfield1').focus();
			return false;
		} else if (planfield2 == '' || planfield2 == null) {
			alert("Enter planfield2");
			$('#planfield2').focus();
			return false;
		} 
		
				
		//Production
		else if (issueMethodcode == null || issueMethodcode == '-1' ||  issueMethodcode == undefined || issueMethodcode == "") {
			alert("Enter issueMethodcode");
			$('#issueMethodcode').focus();
			return false;
		}else if (bomTypecode == null || bomTypecode == '-1' ||  bomTypecode == undefined || bomTypecode == "") {
			alert("Enter bomTypecode");
			$('#bomTypecode').focus();
			return false;
		} 
		
		//Remarks
		else if (itemRemarks == '' || itemRemarks == null) {
			alert("Enter itemRemarks");
			$('#itemRemarks').focus();
			return false;
		}else if (remfield1 == ' ' || remfield1 == null) {
			alert("Enter remfield1");
			$('#remfield1').focus();
			return false;
		} else if (remfield2 == '' || remfield2 == null) {
			alert("Enter remfield2");
			$('#remfield2').focus();
			return false;
		} else if (remfield3 == '' || remfield3 == null) {
			alert("Enter remfield3");
			$('#remfield3').focus();
			return false;
		} else if (remfield4 == '' || remfield4 == null) {
			alert("Enter remfield4");
			$('#remfield4').focus();
			return false;
		}*/ else {
			
			var result = $.ajax({
				type : "POST",
				url : "saveItemMaster?itemCode=" + itemCode + "&itemDesc=" + itemDesc+ "&itemTypeId=" + itemTypeId + "&itemGroupId=" + itemGroupId + "&uomGroupCode=" + uomGroupCode
				+ "&valuationCode=" + valuationCode + "&barCode=" + barCode + "&paytermCode=" + paytermCode+ "&field1=" + field1 + "&field2=" + field2
				+ "&itemGrpImage=" + itemGrpImage
				+ "&shippingCode=" + shippingCode + "&manItemType=" + manItemType + "&characterIdcap=" + characterIdcap+ "&statusId=" + statusId + "&manCapitemMfseq=" + manCapitemMfseq + "&manCapbranchCode=" + manCapbranchCode
				+ "&manCapcapacity=" + manCapcapacity + "&manCapstatus=" + manCapstatus + "&itemExcompCode=" + itemExcompCode+ "&itemExbranchCode=" + itemExbranchCode + "&itemExcostCenterId=" + itemExcostCenterId + "&itemExrolQty=" + itemExrolQty
				+ "&itemExstatus=" + itemExstatus + "&isSlab=" + isSlab + "&itemExdivisionCode=" + itemExdivisionCode+ "&itemVenBpCode=" + itemVenBpCode + "&itemAliasName=" + itemAliasName + "&purBpCode=" + purBpCode
				+ "&purUomCode=" + purUomCode + "&purMinPurchaseQty=" + purMinPurchaseQty + "&purPackingSze=" + purPackingSze+ "&purLength=" + purLength + "&purWidth=" + purWidth + "&purHeight=" + purHeight
				+ "&purVolume=" + purVolume + "&purWeight=" + purWeight + "&purField1=" + purField1+ "&purField2=" + purField2 + "&purParameter1=" + purParameter1 + "&purParameter2=" + purParameter2
				+ "&purParameter3=" + purParameter3 + "&purParameter4=" + purParameter4 + "&salesUomCode=" + salesUomCode+ "&salesPcksze=" + salesPcksze + "&salesLnth=" + salesLnth + "&salesWidth=" + salesWidth
				+ "&salesHght=" + salesHght + "&salesVolume=" + salesVolume + "&salesWeight=" + salesWeight+ "&salesFld1=" + salesFld1 + "&salesFld2=" + salesFld2 + "&salesPrmtr1=" + salesPrmtr1
				+ "&salesPrmtr2=" + salesPrmtr2 + "&salesPrmtr3=" + salesPrmtr3 + "&salesPrmtr4=" + salesPrmtr4+ "&valMethodcode=" + valMethodcode + "&invFld1=" + invFld1 + "&invFld2=" + invFld2
				+ "&invFld3=" + invFld3 + "&planMethCode=" + planMethCode + "&procMethType=" + procMethType+ "&orderInterval=" + orderInterval + "&minQty=" + minQty + "&leadTimePlan=" + leadTimePlan
				+ "&tolDays=" + tolDays + "&planfield1=" + planfield1 + "&planfield2=" + planfield2+ "&issueMethodcode=" + issueMethodcode + "&bomTypecode=" + bomTypecode + "&itemRemarks=" + itemRemarks
				+ "&remfield1=" + remfield1 + "&remfield2=" + remfield2 + "&remfield3=" + remfield3+ "&remfield4=" + remfield4,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result.split(":")[0] == "S") {
				alert("Saved Successfully");
				//clearData23();
				//location.reload();
			} else if (result == "A") {
				alert("Already Exists");
			} else {
				alert("failed");

			}
		}
	});
	
	$('#itemMaster_Reset').click(function () {
		$('#itemCode').val('');
		$('#itemDesc').val('');
		$('#itemTypeId').val('-1');
		$('#itemGroupId').val('-1');
		$('#uomGroupCode').val('-1');
		$('#valuationCode').val('-1');
		$('#barCode').val('');
		$('#paytermCode').val('-1');
		$('#field1').val('');
		$('#field2').val('');
	});
	
	
	
	
	
});