$(document).ready(function () {

    $('#menu-master').addClass('active');
    $('#menu-master > a').attr('aria-expanded', 'true');
    $('#menu-master > ul').addClass('in');

    $('#menu-rights').addClass('active');
    $('#menu-rights > a').attr('aria-expanded', 'true');
    $('#menu-rights > ul').addClass('in');

    $('#menu-roleVsRights').addClass('active');
    $("#RolesVsRightsAdd").hide();

    $('#rolesVsRightsAdd').click(function () {
        $('#RolesVsRightsAdd').show();
    });

    $('.close').click(function () {
        $('#RolesVsRightsAdd').hide();
    });
     $('input:checkbox').removeAttr('checked');

    $('#roleVsRightsReset').click(function(){
        $('#companyId').val('-1');
        $('#roleId').val('-1');
        $('#rightsId').val('-1');
         $("#moduleID").val('-1');
        $('input:checkbox').removeAttr('checked');
    });

    $.ajax({
        type: "POST",
        url: "getCompIdList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#companyId").get(0).options.length = 0;
            $("#companyId").get(0).options[0] = new Option("Select Company", "-1");
             $.each(msg, function (index, data) {
                $("#companyId").get(0).options[$("#companyId").get(0).options.length] = new Option(data.companyName, data.companyCode);

            });
        },
    });


    $("#companyId").on('change', function () {
        var compId = $("#companyId").val();

        $.ajax({
            type: "POST",
            url: "getCompanyBasedRoleList?compId=" + compId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                $("#roleId").get(0).options.length = 0;
                $("#roleId").get(0).options[0] = new Option("Select Role", "-1");
                $.each(msg, function (index, data) {
                    $("#roleId").get(0).options[$("#roleId").get(0).options.length] = new Option(data.roleBasedName, data.roleBasedId);
                });
            },
        });
    });


    $.ajax({
        type: "POST",
        url: "getModuleList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (msg) {
            $("#moduleID").get(0).options.length = 0;
            $("#moduleID").get(0).options[0] = new Option("Select Module", "-1");
            $.each(msg, function (index, data) {
                $("#moduleID").get(0).options[$("#moduleID").get(0).options.length] = new Option(data.moduleName, data.moduleId);
            });
        },
    });

    $("#moduleID").on('change', function () {
        var moduleId = $("#moduleID").val();
        $.ajax({
            type: "POST",
            url: "getModuleBasedFormList?moduleId=" + moduleId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                $("#rightsId").get(0).options.length = 0;
                $("#rightsId").get(0).options[0] = new Option("Select Form Rights", "-1");
                $.each(msg, function (index, data) {
                    $("#rightsId").get(0).options[$("#rightsId").get(0).options.length] = new Option(data.rightsBasedName, data.rightsBasedId);
                });
            },
        });

    });



    var table;
    (function ($) {
        'use strict';
        $(function () {
            table = $('.js-exportable').DataTable({
                responsive: true,
                "iDisplayLength": 10,
                ajax: {
                    type: "POST",
                    url: "getRoleVsRightsList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                },
                "columns": [
                    {
                        "data": "companyName"
                    },
                    {
                        "data": "roleName"
                    },
                    {
                        "data": "rightsName"
                    },
                    {
                        "data": "create"
                    },
                    {
                        "data": "edit"
                    },
                    {
                        "data": "view"
                    },
                    {
                        "data": "action",
                        render: actionFormatter
                    }

                ],
            });
        });
    }(jQuery))
    $('#dtab').on("click", 'tr', function () {
         tableData = table.row(this).data();
         $('#companyEditName').val(tableData.companyName);
          $('#roleEditName').val(tableData.roleName);
          $('#rightsEditName').val(tableData.rightsName);      
          $('#companyEditId').val(tableData.companyCode);
          $('#roleEditId').val(tableData.roleId);
          $('#rightsEditId').val(tableData.rightsId); 
          

        if(tableData.create=="True"){
            $('input[name=createEditlogic]').prop('checked', true);
        } else{
            $('input[name=createEditlogic]').prop('checked', false);
        }   

         if(tableData.edit=="True"){
            $('input[name=editEditlogic]').prop('checked', true);
        } else{
            $('input[name=editEditlogic]').prop('checked', false);
        }

         if(tableData.view=="True"){
            $('input[name=viewEditlogic]').prop('checked', true);
        } else{
            $('input[name=viewEditlogic]').prop('checked', false);
        }
    });



    $('#roleVsRightsSave').click(function () {

        var companyId = $('#companyId').val();
        var roleId = $('#roleId').val();
        var rightsId = $('#rightsId').val();
        var createValue = $("input[name='createlogic']:checked").val();
        var editValue = $("input[name='editlogic']:checked").val();
        var viewValue = $("input[name='viewlogic']:checked").val();

        if(createValue==undefined){
            createValue="False"          
        }else{
            createValue="True"
        }

         if(editValue==undefined){
            editValue="False"          
        }else{
            editValue="True"
        }

         if(viewValue==undefined){
            viewValue="False"          
        }else{
            viewValue="True"
        }


       if (companyId == '' || companyId == 0 || companyId == '-1') {
            alert("Select Company");
            $('#companyId').focus();
            return false;
        }else if (roleId == '' || roleId == 0 || roleId == '-1') {
            alert("Select Role Name");
            $('#roleId').focus();
            return false;
        } else if (rightsId == '' || rightsId == 0 || rightsId == '-1') {
            alert("Select Form Rights");
            $('#rightsId').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "saveRoleVsRightsCreation?companyId=" + companyId + "&roleId=" + roleId
                    + "&rightsId=" + rightsId + "&createValue=" + createValue
                     + "&editValue=" + editValue + "&viewValue=" + viewValue,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;
            if (result == "S") {
                alert("Saved Successfully");
                clearData();
                location.reload();
            } else if (result == "A") {
                alert("Role Name And Form Name Already Exists");
            } else {
                alert("failed");

            }
        }
    });


     $('#roleVsRightsEditUpdate').click(function () {

        var companyEditId = $('#companyEditId').val();
        var roleEditId = $('#roleEditId').val();
        var rightsEditId = $('#rightsEditId').val();
        var createEditValue = $("input[name='createEditlogic']:checked").val();
        var editEditValue = $("input[name='editEditlogic']:checked").val();
        var viewEditValue = $("input[name='viewEditlogic']:checked").val();
       
        if(createEditValue==undefined){
            createEditValue="False"             
        }else{
            createEditValue="True"  
        }

         if(editEditValue==undefined){             
            editEditValue="False"         
        }else{
            editEditValue="True"         
        }

         if(viewEditValue==undefined){
            viewEditValue="False"           
        }else{
            viewEditValue="True"        
        }


       if (companyEditId == '' || companyEditId == 0 || companyEditId == '-1') {
            alert("Select Company");
            $('#companyEditName').focus();
            return false;
        }else if (roleEditId == '' || roleEditId == 0 || roleEditId == '-1') {
            alert("Select Role Name");
            $('#roleEditName').focus();
            return false;
        } else if (rightsEditId == '' || rightsEditId == 0 || rightsEditId == '-1') {
            alert("Select Form Rights");
            $('#rightsEditName').focus();
            return false;
        } else {
            var result = $.ajax({
                type: "POST",
                url: "saveRoleVsRightsEditCreation?companyEditId=" + companyEditId + "&roleEditId=" + roleEditId
                    + "&rightsEditId=" + rightsEditId + "&createEditValue=" + createEditValue
                     + "&editEditValue=" + editEditValue + "&viewEditValue=" + viewEditValue,
                dataType: "json",
                contentType: "application/json",
                processData: false,
                async: false
            }).responseText;
            if (result == "S") {
                alert("Updated Successfully");
                clearData();
                location.reload();
            } else if (result == "A") {
                alert("Role Name And Form Name Already Exists");
            } else {
                alert("failed");

            }
        }
    });


    function clearData(){
        $('#companyId').val('-1');
        $('#roleId').val('-1');
        $('#rightsId').val('-1');
         $("#moduleID").val('-1');
        $('input:checkbox').removeAttr('checked');
    } 


});
