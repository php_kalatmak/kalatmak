$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-assignment').addClass('active');
	$('#menu-assignment > a').attr('aria-expanded', 'true');
	$('#menu-assignment > ul').addClass('in');

	$('#menu-conditionMaster').addClass('active');


	$('#conditionMasterAdd').click(function() {
		$('#ConditionMaster').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});
	var seq = null;


	$("#condTypeEdit").prop('disabled', true);
	$('#condTypeEdit').prop('disabled', true);
	$('#tabEdit').prop('disabled', true);
	$("#countryEdit").prop('disabled', true);
	$("#stateEdit").prop('disabled', true);
	$("#businessEdit").prop('disabled', true);
	$("#compEdit").prop('disabled', true);
	$("#hsnEdit").prop('disabled', true);
	$("#itemEdit").prop('disabled', true);
	$("#itemGrpEdit").prop('disabled', true);
	$("#uomEdit").prop('disabled', true);
	$("#fromDateEdit").prop('disabled', true);
	$("#toDateEdit").prop('disabled', true);

	$("#fromDate").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	$("#toDate").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	$("#fromDateEdit").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	$("#toDateEdit").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#comp").get(0).options.length = 0;
			$("#comp").get(0).options[0] = new Option("Select Company", "-1");
			$("#compEdit").get(0).options.length = 0;
			$("#compEdit").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, item) {
				$("#compEdit").get(0).options[$("#compEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#comp").get(0).options[$("#comp").get(0).options.length] = new Option(item.compName, item.compCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getConditionListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#condType").get(0).options.length = 0;
			$("#condType").get(0).options[0] = new Option("Select Condition", "-1");
			$("#condTypeEdit").get(0).options.length = 0;
			$("#condTypeEdit").get(0).options[0] = new Option("Select Condition", "-1");
			$.each(msg, function(index, item) {
				$("#condTypeEdit").get(0).options[$("#condTypeEdit").get(0).options.length] = new Option(item.conditionName, item.conditionCode);
				$("#condType").get(0).options[$("#condType").get(0).options.length] = new Option(item.conditionName, item.conditionCode);
			});
		},
	});


	var result = $.ajax({
		type : "POST",
		url : "getCountryInGroupComp",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#country").get(0).options.length = 0;
			$("#country").get(0).options[0] = new Option("Select Country", "-1");
			$("#countryEdit").get(0).options.length = 0;
			$("#countryEdit").get(0).options[0] = new Option("Select Country", "-1");
			$.each(msg, function(index, item) {
				$("#country").get(0).options[$("#country").get(0).options.length] = new Option(item.countryDesc, item.countryId);
				$("#countryEdit").get(0).options[$("#countryEdit").get(0).options.length] = new Option(item.countryDesc, item.countryId);
			});
		},
	});

	$("#country").change(function() {
		var country = $("#country").val();
		var result = $.ajax({
			type : "POST",
			url : "getStateINGroupCompany?country=" + country,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#state").get(0).options.length = 0;
				$("#state").get(0).options[0] = new Option("Select State", "-1");
				$.each(msg, function(index, item) {
					$("#state").get(0).options[$("#state").get(0).options.length] = new Option(item.stateDesc, item.stateId);
				});
			},
		});
	});

	$("#countryEdit").change(function() {
		var country = $("#countryEdit").val();
		var result = $.ajax({
			type : "POST",
			url : "getStateINGroupCompany?country=" + country,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#stateEdit").get(0).options.length = 0;
				$("#stateEdit").get(0).options[0] = new Option("Select State", "-1");
				$.each(msg, function(index, item) {
					$("#stateEdit").get(0).options[$("#stateEdit").get(0).options.length] = new Option(item.stateDesc, item.stateId);
				});
			},
		});
	});
	var result = $.ajax({
		type : "POST",
		url : "getStateINGroupCompanyAlone",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#stateEdit").get(0).options.length = 0;
			$("#stateEdit").get(0).options[0] = new Option("Select State", "-1");
			$.each(msg, function(index, item) {
				$("#stateEdit").get(0).options[$("#stateEdit").get(0).options.length] = new Option(item.stateDesc, item.stateId);
			});
		},
	});
	$('#condType').on('change', function() {
		var condition = $("#condType").val();
		var result = $.ajax({
			type : "POST",
			url : "getTabsListinConditionMaster?condition=" + condition,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#tab").get(0).options.length = 0;
				$("#tab").get(0).options[0] = new Option("Select Tab", "-1");
				$.each(msg, function(index, item) {
					$("#tab").get(0).options[$("#tab").get(0).options.length] = new Option(item.tabCode, item.tabCode);
				});
			},
		});
	});
	$('#condTypeEdit').on('change', function() {
		var condition = $("#condTypeEdit").val();
		var result = $.ajax({
			type : "POST",
			url : "getTabsListinConditionMaster?condition=" + condition,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#tabEdit").get(0).options.length = 0;
				$("#tabEdit").get(0).options[0] = new Option("Select Tab", "-1");
				$.each(msg, function(index, item) {
					$("#tabEdit").get(0).options[$("#tabEdit").get(0).options.length] = new Option(item.tabCode, item.tabCode);
				});
			},
		});
	});


	var result = $.ajax({
		type : "POST",
		url : "getTabsListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#tabEdit").get(0).options.length = 0;
			$("#tabEdit").get(0).options[0] = new Option("Select Tab", "-1");
			$.each(msg, function(index, item) {
				$("#tabEdit").get(0).options[$("#tabEdit").get(0).options.length] = new Option(item.tabCode, item.tabCode);
			});
		},
	});
	var result = $.ajax({
		type : "POST",
		url : "getBusinessPartnerInConditionMaster",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#business").get(0).options.length = 0;
			$("#business").get(0).options[0] = new Option("Select Business Partner", "-1");
			$("#businessEdit").get(0).options.length = 0;
			$("#businessEdit").get(0).options[0] = new Option("Select Business Partner", "-1");
			$.each(msg, function(index, item) {
				$("#businessEdit").get(0).options[$("#businessEdit").get(0).options.length] = new Option(item.business, item.businessCode);
				$("#business").get(0).options[$("#business").get(0).options.length] = new Option(item.business, item.businessCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getUOMInConditionMaster",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#uom").get(0).options.length = 0;
			$("#uom").get(0).options[0] = new Option("Select UOM", "-1");
			$("#uomEdit").get(0).options.length = 0;
			$("#uomEdit").get(0).options[0] = new Option("Select UOM", "-1");
			$.each(msg, function(index, item) {
				$("#uomEdit").get(0).options[$("#uomEdit").get(0).options.length] = new Option(item.uom, item.uomCode);
				$("#uom").get(0).options[$("#uom").get(0).options.length] = new Option(item.uom, item.uomCode);
			});
		},
	});


	var result = $.ajax({
		type : "POST",
		url : "getItemInConditionMaster",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#item").get(0).options.length = 0;
			$("#item").get(0).options[0] = new Option("Select Item", "-1");
			$("#itemEdit").get(0).options.length = 0;
			$("#itemEdit").get(0).options[0] = new Option("Select Item", "-1");
			$.each(msg, function(index, item) {
				$("#item").get(0).options[$("#item").get(0).options.length] = new Option(item.item, item.itemCode);
				$("#itemEdit").get(0).options[$("#itemEdit").get(0).options.length] = new Option(item.item, item.itemCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getItemGroupInConditionMaster",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#itemGrp").get(0).options.length = 0;
			$("#itemGrp").get(0).options[0] = new Option("Select Item Group", "-1");
			$("#itemGrpEdit").get(0).options.length = 0;
			$("#itemGrpEdit").get(0).options[0] = new Option("Select Item Group", "-1");
			$.each(msg, function(index, item) {
				$("#itemGrp").get(0).options[$("#itemGrp").get(0).options.length] = new Option(item.itemGrp, item.itemGrpCode);
				$("#itemGrpEdit").get(0).options[$("#itemGrpEdit").get(0).options.length] = new Option(item.itemGrp, item.itemGrpCode);
			});
		},
	});

	$('#condType').on('change', function() {
		$("#countryDiv").hide();
		$("#stateDiv").hide();
		$("#businessDiv").hide();
		$("#compDiv").hide();
		$("#hsnDiv").hide();
		$("#itemDiv").hide();
		$("#itemGrpDiv").hide();
		$("#uomDiv").hide();
		$("#valueDiv").hide();
		$("#fromDateDiv").hide();
		$("#toDateDiv").hide();
	});
	$("#countryDiv").hide();
	$("#stateDiv").hide();
	$("#businessDiv").hide();
	$("#compDiv").hide();
	$("#hsnDiv").hide();
	$("#itemDiv").hide();
	$("#itemGrpDiv").hide();
	$("#uomDiv").hide();
	$("#valueDiv").hide();
	$("#fromDateDiv").hide();
	$("#toDateDiv").hide();

	$('#tab').on('change', function() {
		var tab = $("#tab").val();
		var result = $.ajax({
			type : "POST",
			url : "getTabsAssignListinConditionMaster?tab=" + tab,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				if (msg[0].country == true) {
					$("#countryDiv").show();
				} else {
					$("#countryDiv").hide();
				}


				if (msg[0].state == true) {
					$("#stateDiv").show();
				} else {
					$("#stateDiv").hide();
				}


				if (msg[0].business == true) {
					$("#businessDiv").show();
				} else {
					$("#businessDiv").hide();
				}


				if (msg[0].company == true) {
					$("#compDiv").show();
				} else {
					$("#compDiv").hide();
				}


				if (msg[0].hsn == true) {
					$("#hsnDiv").show();
				} else {
					$("#hsnDiv").hide();
				}


				if (msg[0].item == true) {
					$("#itemDiv").show();
				} else {
					$("#itemDiv").hide();
				}


				if (msg[0].itemGrp == true) {
					$("#itemGrpDiv").show();
				} else {
					$("#itemGrpDiv").hide();
				}


				if (msg[0].uom == true) {
					$("#uomDiv").show();
				} else {
					$("#uomDiv").hide();
				}


				if (msg[0].amtper == true) {
					$("#valueDiv").show();
				} else {
					$("#valueDiv").hide();
				}


				if (msg[0].fromDate == true) {
					$("#fromDateDiv").show();
				} else {
					$("#fromDateDiv").hide();
				}


				if (msg[0].toDate == true) {
					$("#toDateDiv").show();
				} else {
					$("#toDateDiv").hide();
				}
			},
		});
	});




	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getConditionMasterList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "condType"
					},
					{
						"data" : "tab"
					},
					{
						"data" : "country"
					},
					{
						"data" : "state"
					},
					{
						"data" : "business"
					},
					{
						"data" : "company"
					},
					{
						"data" : "hsn"
					},
					{
						"data" : "item"
					},
					{
						"data" : "itemGrp"
					},
					{
						"data" : "uom"
					},
					{
						"data" : "amtper"
					},

					{
						"data" : "fromDate"
					},
					{
						"data" : "toDate"
					},
					{
						"data" : "status"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		seq = tableData.seq;
		$('#condTypeEdit').val(tableData.condTypeCode);
		$('#tabEdit').val(tableData.tab);
		$("#countryEdit").val(tableData.countryCode);
		$("#stateEdit").val(tableData.stateCode);
		$("#businessEdit").val(tableData.businessCode);
		$("#compEdit").val(tableData.companyCode);
		$("#hsnEdit").val(tableData.hsn);
		$("#itemEdit").val(tableData.itemCode);
		$("#itemGrpEdit").val(tableData.itemGrpCode);
		$("#uomEdit").val(tableData.uomCode);
		$("#valueEdit").val(tableData.amtper);
		$("#fromDateEdit").val(tableData.fromDate);
		$("#toDateEdit").val(tableData.toDate);
		$("#statusEdit").val(tableData.status);
	});

	$('#ConditionMasterSave').click(function() {

		var condition = $('#condType').val();
		var tab = $('#tab').val();
		var country = $("#country").val();
		var state = $("#state").val();
		var business = $("#business").val();
		var comp = $("#comp").val();
		var hsn = $("#hsn").val();
		var item = $("#item").val();
		var itemGrp = $("#itemGrp").val();
		var uom = $("#uom").val();
		var value = $("#value").val();
		var fromDate = $("#fromDate").val();
		var toDate = $("#toDate").val();


		if (condition == ' ' || condition == 0 || condition == '-1') {
			alert("Select Condition");
			$('#condType').focus();
			return false;
		} else if (tab == '' || tab == null || tab == 0 || tab == '-1') {
			alert("Select Tab");
			$('#tab').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "saveConditionMaster?condition=" + condition + "&tab=" + tab + "&country=" + country +
					"&state=" + state + "&business=" + business + "&comp=" + comp + "&hsn=" + hsn + "&item=" + item + "&itemGrp=" + itemGrp +
					"&uom=" + uom + "&value=" + value + "&fromDate=" + fromDate + "&toDate=" + toDate,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				location.reload();
				clear();
			} else {
				alert("failed");
			}
		}
	});


	$('#ConditionMasterUpdate').click(function() {

		var condition = $('#condTypeEdit').val();
		var tab = $('#tabEdit').val();
		var country = $("#countryEdit").val();
		var state = $("#stateEdit").val();
		var business = $("#businessEdit").val();
		var comp = $("#compEdit").val();
		var hsn = $("#hsnEdit").val();
		var item = $("#itemEdit").val();
		var itemGrp = $("#itemGrpEdit").val();
		var uom = $("#uomEdit").val();
		var value = $("#valueEdit").val();
		var fromDate = $("#fromDateEdit").val();
		var toDate = $("#toDateEdit").val();
		var status = $("#statusEdit").val();


		if (condition == ' ' || condition == 0 || condition == '-1') {
			alert("Select Condition");
			$('#condType').focus();
			return false;
		} else if (tab == '' || tab == null || tab == 0 || tab == '-1') {
			alert("Select Tab");
			$('#tab').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updateConditionMaster?condition=" + condition + "&tab=" + tab + "&country=" + country +
					"&state=" + state + "&business=" + business + "&comp=" + comp + "&hsn=" + hsn + "&item=" + item + "&itemGrp=" + itemGrp +
					"&uom=" + uom + "&value=" + value + "&fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status + "&seq=" + seq,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				location.reload();
				clear();
			} else {
				alert("failed");
			}
		}
	});

	function clear() {
		$('#condType').val('-1');
		$('#tab').val('-1');
		$("#country").val('-1');
		$("#state").val('-1');
		$("#business").val('-1');
		$("#comp").val('-1');
		$("#hsn").val('');
		$("#item").val('-1');
		$("#itemGrp").val('-1');
		$("#uom").val('-1');
		$("#value").val('');
		$("#fromDate").val('');
		$("#toDate").val('');

		$('#condTypeEdit').val('-1');
		$('#tabEdit').val('-1');
		$("#countryEdit").val('-1');
		$("#stateEdit").val('-1');
		$("#businessEdit").val('-1');
		$("#compEdit").val('-1');
		$("#hsnEdit").val('');
		$("#itemEdit").val('-1');
		$("#itemGrpEdit").val('-1');
		$("#uomEdit").val('-1');
		$("#valueEdit").val('');
		$("#fromDateEdit").val('');
		$("#toDateEdit").val('');
		$("#statusEdit").val('-1');
	}
});