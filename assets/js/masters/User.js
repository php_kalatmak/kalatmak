$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-company').addClass('active');
	$('#menu-company > a').attr('aria-expanded', 'true');
	$('#menu-company > ul').addClass('in');

	$('#menu-User').addClass('active');
	$('#Branch').hide();
	$('input:checkbox').removeAttr('checked');


	$('#UserAdd').click(function() {
		$('#User').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});


	var result = $.ajax({
		type : "POST",
		url : "getbranchfromuserAll",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$.each(msg, function(index, item) {
				$("#branch_map_Edit").get(0).options[$("#branch_map_Edit").get(0).options.length] = new Option(item.branchId + ' - ' + item.branchdesc, item.branchId);
			});
			$('#branch_map_Edit').multiselect({
			});
		},
	});
	var result = $.ajax({
		type : "POST",
		url : "getroleidfromuserAll",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$.each(msg, function(index, item) {
				$("#roleid_map_Edit").get(0).options[$("#roleid_map_Edit").get(0).options.length] = new Option(item.roleId + '-' + item.roleName, item.roleId);
			});
			$('#roleid_map_Edit').multiselect({
			});
		},
	});


	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				//processing : true,
				//serverSide : true,
				//dom : '<"html5buttons"B>lTfgtip',
				//buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ],
				ajax : {
					type : "POST",
					url : "getUserTypeIdList?compId=" + 1001,
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
					//	success : function(data) {
					//		alert(data);

				//	},	//capacity
				},
				"columns" : [
					{
						"data" : "companydesc"
					},
					{
						"data" : "usercode"
					},
					{
						"data" : "userdesc"
					},
					{
						"data" : "emailId"
					},
					{
						"data" : "mobileno"
					},
					{
						"data" : "weblogic"
					},
					{
						"data" : "mobilelogic"
					},

					{
						"data" : "pwdneverexpired"
					},

					{
						"data" : "locked"
					},
					{
						"data" : "statusdesc"
					},

					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))

	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$('#companyEdit').val(tableData.companyId);
		$('#usercodeEdit').val(tableData.usercode);
		$('#usernameEdit').val(tableData.userdesc);
		$('#passwordEdit').val(tableData.password);
		$('#EmailIdEdit').val(tableData.emailId);
		$('#mobilenoEdit').val(tableData.mobileno);
		$('#passwordexpperEdit').val(tableData.PasswordExpir);
		$('#passexpiredtEdit').val(tableData.LastPassWordexpireDate);
		$('#statususerEdit').val(tableData.statusId);
		$('#getcompId').val(tableData.companyId);
		var result = $.ajax({
			type : "POST",
			url : "getUserBranchMapping?userCode=" + tableData.usercode,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#branch_map_Edit").multiselect('deselectAll', true);
				$("#branch_map_Edit").multiselect('select', msg);
			}
		});

		var result = $.ajax({
			type : "POST",
			url : "getUserRoleMapping?userCode=" + tableData.usercode,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$("#roleid_map_Edit").multiselect('deselectAll', true);
				$("#roleid_map_Edit").multiselect('select', msg);
			}
		});

		if (tableData.weblogic == true) {
			$('input[name=weblogicEdit]').prop('checked', true);
		} else {
			$('input[name=weblogicEdit]').prop('checked', false);
		}

		if (tableData.mobilelogic == true) {
			$('input[name=mobilelogicEdit]').prop('checked', true);
		} else {
			$('input[name=mobilelogicEdit]').prop('checked', false);
		}

		if (tableData.pwdneverexpired == true) {
			$('input[name=passwordcheckbEdit]').prop('checked', true);
		} else {
			$('input[name=passwordcheckbEdit]').prop('checked', false);
		}

		if (tableData.locked == true) {
			$('input[name=LockEdit]').prop('checked', true);
		} else {
			$('input[name=LockEdit]').prop('checked', false);
		}


	});

	$("#User_Reset").click(function() {
		$('#usercode').val('');
		$('#UserName').val('');
		$('#PassWord').val('');
		$('#emailId').val('');
		$('#Mobile').val('');
		$('#companyId').val('-1');
		$('#passwordexpper').val('');
		$('#passexpiredt').val('');
	});

	$("#passexpiredt").datepicker({
		dateFormat : "dd/mm/yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1950:" + (new Date().getFullYear() + 15)
	});

	$("#passexpiredtEdit").datepicker({
		dateFormat : "dd/mm/yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1950:" + (new Date().getFullYear() + 15)
	});



	$('#passwordexpper').on('change', function(event) {
		var days = $('#passwordexpper').val();
		var today = new Date();
		var prevdays = parseInt(today.getDate())
		today.setDate(prevdays + parseInt(days));
		$("#passexpiredt").datepicker('setDate', today);
	});
	$('#passwordexpperEdit').on('keyup', function(event) {
		var days = $('#passwordexpperEdit').val();
		var today = new Date();
		var prevdays = parseInt(today.getDate())
		today.setDate(prevdays + parseInt(days));
		$("#passexpiredtEdit").datepicker('setDate', today);
	});


	$.ajax({
		type : "POST",
		url : "getcompanyList",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#companyId").get(0).options.length = 0;
			$("#companyId").get(0).options[0] = new Option("Select Company", "-1");
			$("#companyEdit").get(0).options.length = 0;
			$("#companyEdit").get(0).options[0] = new Option("Select Company", "-1");

			$.each(msg, function(index, data) {
				$("#companyId").get(0).options[$("#companyId").get(0).options.length] = new Option(data.companyname, data.companycode);
				$("#companyEdit").get(0).options[$("#companyEdit").get(0).options.length] = new Option(data.companyname, data.companycode);
			});
		},
	});
	$('#companyId').on('change', function() {

		var compId = $('#companyId').val();
		var result = $.ajax({
			type : "POST",
			url : "getbranchfromuser?compId=" + compId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$.each(msg, function(index, item) {
					$("#branch_map").get(0).options[$("#branch_map").get(0).options.length] = new Option(item.branchId + ' - ' + item.branchdesc, item.branchId);
				});
				$('#branch_map').multiselect({
				});
			},
		});
		var result = $.ajax({
			type : "POST",
			url : "getroleidfromuser?compId=" + compId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$.each(msg, function(index, item) {
					$("#roleid_map").get(0).options[$("#roleid_map").get(0).options.length] = new Option(item.roleId + '-' + item.roleName, item.roleId);
				});
				$('#roleid_map').multiselect({
				});
			},
		});
	});


	$('#companyEdit').on('change', function() {
		var compId = $('#companyEdit').val();
		var result = $.ajax({
			type : "POST",
			url : "getbranchfromuser?compId=" + compId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$.each(msg, function(index, item) {
					$("#branch_map_Edit").get(0).options[$("#branch_map_Edit").get(0).options.length] = new Option(item.branchId + ' - ' + item.branchdesc, item.branchId);
				});
				$('#branch_map_Edit').multiselect({
				});
			},
		});
		var result = $.ajax({
			type : "POST",
			url : "getroleidfromuser?compId=" + compId,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			async : false,
			success : function(msg) {
				$.each(msg, function(index, item) {
					$("#roleid_map_Edit").get(0).options[$("#roleid_map_Edit").get(0).options.length] = new Option(item.roleId + '-' + item.roleName, item.roleId);
				});
				$('#roleid_map_Edit').multiselect({
				});
			},
		});
	});
	$("input[name='passwordcheckbox']").click(function() {
		if ($(this).prop("checked") == true) {
			//alert("Checkbox is checked.");
			$('#passwordexpper').val('');
			$('#passexpiredt').val('');
			$('#passwordexpper').prop('readonly', true);
			$('#passexpiredt').prop('readonly', true);
			$("#passexpiredt").datepicker("option", "disabled", true);
		} else if ($(this).prop("checked") == false) {
			//alert("Checkbox is unchecked.");
			$('#passwordexpper').prop('readonly', false);
			$('#passexpiredt').prop('readonly', false);
			$("#passexpiredt").datepicker("option", "disabled", false);

		}
	});


	$('#UserSave').click(function() {

		var usercode = $('#usercode').val();
		var UserName = $('#UserName').val();
		var PassWord = $('#PassWord').val();
		var EmailId = $('#emailId').val();
		var Mobile = $('#Mobile').val();
		var companyId = $('#companyId').val();
		var weblogic = $("input[name='weblogic']:checked").val();
		var mobilelogic = $("input[name='mobilelogic']:checked").val();
		var passwordcheckbox = $("input[name='passwordcheckbox']:checked").val();
		var lock = $("input[name='lock']:checked").val();
		var passwordexpper = $('#passwordexpper').val();
		var passexpiredt = $('#passexpiredt').val();
		var roleidmap = $('#roleid_map').val();
		var branchmap = $('#branch_map').val();

		if (usercode == null || usercode == "") {
			alert("Enter User Code");
			$('#usercode').focus();
			return false;
		} else if (UserName == null || UserName == "") {
			alert("Enter User Name");
			$('#UserName').focus();
			return false;
		} else if (PassWord == '' || PassWord == null) {
			alert("Enter PassWord");
			$('#PassWord').focus();
			return false;
		} else if (EmailId == '' || EmailId == null) {
			alert("Enter Email Id ");
			$('#emailId').focus();
			return false;
		} else if (Mobile == '' || Mobile == null) {
			alert("Enter Mobile Number ");
			$('#Mobile').focus();
			return false;
		} else if (companyId == null || companyId == undefined || companyId == "" || companyId == "-1") {
			alert("Select Company Id ");
			$('#companyId').focus();
			return false;
		} else if (branchmap == null || branchmap == undefined || branchmap == "" || branchmap == "-1") {
			alert("Select Branch");
			$('#branch_map').focus();
			return false;
		} else if (roleidmap == null || roleidmap == undefined || roleidmap == "" || roleidmap == "-1") {
			alert("Select Role");
			$('#roleidmap').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "SaveUserDetails?usercode=" + usercode + "&UserName=" + UserName + "&PassWord=" + PassWord
					+ "&EmailId=" + EmailId + "&Mobile=" + Mobile + "&weblogic=" + weblogic
					+ "&mobilelogic=" + mobilelogic + "&passwordcheckbox=" + passwordcheckbox
					+ "&lock=" + lock + "&passwordexpper=" + passwordexpper + "&passexpiredt="
					+ passexpiredt + "&companyId=" + companyId + "&branchmap=" + branchmap
					+ "&roleidmap=" + roleidmap,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;

			if (result == "S") {
				alert("Saved Successfully");
				$('#usercode').val('');
				$('#UserName').val('');
				$('#PassWord').val('');
				$('#emailId').val('');
				$('#Mobile').val('');
				$('#companyId').val('-1');
				$('#passwordexpper').val('');
				$('#passexpiredt').val('');
				location.reload();
				$("#StorageAss").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Already Exists");
			} else {
				alert("failed");

			}
		}
	});
	$('#userUpdate').click(function() {

		var usercode = $('#usercodeEdit').val();
		var UserName = $('#usernameEdit').val();
		var PassWord = $('#passwordEdit').val();
		var EmailId = $('#EmailIdEdit').val();
		var Mobile = $('#mobilenoEdit').val();
		var weblogic = $("input[name='weblogicEdit']:checked").val();
		var mobilelogic = $("input[name='mobilelogicEdit']:checked").val();
		var passwordcheckbox = $("input[name='passwordcheckbEdit']:checked").val();
		var lock = $("input[name='LockEdit']:checked").val();
		var passwordexpper = $('#passwordexpperEdit').val();
		var passexpiredt = $('#passexpiredtEdit').val();
		var status = $('#statususerEdit').val();
		var companyId = $('#companyEdit').val();

		alert(companyId);

		var roleidmap = $('#roleid_map_Edit').val();
		var branchmap = $('#branch_map_Edit').val();

		if (weblogic == undefined) {
			weblogic = "false"
		} else {
			weblogic = "true"
		}

		if (mobilelogic == undefined) {
			mobilelogic = "false"
		} else {
			mobilelogic = "true"
		}

		if (passwordcheckbox == undefined) {
			passwordcheckbox = "false"
		} else {
			passwordcheckbox = "true"
		}
		if (lock == undefined) {
			lock = "false"
		} else {
			lock = "true"
		}


		if (usercode == null || usercode == "") {
			alert("Enter User Code");
			$('#usercodeEdit').focus();
			return false;
		} else if (UserName == null || UserName == "") {
			alert("Enter User Name");
			$('#usernameEdit').focus();
			return false;
		} else if (PassWord == '' || PassWord == null) {
			alert("Enter PassWord");
			$('#passwordEdit').focus();
			return false;
		} else if (EmailId == '' || EmailId == null) {
			alert("Enter Email Id ");
			$('#EmailIdEdit').focus();
			return false;
		} else if (Mobile == '' || Mobile == null) {
			alert("Enter Mobile Number ");
			$('#mobilenoEdit').focus();
			return false;
		} else if (companyId == null || companyId == undefined || companyId == "" || companyId == "-1") {
			alert("Select Company Id ");
			$('#companyEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "UpdateUserDetails?usercode=" + usercode + "&UserName=" + UserName + "&PassWord=" + PassWord + "&EmailId=" +
					EmailId + "&Mobile=" + Mobile + "&weblogic=" + weblogic + "&mobilelogic=" + mobilelogic + "&passwordcheckbox=" +
					passwordcheckbox + "&lock=" + lock + "&passwordexpper=" + passwordexpper + "&passexpiredt=" + passexpiredt +
					"&companyId=" + companyId + "&status=" + status + "&branchmap=" + branchmap + "&roleidmap=" + roleidmap,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;

			if (result == "S") {
				alert("Update Successfully");
				location.reload();
				$("#userEdit").hide();
				$("#dtab").show();
			} else if (result == "A") {
				alert("Already Exists");
			} else {
				alert("failed");

			}
		}
	});
	var result = $.ajax({
		type : "POST",
		url : "getStatusEdits",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#statususerEdit").get(0).options.length = 0;
			$("#statususerEdit").get(0).options[0] = new Option("Select Status", "-1");
			$.each(msg, function(index, item) {
				$("#statususerEdit").get(0).options[$("#statususerEdit").get(0).options.length] = new Option(item.statusDesc, item.statusId);
			});
		},
	});
});