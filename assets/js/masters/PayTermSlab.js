$(document).ready(function() {
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-payTermSlab').addClass('active');
	
	$('#PaytermSlabadd').hide();
	$('#paytermSlabEdit').hide();

	$('#PaytermSlab').click(function() {
		$('#PaytermSlabadd').show();
	});
	
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	
	$('#payTermSlabSave').click(
			function() {

				var payTermCode = $('#payTermCode').val();
				var slabSeqId = $('#slabSeqId').val();
				var creditDays = $('#creditDays').val();
				var percentage = $('#percentage').val();
				
				
				if (payTermCode == ' ' || payTermCode == 0) {
					alert("Enter UOM Code");
					$('#payTermCode').focus();
					return false;
				} else if (slabSeqId == '' || slabSeqId == null) {
					alert("Enter UOM Name");
					$('#slabSeqId').focus();
					return false;
				}else if (creditDays == '' || creditDays == null) {
					alert("Enter UOM Name");
					$('#creditDays').focus();
					return false;
				}else if (percentage == '' || percentage == null) {
					alert("Enter UOM Name");
					$('#percentage').focus();
					return false;
				}else {
					var result = $.ajax({
						type : "POST",
						url : "savePayTermSlab?payTermCode="+ $("#payTermCode").val()+ "&slabSeqId="+ $("#slabSeqId").val()
						+ "&creditDays="+ $("#creditDays").val()+ "&percentage="+ $("#percentage").val(),
						dataType : "json",
						contentType : "application/json",
						processData : false,
						async : false
					}).responseText;
					if (result == "S") {
						alert("Saved Successfully");
						clearDatapaySlab();
						location.reload();
					} else if (result == "A") {
						alert("System Corrupted");
					} else if (result == "Z") {
						alert("Already Payterm Slab Code Available");
					} else if (result == "X") {
						alert("Already Payterm Slab Name Available");
					} else {
						alert("failed");

					}
				}
			});
	
	function clearDatapaySlab(){
		$('#payTermCode').val('');
		$('#slabSeqId').val('');
		$('#creditDays').val('');
		$('#percentage').val('');
	}
	
	 $('#paySlab_Reset').click(function() {
		    $('#payTermCode').val('');
			$('#slabSeqId').val('');
			$('#creditDays').val('');
			$('#percentage').val('');
		  });
	 
	 var table;
		(function($) {
			'use strict';
			$(function() {
				table = $('.js-exportable')
						.DataTable(
								{
									responsive : true,
									"iDisplayLength" : 5,
									ajax : {
										type : "POST",
										url : "getPaySlabTypeIdList",
										contentType : "application/json; charset=utf-8",
										dataType : "json",
										async : false,
									},
									"columns" : [ {
										"data" : "payTermCodeTbl"
									}, {
										"data" : "SlabSeqIdTbl"
									}, {
										"data" : "creditDaysTbl"
									}, {
										"data" : "percentageTbl"
									},{
										"data" : "action",
										render : actionFormatter
									}, ]

								});
			});

		}(jQuery))
		
		
		$('#dtab').on("click", 'tr', function() {
			tableData = table.row(this).data();
			$('#paymentTermCodeEdit').val(tableData.payTermCodeTbl);
			$('#slabSeqIdEdit').val(tableData.SlabSeqIdTbl);
			$('#creditDaysEdit').val(tableData.creditDaysTbl);
			$('#percentageEdit').val(tableData.percentageTbl);
		});
		
		
		$('#paySlabUpdate').click(function() {
			var paymentTermCodeEdit = $('#paymentTermCodeEdit').val();
			var slabSeqIdEdit = $('#slabSeqIdEdit').val();
			var creditDaysEdit = $('#creditDaysEdit').val();
			var percentageEdit = $('#percentageEdit').val();
			if (paymentTermCodeEdit == ' ' || paymentTermCodeEdit == 0 || paymentTermCodeEdit == '-1') {
				alert("Enter Payterm Code");
				$('#paymentTermCodeEdit').focus();
				return false;
			} else if (slabSeqIdEdit == ' ' || slabSeqIdEdit == 0) {
				alert("Enter Slab Seq Id");
				$('#slabSeqIdEdit').focus();
				return false;
			} else if (creditDaysEdit == '' || creditDaysEdit == null) {
				alert("Enter Credid Days");
				$('#creditDaysEdit').focus();
				return false;
			} else if (percentageEdit == '' || percentageEdit == null) {
				alert("Enter Percentage");
				$('#percentageEdit').focus();
				return false;
			} else {
				var result = $.ajax({
					type : "POST",
					url : "updatePayslab?paymentTermCodeEdit=" + paymentTermCodeEdit + "&slabSeqIdEdit=" + slabSeqIdEdit
						+ "&creditDaysEdit=" + creditDaysEdit + "&percentageEdit=" + percentageEdit,
					dataType : "json",
					contentType : "application/json",
					processData : false,
					async : false
				}).responseText;
				if (result == "S") {
					alert("Updated Successfully");
					location.reload();
					clearData23();
				} else if (result == "A") {
					alert("Currency Name Already Exists");
				} else {
					alert("failed");
				}
			}
		});
	
});