var editor;
$(document).ready(function() {

	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-postingPeriod').addClass('active');
	$('#PostingPeriod').hide();

	$('#postingPeriodAdd').click(function() {
		$('#PostingPeriod').show();
	});
	$(".close").click(function() {
		$(".modal").hide();
	});

	var result = $.ajax({
		type : "POST",
		url : "getCompListinAssignment",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#comp").get(0).options.length = 0;
			$("#comp").get(0).options[0] = new Option("Select Company", "-1");
			$("#compEdit").get(0).options.length = 0;
			$("#compEdit").get(0).options[0] = new Option("Select Company", "-1");
			$.each(msg, function(index, item) {
				$("#compEdit").get(0).options[$("#compEdit").get(0).options.length] = new Option(item.compName, item.compCode);
				$("#comp").get(0).options[$("#comp").get(0).options.length] = new Option(item.compName, item.compCode);
			});
		},
	});
	$("#compEdit").prop('disabled', true);
	$("#subPeriodEdit").prop('disabled', true);

	var result = $.ajax({
		type : "POST",
		url : "getSubPeriod",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#subPeriod").get(0).options.length = 0;
			$("#subPeriod").get(0).options[0] = new Option("Select Sub Period", "-1");
			$("#subPeriodEdit").get(0).options.length = 0;
			$("#subPeriodEdit").get(0).options[0] = new Option("Select Sub Period", "-1");
			$.each(msg, function(index, item) {
				$("#subPeriodEdit").get(0).options[$("#subPeriodEdit").get(0).options.length] = new Option(item.subPeriodName, item.subPeriodCode);
				$("#subPeriod").get(0).options[$("#subPeriod").get(0).options.length] = new Option(item.subPeriodName, item.subPeriodCode);
			});
		},
	});

	var result = $.ajax({
		type : "POST",
		url : "getPeriodStatus",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#periodStatusEdit").get(0).options.length = 0;
			$("#periodStatusEdit").get(0).options[0] = new Option("Select Period Status", "-1");
			$.each(msg, function(index, item) {
				$("#periodStatusEdit").get(0).options[$("#periodStatusEdit").get(0).options.length] = new Option(item.periodStatusName, item.periodStatusCode);
			});
		},
	});

	$("#subPeriod").change(function() {
		var subPeriod = $("#subPeriod").val();
		if (subPeriod == "M") {
			$("#noofperiod").val(12);
		} else {
			$("#noofperiod").val(1);
		}
	});
	$("#fiscalStart").datepicker({
		dateFormat : "dd-mm-yy",
		changeMonth : true,
		changeYear : true,
		yearRange : "1980:" + new Date(),
		maxDate : new Date()
	});

	var table;
	(function($) {
		'use strict';
		$(function() {
			table = $('.js-exportable').DataTable({
				responsive : true,
				"iDisplayLength" : 5,
				ajax : {
					type : "POST",
					url : "getPostingPeriodList",
					contentType : "application/json; charset=utf-8",
					dataType : "json",
					async : false,
				},
				"columns" : [
					{
						"data" : "company"
					},
					{
						"data" : "period"
					},
					/*{
						"data" : "subPeriod"
					},*/
					{
						"data" : "fiscal"
					},
					{
						"data" : "fiscalStart"
					},
					{
						"data" : "periodStart"
					},
					{
						"data" : "periodEnd"
					},
					{
						"data" : "status"
					},
					{
						"data" : "action",
						render : actionFormatter
					}

				],
			});
		});
	}(jQuery))
	$('#dtab').on("click", 'tr', function() {
		tableData = table.row(this).data();
		$("#compEdit").val(tableData.companyCode);
		$("#periodCodeEdit").val(tableData.periodCode);
		$("#periodEdit").val(tableData.period);
		$("#subPeriodEdit").val(tableData.subPeriodCode);
		$("#fiscalStartEdit").val(tableData.fiscalStart);
		$("#periodStartEdit").val(tableData.periodStart);
		$("#periodEndEdit").val(tableData.periodEnd);
		$("#fiscalEdit").val(tableData.fiscal);
		$("#periodStatusEdit").val(tableData.statusCode);
	});


	$('#periodSave').click(function() {

		var company = $('#comp').val();
		var periodCode = $('#periodCode').val();
		var period = $('#period').val();
		var subPeriod = $('#subPeriod').val();
		var noofperiod = $('#noofperiod').val();
		var fiscalStart = $("#fiscalStart").val();
		if (company == ' ' || company == 0 || company == '-1') {
			alert("Select Company");
			$('#comp').focus();
			return false;
		} else if (periodCode == ' ' || periodCode == 0 || periodCode == '-1') {
			alert("Enter Period Code");
			$('#periodCode').focus();
			return false;
		} else if (period == '' || period == null) {
			alert("Enter Period Name");
			$('#period').focus();
			return false;
		} else if (subPeriod == '' || subPeriod == null || subPeriod == '-1') {
			alert("Select Sub Period");
			$('#subPeriod').focus();
			return false;
		} else if (fiscalStart == '' || fiscalStart == null) {
			alert("Select Date");
			$('#fiscalStart').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "savePostingPeriod?company=" + company + "&periodCode=" + periodCode
					+ "&period=" + period + "&subPeriod=" + subPeriod + "&noofperiod=" + noofperiod + "&fiscalStart=" + fiscalStart,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Saved Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
				alert("Posting Period Exists");
			} else {
				alert("failed");
			}
		}
	});

	function clearData23() {
		$('#comp').val('-1');
		$('#periodCode').val('');
		$('#period').val('');
		$('#subPeriod').val('-1');
		$('#noofperiod').val('');
		$("#fiscalStart").val('');
		$('#compEdit').val('-1');
		$('#periodCodeEdit').val('');
		$('#periodEdit').val('');
		$('#subPeriodEdit').val('-1');
		$("#fiscalStartEdit").val('');
		$("#periodStatusEdit").val('-1');
	}


	$('#periodUpdate').click(function() {

		var company = $('#compEdit').val();
		var periodCode = $('#periodCodeEdit').val();
		var period = $('#periodEdit').val();

		var subPeriod = $('#subPeriodEdit').val();
		var fYear = $('#fiscalEdit').val();
		var fiscalStart = $("#fiscalStartEdit").val();

		var periodStartEdit = $('#periodStartEdit').val();
		var periodEndEdit = $('#periodEndEdit').val();
		var periodStatusEdit = $("#periodStatusEdit").val();

		if (periodStatusEdit == ' ' || periodStatusEdit == 0 || periodStatusEdit == '-1') {
			alert("Select Status");
			$('#periodStatusEdit').focus();
			return false;
		} else {
			var result = $.ajax({
				type : "POST",
				url : "updatePostingPeriod?company=" + company + "&periodCode=" + periodCode
					+ "&period=" + period + "&subPeriod=" + subPeriod + "&fYear=" + fYear + "&fiscalStart=" + fiscalStart
					+ "&periodStartEdit=" + periodStartEdit + "&periodEndEdit=" + periodEndEdit + "&periodStatusEdit=" + periodStatusEdit,
				dataType : "json",
				contentType : "application/json",
				processData : false,
				async : false
			}).responseText;
			if (result == "S") {
				alert("Updated Successfully");
				clearData23();
				location.reload();
			} else if (result == "A") {
			} else {
				alert("failed");
			}
		}
	});

});