var editor;
$(document)
		.ready(
				function() {

					$('#menu-master').addClass('active');
					$('#menu-master > a').attr('aria-expanded', 'true');
					$('#menu-master > ul').addClass('in');

					$('#menu-general').addClass('active');
					$('#menu-general > a').attr('aria-expanded', 'true');
					$('#menu-general > ul').addClass('in');

					$('#menu-uom').addClass('active');

					$('#Uomadd').hide();
					$('#uomEdit').hide();

					$('#uomAdd').click(function() {
						$('#Uomadd').show();
					});

					
					  $('#uom_Reset').click(function() {
						  // alert("Reset :::: ");
						  $("#uomCode").val(''); 
						  $("#uomName").val('');
						  });
					 

					$(".close").click(function() {
						$(".modal").hide();
					});
					
	/*-----------------------------------------*/				
					
					var result = $.ajax({
						type : "POST",
						url : "getStatusList",
						contentType : "applcation/json; charset=utf-8",
						dataType : "json",
						async : false,
						success : function(msg){
							$("#uomStatusEdit").get(0).options.length = 0;
							$("#uomStatusEdit").get(0).options[0] = new Option("Select Status", "-1");
							$.each(msg, function(index, item) {
								$("#uomStatusEdit").get(0).options[$("#uomStatusEdit").get(0).options.length] = new Option(item.statusDesc, item.statusCode);
							});
						}
					});
					
					
					
/*--------------------------------------------------*/
					var table;
					(function($) {
						'use strict';
						$(function() {
							table = $('.js-exportable')
									.DataTable(
											{
												responsive : true,
												"iDisplayLength" : 5,
												ajax : {
													type : "POST",
													url : "getUomTypeIdList",
													contentType : "application/json; charset=utf-8",
													dataType : "json",
													async : false,
												},
												"columns" : [ {
													"data" : "uomTbId"
												}, {
													"data" : "uomTbDesc"
												}, {
													"data" : "action",
													render : actionFormatter
												}, ]

											});
						});

					}(jQuery))

					$('#dtab').on("click", 'tr', function() {
						tableData = table.row(this).data();
						$('#uomCodeEdit').val(tableData.uomTbId);
						$('#uomDescNewEdit').val(tableData.uomTbDesc);
					});

					$('#uomSave').click(
							function() {

								var uomCode = $('#uomCode').val();
								var uomName = $('#uomName').val();
								if (uomCode == ' ' || uomCode == 0) {
									alert("Enter UOM Code");
									$('#uomCode').focus();
									return false;
								} else if (uomName == '' || uomName == null) {
									alert("Enter UOM Name");
									$('#uomName').focus();
									return false;
								} else {
									var result = $.ajax({
										type : "POST",
										url : "saveUom?uomCode="
												+ $("#uomCode").val()
												+ "&uomName="
												+ $("#uomName").val(),
										dataType : "json",
										contentType : "application/json",
										processData : false,
										async : false
									}).responseText;
									if (result == "S") {
										alert("Saved Successfully");
										clearDataUOM();
										location.reload();
									} else if (result == "A") {
										alert("System Corrupted");
									} else if (result == "Z") {
										alert("Already GST Code Available");
									} else if (result == "X") {
										alert("Already GST Name Available");
									} else {
										alert("failed");

									}
								}
							});

					function clearDataUOM() {
						$('#uomCode').val('');
						$('#uomName').val('');
					}

					$('#uomEditUpdate').click(function() {

										var uomCodeEdit = $('#uomCodeEdit').val();
										var uomDescNewEdit = $('#uomDescNewEdit').val();
										var uomStatusEdit = $('#uomStatusEdit').val();
										/*var uomTypeDropText = $("#uomDescEdit")
												.find("option:selected").text();*/

										/*alert("uomDescEdit :" + uomDescEdit);
										alert("uomDescNewEdit :"+ uomDescNewEdit);
										alert("uomTypeDropText :"+ uomTypeDropText);*/

										if (uomCodeEdit == null|| uomCodeEdit == undefined|| uomCodeEdit == "") {
											alert("Select UOM Code");
											$('#uomCodeEdit').focus();
											return false;
										}else if (uomDescNewEdit == null|| uomDescNewEdit == undefined|| uomDescNewEdit == "") {
											alert("Enter UOM Desc");
											$('#uomDescNewEdit').focus();
											return false;
										} else if (uomStatusEdit == null|| uomStatusEdit == undefined|| uomStatusEdit == "") {
											alert("Enter UOM Status");
											$('#uomStatusEdit').focus();
											return false;
										} else {
											var result = $
													.ajax({
														type : "POST",
														url : "updateUomType?uomCodeEdit="
																+ uomCodeEdit
																+ "&uomDescNewEdit="
																+ uomDescNewEdit
																+ "&uomStatusEdit="
																+ uomStatusEdit,
														dataType : "json",
														contentType : "application/json",
														processData : false,
														async : false
													}).responseText;
											if (result == "S") {
												alert("Updated Successfully");
												clearDataUpdateUOM();
												location.reload();
											} else if (result == "D") {
												alert("Already Exists")
											} else {
												alert("Failed");
											}
										}
									});

					function clearDataUpdateUOM() {
						// alert("Clear");
						$('#uomDescEdit').val('-1');
						$('#uomDescNewEdit').val('');
						$('#uomStatusEdit').val('-1');
					}

				});