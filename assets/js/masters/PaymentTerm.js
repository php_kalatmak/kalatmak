$(document).ready(function() {
	
	$('#menu-master').addClass('active');
	$('#menu-master > a').attr('aria-expanded', 'true');
	$('#menu-master > ul').addClass('in');

	$('#menu-general').addClass('active');
	$('#menu-general > a').attr('aria-expanded', 'true');
	$('#menu-general > ul').addClass('in');

	$('#menu-paymentTerm').addClass('active');
	
	$('#Paytermadd').hide();
	$('#paytermEdit').hide();
	
	
	
	var checkboxSlab =$("#isSlab");
	var slabLevel = $("#slabLevel").val();
	
	//$("#isSlab").hide();
	$("#slabLevel").hide();
	
	checkboxSlab.change(function(){
		if(checkboxSlab.is(':checked')){
			$("#slabLevel").show();
		}else{
			$("#slabLevel").hide();
		}
	});
	
		
	
	$('#payTermAdd').click(function() {
		$('#Paytermadd').show();
	});
	
	$(".close").click(function() {
		$(".modal").hide();
	});
	
	var result = $.ajax({
		type : "POST",
		url : "getPaytermSlab",
		contentType : "application/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg) {
			$("#slabLevel").get(0).options.length = 0;
			$("#slabLevelEdit").get(0).options.length = 0;
			$("#slabLevel").get(0).options[0] = new Option("Select Slab", "-1");
			$("#slabLevelEdit").get(0).options[0] = new Option("Select Slab", "-1");
		$.each(msg, function(index, item) {
				$("#slabLevel").get(0).options[$("#slabLevel").get(0).options.length] = new Option(item.slabId, item.slabId);
				$("#slabLevelEdit").get(0).options[$("#slabLevelEdit").get(0).options.length] = new Option(item.slabId, item.slabId);
			});
		},
	});
	
	
	
	
	
	$('#paymentTermSave').click(
			function() {

				var paymentTermCode = $('#paymentTermCode').val();
				var paymentTermDesc = $('#paymentTermDesc').val();
				var creditDays = $('#creditDays').val();
				//var isSlab = $('#isSlab').val();
				var isSlab = $("input[name='load']:checked").val();
				var slabLevel = $('#slabLevel').val();
				var toleranceDays = $('#toleranceDays').val();
				
				
						
				
//alert("paymentTermCode :"+paymentTermCode+" paymentTermDesc :"+paymentTermDesc
		//+" creditDays :"+creditDays+" isSlab :"+isSlab+" slabLevel :"+slabLevel+" toleranceDays :"+toleranceDays);
				
				if (paymentTermCode == ' ' || paymentTermCode == 0) {
					alert("Enter PayTerm Code");
					$('#paymentTermCode').focus();
					return false;
				} else if (paymentTermDesc == '' || paymentTermDesc == null) {
					alert("Enter PayTerm Name");
					$('#paymentTermDesc').focus();
					return false;
				}else if (creditDays == '' || creditDays == null) {
					alert("Enter Credit Days");
					$('#creditDays').focus();
					return false;
				}/*else if (isSlab == '' || isSlab == null) {
					alert("Enter isSlab");
					$('#isSlab').focus();
					return false;
				}else if (slabLevel == '' || slabLevel == null) {
					alert("Enter UOM Name");
					$('#slabLevel').focus();
					return false;
				}*/ else if (toleranceDays == '' || toleranceDays == null) {
					alert("Enter Tolerance Days");
					$('#toleranceDays').focus();
					return false;
				}else {
					var result = $.ajax({
						type : "POST",
						url : "savePaymentTerm?paymentTermCode="+ $("#paymentTermCode").val()+ "&paymentTermDesc="+ $("#paymentTermDesc").val()
						+ "&creditDays="+ $("#creditDays").val()+ "&isSlab="+ isSlab
						+ "&slabLevel="+ $("#slabLevel").val()+ "&toleranceDays="+ $("#toleranceDays").val(),
						dataType : "json",
						contentType : "application/json",
						processData : false,
						async : false
					}).responseText;
					if (result == "S") {
						alert("Saved Successfully");
						clearDataPayterm();
						location.reload();
					} else if (result == "A") {
						alert("System Corrupted");
					} else if (result == "Z") {
						alert("Already GST Code Available");
					} else if (result == "X") {
						alert("Already GST Name Available");
					} else {
						alert("failed");

					}
				}
			});
	
	
	var result = $.ajax({
		type : "POST",
		url : "getStatusList",
		contentType : "applcation/json; charset=utf-8",
		dataType : "json",
		async : false,
		success : function(msg){
			$("#StatusEdit").get(0).options.length = 0;
			$("#StatusEdit").get(0).options[0] = new Option("Select Status", "-1");
			$.each(msg, function(index, item) {
				$("#StatusEdit").get(0).options[$("#StatusEdit").get(0).options.length] = new Option(item.statusDesc, item.statusCode);
			});
		}
	});
	
	
	
	
	function clearDataPayterm() {
		 $('#paytermCodeEdit').val('');
		 $('#paytermDescEdit').val('');
		 $('#creditDaysEdit').val('');
		//var isSlabEdit = $('#isSlabEdit').val();
		 $("input[name='load']:checked").val('');
		 $('#slabLevelEdit').val('');
		 $('#toleranceEdit').val('');
		 $('#StatusEdit').val('-1');
	}
	
	function clearDataPayterm(){
		$('#paymentTermCode').val('');
	    $('#paymentTermDesc').val('');
		$('#creditDays').val('');
		//var isSlab = $('#isSlab').val();
		$("input[name='load']:checked").val('');
		$('#slabLevel').val('');
		$('#toleranceDays').val('');
	}
	
	
	$('#payterm_Reset').click(function() {
		$('#paymentTermCode').val('');
	    $('#paymentTermDesc').val('');
		$('#creditDays').val('');
		//var isSlab = $('#isSlab').val();
		$("input[name='load']:checked").val('');
		$('#slabLevel').val('-1');
		$('#toleranceDays').val('');
	});
	
	
	 var table;
		(function($) {
			'use strict';
			$(function() {
				table = $('.js-exportable')
						.DataTable(
								{
									responsive : true,
									"iDisplayLength" : 5,
									ajax : {
										type : "POST",
										url : "getPayTermTypeIdList",
										contentType : "application/json; charset=utf-8",
										dataType : "json",
										async : false,
									},
									"columns" : [ {
										"data" : "payTermTbCode"
									}, {
										"data" : "payTermTbDesc"
									}, {
										"data" : "creditDays"
									}, {
										"data" : "isSlab"
									}, {
										"data" : "tolDays"
									}, {
										"data" : "slabLevel"
									}, {
										"data" : "action",
										render : actionFormatter
									}, ]

								});
			});

		}(jQuery))
		$('#dtab').on("click", 'tr', function() {
			tableData = table.row(this).data();
			$('#paytermCodeEdit').val(tableData.payTermTbCode);
			$('#paytermDescEdit').val(tableData.payTermTbDesc);
			$('#creditDaysEdit').val(tableData.creditDays);
			//alert(tableData.isSlab);
			//$('#isSlabEdit').val(tableData.isSlab);
			$('#toleranceEdit').val(tableData.tolDays);
			
			$('#slabLevelEdit').val(tableData.slabLevel);
			
			
			if(tableData.isSlab == true){
	            $('input[name=load]').prop('checked', true);
	        } else{
	            $('input[name=load]').prop('checked', false);
	        } 
			
			
		});
		
		
		
		$('#paytermEditUpdate').click(function() {
			var paytermCodeEdit = $('#paytermCodeEdit').val();
			var paytermDescEdit = $('#paytermDescEdit').val();
			var creditDaysEdit = $('#creditDaysEdit').val();
			//var isSlabEdit = $('#isSlabEdit').val();
			var isSlabEdit = $("input[name='load']:checked").val();
			var slabLevelEdit = $('#slabLevelEdit').val();
			var toleranceEdit = $('#toleranceEdit').val();
			var StatusEdit = $('#StatusEdit').val();
			
			if (paytermCodeEdit == ' ' || paytermCodeEdit == 0 || paytermCodeEdit == '-1') {
				alert("Enter Payterm Code");
				$('#paytermCodeEdit').focus();
				return false;
			} else if (paytermDescEdit == ' ' || paytermDescEdit == 0) {
				alert("Enter Slab Seq Id");
				$('#paytermDescEdit').focus();
				return false;
			} else if (creditDaysEdit == '' || creditDaysEdit == null) {
				alert("Enter Credid Days");
				$('#creditDaysEdit').focus();
				return false;
			} else if (toleranceEdit == '' || toleranceEdit == null) {
				alert("Enter Percentage");
				$('#toleranceEdit').focus();
				return false;
			}else if (StatusEdit == '' || StatusEdit == null) {
				alert("Enter Percentage");
				$('#StatusEdit').focus();
				return false;
			} else {
				var result = $.ajax({
					type : "POST",
					url : "updatePayTerm?paytermCodeEdit=" + paytermCodeEdit + "&paytermDescEdit=" + paytermDescEdit
						+ "&creditDaysEdit=" + creditDaysEdit +"&isSlabEdit="+isSlabEdit+"&slabLevelEdit="+slabLevelEdit
						+"&toleranceEdit=" + toleranceEdit+"&StatusEdit="+StatusEdit,
					dataType : "json",
					contentType : "application/json",
					processData : false,
					async : false
				}).responseText;
				if (result == "S") {
					alert("Updated Successfully");
					clearDataPayterm();
					location.reload();
					
				} else if (result == "A") {
					alert("Payterm Already Exists");
				} else {
					alert("failed");
				}
			}
		});	
		
		
		
		

});